{
"tire_R_185_60_14_altb_sport": {
    "information":{
        "authors":"BeamNG",
        "name":"185/60R14 Sport Rear Tires",
        "value":110,
    },
    "slotType" : "tire_R_14x7_altb",
    "flexbodies": [
        ["mesh", "[group]:", "nonFlexMaterials"],
        ["tire_01a_14x8_23", ["wheel_RR","tire_RR"], [], {"pos":{"x":-0.50, "y":0.0, "z":0.0}, "rot":{"x":0, "y":0, "z":180}, "scale":{"x":0.965, "y":1.0, "z":1.0}}],
        ["tire_01a_14x8_23", ["wheel_RL","tire_RL"], [], {"pos":{"x": 0.50, "y":0.0, "z":0.0}, "rot":{"x":0, "y":0, "z":0},   "scale":{"x":0.965, "y":1.0, "z":1.0}}],
    ],
    "variables": [
        ["name", "type", "unit", "category", "default", "min", "max", "title", "description"],
        ["$tirepressure_R", "range", "psi", "Wheels", 23, 0, 50, "Tire Pressure", "Relative to atmospheric pressure", {"subCategory":"Rear"}],
    ],
    "pressureWheels": [
        ["name","hubGroup","group","node1:","node2:","nodeS","nodeArm:","wheelDir"],
        {"disableMeshBreaking":false,"disableHubMeshBreaking":false},
        {"hasTire":true},
        {"enableTireReinfBeams":false},
        {"enableTireLbeams":true},
        {"enableTireSideReinfBeams":false},
        {"enableTreadReinfBeams":true},
        {"enableTirePeripheryReinfBeams":true},

        //general settings
        {"radius":0.29},
        {"tireWidth":0.155},

        //tire options
        {"wheelSideBeamSpring":"$=$tirepressure_R*652","wheelSideBeamDamp":25},
        {"wheelSideBeamSpringExpansion":301000,"wheelSideBeamDampExpansion":25},
        {"wheelSideTransitionZone":0.1,"wheelSideBeamPrecompression":0.985},

        {"wheelReinfBeamSpring":24000,"wheelReinfBeamDamp":150},
        {"wheelReinfBeamDampCutoffHz":500,"wheelReinfBeamPrecompression":0.98},

        {"wheelTreadBeamSpring":85000,"wheelTreadBeamDamp":55},
        {"wheelTreadBeamDampCutoffHz":500,"wheelTreadBeamPrecompression":0.98},

        {"wheelTreadReinfBeamSpring":131000,"wheelTreadReinfBeamDamp":45},
        {"wheelTreadReinfBeamDampCutoffHz":500,"wheelTreadReinfBeamPrecompression":0.98},

        {"wheelPeripheryBeamSpring":61000,"wheelPeripheryBeamDamp":35},
        {"wheelPeripheryBeamDampCutoffHz":500,"wheelPeripheryBeamPrecompression":0.98},

        {"wheelPeripheryReinfBeamSpring":81000,"wheelPeripheryReinfBeamDamp":28},
        {"wheelPeripheryReinfBeamDampCutoffHz":500,"wheelPeripheryReinfBeamPrecompression":0.98},

        //general tire values
        {"nodeWeight":0.125},
        {"nodeMaterial":"|NM_RUBBER"},
        {"triangleCollision":false},
        {"pressurePSI":"$tirepressure_R"},
        {"dragCoef":5},

        //groundmodel friction multipliers
        {"frictionCoef":1.0},
        {"slidingFrictionCoef":1.0},
        {"stribeckExponent":1.9},
        {"stribeckVelMult":1.3},
        {"treadCoef":0.5},

        //advanced friction values
        {"noLoadCoef":1.38},
        {"loadSensitivitySlope":0.0002},
        {"fullLoadCoef":0.4},
        {"softnessCoef":0.7},

        //deform values
        {"wheelSideBeamDeform":11000,"wheelSideBeamStrength":15000},
        {"wheelTreadBeamDeform":10000,"wheelTreadBeamStrength":13000},
        {"wheelPeripheryBeamDeform":45000,"wheelPeripheryBeamStrength":45000},
    ],
},
"tire_R_195_65_14_altb_sport": {
    "information":{
        "authors":"BeamNG",
        "name":"195/65R14 Sport Rear Tires",
        "value":130,
    },
    "slotType" : "tire_R_14x7_altb",
    "flexbodies": [
        ["mesh", "[group]:", "nonFlexMaterials"],
        ["tire_01a_14x7_24",    ["wheel_RR","tire_RR"], [], {"pos":{"x":-0.50, "y":0.0, "z":0.0}, "rot":{"x":0, "y":0, "z":180}, "scale":{"x":1.12, "y":0.995, "z":0.995}}],
        ["tire_01a_14x7_24",    ["wheel_RL","tire_RL"], [], {"pos":{"x": 0.50, "y":0.0, "z":0.0}, "rot":{"x":0, "y":0, "z":0},   "scale":{"x":1.12, "y":0.995, "z":0.995}}],
    ],
    "variables": [
        ["name", "type", "unit", "category", "default", "min", "max", "title", "description"],
        ["$tirepressure_R", "range", "psi", "Wheels", 27, 0, 50, "Tire Pressure", "Relative to atmospheric pressure", {"subCategory":"Rear"}],
    ],
    "pressureWheels": [
        ["name","hubGroup","group","node1:","node2:","nodeS","nodeArm:","wheelDir"],
        {"disableMeshBreaking":false,"disableHubMeshBreaking":false},
        {"hasTire":true},
        {"enableTireReinfBeams":false},
        {"enableTireLbeams":true},
        {"enableTireSideReinfBeams":false},
        {"enableTreadReinfBeams":true},
        {"enableTirePeripheryReinfBeams":true},

        //general settings
        {"radius":0.305},
        {"tireWidth":0.175},

        //tire options
        {"wheelSideBeamSpring":"$=$tirepressure_R*480","wheelSideBeamDamp":15},
        {"wheelSideBeamSpringExpansion":361000,"wheelSideBeamDampExpansion":25},
        {"wheelSideTransitionZone":0.08,"wheelSideBeamPrecompression":0.985},

        {"wheelReinfBeamSpring":17000,"wheelReinfBeamDamp":160},
        {"wheelReinfBeamDampCutoffHz":500,"wheelReinfBeamPrecompression":0.98},

        {"wheelTreadBeamSpring":70000,"wheelTreadBeamDamp":65},
        {"wheelTreadBeamDampCutoffHz":500,"wheelTreadBeamPrecompression":0.98},

        {"wheelTreadReinfBeamSpring":140000,"wheelTreadReinfBeamDamp":45},
        {"wheelTreadReinfBeamDampCutoffHz":500,"wheelTreadReinfBeamPrecompression":0.98},

        {"wheelPeripheryBeamSpring":45000,"wheelPeripheryBeamDamp":35},
        {"wheelPeripheryBeamDampCutoffHz":500,"wheelPeripheryBeamPrecompression":0.98},

        {"wheelPeripheryReinfBeamSpring":95000,"wheelPeripheryReinfBeamDamp":25},
        {"wheelPeripheryReinfBeamDampCutoffHz":500,"wheelPeripheryReinfBeamPrecompression":0.98},

        //general tire values
        {"nodeWeight":0.14},
        {"nodeMaterial":"|NM_RUBBER"},
        {"triangleCollision":false},
        {"pressurePSI":"$tirepressure_R"},
        {"dragCoef":5},

        //groundmodel friction multipliers
        {"frictionCoef":1.0},
        {"slidingFrictionCoef":1.0},
        {"treadCoef":0.5},

        //advanced friction values
        {"noLoadCoef":1.43},
        {"loadSensitivitySlope":0.000168},
        {"fullLoadCoef":0.4},
        {"softnessCoef":0.7},

        //deform values
        {"wheelSideBeamDeform":14000,"wheelSideBeamStrength":18000},
        {"wheelTreadBeamDeform":11000,"wheelTreadBeamStrength":14000},
        {"wheelPeripheryBeamDeform":45000,"wheelPeripheryBeamStrength":45000},
    ],
},
"tire_R_195_75_14_altb_sport": {
    "information":{
        "authors":"BeamNG",
        "name":"195/75R14 Grip-All Radial T/A Rear Tires",
        "value":150,
    },
    "slotType" : "tire_R_14x7_altb",
    "flexbodies": [
        ["mesh", "[group]:", "nonFlexMaterials"],
        ["tire_01c_14x7_26", ["wheel_RR","tire_RR"], [], {"pos":{"x":-0.50, "y":0.0, "z":0.0}, "rot":{"x":0, "y":0, "z":180}, "scale":{"x":0.985, "y":1, "z":1}}],
        ["tire_01c_14x7_26", ["wheel_RL","tire_RL"], [], {"pos":{"x": 0.50, "y":0.0, "z":0.0}, "rot":{"x":0, "y":0, "z":0},   "scale":{"x":0.985, "y":1, "z":1}}],
    ],
    "variables": [
        ["name", "type", "unit", "category", "default", "min", "max", "title", "description"],
        ["$tirepressure_R", "range", "psi", "Wheels", 30, 0, 50, "Tire Pressure", "Relative to atmospheric pressure", {"subCategory":"Rear"}],
    ],
    "pressureWheels": [
        ["name","hubGroup","group","node1:","node2:","nodeS","nodeArm:","wheelDir"],
        {"disableMeshBreaking":false,"disableHubMeshBreaking":false},
        {"hasTire":true},
        {"enableTireReinfBeams":false},
        {"enableTireLbeams":true},
        {"enableTireSideReinfBeams":false},
        {"enableTreadReinfBeams":true},
        {"enableTirePeripheryReinfBeams":true},

        //general settings
        {"radius":0.325},
        {"tireWidth":0.175},

        //tire options
        {"wheelSideBeamSpring":"$=$tirepressure_R*736","wheelSideBeamDamp":35},
        {"wheelSideBeamSpringExpansion":411000,"wheelSideBeamDampExpansion":35},
        {"wheelSideTransitionZone":0.08,"wheelSideBeamPrecompression":0.985},

        {"wheelReinfBeamSpring":25000,"wheelReinfBeamDamp":170},
        {"wheelReinfBeamDampCutoffHz":500,"wheelReinfBeamPrecompression":0.98},

        {"wheelTreadBeamSpring":55000,"wheelTreadBeamDamp":65},
        {"wheelTreadBeamDampCutoffHz":500,"wheelTreadBeamPrecompression":0.98},

        {"wheelTreadReinfBeamSpring":105000,"wheelTreadReinfBeamDamp":55},
        {"wheelTreadReinfBeamDampCutoffHz":500,"wheelTreadReinfBeamPrecompression":0.98},

        {"wheelPeripheryBeamSpring":55000,"wheelPeripheryBeamDamp":35},
        {"wheelPeripheryBeamDampCutoffHz":500,"wheelPeripheryBeamPrecompression":0.98},

        {"wheelPeripheryReinfBeamSpring":105000,"wheelPeripheryReinfBeamDamp":35},
        {"wheelPeripheryReinfBeamDampCutoffHz":500,"wheelPeripheryReinfBeamPrecompression":0.98},

        //general tire values
        {"nodeWeight":0.15},
        {"nodeMaterial":"|NM_RUBBER"},
        {"triangleCollision":false},
        {"pressurePSI":"$tirepressure_R"},
        {"dragCoef":5},

        //groundmodel friction multipliers
        {"frictionCoef":1.0},
        {"slidingFrictionCoef":1.0},
        {"treadCoef":0.5},

        //advanced friction values
        {"noLoadCoef":1.36},
        {"loadSensitivitySlope":0.00015},
        {"fullLoadCoef":0.5},
        {"softnessCoef":0.7},

        //deform values
        {"wheelSideBeamDeform":19000,"wheelSideBeamStrength":26000},
        {"wheelTreadBeamDeform":14000,"wheelTreadBeamStrength":18000},
        {"wheelPeripheryBeamDeform":45000,"wheelPeripheryBeamStrength":45000},
    ],
},
}