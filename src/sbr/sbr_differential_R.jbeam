{
"sbr_differential_R": {
    "information":{
        "authors":"BeamNG",
        "name":"Open Rear Differential",
        "value":1750,
    },
    "slotType" : "sbr_differential_R",
    "slots": [
        ["type", "default", "description"],
        ["sbr_finaldrive_R","sbr_finaldrive_R_344", "Rear Final Drive", {"coreSlot":true}],
        ["sbr_halfshafts_R","sbr_halfshafts_R", "Rear Halfshafts"],
    ],
    "controller": [
        ["fileName"],
        ["drivingDynamics/actuators/electronicDiffLock" {"name":"lockRear", "differentialName":"differential_R"}]
    ],
    "powertrain" : [
        ["type", "name", "inputName", "inputIndex"],
        ["differential", "differential_R", "transfercase", 1, {"diffType":"open", "uiName":"Rear Differential","defaultVirtualInertia":0.25}],
    ],
    "differential_R": {
        "friction": 3.7,
        "dynamicFriction": 0.0018,
        "torqueLossCoef": 0.03,
    },
    "nodes": [
         ["id", "posX", "posY", "posZ"],
         {"frictionCoef":0.5},
         {"nodeMaterial":"|NM_METAL"},
         {"selfCollision":false},
         {"collision":true},
         {"group":"sbr_reardiff"},
         {"nodeWeight":24},
         ["rdiff", 0.0, 1.30, 0.25],
         {"group":""},
    ],
    "beams": [
          ["id1:", "id2:"],
          {"beamPrecompression":1, "beamType":"|NORMAL", "beamLongBound":1.0, "beamShortBound":1.0},
          {"beamSpring":7700550,"beamDamp":125},
          {"beamDeform":660000,"beamStrength":"FLT_MAX"},
          //differential
          ["rdiff","e1r"],
          ["rdiff","e3r"],
          ["rdiff","e1l"],
          ["rdiff","e3l"],
          ["rdiff","tra1"],
          {"beamPrecompression":1, "beamType":"|NORMAL", "beamLongBound":1.0, "beamShortBound":1.0},
    ],
},

"sbr_differential_R_active": {
    "information":{
        "authors":"BeamNG",
        "name":"Active Rear Differential",
        "value":1750,
    },
    "slotType" : "sbr_differential_R",
    "slots": [
        ["type", "default", "description"],
        ["sbr_finaldrive_R","sbr_finaldrive_R_344", "Rear Final Drive", {"coreSlot":true}],
        ["sbr_halfshafts_R","sbr_halfshafts_R", "Rear Halfshafts"],
    ],
    "controller": [
        ["fileName"],
        ["drivingDynamics/actuators/activeDiffBias" {"name":"activeBiasRear", "differentialName":"differential_R"}]
    ],
    "powertrain" : [
        ["type", "name", "inputName", "inputIndex"],
        ["differential", "differential_R", "transfercase", 1, {"diffType":"torqueVectoring", "uiName":"Rear Differential","defaultVirtualInertia":0.25}],
    ],
    "differential_R": {
        "friction": 3.7,
        "dynamicFriction": 0.0018,
        "torqueLossCoef": 0.03,
    },
    "nodes": [
         ["id", "posX", "posY", "posZ"],
         {"frictionCoef":0.5},
         {"nodeMaterial":"|NM_METAL"},
         {"selfCollision":false},
         {"collision":true},
         {"group":"sbr_reardiff"},
         {"nodeWeight":24},
         ["rdiff", 0.0, 1.30, 0.25],
         {"group":""},
    ],
    "beams": [
          ["id1:", "id2:"],
          {"beamPrecompression":1, "beamType":"|NORMAL", "beamLongBound":1.0, "beamShortBound":1.0},
          {"beamSpring":7700550,"beamDamp":125},
          {"beamDeform":660000,"beamStrength":"FLT_MAX"},
          //differential
          ["rdiff","e1r"],
          ["rdiff","e3r"],
          ["rdiff","e1l"],
          ["rdiff","e3l"],
          ["rdiff","tra1"],
          {"beamPrecompression":1, "beamType":"|NORMAL", "beamLongBound":1.0, "beamShortBound":1.0},
    ],
},

"sbr_differential_R_welded": {
    "information":{
        "authors":"BeamNG",
        "name":"Welded Rear Differential",
        "value":1900,
    },
    "slotType" : "sbr_differential_R",
    "slots": [
        ["type", "default", "description"],
        ["sbr_finaldrive_R","sbr_finaldrive_R_344", "Rear Final Drive", {"coreSlot":true}],
        ["sbr_halfshafts_R","sbr_halfshafts_R", "Rear Halfshafts"],
    ],
    "powertrain" : [
        ["type", "name", "inputName", "inputIndex"],
        ["differential", "differential_R", "transfercase", 1, {"diffType":"locked", "lockTorque":10000, "uiName":"Rear Differential","defaultVirtualInertia":0.25}],
    ],
    "differential_R": {
        "friction": 3.7,
        "dynamicFriction": 0.0018,
        "torqueLossCoef": 0.03,
    },
    "nodes": [
         ["id", "posX", "posY", "posZ"],
         {"frictionCoef":0.5},
         {"nodeMaterial":"|NM_METAL"},
         {"selfCollision":false},
         {"collision":true},
         {"group":"sbr_reardiff"},
         {"nodeWeight":24},
         ["rdiff", 0.0, 1.30, 0.25],
         {"group":""},
    ],
    "beams": [
          ["id1:", "id2:"],
          {"beamPrecompression":1, "beamType":"|NORMAL", "beamLongBound":1.0, "beamShortBound":1.0},
          {"beamSpring":7700550,"beamDamp":125},
          {"beamDeform":660000,"beamStrength":"FLT_MAX"},
          //differential
          ["rdiff","e1r"],
          ["rdiff","e3r"],
          ["rdiff","e1l"],
          ["rdiff","e3l"],
          ["rdiff","tra1"],
          {"beamPrecompression":1, "beamType":"|NORMAL", "beamLongBound":1.0, "beamShortBound":1.0},
    ],
},
"sbr_differential_R_LSD": {
    "information":{
        "authors":"BeamNG",
        "name":"Limited Slip Rear Differential",
        "value":2150,
    },
    "slotType" : "sbr_differential_R",
    "slots": [
        ["type", "default", "description"],
        ["sbr_finaldrive_R","sbr_finaldrive_R_344", "Rear Final Drive", {"coreSlot":true}],
        ["sbr_halfshafts_R","sbr_halfshafts_R", "Rear Halfshafts"],
    ],
    "powertrain" : [
        ["type", "name", "inputName", "inputIndex"],
        ["differential", "differential_R", "transfercase", 1, {"diffType":"lsd", "lsdPreload":100, "lsdLockCoef":0.1, "lsdRevLockCoef":0.025, "uiName":"Rear Differential","defaultVirtualInertia":0.25}],
    ],
    "differential_R": {
        "friction": 3.7,
        "dynamicFriction": 0.0018,
        "torqueLossCoef": 0.03,
    },
    "nodes": [
         ["id", "posX", "posY", "posZ"],
         {"frictionCoef":0.5},
         {"nodeMaterial":"|NM_METAL"},
         {"selfCollision":false},
         {"collision":true},
         {"group":"sbr_reardiff"},
         {"nodeWeight":24},
         ["rdiff", 0.0, 1.30, 0.25],
         {"group":""},
    ],
    "beams": [
          ["id1:", "id2:"],
          {"beamPrecompression":1, "beamType":"|NORMAL", "beamLongBound":1.0, "beamShortBound":1.0},
          {"beamSpring":7700550,"beamDamp":125},
          {"beamDeform":660000,"beamStrength":"FLT_MAX"},
          //differential
          ["rdiff","e1r"],
          ["rdiff","e3r"],
          ["rdiff","e1l"],
          ["rdiff","e3l"],
          ["rdiff","tra1"],
          {"beamPrecompression":1, "beamType":"|NORMAL", "beamLongBound":1.0, "beamShortBound":1.0},
    ],
},
"sbr_differential_R_race": {
    "information":{
        "authors":"BeamNG",
        "name":"Race Limited Slip Rear Differential",
        "value":3850,
    },
    "slotType" : "sbr_differential_R",
    "slots": [
        ["type", "default", "description"],
        ["sbr_finaldrive_R","sbr_finaldrive_R_race", "Rear Final Drive", {"coreSlot":true}],
        ["sbr_halfshafts_R","sbr_halfshafts_R", "Rear Halfshafts"],
    ],
    "variables": [
        ["name", "type", "unit", "category", "default", "min", "max", "title", "description"],
        ["$lsdpreload_R", "range", "N/m", "Differentials", 150, 0, 500, "Pre-load Torque", "Initial locking torque between left and right wheels", {"subCategory":"Rear"}]
        ["$lsdlockcoef_R", "range", "", "Differentials", 0.12, 0, 0.5, "Power Lock Rate", "Additional locking torque proportional to engine torque", {"minDis":0, "maxDis":100,"subCategory":"Rear"}],
        ["$lsdlockcoefrev_R", "range", "", "Differentials", 0.06, 0, 0.5, "Coast Lock Rate", "Additional locking torque proportional to engine braking", {"minDis":0, "maxDis":100,"subCategory":"Rear"}],
    ],
    "powertrain" : [
        ["type", "name", "inputName", "inputIndex"],
        ["differential", "differential_R", "transfercase", 1, {"diffType":"lsd", "lsdPreload":"$lsdpreload_R", "lsdLockCoef":"$lsdlockcoef_R", "lsdRevLockCoef":"$lsdlockcoefrev_R", "uiName":"Rear Differential","defaultVirtualInertia":0.25}],
    ],
    "differential_R": {
        "friction": 3.7,
        "dynamicFriction": 0.0018,
        "torqueLossCoef": 0.03,
    },
    "nodes": [
         ["id", "posX", "posY", "posZ"],
         {"frictionCoef":0.5},
         {"nodeMaterial":"|NM_METAL"},
         {"selfCollision":false},
         {"collision":true},
         {"group":"sbr_reardiff"},
         {"nodeWeight":24},
         ["rdiff", 0.0, 1.3, 0.25],
         {"group":""},
    ],
    "beams": [
          ["id1:", "id2:"],
          {"beamPrecompression":1, "beamType":"|NORMAL", "beamLongBound":1.0, "beamShortBound":1.0},
          {"beamSpring":7700550,"beamDamp":125},
          {"beamDeform":660000,"beamStrength":"FLT_MAX"},
          //differential
          ["rdiff","e1r"],
          ["rdiff","e3r"],
          ["rdiff","e1l"],
          ["rdiff","e3l"],
          ["rdiff","tra1"],
          {"beamPrecompression":1, "beamType":"|NORMAL", "beamLongBound":1.0, "beamShortBound":1.0},
    ],
},
"sbr_finaldrive_R_344": {
    "information":{
        "authors":"BeamNG",
        "name":"3.44:1 Rear Final Drive",
        "value":250,
    },

    "slotType" : "sbr_finaldrive_R",

    "differential_R" : {
        "gearRatio":3.44,
    },
},
"sbr_finaldrive_R_393": {
    "information":{
        "authors":"BeamNG",
        "name":"3.93:1 Rear Final Drive",
        "value":250,
    },

    "slotType" : "sbr_finaldrive_R",

    "differential_R" : {
        "gearRatio":3.93,
    },
},
"sbr_finaldrive_R_race": {
    "information":{
        "authors":"BeamNG",
        "name":"Race Adjustable Rear Final Drive",
        "value":650,
    },

    "slotType" : "sbr_finaldrive_R",

    "variables": [
        ["name", "type", "unit", "category", "default", "min", "max", "title", "description"],
        ["$finaldrive_R", "range", ":1", "Differentials", 3.44, 2.0, 6.0, "Final Drive Gear Ratio", "Torque multiplication ratio", {"subCategory":"Rear", "stepDis":0.01}],
    ],

    "differential_R" : {
        "gearRatio":"$finaldrive_R",
    },
},
"sbr_halfshafts_R": {
    "information":{
        "authors":"BeamNG",
        "name":"Rear Halfshafts",
        "value":200,
    },
    "slotType" : "sbr_halfshafts_R",
    "flexbodies": [
         ["mesh", "[group]:", "nonFlexMaterials"],
         ["sbr_halfshaft_R", ["sbr_reardiff", "wheelhub_RL", "wheelhub_RR","sbr_transaxle"]],
    ],
    "beams": [
          ["id1:", "id2:"],
          {"beamType":"|NORMAL", "beamLongBound":1.0, "beamShortBound":1.0},

          //halfshafts
          {"beamPrecompression":1, "beamType":"|BOUNDED", "beamLongBound":0.02, "beamShortBound":0.02},
          {"beamSpring":0,"beamDamp":0,"beamDeform":3600,"beamStrength":9500},
          {"beamLimitSpring":7501000,"beamLimitDamp":250},
          ["rw1r","rdiff", {"name":"halfshaft_RR", "breakGroup":"wheel_RR", "breakGroupType":1, "deformGroup":"wheelaxleRR", "deformationTriggerRatio":0.001, "optional":true}],
          ["rw1l","rdiff", {"name":"halfshaft_RL", "breakGroup":"wheel_RL", "breakGroupType":1, "deformGroup":"wheelaxleRL", "deformationTriggerRatio":0.001, "optional":true}],
          {"beamPrecompression":1, "beamType":"|NORMAL", "beamLongBound":1.0, "beamShortBound":1.0},
    ],
    "powertrain" : [
        ["type", "name", "inputName", "inputIndex"],
        ["shaft", "wheelaxleRL", "differential_R", 1, {"deformGroups":["wheelaxleRL"], "breakTriggerBeam":"halfshaft_RL", "uiName":"Rear Left Halfshaft", "friction":1.51, "dynamicFriction":0.0036}],
        ["shaft", "wheelaxleRR", "differential_R", 2, {"deformGroups":["wheelaxleRR"], "breakTriggerBeam":"halfshaft_RR", "uiName":"Rear Right Halfshaft", "friction":1.51, "dynamicFriction":0.0036}],
    ],
},
}