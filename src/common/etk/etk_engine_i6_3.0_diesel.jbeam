{
"etk_engine_i6_3.0_diesel": {
    "information":{
        "authors":"BeamNG",
        "name":"3.0L I6 Diesel Engine",
        "value":8600,
    },
    "slotType": "etk_engine",
    "slots": [
        ["type", "default", "description"],
        ["etk_enginemounts","etk_enginemounts", "Engine Mounts", {"coreSlot":true}],
        ["etk_oilpan_i6","etk_oilpan_i6", "Oil Pan", {"coreSlot":true}],
        ["etk_intake_i6_3.0_diesel","etk_intake_i6_3.0_diesel_turbo", "Intake", {"coreSlot":true}],
        ["etk_exhaust_i6_3.0_diesel","etk_exhaust_i6_3.0_diesel", "Exhaust"],
        ["etk_engine_i6_3.0_diesel_ecu","etk_engine_i6_3.0_diesel_ecu_260", "Engine Management", {"coreSlot":true}],
        ["n2o_system","", "Nitrous Oxide System"],
        ["etk_engine_i6_3.0_diesel_internals","etk_engine_i6_3.0_diesel_internals", "Engine Long Block", {"coreSlot":true}],
        ["etk_transmission","etk_transmission_6M", "Transmission"],
    ],
    "powertrain": [
        ["type", "name", "inputName", "inputIndex"],
        ["combustionEngine", "mainEngine", "dummy", 0],
    ],
    "mainEngine": {
        "torque":[
            ["rpm", "torque"],
            [0, 0],
            [350, 160],
            [700, 202],
            [1000, 233],
            [2000, 337.5],
            [3000, 352],
            [4000, 326],
            [5000, 252],
            [6000, 130],
        ],
        "idleRPM":750,
        //max capable
        "maxRPM":5500,
        "revLimiterType": "soft",
        "inertia":0.24,
        "friction":18,
        "engineBrakeTorque":30,
        "dynamicFriction":0.0239,
        "burnEfficiency":[
            [0, 0.2],
            [0.05, 0.38],
            [0.4, 0.46],
            [0.7, 0.49],
            [1, 0.3],
        ],
        //fuel system
        "energyStorage": "mainTank",
        "requiredEnergyType":"diesel",

        //exhaust
        "particulates":0.015
        "instantAfterFireCoef": 0,
        "sustainedAfterFireCoef": 0,

        //cooling and oil system
        "thermalsEnabled":true,
        "engineBlockMaterial":"aluminum",
        "engineBlockAirCoolingEfficiency":31,

        //engine durability
        "cylinderWallTemperatureDamageThreshold":150,
        "headGasketDamageThreshold":1500000,
        "pistonRingDamageThreshold":1500000,
        "connectingRodDamageThreshold":2000000,
        "maxTorqueRating": 800,
        "maxOverTorqueDamage": 600,

        //node beam interface
        "torqueReactionNodes:":["e3r","e4r","e2l"],
        "waterDamage": {"[engineGroup]:":["engine_intake"]},
        "radiator": {"[engineGroup]:":["radiator"]},
        "engineBlock": {"[engineGroup]:":["engine_block"]},
        "breakTriggerBeam":"engine",
        "uiName":"Engine",
        "soundConfig": "soundConfig",
        "soundConfigExhaust": "soundConfigExhaust",

        //starter motor
        "starterSample":"event:>Engine>Starter>v6diesel_2011_eng",
        "starterSampleExhaust":"event:>Engine>Starter>v6diesel_2011_exh",
        "shutOffSampleEngine":"event:>Engine>Shutoff>v6diesel_2011_eng",
        "shutOffSampleExhaust":"event:>Engine>Shutoff>v6diesel_2011_exh",
        "starterVolume":0.76,
        "starterVolumeExhaust":0.76,
        "shutOffVolumeEngine":0.76,
        "shutOffVolumeExhaust":0.76,
        "idleRPMStartRate":1,
        "idleRPMStartCoef":1,

        //engine deform groups
        "deformGroups":["mainEngine", "mainEngine_intake", "mainEngine_accessories"]
    },
    "soundConfig": {
        "sampleName": "I6D_2_engine",
        "intakeMuffling": 1,

        "mainGain": -9,
        "onLoadGain":1.0,
        "offLoadGain":0.54,

        "maxLoadMix": 0.85,
        "minLoadMix": 0,

        "lowShelfGain":4,
        "lowShelfFreq":350,

        "highShelfGain":6,
        "highShelfFreq":3500,

        "eqLowGain": 0,
        "eqLowFreq": 500,
        "eqLowWidth": 0.1,

        "eqHighGain": -8,
        "eqHighFreq": 1400,
        "eqHighWidth": 0.2,

        "fundamentalFrequencyCylinderCount":6,
        "eqFundamentalGain": -2,
    },
    "soundConfigExhaust": {
        "sampleName": "I6D_2_exhaust",

        "mainGain": -4,
        "onLoadGain":1.0,
        "offLoadGain":0.50,

        "maxLoadMix": 0.8,
        "minLoadMix": 0,

        "lowShelfGain":-4,
        "lowShelfFreq":140,

        "highShelfGain":6,
        "highShelfFreq":2000,

        "eqLowGain": 0,
        "eqLowFreq": 500,
        "eqLowWidth": 0.2,

        "eqHighGain": 4,
        "eqHighFreq": 1500,
        "eqHighWidth": 0.2,

        "fundamentalFrequencyCylinderCount":6,
        "eqFundamentalGain": -7,
    },
    "vehicleController": {
        "clutchLaunchStartRPM":1200,
        "clutchLaunchTargetRPM":1500,
        //**highShiftDown can be overwritten by automatic transmissions**
        "highShiftDownRPM":[0,0,0,2400,2900,3200,3600,3600],
        //**highShiftUp can be overwritten by intake modifications**
        "highShiftUpRPM":5200,
    },
    "flexbodies": [
        ["mesh", "[group]:", "nonFlexMaterials"],
        ["etk800_engbaycrap", ["etk800_body"]],
        ["etkc_engbaycrap", ["etkc_body"],[]{"pos":{"x":0,"y":-0.11,"z":0.03}}],
        ["etk_engine_i6", ["etk_engine","etk_transmission"],[]{"pos":{"x":0,"y":0,"z":0}}],
        {"deformGroup":"radtube_break", "deformMaterialBase":"etk800", "deformMaterialDamaged":"invis"},
        ["etk_radtube_i6", ["etk_radiator","etk_engine"],[]{"pos":{"x":0,"y":0,"z":0}}],
        {"deformGroup":""},
    ],
    "nodes": [
         ["id", "posX", "posY", "posZ"],
         //--3.0L I6 Engine--
         {"selfCollision":false},
         {"collision":true},
         {"nodeMaterial":"|NM_METAL"},
         {"frictionCoef":0.5},
         {"group":"etk_engine"},
         {"nodeWeight":23},
         {"engineGroup":"engine_block"},
         {"chemEnergy":2000,"burnRate":0.39,"flashPoint":800,"specHeat": 0.1,"selfIgnitionCoef":false,"smokePoint":650,"baseTemp":"thermals","conductionRadius":0.13},
         ["e1r", -0.05, -0.97, 0.26],
         ["e1l", 0.16, -0.97, 0.26],
         ["e2r", -0.05, -1.60, 0.35],
         ["e2l", 0.16, -1.60, 0.35],
         {"chemEnergy":false,"burnRate":false,"flashPoint":false, "specHeat": false,"selfIgnitionCoef":false,"smokePoint":false,"baseTemp":false,"conductionRadius":false},
         {"engineGroup":["engine_block","engine_intake"]},
         ["e3r", -0.26, -0.97, 0.85],
         ["e3l", 0.08, -0.97, 0.85],
         ["e4r", -0.26, -1.60, 0.85, {"isExhaust":"mainEngine"}],
         ["e4l", 0.08, -1.60, 0.85],
         {"group":""},
         {"engineGroup":""},
         //engine mount nodes
         ["em1r", -0.27, -1.45, 0.52, {"nodeWeight":2}],
         ["em1l", 0.27, -1.45, 0.52, {"nodeWeight":2}],
         {"group":""},
    ],
    "beams": [
          ["id1:", "id2:"],
          {"beamType":"|NORMAL", "beamLongBound":1.0, "beamShortBound":1.0},
          {"beamSpring":20001000,"beamDamp":500},
          {"beamDeform":250000,"beamStrength":"FLT_MAX"},
          //engine
          {"deformGroup":"mainEngine", "deformationTriggerRatio":0.001}
          ["e1r","e1l"],
          ["e2r","e2l"],
          ["e3r","e3l"],
          ["e4r","e4l"],

          ["e1r","e2r"],
          ["e1l","e2l"],
          ["e3r","e4r"],
          ["e3l","e4l"],

          ["e1r","e3r"],
          ["e1l","e3l"],
          ["e2r","e4r"],
          ["e2l","e4l"],

          ["e2r","e3r"],
          ["e2l","e3l"],
          ["e2r","e3l"],
          ["e2l","e3r"],

          ["e1r","e4r"],
          ["e1l","e4l"],
          ["e1r","e4l"],
          ["e1l","e4r"],

          ["e1r","e2l"],
          ["e1l","e2r"],
          ["e3r","e4l"],
          ["e3l","e4r"],

          ["e1r","e3l"],
          ["e1l","e3r"],
          ["e2r","e4l"],
          ["e2l","e4r"],
          {"deformGroup":""}
          {"breakGroup":""},

          //radhose and intake breaking beams
          {"beamSpring":4000,"beamDamp":20},
          {"beamDeform":1000,"beamStrength":2000},
          {"deformGroup":"radtube_break","deformationTriggerRatio":0.01},
          ["f15", "e4l"],
          ["f15r", "e4l"],
          ["f15r", "e4r"],
          //["f11", "e2l"],
          ["f11rr", "e2l"],
          ["f11rr", "e2r"],
          {"deformGroup":""},

          //engine mount nodes
          {"beamSpring":3400000,"beamDamp":150},
          {"beamDeform":90000,"beamStrength":"FLT_MAX"},
          ["em1r","e3l"],
          ["em1r","e3r"],
          ["em1r","e4l"],
          ["em1r","e4r"],
          ["em1r", "e1r"],
          ["em1r", "e1l"],
          ["em1r", "e2l"],
          ["em1r", "e2r"],

          ["em1l","e3l"],
          ["em1l","e3r"],
          ["em1l","e4l"],
          ["em1l","e4r"],
          ["em1l", "e1r"],
          ["em1l", "e1l"],
          ["em1l", "e2l"],
          ["em1l", "e2r"],
          {"beamPrecompression":1, "beamType":"|NORMAL", "beamLongBound":1.0, "beamShortBound":1.0},
    ],
},
"etk_engine_i6_3.0_diesel_internals": {
    "information":{
        "authors":"BeamNG",
        "name":"Stock Long Block",
        "value":1200,
    },
    "slotType" : "etk_engine_i6_3.0_diesel_internals",
    "mainEngine":{
    },
},
"etk_engine_i6_3.0_diesel_internals_heavy": {
    "information":{
        "authors":"BeamNG",
        "name":"Heavy Duty Long Block",
        "value":3600,
    },
    "slotType" : "etk_engine_i6_3.0_diesel_internals",
    "mainEngine":{
        //max rpm physically capable of
        "$+maxRPM":250,
        "$*friction":1.2,
        "$*dynamicFriction":1.1,
        "$*inertia":1.3,
        "$*engineBrakeTorque":1.15,
        //engine durability
        "cylinderWallTemperatureDamageThreshold":175,
        "headGasketDamageThreshold":1700000,
        "pistonRingDamageThreshold":1700000,
        "connectingRodDamageThreshold":2200000,
        "$*maxTorqueRating": 1.65,
        "$*maxOverTorqueDamage": 1.5,
    },
},
"etk_intake_i6_3.0_diesel_turbo": {
    "information":{
        "authors":"BeamNG",
        "name":"Stock Turbocharger",
        "value":2800,
    },
    "slotType": "etk_intake_i6_3.0_diesel",
    "turbocharger": {
        "bovEnabled":false,
        "hissLoopEvent":"event:>Vehicle>Forced_Induction>Turbo_01>turbo_hiss",
        "whineLoopEvent":"event:>Vehicle>Forced_Induction>Turbo>Spin>turbo_spin_15",
        "turboSizeCoef": 0.0,
        "bovSoundVolumeCoef": 0.0,
        "hissVolumePerPSI": 0.025,
        "whineVolumePer10kRPM": 0.011,
        "whinePitchPer10kRPM": 0.04,
        "wastegateStart":19,
        "maxExhaustPower": 3000,
        "backPressureCoef": 0.000005,
        "pressureRatePSI": 30,
        "frictionCoef": 25.5,
        "inertia":1.30,
        "damageThresholdTemperature": 640,
        "pressurePSI":[
            //turbineRPM, pressure(PSI)
            [0,         -3.5],
            [24000,     -1.5],
            [60000,     19],
            [90000,     23],
            [150000,    28],
            [200000,    35],
            [250000,    40],
        ],
        "engineDef":[
            //engineRPM, efficiency, exhaustFactor
            [0,     0.0,    0.5],
            [650,   0.3,   0.55],
            [850,   0.4,   0.55],
            [1000,   0.45,   0.65],
            [1000,   0.45,   0.75],
            [1450,  0.82,    0.8],
            [2000,  0.70,    0.8],
            [2500,  0.65,    1.0],
            [3000,  0.65,    1.0],
            [4000,  0.62,    1.0],
            [4500,  0.65,    1.0],
            [5000,  0.65,    1.0],
            [6000,  0.45,    1.0],
        ],
    },
    "mainEngine": {
        //turbocharger name
        "turbocharger":"turbocharger",

        //turbocharger deform groups
        "deformGroups_turbo":["mainEngine_turbo","mainEngine_intercooler","mainEngine_piping"]
    },
    "flexbodies": [
        ["mesh", "[group]:", "nonFlexMaterials"],
        ["etk_header_i6_turbo", ["etk_engine", "etk_header"],[]{"pos":{"x":0,"y":0,"z":0}}],
        ["etk_intake_i6_turbo", ["etk_engine","etk_radiator"],[]{"pos":{"x":0,"y":0,"z":0}}],
        {"deformGroup":"mainEngine_piping", "deformMaterialBase":"etk800", "deformMaterialDamaged":"invis"},
        ["etk_icpipe_t_i6", ["etk_engine","etk_radiator"],[]{"pos":{"x":0,"y":0,"z":0}}],
        ["etk_icpipe_i_i6", ["etk_engine","etk_radiator"],[]{"pos":{"x":0,"y":0,"z":0}}],
        {"deformGroup":""},
        ["etk_intercooler", ["etk_intercooler", "etk_radiator"],[]{"pos":{"x":0,"y":0,"z":0}}],
    ],
    "nodes": [
         ["id", "posX", "posY", "posZ"],
         {"selfCollision":false},
         {"collision":true},
         {"frictionCoef":0.5},
         {"nodeMaterial":"|NM_METAL"},

         //turbo
         {"nodeWeight":4.5},
         {"group":"etk_turbo"},
         ["turbo", -0.2, -1.3, 0.52],

         //exhaust manifold
         {"group":"etk_header"},
         ["exm1r", -0.2, -0.9, 0.27],

         //intercooler
         {"nodeWeight":2.5},
         {"group":"etk_intercooler"},
         ["interc1r", -0.3, -2.1, 0.32],
         ["interc1l", 0.3, -2.1, 0.32],

         //airbox
         {"nodeWeight":1.0},
         {"group":"etk_airbox"},
         {"nodeMaterial":"|NM_PLASTIC"},
         ["airb1", 0.35, -1.75, 0.8],
         ["airb2", -0.35, -1.9, 0.76],
         {"engineGroup":""},
         {"group":""},
    ],
    "beams": [
          ["id1:", "id2:"],
          {"beamPrecompression":1, "beamType":"|NORMAL", "beamLongBound":1.0, "beamShortBound":1.0},

          //turbo
          {"beamSpring":2750550,"beamDamp":125},
          {"beamDeform":30000,"beamStrength":"FLT_MAX"},
          {"deformLimitExpansion":""},
          {"deformGroup":"mainEngine_turbo", "deformationTriggerRatio":0.001}
          ["turbo", "e1r"],
          ["turbo", "e1l"],
          ["turbo", "e2r"],
          ["turbo", "e2l"],
          ["turbo", "e3r"],
          ["turbo", "e3l"],
          ["turbo", "e4l"],
          ["turbo", "e4r", {"isExhaust":"mainEngine"}],

          //exhaust manifold
          {"beamSpring":2750550,"beamDamp":125},
          {"beamDeform":30000,"beamStrength":"FLT_MAX"},
          ["exm1r", "e1r"],
          ["exm1r", "e1l"],
          ["exm1r", "e2r"],
          ["exm1r", "e2l"],
          ["exm1r", "e3r"],
          ["exm1r", "e3l"],
          ["exm1r", "e4r"],
          ["exm1r", "e4l"],
          ["exm1r", "turbo", {"isExhaust":"mainEngine"}],

          //intercooler
          {"beamSpring":1001000,"beamDamp":100},
          {"beamDeform":5000,"beamStrength":"FLT_MAX"},
          {"deformGroup":"mainEngine_intercooler", "deformationTriggerRatio":0.1}
          {"deformLimitExpansion":1.5},
          ["interc1r", "interc1l"],
          ["interc1l", "f18l"],
          ["interc1l", "f11ll"],
          ["interc1l", "f13ll"],
          ["interc1l", "f18"],
          ["interc1l", "f15"],
          ["interc1r", "f11rr"],
          ["interc1r", "f18r"],
          ["interc1r", "f18"],
          ["interc1r", "f13rr"],
          ["interc1r", "f15"],
          ["interc1r", "f10rr"],
          ["interc1l", "f10ll"],
          {"deformLimitExpansion":""},

          //intercooler piping
          {"beamSpring":501000,"beamDamp":400},
          {"beamDeform":5000,"beamStrength":15000},
          {"deformGroup":"mainEngine_piping", "deformationTriggerRatio":0.01}
          ["interc1l", "e2l"],
          ["interc1l", "e4l"],
          ["interc1l", "e2r"],
          ["interc1l", "e4r"],
          ["interc1l", "e2l"],
          ["interc1l", "e4l"],
          ["interc1l", "e2r"],
          ["interc1l", "e4r"],
          ["interc1r", "turbo"],
          ["interc1l", "turbo"],
          ["interc1l", "airb1"],
          ["interc1r", "airb1"],
          {"deformGroup":""}

          //airbox
          {"beamSpring":101000,"beamDamp":300},
          {"beamDeform":3000,"beamStrength":25000},
          {"deformGroup":"mainEngine_intake", "deformationTriggerRatio":0.01}
          ["airb1", "e3r"],
          ["airb1", "e3l"],
          ["airb1", "e4r"],
          ["airb1", "e4l"],
          ["airb1", "e2l"],
          ["airb1", "e2r"],
          ["airb2", "e3r"],
          ["airb2", "e3l"],
          ["airb2", "e4r"],
          ["airb2", "e4l"],
          ["airb2", "e2l"],
          ["airb2", "e2r"],
          ["airb2", "airb1"],
          {"deformGroup":""}

          //attach to body
          {"beamDeform":2000,"beamStrength":15000},
          ["airb2", "f13rr"],
          ["airb2", "f15r"],
          ["airb2", "f15"],
          ["airb2", "f11rr"],
          ["airb2", "f15rr"],

          ["airb1", "f12ll"],
          ["airb1", "f15ll"],
          ["airb1", "f13ll"],
          {"beamPrecompression":1, "beamType":"|NORMAL", "beamLongBound":1.0, "beamShortBound":1.0},
    ],
},
}