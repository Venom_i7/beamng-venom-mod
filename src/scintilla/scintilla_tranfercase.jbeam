{
    "scintilla_transfer_case_RWD": {
        "information":{
            "authors":"BeamNG",
            "name":"RWD Output Shaft",
            "value":500,
        },
        "slotType" : "scintilla_transfer_case",
        "powertrain" : [
            ["type", "name", "inputName", "inputIndex"],
            ["shaft", "transfercase", "gearbox", 1, {"deformGroups":["gearbox"], "outputPortOverride":[1], "friction":0.33, "dynamicFriction":0.00037,"uiName":"Rear Output Shaft"}],
        ],
    },
    "scintilla_transfer_case_clutchex": {
        "information":{
            "authors":"BeamNG",
            "name":"Clutchex AWD Transfer Case",
            "value":3100,
        },
        "slotType" : "scintilla_transfer_case",
        "slots": [
            ["type", "default", "description"],
            ["scintilla_front_output_ratio","scintilla_front_output_ratio","Front Output Gear Ratio", {"coreSlot":true}],
        ],
        "powertrain" : [
            ["type", "name", "inputName", "inputIndex"],
            ["splitShaft", "transfercase", "gearbox", 1, {"splitType":"locked", "primaryOutputID":1, "lockTorque":550, "friction":1.15, "dynamicFriction":0.00053, "torqueLossCoef": 0.012, "uiName":"Electrically Clutched Torque Splitter","defaultVirtualInertia":0.1}],
        ],
        "controller": [
            ["fileName"],
            ["drivingDynamics/actuators/electronicSplitShaftLock" {"name": "electronicSplitShaftLock", "splitShaftName":"transfercase", "clutchPIDkP":0.06, "clutchPIDkI":0, "clutchPIDkD":0}]
        ],
        "flexbodies": [
             ["mesh", "[group]:", "nonFlexMaterials"],
             ["scintilla_transfercase", ["scintilla_engine","scintilla_transfercase"]],
        ],
        "nodes": [
             ["id", "posX", "posY", "posZ"],
             {"selfCollision":false},
             {"collision":true},
             {"nodeMaterial":"|NM_METAL"},
             {"frictionCoef":0.5},

             //transfer case
             {"nodeWeight":25.0},
             {"group":"scintilla_transfercase"},
             ["trc1r", -0.177, 0.52, 0.18],
        ],
        "beams": [
              ["id1:", "id2:"],
              //differential node
              {"beamType":"|NORMAL", "beamLongBound":1.0, "beamShortBound":1.0},
              {"beamSpring":2501000,"beamDamp":150},
              {"beamDeform":35000,"beamStrength":"FLT_MAX"},
              {"deformLimitExpansion":1.2},
              {"deformGroup":"transfercase", "deformationTriggerRatio":0.001},
              ["trc1r", "e1r"],
              ["trc1r", "e3r"],
              ["trc1r", "e2r"],
              ["trc1r", "e4r"],
              ["trc1r", "e1l"],
              ["trc1r", "e3l"],
              ["trc1r", "e2l"],
              ["trc1r", "e4l"],
              {"deformGroup":""},
        ],
    },
    "scintilla_transfer_case_clutchex_race": {
        "information":{
            "authors":"BeamNG",
            "name":"Clutchex Race AWD Transfer Case",
            "value":3100,
        },
        "slotType" : "scintilla_transfer_case",
        "slots": [
            ["type", "default", "description"],
            ["scintilla_front_output_ratio","scintilla_front_output_ratio","Front Output Gear Ratio", {"coreSlot":true}],
        ],
        "variables": [
            ["name", "type", "unit", "category", "default", "min", "max", "title", "description"],
            ["$splitshaft_stiffness", "range", "", "Transfer Case", 0.09, 0.02, 0.2, "Locking Rate", "How strongly the center clutch engages in response to rear wheel slip", {"minDis":0, "maxDis":100}],
            ["$splitshaft_threshold", "range", "", "Transfer Case", 16, 0, 40, "Slip Threshold", "How much rear wheelslip before the center clutch begins to engage", {"minDis":0, "maxDis":40}],
            ["$splitshaft_minlock", "range", "", "Transfer Case", 0, 0, 0.2, "Minimum Lock", "Prevents the center clutch from opening fully", {"minDis":0, "maxDis":20}],
            ["$splitshaft_torque", "range", "N-m", "Transfer Case", 600, 0, 1000, "Maximum Torque", "Torque transmittable when center clutch fully closes", {"stepDis":50}],
        ],
        "powertrain" : [
            ["type", "name", "inputName", "inputIndex"],
            ["splitShaft", "transfercase", "gearbox", 1, {"splitType":"locked", "primaryOutputID":1, "lockTorque":"$splitshaft_torque", "friction":1.15, "dynamicFriction":0.00053, "torqueLossCoef": 0.012, "uiName":"Electrically Clutched Torque Splitter","defaultVirtualInertia":0.1}],
        ],
        "controller": [
            ["fileName"],
            ["drivingDynamics/actuators/electronicSplitShaftLock" {"name": "electronicSplitShaftLock", "splitShaftName":"transfercase", "clutchPIDkP":"$splitshaft_stiffness", "avDiffThreshold": "$splitshaft_threshold", "minimumLock":"$splitshaft_minlock", "clutchPIDkI":0, "clutchPIDkD":0}]
        ],
        "flexbodies": [
             ["mesh", "[group]:", "nonFlexMaterials"],
             ["scintilla_transfercase", ["scintilla_engine","scintilla_transfercase"]],
        ],
        "nodes": [
             ["id", "posX", "posY", "posZ"],
             {"selfCollision":false},
             {"collision":true},
             {"nodeMaterial":"|NM_METAL"},
             {"frictionCoef":0.5},

             //transfer case
             {"nodeWeight":25.0},
             {"group":"scintilla_transfercase"},
             ["trc1r", -0.177, 0.52, 0.18],
        ],
        "beams": [
              ["id1:", "id2:"],
              //differential node
              {"beamType":"|NORMAL", "beamLongBound":1.0, "beamShortBound":1.0},
              {"beamSpring":2501000,"beamDamp":150},
              {"beamDeform":35000,"beamStrength":"FLT_MAX"},
              {"deformLimitExpansion":1.2},
              {"deformGroup":"transfercase", "deformationTriggerRatio":0.001},
              ["trc1r", "e1r"],
              ["trc1r", "e3r"],
              ["trc1r", "e2r"],
              ["trc1r", "e4r"],
              ["trc1r", "e1l"],
              ["trc1r", "e3l"],
              ["trc1r", "e2l"],
              ["trc1r", "e4l"],
              {"deformGroup":""},
        ],
    },
    "scintilla_transfer_case_AWD_race": {
        "information":{
            "authors":"BeamNG",
            "name":"Race AWD Transfer Case",
            "value":6400,
        },
        "slotType" : "scintilla_transfer_case",
        "slots": [
            ["type", "default", "description"],
            ["scintilla_front_output_ratio","scintilla_front_output_ratio","Front Output Gear Ratio", {"coreSlot":true}],
        ],
        "variables": [
            ["name", "type", "unit", "category", "default", "min", "max", "title", "description"],
            ["$difftorquesplit_C", "range", "", "Differentials", 0.70, 0.6, 0.85, "Base Torque Split", "Percent torque to rear wheels", {"minDis":60, "maxDis":85, "stepDis":1, "subCategory":"Center"}],
            ["$lsdpreload_C", "range", "N/m", "Differentials", 50, 0, 500, "Pre-load Torque", "Initial cross torque between front and rear wheels", {"subCategory":"Center"}],
            ["$lsdlockcoef_C", "range", "", "Differentials", 0.125, 0, 0.5, "Power Lock Rate", "Additional locking torque proportional to engine torque", {"minDis":0, "maxDis":100,"subCategory":"Center"}],
            ["$lsdlockcoefrev_C", "range", "", "Differentials", 0.02, 0, 0.5, "Coast Lock Rate" , "Additional locking torque proportional to engine braking", {"minDis":0, "maxDis":100,"subCategory":"Center"}],
        ],
        "powertrain" : [
            ["type", "name", "inputName", "inputIndex"],
            ["differential", "transfercase", "gearbox", 1, {"primaryOutputID":1, "diffType":"lsd", "lsdPreload":"$lsdpreload_C", "lsdLockCoef":"$lsdlockcoef_C", "lsdRevLockCoef":"$lsdlockcoefrev_C", "diffTorqueSplit":"$difftorquesplit_C", "friction":1.65, "dynamicFriction":0.00054, "torqueLossCoef":0.0105, "uiName":"Center Differential","defaultVirtualInertia":0.1,"speedLimitCoef":0.1}],
        ],
        "flexbodies": [
             ["mesh", "[group]:", "nonFlexMaterials"],
             ["scintilla_transfercase", ["scintilla_engine","scintilla_transfercase"]],
        ],
        "nodes": [
             ["id", "posX", "posY", "posZ"],
             {"selfCollision":false},
             {"collision":true},
             {"nodeMaterial":"|NM_METAL"},
             {"frictionCoef":0.5},

             //transfer case
             {"nodeWeight":22.0},
             {"group":"scintilla_transfercase"},
             ["trc1r", -0.177, 0.52, 0.18],
        ],
        "beams": [
              ["id1:", "id2:"],
              //differential node
              {"beamType":"|NORMAL", "beamLongBound":1.0, "beamShortBound":1.0},
              {"beamSpring":2501000,"beamDamp":150},
              {"beamDeform":35000,"beamStrength":"FLT_MAX"},
              {"deformLimitExpansion":1.2},
              {"deformGroup":"transfercase", "deformationTriggerRatio":0.001},
              ["trc1r", "e1r"],
              ["trc1r", "e3r"],
              ["trc1r", "e2r"],
              ["trc1r", "e4r"],
              ["trc1r", "e1l"],
              ["trc1r", "e3l"],
              ["trc1r", "e2l"],
              ["trc1r", "e4l"],
              {"deformGroup":""},
        ],
    }
    "scintilla_front_output_ratio": {
        "information":{
            "authors":"BeamNG",
            "name":"0.946:1 Front Output Gear Ratio",
            "value":350,
        },
        "slotType" : "scintilla_front_output_ratio",
        "powertrain" : [
            ["type", "name", "inputName", "inputIndex"],
            ["shaft", "transfercaseOutput", "transfercase", 2, {"gearRatio":0.946, "uiName":"Front Output Gear"}],
        ],
    },
    "scintilla_front_output_ratio_1_1": {
        "information":{
            "authors":"BeamNG",
            "name":"1:1 Front Output Gear Ratio",
            "value":475,
        },
        "slotType" : "scintilla_front_output_ratio",
        "powertrain" : [
            ["type", "name", "inputName", "inputIndex"],
            ["shaft", "transfercaseOutput", "transfercase", 2, {"gearRatio":1, "uiName":"Front Output Gear"}],
        ],
    },
    "scintilla_front_output_ratio_race": {
        "information":{
            "authors":"BeamNG",
            "name":"Adjustable Front Output Gear Ratio",
            "value":1460,
        },
        "slotType" : "scintilla_front_output_ratio",
        "variables": [
            ["name", "type", "unit", "category", "default", "min", "max", "title", "description"],
            ["$tcase_frontratio", "range", ":1", "Transfer Case", 0.946, 0.85, 1.15, "Front Output Gear Ratio", "Accounts for tire diameter ratio"{"stepDis":0.001}],
        ],
        "powertrain" : [
            ["type", "name", "inputName", "inputIndex"],
            ["shaft", "transfercaseOutput", "transfercase", 2, {"gearRatio":"$tcase_frontratio", "uiName":"Front Drive Gear"}],
        ],
    },
}