{
  "sbr_motor_electric_R_venom": {
    "information": {
      "authors": "Venom",
      "name": "Venom Rear Electric Motor",
      "value": 44500
    },
    "slotType": "sbr_engine",
    "slots": [
      ["type", "default", "description"],
      ["sbr_differential_R_electric", "sbr_differential_R_electric", "Rear Differential"]
    ],
    "variables": [
      ["name", "type", "unit", "category", "default", "min", "max", "title", "description"],
      ["$venomBackMotorOverdrive", "range", "%", "Motor", 0, -300, 500, "Rear Motor Overdrive", "KuyPeD Additonal motor torque by percentage", { "stepDis": 1 }]
    ],
    "powertrain": [
      ["type", "name", "inputName", "inputIndex"],
      ["electricMotor", "rearMotor", "dummy", 0]
    ],
    "rearMotor": {
      "torque": [
        ["rpm", "torque"],
        [0, "$=750*(1+($venomBackMotorOverdrive/100))"],
        [500, "$=775*(1+($venomBackMotorOverdrive/100))"],
        [1000, "$=800*(1+($venomBackMotorOverdrive/100))"],
        [1500, "$=825*(1+($venomBackMotorOverdrive/100))"],
        [2000, "$=850*(1+($venomBackMotorOverdrive/100))"],
        [3000, "$=875*(1+($venomBackMotorOverdrive/100))"],
        [4000, "$=900*(1+($venomBackMotorOverdrive/100))"],
        [5000, "$=925*(1+($venomBackMotorOverdrive/100))"],
        [6000, "$=950*(1+($venomBackMotorOverdrive/100))"],
        [7000, "$=975*(1+($venomBackMotorOverdrive/100))"],
        [8000, "$=1000*(1+($venomBackMotorOverdrive/100))"],
        [9000, "$=1000*(1+($venomBackMotorOverdrive/100))"],
        [10000, "$=1000*(1+($venomBackMotorOverdrive/100))"],
        [11000, "$=950*(1+($venomBackMotorOverdrive/100))"],
        [12000, "$=900*(1+($venomBackMotorOverdrive/100))"],
        [13000, "$=850*(1+($venomBackMotorOverdrive/100))"],
        [14000, "$=800*(1+($venomBackMotorOverdrive/100))"],
        [15000, "$=750*(1+($venomBackMotorOverdrive/100))"],
        [16000, "$=700*(1+($venomBackMotorOverdrive/100))"],
        [17000, "$=650*(1+($venomBackMotorOverdrive/100))"],
        [18000, "$=600*(1+($venomBackMotorOverdrive/100))"],
        [19000, "$=550*(1+($venomBackMotorOverdrive/100))"],
        [20000, "$=500*(1+($venomBackMotorOverdrive/100))"]
      ],

      "maxRPM": 20000,
      "maxRegenPower": 275,
      "maxRegenTorque": 125,
      "inertia": 0.12,
      "friction": 1.45,
      "dynamicFriction": 0.00012,
      "electricalEfficiency": 0.995,
      "torqueReactionNodes:": ["rem1l", "rem1r", "rem4r"],
      "energyStorage": "mainBattery",
      "electricsThrottleFactorName": "throttleFactorRear",

      //node beam interface
      "waterDamage": { "[engineGroup]:": ["rear_motor_waterdamage"] },
      "engineBlock": { "[engineGroup]:": ["rear_motor_block"] },
      "breakTriggerBeam": "rear_motor",
      "uiName": "Rear Motor",

      "soundConfig": "soundConfigRear"
    },
    "soundConfigRear": {
      "sampleName": "ElectricMotor",
      "mainGain": -4.5
      //"maxLoadMix": 0.65,
      //"minLoadMix": 0,
      //"onLoadGain":1,
      //"offLoadGain":0.65,
      //"eqLowGain": -15,
      //"eqLowFreq": 100,
      //"eqLowWidth": 0.05,
      //"eqHighGain": -5,
      //"eqHighFreq": 6000,
      //"eqHighWidth": 0.1,
      //"eqFundamentalGain": -5,
    },
    "vehicleController": {
      "shiftLogicName": "electricMotor",
      "motorNames": ["rearMotor", "frontMotor"],
      "topSpeedLimitReverse": 15,
      "onePedalRegenCoef": 0.85
    },
    "flexbodies": [
      ["mesh", "[group]:", "nonFlexMaterials"],
      ["sbr_emotor_large", ["sbr_rear_motor"]],
      ["sbr_emotor_large_mount", ["sbr_rear_motor"]],
      ["sbr_emotor_large_support", ["sbr_subframe_R", "sbr_lowermounts_R", "sbr_uppermounts_R"]],
      ["sbr_wiring_emotor_b", ["sbr_body"]],
      ["sbr_ecu_R", ["sbr_body"]],
      ["sbr_wiring_battery_R", ["sbr_body"]],
      ["sbr_trunk", ["sbr_body"]]
    ],
    "nodes": [
      ["id", "posX", "posY", "posZ"],
      { "frictionCoef": 0.5 },
      { "nodeMaterial": "|NM_METAL" },
      { "selfCollision": false },
      { "collision": true },
      { "group": "sbr_rear_motor" },
      { "engineGroup": "rear_motor_block" },
      { "nodeWeight": 10 },
      {
        "chemEnergy": 1000,
        "burnRate": 0.39,
        "flashPoint": 800,
        "specHeat": 0.2,
        "selfIgnitionCoef": false,
        "smokePoint": 650,
        "baseTemp": "thermals",
        "conductionRadius": 0.15
      },
      ["rem1r", -0.38, 1.38, 0.26],
      ["rem1l", 0.38, 1.38, 0.26],
      ["rem2r", -0.38, 1.58, 0.26],
      ["rem2l", 0.38, 1.58, 0.26],
      {
        "chemEnergy": false,
        "burnRate": false,
        "flashPoint": false,
        "specHeat": false,
        "selfIgnitionCoef": false,
        "smokePoint": false,
        "baseTemp": false,
        "conductionRadius": false
      },
      { "engineGroup": ["rear_motor_block", "rear_motor_waterdamage"] },
      ["rem3r", -0.38, 1.38, 0.47],
      ["rem3l", 0.38, 1.38, 0.47],
      ["rem4r", -0.38, 1.58, 0.47],
      ["rem4l", 0.38, 1.58, 0.47],
      { "engineGroup": "" },
      { "nodeWeight": 4 },
      ["rem5r", -0.21, 1.615, 0.225],
      ["rem5l", 0.21, 1.615, 0.225],
      { "selfCollision": false },
      { "collision": true },
      { "group": "sbr_transaxle" },
      { "nodeWeight": 10 },
      ["tra1", 0.0, 1.12, 0.19],
      { "nodeWeight": 40 },
      { "group": ["rear_motor_block", "sbr_reardiff"] },
      ["rem6", 0.0, 1.44, 0.34],
      { "engineGroup": "" },
      { "group": "" }
    ],
    "beams": [
      ["id1:", "id2:"],
      { "beamType": "|NORMAL", "beamLongBound": 1.0, "beamShortBound": 1.0 },
      { "beamSpring": 5001000, "beamDamp": 500 },
      { "beamDeform": 100000, "beamStrength": "FLT_MAX" },
      //engine
      ["rem1r", "rem1l"],
      ["rem2r", "rem2l"],
      ["rem3r", "rem3l"],
      ["rem4r", "rem4l"],

      ["rem1r", "rem2r"],
      ["rem1l", "rem2l"],
      ["rem3r", "rem4r"],
      ["rem3l", "rem4l"],

      ["rem1r", "rem3r"],
      ["rem1l", "rem3l"],
      ["rem2r", "rem4r"],
      ["rem2l", "rem4l"],

      ["rem2r", "rem3r"],
      ["rem2l", "rem3l"],
      ["rem2r", "rem3l"],
      ["rem2l", "rem3r"],

      ["rem1r", "rem4r"],
      ["rem1l", "rem4l"],
      ["rem1r", "rem4l"],
      ["rem1l", "rem4r"],

      ["rem1r", "rem2l"],
      ["rem1l", "rem2r"],
      ["rem3r", "rem4l"],
      ["rem3l", "rem4r"],

      ["rem1r", "rem3l"],
      ["rem1l", "rem3r"],
      ["rem2r", "rem4l"],
      ["rem2l", "rem4r"],

      { "beamSpring": 3751000, "beamDamp": 250 },
      ["rem5l", "rem5r"],
      ["rem5l", "rem2l"],
      ["rem5r", "rem2r"],
      ["rem5l", "rem2r"],
      ["rem5r", "rem2l"],
      ["rem5l", "rem1l"],
      ["rem5r", "rem1r"],
      //["rem5l", "tra2"],
      //["rem5r", "tra2"],
      ["rem5r", "rem4r"],
      ["rem5l", "rem4l"],
      ["rem5l", "rem4r"],
      ["rem5r", "rem4l"],

      ["rem6", "rdiff", { "optional": true }],
      ["rem6", "rem1l"],
      ["rem6", "rem3l"],
      ["rem6", "rem1r"],
      ["rem6", "rem3r"],
      ["rem6", "rem2l"],
      ["rem6", "rem4l"],
      ["rem6", "rem2r"],
      ["rem6", "rem4r"],

      //--TRANSMISSION CONE--
      { "beamSpring": 5001000, "beamDamp": 250 },
      { "beamDeform": 75000, "beamStrength": "FLT_MAX" },
      ["tra1", "rem1r"],
      ["tra1", "rem3r"],
      ["tra1", "rem1l"],
      ["tra1", "rem3l"],

      //motor and transaxle mounting
      { "optional": true },
      { "beamDeform": 14000, "beamStrength": 90000 },
      { "beamSpring": 501000, "beamDamp": 1000 },
      ["rem5l", "rx2l", { "dampCutoffHz": 500 }],
      ["rem5l", "rx5l", { "dampCutoffHz": 500 }],
      ["rem5l", "r2ll", { "name": "rear_motor", "beamStrength": 50000, "disableMeshBreaking": true, "disableTriangleBreaking": true }],
      ["rem5l", "r5ll"],
      ["rem5l", "r2rr"],
      ["rem5l", "r5rr"],
      ["rem5l", "r1"],
      ["rem5l", "rx1", { "dampCutoffHz": 500 }],

      ["rem5r", "rx2r", { "dampCutoffHz": 500 }],
      ["rem5r", "rx5r", { "dampCutoffHz": 500 }],
      ["rem5r", "r2rr", { "name": "rear_motor", "beamStrength": 50000, "disableMeshBreaking": true, "disableTriangleBreaking": true }],
      ["rem5r", "r5rr"],
      ["rem5r", "r2ll"],
      ["rem5r", "r5ll"],
      ["rem5r", "r1"],
      ["rem5r", "rx1", { "dampCutoffHz": 500 }],

      { "beamSpring": 501000, "beamDamp": 500 },
      //transmission soft mounts
      ["tra1", "f9r"],
      ["tra1", "f9l"],
      ["tra1", "f4r"],
      ["tra1", "f4l"],
      ["tra1", "r1"],

      ["tra1", "rx1l", { "dampCutoffHz": 500 }],
      ["tra1", "rx1r", { "dampCutoffHz": 500 }],

      //engine limiters
      { "beamPrecompression": 1, "beamType": "|SUPPORT", "beamLongBound": 1.0 },
      { "beamSpring": 2501000, "beamDamp": 150, "beamDeform": 250000, "beamStrength": "FLT_MAX" },
      { "beamPrecompression": 0.9 },
      //upper limiter
      ["rem2r", "tb1"],
      ["rem2l", "tb1"],
      ["rem1l", "r1"],
      ["rem1r", "r1"],

      //rear body to engine
      { "beamPrecompression": 0.7 },
      ["r6", "e1r"],
      ["r6", "e1l"],
      ["r3", "e3r"],
      ["r3", "e3l"],

      //side to engine
      { "beamPrecompression": 0.8 },
      ["r2rr", "e3l"],
      ["r2rr", "e4l"],
      ["r2ll", "e3r"],
      ["r2ll", "e4r"],

      //front body to engine
      { "beamSpring": 5001000, "beamDamp": 150, "beamDeform": "FLT_MAX", "beamStrength": "FLT_MAX" },
      { "beamPrecompression": 0.95 },
      ["f4l", "e4r"],
      ["f4r", "e4l"],
      //{"beamPrecompression":0.9},
      ["e2l", "f9r"],
      ["e2r", "f9l"],

      //transmission limiter
      { "beamSpring": 2501000, "beamDamp": 150, "beamDeform": 250000, "beamStrength": "FLT_MAX" },
      { "beamPrecompression": 0.95 },
      ["tra1", "f9r"],
      ["tra1", "f9l"],
      ["tra1", "f4r"],
      ["tra1", "f4l"],
      ["tra1", "q3r"],
      ["tra1", "q3l"],
      { "beamPrecompression": 0.8 },
      ["tra1", "r1"],
      { "beamPrecompression": 1 },
      { "optional": false },
      { "beamPrecompression": 1, "beamType": "|NORMAL", "beamLongBound": 1.0, "beamShortBound": 1.0 }
    ]
  },
  "sbr_motor_electric_F_venom": {
    "information": {
      "authors": "Venom",
      "name": "Venom Front Electric Motor",
      "value": 27500
    },
    "slotType": "sbr_differential_F",
    "slots": [
      ["type", "default", "description"],
      ["sbr_differential_F_electric", "sbr_differential_F_electric", "Front Differential"]
    ],
    "variables": [
      ["name", "type", "unit", "category", "default", "min", "max", "title", "description"],
      ["$venomFrontMotorOverdrive", "range", "%", "Motor", 0, -300, 500, "Front Motor Overdrive", "KuyPeD Additonal motor torque by percentage", { "stepDis": 1 }]
    ],
    "powertrain": [
      ["type", "name", "inputName", "inputIndex"],
      ["electricMotor", "frontMotor", "dummy", 0]
    ],
    "frontMotor": {
      "torque": [
        ["rpm", "torque"],
        [0, "$=400*(1+($venomFrontMotorOverdrive/100))"],
        [500, "$=400*(1+($venomFrontMotorOverdrive/100))"],
        [1000, "$=400*(1+($venomFrontMotorOverdrive/100))"],
        [1500, "$=400*(1+($venomFrontMotorOverdrive/100))"],
        [2000, "$=400*(1+($venomFrontMotorOverdrive/100))"],
        [3000, "$=400*(1+($venomFrontMotorOverdrive/100))"],
        [4000, "$=400*(1+($venomFrontMotorOverdrive/100))"],
        [5000, "$=400*(1+($venomFrontMotorOverdrive/100))"],
        [6000, "$=400*(1+($venomFrontMotorOverdrive/100))"],
        [7000, "$=400*(1+($venomFrontMotorOverdrive/100))"],
        [8000, "$=400*(1+($venomFrontMotorOverdrive/100))"],
        [9000, "$=400*(1+($venomFrontMotorOverdrive/100))"],
        [10000, "$=400*(1+($venomFrontMotorOverdrive/100))"],
        [11000, "$=380*(1+($venomFrontMotorOverdrive/100))"],
        [12000, "$=360*(1+($venomFrontMotorOverdrive/100))"],
        [13000, "$=340*(1+($venomFrontMotorOverdrive/100))"],
        [14000, "$=320*(1+($venomFrontMotorOverdrive/100))"],
        [15000, "$=300*(1+($venomFrontMotorOverdrive/100))"],
        [16000, "$=280*(1+($venomFrontMotorOverdrive/100))"],
        [17000, "$=260*(1+($venomFrontMotorOverdrive/100))"],
        [18000, "$=240*(1+($venomFrontMotorOverdrive/100))"],
        [19000, "$=220*(1+($venomFrontMotorOverdrive/100))"],
        [20000, "$=200*(1+($venomFrontMotorOverdrive/100))"]
      ],
      "maxRPM": 20000,
      "maxRegenPower": 175,
      "maxRegenTorque": 125,
      "inertia": 0.12,
      "friction": 0.65,
      "dynamicFriction": 0.00012,
      "electricalEfficiency": 0.995,
      "torqueReactionNodes:": ["fem1l", "fem1r", "fem4r"],
      "energyStorage": "mainBattery",
      "electricsThrottleFactorName": "throttleFactorFront",

      //node beam interface
      "waterDamage": { "[engineGroup]:": ["front_motor_waterdamage"] },
      "engineBlock": { "[engineGroup]:": ["front_motor_block"] },
      "breakTriggerBeam": "front_motor",
      "uiName": "Front Motor",

      "soundConfig": "soundConfigFront"
    },
    "soundConfigFront": {
      "sampleName": "ElectricMotor",
      "mainGain": -10
      //"maxLoadMix": 0.65,
      //"minLoadMix": 0,
      //"onLoadGain":1,
      //"offLoadGain":0.65,
      //"eqLowGain": -15,
      //"eqLowFreq": 100,
      //"eqLowWidth": 0.05,
      //"eqHighGain": -5,
      //"eqHighFreq": 6000,
      //"eqHighWidth": 0.1,
      //"eqFundamentalGain": -5,
    },
    "vehicleController": {
      "shiftLogicName": "electricMotor",
      "motorNames": ["rearMotor", "frontMotor"],
      "topSpeedLimitReverse": 15,
      "onePedalRegenCoef": 0.85
    },
    "flexbodies": [
      ["mesh", "[group]:", "nonFlexMaterials"],
      ["sbr_emotor_small", ["sbr_front_motor"]],
      ["sbr_emotor_small_mount", ["sbr_front_motor"]],
      ["sbr_ecu_F", ["sbr_body"]],
      ["sbr_wiring_battery_F", ["sbr_body"]],
      { "deformGroup": "wiring_emotor_a_break", "deformMaterialBase": "sbr_emotor", "deformMaterialDamaged": "invis" },
      ["sbr_wiring_emotor_a", ["sbr_body", "sbr_front_motor"]],
      { "deformGroup": "" }
    ],
    "nodes": [
      ["id", "posX", "posY", "posZ"],
      { "frictionCoef": 0.5 },
      { "nodeMaterial": "|NM_METAL" },
      { "selfCollision": false },
      { "collision": true },
      { "group": "sbr_front_motor" },
      { "engineGroup": "front_motor_block" },
      { "nodeWeight": 7.75 },
      {
        "chemEnergy": 1000,
        "burnRate": 0.39,
        "flashPoint": 800,
        "specHeat": 0.2,
        "selfIgnitionCoef": false,
        "smokePoint": 650,
        "baseTemp": "thermals",
        "conductionRadius": 0.15
      },
      ["fem1r", -0.16, -1.48, 0.2],
      ["fem1l", 0.16, -1.48, 0.2],
      ["fem2r", -0.16, -1.32, 0.2],
      ["fem2l", 0.16, -1.32, 0.2],
      {
        "chemEnergy": false,
        "burnRate": false,
        "flashPoint": false,
        "specHeat": false,
        "selfIgnitionCoef": false,
        "smokePoint": false,
        "baseTemp": false,
        "conductionRadius": false
      },
      { "engineGroup": ["front_motor_block", "front_motor_waterdamage"] },
      ["fem3r", -0.16, -1.48, 0.35],
      ["fem3l", 0.16, -1.48, 0.35],
      ["fem4r", -0.16, -1.32, 0.35],
      ["fem4l", 0.16, -1.32, 0.35],
      { "engineGroup": "" },
      { "nodeWeight": 3.5 },
      ["fem5r", -0.21, -1.33, 0.335],
      ["fem5l", 0.21, -1.33, 0.335],
      { "group": ["sbr_front_motor", "sbr_frontdiff"] },
      ["fem6", 0.0, -1.53, 0.235],
      { "group": "" }
    ],
    "beams": [
      ["id1:", "id2:"],
      { "beamType": "|NORMAL", "beamLongBound": 1.0, "beamShortBound": 1.0 },
      { "beamSpring": 3751000, "beamDamp": 250 },
      { "beamDeform": 100000, "beamStrength": "FLT_MAX" },
      //engine
      ["fem1r", "fem1l"],
      ["fem2r", "fem2l"],
      ["fem3r", "fem3l"],
      ["fem4r", "fem4l"],

      ["fem1r", "fem2r"],
      ["fem1l", "fem2l"],
      ["fem3r", "fem4r"],
      ["fem3l", "fem4l"],

      ["fem1r", "fem3r"],
      ["fem1l", "fem3l"],
      ["fem2r", "fem4r"],
      ["fem2l", "fem4l"],

      ["fem2r", "fem3r"],
      ["fem2l", "fem3l"],
      ["fem2r", "fem3l"],
      ["fem2l", "fem3r"],

      ["fem1r", "fem4r"],
      ["fem1l", "fem4l"],
      ["fem1r", "fem4l"],
      ["fem1l", "fem4r"],

      ["fem1r", "fem2l"],
      ["fem1l", "fem2r"],
      ["fem3r", "fem4l"],
      ["fem3l", "fem4r"],

      ["fem1r", "fem3l"],
      ["fem1l", "fem3r"],
      ["fem2r", "fem4l"],
      ["fem2l", "fem4r"],

      { "beamSpring": 2501000, "beamDamp": 100 },
      ["fem6", "fem1l"],
      ["fem6", "fem1r"],
      ["fem6", "fem3r"],
      ["fem6", "fem3l"],
      ["fem6", "fem2l"],
      ["fem6", "fem2r"],
      ["fem6", "fem4r"],
      ["fem6", "fem4l"],

      ["fem5l", "fem4l"],
      ["fem5l", "fem2l"],
      ["fem5l", "fem4r"],
      ["fem5l", "fem2r"],
      ["fem5l", "fem3l"],
      ["fem5l", "fem1l"],

      ["fem5r", "fem4r"],
      ["fem5r", "fem2r"],
      ["fem5r", "fem4l"],
      ["fem5r", "fem2l"],
      ["fem5r", "fem3r"],
      ["fem5r", "fem1r"],

      //motor mounts
      { "beamSpring": 2501000, "beamDamp": 500 },
      { "beamDeform": 17000, "beamStrength": 90000 },
      ["fem5r", "fx3r", { "name": "front_motor", "beamStrength": 50000, "disableMeshBreaking": true, "disableTriangleBreaking": true }],
      ["fem5r", "fx2r"],
      ["fem5r", "fx1r"],
      ["fem5r", "f10rr"],
      ["fem5r", "f12rr"],
      ["fem5r", "fx5r"],

      ["fem5l", "fx3l", { "name": "front_motor", "beamStrength": 50000, "disableMeshBreaking": true, "disableTriangleBreaking": true }],
      ["fem5l", "fx2l"],
      ["fem5l", "fx1l"],
      ["fem5l", "f10ll"],
      ["fem5l", "f12ll"],
      ["fem5l", "fx5l"],

      ["fem6", "fx3r"],
      ["fem6", "fx3l"],
      ["fem6", "fx1r"],
      ["fem6", "fx1l"],
      ["fem6", "fx2l"],
      ["fem6", "fx2r"],

      { "beamSpring": 501000, "beamDamp": 2500 },
      { "beamDeform": 14000, "beamStrength": "FLT_MAX" },
      ["fem3r", "f1r"],
      ["fem3r", "fx1l"],
      ["fem3l", "f1l"],
      ["fem3l", "fx1r"],

      //break wiring
      { "beamSpring": 14100, "beamDamp": 0 },
      { "beamDeform": 1315.85, "beamStrength": 3096.05 },
      { "breakGroup": "wiring_emotor_a" },
      { "deformGroup": "wiring_emotor_a_break" },
      { "optional": true },
      ["fem3l", "f5r"],
      ["fem3r", "f5l"],
      { "optional": false },
      { "deformGroup": "" },
      { "breakGroup": "" }
    ]
  },
  "sbr_finaldrive_electric_R_venom": {
    "information": {
      "authors": "BeamNG",
      "name": "Venom Adjustable Rear Final Drive",
      "value": 1750
    },

    "slotType": "sbr_finaldrive_electric_R",

    "variables": [
      ["name", "type", "unit", "category", "default", "min", "max", "title", "description"],
      ["$finaldrive_R", "range", ":1", "Differentials", 7.6, 1.0, 10.0, "Final Drive Gear Ratio", "Torque multiplication ratio", { "subCategory": "Rear", "stepDis": 0.01 }]
    ],

    "differential_R": {
      "gearRatio": "$finaldrive_R"
    }
  },
  "sbr_finaldrive_electric_F_venom": {
    "information": {
      "authors": "Venom",
      "name": "Venom Adjustable Front Final Drive",
      "value": 1750
    },

    "slotType": "sbr_finaldrive_electric_F",

    "variables": [
      ["name", "type", "unit", "category", "default", "min", "max", "title", "description"],
      ["$finaldrive_F", "range", ":1", "Differentials", 7.6, 1.0, 10.0, "Final Drive Gear Ratio", "Torque multiplication ratio", { "subCategory": "Front", "stepDis": 0.01 }]
    ],

    "torsionReactorF": {
      "gearRatio": "$finaldrive_F"
    }
  },
  "sbr_battery_venom": {
    "information": {
      "authors": "Venom",
      "name": "Venom Solid State Battery",
      "value": 98990
    },
    "slotType": "sbr_fueltank",
    "flexbodies": [
      ["mesh", "[group]:", "nonFlexMaterials"],
      ["sbr_battery", ["sbr_body"]],
      ["sbr_battery_R", ["sbr_body"]],
      ["sbr_battery_L", ["sbr_body"]],
      ["sbr_battery_coolinglines_a", ["sbr_body"]],
      ["sbr_battery_coolinglines_b", ["sbr_body"]],
      ["sbr_radiators_small", ["sbr_body"]],
      ["sbr_wiring_battery_RL", ["sbr_body"]]
    ],
    "energyStorage": [
      ["type", "name"],
      ["electricBattery", "mainBattery"]
    ],
    "variables": [
      ["name", "type", "unit", "category", "default", "min", "max", "title", "description"],
      ["$fuel", "range", "kWh", "Chassis", 750, 0, 1000, "Battery Level", "Initial battery charge", { "stepDis": 0.5 }]
    ],
    "mainBattery": {
      "energyType": "electricEnergy",
      "batteryCapacity": 1000,
      "startingCapacity": "$fuel"
    },
    "nodes": [
      ["id", "posX", "posY", "posZ"],
      { "frictionCoef": 0.5 },
      { "nodeMaterial": "|NM_METAL" },
      { "selfCollision": false },
      { "collision": false },
      //{"group":"tunnel_battery"},
      { "engineGroup": "tunnel_battery" },
      {
        "chemEnergy": 1000,
        "burnRate": 0.39,
        "flashPoint": 800,
        "specHeat": 0.2,
        "selfIgnitionCoef": false,
        "smokePoint": 650,
        "baseTemp": "thermals",
        "conductionRadius": 0.15
      },
      { "nodeWeight": 50 },
      ["btt1", 0.0, -0.85, 0.25],
      ["btt2", 0.0, -0.05, 0.25],
      ["btt3", 0.0, 0.75, 0.25],
      { "nodeWeight": 25 },
      ["btt4l", 0.45, 0.75, 0.27],
      ["btt4r", -0.45, 0.75, 0.27],
      {
        "chemEnergy": false,
        "burnRate": false,
        "flashPoint": false,
        "specHeat": false,
        "selfIgnitionCoef": false,
        "smokePoint": false,
        "baseTemp": false,
        "conductionRadius": false
      },
      { "engineGroup": "" },
      { "group": "" }
    ],
    "beams": [
      ["id1:", "id2:"],
      { "beamType": "|NORMAL", "beamLongBound": 1.0, "beamShortBound": 1.0 },
      { "beamSpring": 501000, "beamDamp": 150 },
      { "beamDeform": 5000, "beamStrength": "FLT_MAX" },
      ["btt1", "f1l"],
      ["btt1", "f1r"],
      ["btt1", "f5r"],
      ["btt1", "f5l"],
      ["btt1", "f2l"],
      ["btt1", "f2r"],

      ["btt2", "f2r"],
      ["btt2", "f7r"],
      ["btt2", "f2l"],
      ["btt2", "f7l"],
      ["btt2", "f1l"],
      ["btt2", "f1r"],
      ["btt2", "f3l"],
      ["btt2", "f3r"],

      ["btt3", "f4r"],
      ["btt3", "f9r"],
      ["btt3", "f4l"],
      ["btt3", "f9l"],
      ["btt3", "f3l"],
      ["btt3", "f3r"],

      ["btt4l", "f4l"],
      ["btt4l", "f3l"],
      ["btt4l", "f8l"],
      ["btt4l", "f9l"],
      ["btt4l", "btt3"],

      ["btt4r", "f4r"],
      ["btt4r", "f3r"],
      ["btt4r", "f8r"],
      ["btt4r", "f9r"],
      ["btt4r", "btt3"],
      { "beamPrecompression": 1, "beamType": "|NORMAL", "beamLongBound": 1.0, "beamShortBound": 1.0 }
    ]
  }
}
