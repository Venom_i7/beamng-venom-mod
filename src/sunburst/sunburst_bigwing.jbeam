{
"sunburst_bigwing_01a": {
    "information":{
        "authors":"BeamNG",
        "name":"Nomi GTT Wing",
        "value":2600,
    },
    "slotType" : "sunburst_spoiler",
    "flexbodies": [
         ["mesh", "[group]:", "nonFlexMaterials"],
         ["bigwing_01a_a", ["sunburst_wing_a"],[],{"pos":{"x":0,"y":0,"z":0.0}}],
         ["bigwing_01a_b", ["sunburst_wing_b"],[],{"pos":{"x":0,"y":0,"z":0.0}}],
         ["bigwing_01a_c", ["sunburst_wing_c"],[],{"pos":{"x":0,"y":0,"z":0.0}}],
         ["bigwing_01a_d", ["sunburst_trunk"],[],{"pos":{"x":0,"y":0.0,"z":0.0}}],
    ],
    "trunkCoupler":{
        "$+openForceMagnitude":  40,
        "$+openForceDuration":   0,
        "$+closeForceMagnitude": 10,
        "$+closeForceDuration":  0,
    }
    "nodes": [
         ["id", "posX", "posY", "posZ"],
         //--TRUNK--
         {"nodeMaterial":"|NM_PLASTIC"},
         {"frictionCoef":0.5},
         {"group":"sunburst_wing_b"},
         {"collision":true},
         {"nodeWeight":0.30},
         {"selfCollision":true},
         //left wing support lower
         ["sp1l", 0.54, 1.87, 0.95],
         ["sp2l", 0.54, 2.035, 0.94],

         //left wing support upper
         ["sp3l", 0.54, 1.92, 1.15],
         ["sp4l", 0.54, 2.085, 1.215],

         {"group":"sunburst_wing_c"},
         //right wing support lower
         ["sp1r", -0.54, 1.87, 0.95],
         ["sp2r", -0.54, 2.035, 0.94],

         //right wing support upper
         ["sp3r", -0.54, 1.92, 1.15],
         ["sp4r", -0.54, 2.085, 1.215],

         //wing
         {"nodeWeight":0.2},
         {"group":"sunburst_wing_a"},
         ["wing1rr", -0.82, 1.82, 1.27],
         ["wing1r", -0.366, 1.85, 1.22],
         ["wing1", 0.0, 1.85, 1.22],
         ["wing1l", 0.366, 1.85, 1.22],
         ["wing1ll", 0.82, 1.82, 1.27],

         ["wing2ll2", 0.82, 2.16, 1.29, {"group":"","selfCollision":false,"collision":false}],
         ["wing2ll", 0.82, 2.16, 1.29],
         ["wing2l", 0.360, 2.16, 1.29],
         ["wing2", 0.0, 2.16, 1.29],
         ["wing2r", -0.360, 2.16, 1.29],
         ["wing2rr", -0.82, 2.16, 1.29],
         ["wing2rr2", -0.82, 2.16, 1.29, {"group":"","selfCollision":false,"collision":false}],

         ["wing3rr2", -0.82, 1.82, 1.11, {"group":"","selfCollision":false,"collision":false}],
         ["wing3rr", -0.82, 1.82, 1.11],
         ["wing3ll", 0.82, 1.82, 1.11],
         ["wing3ll2", 0.82, 1.82, 1.11, {"group":"","selfCollision":false,"collision":false}],

         ["wing4rr", -0.82, 2.16, 1.15],
         ["wing4ll", 0.82, 2.16, 1.15],

         {"collision":false},
         {"selfCollision":false},
         {"nodeWeight":0.30},
         {"group":""},
         ["wing0", 0, 2.0, 1.05],
         {"group":""},
    ],
    "beams": [
          ["id1:", "id2:"],
          {"beamPrecompression":1, "beamType":"|NORMAL", "beamLongBound":1.0, "beamShortBound":1.0},
          //wing reinf
          {"beamSpring":101000,"beamDamp":25},
          {"beamDeform":2000,"beamStrength":"FLT_MAX"},
          ["wing2", "wing0"],
          ["wing1", "wing0"],
          ["wing2l", "wing0"],
          ["wing1l", "wing0"],
          ["wing2r", "wing0"],
          ["wing1r", "wing0"],
          ["wing1rr", "wing0"],
          ["wing2rr2", "wing0"],
          ["wing1ll", "wing0"],
          ["wing2ll", "wing0"],

          //left wing support
          {"beamSpring":401000,"beamDamp":25},
          {"beamDeform":10000,"beamStrength":"FLT_MAX"},
          ["sp1l","sp2l"],
          ["sp1l","sp3l"],
          ["sp1l","sp4l"],
          ["sp2l","sp3l"],
          ["sp2l","sp4l"],
          ["sp3l","sp4l"],

          //right wing support
          ["sp1r","sp2r"],
          ["sp1r","sp3r"],
          ["sp1r","sp4r"],
          ["sp2r","sp3r"],
          ["sp2r","sp4r"],
          ["sp3r","sp4r"],

          //left wing
          {"beamSpring":201000,"beamDamp":25},
          {"beamDeform":5000,"beamStrength":25000},
          ["wing1l","wing1"],
          ["wing1l","wing2"],
          ["wing1l","wing2l"],
          ["wing1l","wing1ll"],
          ["wing1l","wing2ll"],
          ["wing1l","wing3ll"],
          ["wing1l","wing4ll"],

          ["wing2l","wing1ll"],
          ["wing2l","wing2ll"],
          ["wing2l","wing3ll"],
          ["wing2l","wing4ll"],

          //side plate
          ["wing1ll","wing2ll2"],
          ["wing1ll","wing3ll2"],
          ["wing1ll","wing4ll"],

          ["wing2ll2","wing3ll2"],
          ["wing2ll2","wing4ll"],

          ["wing3ll2","wing4ll"],

          //right wing
          ["wing1r","wing1"],
          ["wing1r","wing2"],
          ["wing1r","wing2r"],
          ["wing1r","wing1rr"],
          ["wing1r","wing2rr"],
          ["wing1r","wing3rr"],
          ["wing1r","wing4rr"],

          ["wing2r","wing1rr"],
          ["wing2r","wing2rr"],
          ["wing2r","wing3rr"],
          ["wing2r","wing4rr"],

          //side plate
          ["wing1rr","wing2rr2"],
          ["wing1rr","wing3rr2"],
          ["wing1rr","wing4rr"],

          ["wing2rr2","wing3rr2"],
          ["wing2rr2","wing4rr"],

          ["wing3rr2","wing4rr"],

          //0 length beam to prevent aero normals from being bent
          {"beamSpring":51000,"beamDamp":25},
          {"beamDeform":"FLT_MAX","beamStrength":"FLT_MAX"},
          ["wing2ll","wing2ll2", {"optional":true}],
          ["wing3ll","wing3ll2", {"optional":true}],

          ["wing2rr","wing2rr2", {"optional":true}],
          ["wing3rr","wing3rr2", {"optional":true}],

          //cross-wing stiffeners
          {"beamDeform":1000,"beamStrength":25000},
          {"beamSpring":51000,"beamDamp":25},
          ["wing3rr","wing3ll"],
          ["wing3rr","wing4ll"],

          ["wing4rr","wing3ll"],
          ["wing4rr","wing4ll"],

          //rigidifiers
          {"beamSpring":101000,"beamDamp":25},
          ["wing1","wing2"],
          ["wing1","wing2l"],
          ["wing1","wing1ll"],
          ["wing1","wing2ll"],
          ["wing1","wing3ll"],
          ["wing1","wing4ll"],

          ["wing2","wing2l"],
          ["wing2","wing1ll"],
          ["wing2","wing2ll"],
          ["wing2","wing3ll"],
          ["wing2","wing4ll"],

          ["wing1","wing2r"],
          ["wing1","wing1rr"],
          ["wing1","wing2rr"],
          ["wing1","wing3rr"],
          ["wing1","wing4rr"],

          ["wing2","wing2r"],
          ["wing2","wing1rr"],
          ["wing2","wing2rr"],
          ["wing2","wing3rr"],
          ["wing2","wing4rr"],

          //left support trunk attach
          {"breakGroup":"spoiler_trunk_L"},
          {"beamSpring":401000,"beamDamp":25},
          {"beamDeform":4500,"beamStrength":7600},
          ["sp1l","t2ll"],
          ["sp1l","t3ll"],
          ["sp1l","t3l"],
          ["sp1l","t2l"],
          ["sp1l","t5"],

          ["sp2l","t2ll"],
          ["sp2l","t3ll"],
          ["sp2l","t3l"],
          ["sp2l","t2l"],
          ["sp2l","t5"],

          //right support trunk attach
          {"breakGroup":"spoiler_trunk_R"},
          ["sp1r","t2rr"],
          ["sp1r","t3rr"],
          ["sp1r","t3r"],
          ["sp1r","t2r"],
          ["sp1r","t5"],

          ["sp2r","t2rr"],
          ["sp2r","t3rr"],
          ["sp2r","t3r"],
          ["sp2r","t2r"],
          ["sp2r","t5"],

          ["sp2r", "t4r"],
          ["sp1r", "t4r"],

          //left support trunk rigidifiers
          {"beamSpring":151000,"beamDamp":25},
          {"beamDeform":4500,"beamStrength":7600},
          {"breakGroup":"spoiler_trunk_L"},
          ["sp3l","t2ll"],
          ["sp3l","t3ll"],
          ["sp3l","t3l"],
          ["sp3l","t2l"],

          ["sp4l","t2ll"],
          ["sp4l","t3ll"],
          ["sp4l","t3l"],
          ["sp4l","t2l"],

          //right support trunk rigidifiers
          {"breakGroup":"spoiler_trunk_R"},
          ["sp3r","t2rr"],
          ["sp3r","t3rr"],
          ["sp3r","t3r"],
          ["sp3r","t2r"],

          ["sp4r","t2rr"],
          ["sp4r","t3rr"],
          ["sp4r","t3r"],
          ["sp4r","t2r"],

          //left support wing attach
          {"beamSpring":201000,"beamDamp":25},
          {"beamDeform":3800,"beamStrength":5600},
          {"breakGroup":"spoiler_L"},
          ["sp3l","wing1l"],
          ["sp3l","wing2l"],
          ["sp3l","wing1ll"],
          ["sp3l","wing2ll"],

          ["sp4l","wing1l"],
          ["sp4l","wing2l"],
          ["sp4l","wing1ll"],
          ["sp4l","wing2ll"],

          ["sp1l","wing1l"],
          ["sp1l","wing2l"],
          ["sp1l","wing1ll"],
          ["sp1l","wing2ll"],

          ["sp2l","wing1l"],
          ["sp2l","wing2l"],
          ["sp2l","wing1ll"],
          ["sp2l","wing2ll"],

          ["sp3l","wing0"],
          ["sp4l","wing0"],

          //["sp3l","wing1"],
          //["sp3l","wing2"],
          //["sp4l","wing1"],
          //["sp4l","wing2"],

          //right support wing attach
          {"breakGroup":"spoiler_R"},
          ["sp3r","wing1r"],
          ["sp3r","wing2r"],
          ["sp3r","wing1rr"],
          ["sp3r","wing2rr"],

          ["sp4r","wing1r"],
          ["sp4r","wing2r"],
          ["sp4r","wing1rr"],
          ["sp4r","wing2rr"],

          ["sp1r","wing1r"],
          ["sp1r","wing2r"],
          ["sp1r","wing1rr"],
          ["sp1r","wing2rr"],

          ["sp2r","wing1r"],
          ["sp2r","wing2r"],
          ["sp2r","wing1rr"],
          ["sp2r","wing2rr"],

          ["sp3r","wing0"],
          ["sp4r","wing0"],

          //["sp3r","wing1"],
          //["sp3r","wing2"],
          //["sp4r","wing1"],
          //["sp4r","wing2"],
          {"breakGroup":""},

          //damp help
          {"beamSpring":0,"beamDamp":25},
          {"breakGroup":["spoiler_L","spoiler_trunk_L"]},
          {"breakGroupType":1},
          ["wing4rr", "t1rr"],
          ["wing1rr", "t3rr"],
          {"breakGroup":["spoiler_R","spoiler_trunk_R"]},
          ["wing4ll", "t1ll"],
          ["wing1ll", "t3ll"],
          {"breakGroupType":0},
          {"breakGroup":""},
    ],
    "triangles": [
        ["id1:","id2:","id3:"],
        {"dragCoef":30},
        {"groundModel":"plastic"},
        ["wing3ll","wing2ll","wing1l", {"liftCoef":90, "stallAngle":0.4}],
        ["wing2ll","wing2l","wing1l", {"liftCoef":90, "stallAngle":0.4}],

        ["wing1l","wing2l","wing2", {"liftCoef":90, "stallAngle":0.4}],
        ["wing1l","wing2","wing1", {"liftCoef":90, "stallAngle":0.4}],

        ["wing1","wing2","wing1r", {"liftCoef":90, "stallAngle":0.4}],
        ["wing2","wing2r","wing1r", {"liftCoef":90, "stallAngle":0.4}],

        ["wing1r","wing2r","wing2rr", {"liftCoef":90, "stallAngle":0.4}],
        ["wing1r","wing2rr","wing3rr", {"liftCoef":90, "stallAngle":0.4}],

        //side
        {"dragCoef":30},
        ["wing2ll2","wing1ll","wing3ll2"],
        ["wing2ll2","wing3ll2","wing4ll"],
        ["wing2rr2","wing3rr2","wing1rr"],
        ["wing2rr2","wing4rr","wing3rr2"],

        //supports
        {"dragCoef":5},
        ["sp3l", "sp1l", "sp2l"],
        ["sp2l", "sp4l", "sp3l"],

        ["sp1r", "sp3r", "sp2r"],
        ["sp4r", "sp2r", "sp3r"],
    ],
},
"sunburst_bigwing_02a": {
    "information":{
        "authors":"BeamNG",
        "name":"Nomi GTRX Wing",
        "value":3900,
    },
    "slotType" : "sunburst_spoiler",
    "flexbodies": [
         ["mesh", "[group]:", "nonFlexMaterials"],
         ["bigwing_02a_a", ["sunburst_wing_a"],[],{"pos":{"x":0,"y":0,"z":0.0}}],
         ["bigwing_02a_b", ["sunburst_wing_b"],[],{"pos":{"x":0,"y":0,"z":0.0}}],
         ["bigwing_02a_c", ["sunburst_wing_c"],[],{"pos":{"x":0,"y":0,"z":0.0}}],
         ["bigwing_02a_d", ["sunburst_trunk"],[],{"pos":{"x":0,"y":0.0,"z":0.0}}],
    ],
    "trunkCoupler":{
        "$+openForceMagnitude":  65,
        "$+openForceDuration":   0,
        "$+closeForceMagnitude": 20,
        "$+closeForceDuration":  0,
    }
    "nodes": [
         ["id", "posX", "posY", "posZ"],
         //--TRUNK--
         {"nodeMaterial":"|NM_PLASTIC"},
         {"frictionCoef":0.5},
         {"group":"sunburst_wing_b"},
         {"collision":true},
         {"nodeWeight":0.48},
         {"selfCollision":true},
         //left wing support lower
         ["sp1l", 0.30, 1.68, 0.95],
         ["sp2l", 0.30, 1.92, 0.92],

         //left wing support upper
         ["sp3l", 0.30, 1.88, 1.27],
         ["sp4l", 0.30, 2.13, 1.31],

         {"group":"sunburst_wing_c"},
         //right wing support lower
         ["sp1r", -0.30, 1.68, 0.95],
         ["sp2r", -0.30, 1.92, 0.92],

         //right wing support upper
         ["sp3r", -0.30, 1.88, 1.27],
         ["sp4r", -0.30, 2.13, 1.31],

         //wing
         {"nodeWeight":0.3},
         {"group":"sunburst_wing_a"},
         ["wing1rr", -0.87, 1.80, 1.3],
         ["wing1r", -0.366, 1.80, 1.3],
         ["wing1", 0.0, 1.80, 1.3],
         ["wing1l", 0.366, 1.80, 1.3],
         ["wing1ll", 0.87, 1.80, 1.3],

         ["wing2ll2", 0.87, 2.30, 1.48, {"group":"","selfCollision":false,"collision":false}],
         ["wing2ll", 0.87, 2.30, 1.48, {"selfCollision":false}],
         ["wing2l", 0.360, 2.30, 1.48],
         ["wing2", 0.0, 2.30, 1.48],
         ["wing2r", -0.360, 2.30, 1.48],
         ["wing2rr", -0.87, 2.30, 1.48, {"selfCollision":false}],
         ["wing2rr2", -0.87, 2.30, 1.48, {"group":"","selfCollision":false,"collision":false}],

         ["wing3rr2", -0.87, 1.80, 1.48, {"group":"","selfCollision":false,"collision":false}],
         ["wing3rr", -0.87, 1.80, 1.48, {"selfCollision":false}],
         ["wing3ll", 0.87, 1.80, 1.48, {"selfCollision":false}],
         ["wing3ll2", 0.87, 1.80, 1.48, {"group":"","selfCollision":false,"collision":false}],

         ["wing4rr", -0.87, 2.30, 1.25],
         ["wing4ll", 0.87, 2.30, 1.25],

         {"collision":false},
         {"selfCollision":false},
         {"nodeWeight":0.35},
         {"group":""},
         ["wing0", 0, 2.0, 1.15],
         {"group":""},
    ],
    "beams": [
          ["id1:", "id2:"],
          {"beamPrecompression":1, "beamType":"|NORMAL", "beamLongBound":1.0, "beamShortBound":1.0},
          //wing reinf
          {"beamSpring":101000,"beamDamp":50},
          {"beamDeform":2500,"beamStrength":"FLT_MAX"},
          ["wing2", "wing0"],
          ["wing1", "wing0"],
          ["wing2l", "wing0"],
          ["wing1l", "wing0"],
          ["wing2r", "wing0"],
          ["wing1r", "wing0"],
          ["wing1rr", "wing0"],
          ["wing2rr2", "wing0"],
          ["wing1ll", "wing0"],
          ["wing2ll", "wing0"],

          //left wing support
          {"beamSpring":701000,"beamDamp":25},
          {"beamDeform":10000,"beamStrength":"FLT_MAX"},
          ["sp1l","sp2l"],
          ["sp1l","sp3l"],
          ["sp1l","sp4l"],
          ["sp2l","sp3l"],
          ["sp2l","sp4l"],
          ["sp3l","sp4l"],

          //right wing support
          ["sp1r","sp2r"],
          ["sp1r","sp3r"],
          ["sp1r","sp4r"],
          ["sp2r","sp3r"],
          ["sp2r","sp4r"],
          ["sp3r","sp4r"],

          //left wing
          {"beamSpring":251000,"beamDamp":25},
          {"beamDeform":9000,"beamStrength":29000},
          ["wing1l","wing1"],
          ["wing1l","wing2"],
          ["wing1l","wing2l"],
          ["wing1l","wing1ll"],
          ["wing1l","wing2ll"],
          ["wing1l","wing3ll"],
          ["wing1l","wing4ll"],

          ["wing1","wing2"],
          ["wing1","wing2l"],
          ["wing2","wing2l"],

          ["wing2l","wing1ll"],
          ["wing2l","wing2ll"],
          ["wing2l","wing3ll"],
          ["wing2l","wing4ll"],

          //side plate
          ["wing1ll","wing2ll2"],
          ["wing1ll","wing3ll2"],
          ["wing1ll","wing4ll"],

          ["wing2ll2","wing3ll2"],
          ["wing2ll2","wing4ll"],

          ["wing3ll2","wing4ll"],

          //right wing
          ["wing1r","wing1"],
          ["wing1r","wing2"],
          ["wing1r","wing2r"],
          ["wing1r","wing1rr"],
          ["wing1r","wing2rr"],
          ["wing1r","wing3rr"],
          ["wing1r","wing4rr"],

          ["wing1","wing2r"],
          ["wing2","wing2r"],

          ["wing2r","wing1rr"],
          ["wing2r","wing2rr"],
          ["wing2r","wing3rr"],
          ["wing2r","wing4rr"],

          //side plate
          ["wing1rr","wing2rr2"],
          ["wing1rr","wing3rr2"],
          ["wing1rr","wing4rr"],

          ["wing2rr2","wing3rr2"],
          ["wing2rr2","wing4rr"],

          ["wing3rr2","wing4rr"],

          //0 length beam to prevent aero normals from being bent
          {"beamSpring":101000,"beamDamp":25},
          {"beamDeform":"FLT_MAX","beamStrength":"FLT_MAX"},
          ["wing2ll","wing2ll2", {"optional":true}],
          ["wing3ll","wing3ll2", {"optional":true}],

          ["wing2rr","wing2rr2", {"optional":true}],
          ["wing3rr","wing3rr2", {"optional":true}],

          //cross-wing stiffeners
          {"beamDeform":2000,"beamStrength":30000},
          {"beamSpring":151000,"beamDamp":25},
          ["wing3rr","wing3ll"],
          ["wing3rr","wing4ll"],

          ["wing4rr","wing3ll"],
          ["wing4rr","wing4ll"],

          ["wing1","wing1ll"],
          ["wing1","wing2ll"],
          ["wing1","wing3ll"],
          ["wing1","wing4ll"],

          ["wing2","wing1ll"],
          ["wing2","wing2ll"],
          ["wing2","wing3ll"],
          ["wing2","wing4ll"],

          ["wing1","wing1rr"],
          ["wing1","wing2rr"],
          ["wing1","wing3rr"],
          ["wing1","wing4rr"],

          ["wing2","wing1rr"],
          ["wing2","wing2rr"],
          ["wing2","wing3rr"],
          ["wing2","wing4rr"],

          //left support trunk attach
          {"breakGroup":"spoiler_trunk_L"},
          {"beamSpring":451000,"beamDamp":35},
          {"beamDeform":5000,"beamStrength":8000},
          ["sp1l","t2ll"],
          ["sp1l","t3ll"],
          ["sp1l","t3l"],
          ["sp1l","t2l"],
          ["sp1l","t5"],

          ["sp2l","t2ll"],
          ["sp2l","t3ll"],
          ["sp2l","t3l"],
          ["sp2l","t2l"],
          ["sp2l","t5"],

          ["sp2l", "t4l"],
          ["sp1l", "t4l"],

          //right support trunk attach
          {"breakGroup":"spoiler_trunk_R"},
          ["sp1r","t2rr"],
          ["sp1r","t3rr"],
          ["sp1r","t3r"],
          ["sp1r","t2r"],
          ["sp1r","t5"],

          ["sp2r","t2rr"],
          ["sp2r","t3rr"],
          ["sp2r","t3r"],
          ["sp2r","t2r"],
          ["sp2r","t5"],

          ["sp2r", "t4r"],
          ["sp1r", "t4r"],

          //left support trunk rigidifiers
          {"beamSpring":151000,"beamDamp":35},
          {"beamDeform":4800,"beamStrength":7600},
          {"breakGroup":"spoiler_trunk_L"},
          ["sp3l","t1ll"],
          ["sp3l","t3ll"],
          ["sp3l","t3l"],
          ["sp3l","t2l"],

          ["sp4l","t1ll"],
          ["sp4l","t3ll"],
          ["sp4l","t3l"],
          ["sp4l","t2l"],

          //right support trunk rigidifiers
          {"breakGroup":"spoiler_trunk_R"},
          ["sp3r","t1rr"],
          ["sp3r","t3rr"],
          ["sp3r","t3r"],
          ["sp3r","t2r"],

          ["sp4r","t1rr"],
          ["sp4r","t3rr"],
          ["sp4r","t3r"],
          ["sp4r","t2r"],

          //left support wing attach
          {"beamSpring":501000,"beamDamp":25},
          {"beamDeform":6000,"beamStrength":6500},
          {"breakGroup":"spoiler_L"},
          ["sp3l","wing1l"],
          ["sp3l","wing2l"],
          ["sp3l","wing1ll"],
          ["sp3l","wing2ll"],

          ["sp4l","wing1l"],
          ["sp4l","wing2l"],
          ["sp4l","wing1ll"],
          ["sp4l","wing2ll"],

          ["sp1l","wing1l"],
          ["sp1l","wing2l"],
          ["sp1l","wing1ll"],
          ["sp1l","wing2ll"],

          ["sp2l","wing1l"],
          ["sp2l","wing2l"],
          ["sp2l","wing1ll"],
          ["sp2l","wing2ll"],

          ["sp3l","wing0"],
          ["sp4l","wing0"],

          //["sp3l","wing1"],
          //["sp3l","wing2"],
          //["sp4l","wing1"],
          //["sp4l","wing2"],

          //right support wing attach
          {"beamDeform":6000,"beamStrength":6500},
          {"breakGroup":"spoiler_R"},
          ["sp3r","wing1r"],
          ["sp3r","wing2r"],
          ["sp3r","wing1rr"],
          ["sp3r","wing2rr"],

          ["sp4r","wing1r"],
          ["sp4r","wing2r"],
          ["sp4r","wing1rr"],
          ["sp4r","wing2rr"],

          ["sp1r","wing1r"],
          ["sp1r","wing2r"],
          ["sp1r","wing1rr"],
          ["sp1r","wing2rr"],

          ["sp2r","wing1r"],
          ["sp2r","wing2r"],
          ["sp2r","wing1rr"],
          ["sp2r","wing2rr"],

          ["sp3r","wing0"],
          ["sp4r","wing0"],

          //["sp3r","wing1"],
          //["sp3r","wing2"],
          //["sp4r","wing1"],
          //["sp4r","wing2"],
          {"breakGroup":""},

          //damp help
          {"beamSpring":0,"beamDamp":25},
          {"breakGroup":["spoiler_L","spoiler_trunk_L"]},
          {"breakGroupType":1},
          ["wing4rr", "t1rr"],
          ["wing1rr", "t3rr"],
          {"breakGroup":["spoiler_R","spoiler_trunk_R"]},
          ["wing4ll", "t1ll"],
          ["wing1ll", "t3ll"],
          {"breakGroupType":0},
          {"breakGroup":""},
    ],
    "triangles": [
        ["id1:","id2:","id3:"],
        {"dragCoef":60},
        {"groundModel":"plastic"},
        ["wing1ll","wing2ll","wing1l", {"liftCoef":165, "stallAngle":0.4}],
        ["wing2ll","wing2l","wing1l", {"liftCoef":165, "stallAngle":0.4}],

        ["wing1l","wing2l","wing2", {"liftCoef":165, "stallAngle":0.4}],
        ["wing1l","wing2","wing1", {"liftCoef":165, "stallAngle":0.4}],

        ["wing1","wing2","wing1r", {"liftCoef":165, "stallAngle":0.4}],
        ["wing2","wing2r","wing1r", {"liftCoef":165, "stallAngle":0.4}],

        ["wing1r","wing2r","wing2rr", {"liftCoef":165, "stallAngle":0.4}],
        ["wing1r","wing2rr","wing1rr", {"liftCoef":165, "stallAngle":0.4}],

        //side
        {"dragCoef":40},
        ["wing2ll2","wing3ll2","wing1ll"],
        ["wing2ll2","wing1ll","wing4ll"],
        ["wing2rr2","wing1rr","wing3rr2"],
        ["wing2rr2","wing4rr","wing1rr"],

        //supports
        {"dragCoef":5},
        ["sp3l", "sp1l", "sp2l"],
        ["sp2l", "sp4l", "sp3l"],

        ["sp1r", "sp3r", "sp2r"],
        ["sp4r", "sp2r", "sp3r"],
    ],
},
"sunburst_bigwing_03a": {
    "information":{
        "authors":"BeamNG",
        "name":"Nomi GTC Adjustable Wing",
        "value":5650,
    },
    "slotType" : "sunburst_spoiler",
    "flexbodies": [
         ["mesh", "[group]:", "nonFlexMaterials"],
         ["bigwing_03a_a", ["sunburst_wing_a"],[],{"pos":{"x":0,"y":0.03,"z":0.04},"rot":{"x":-2,"y":0,"z":0}}],
         ["bigwing_03a_b", ["sunburst_wing_b"],[],{"pos":{"x":0,"y":0.03,"z":0.04},"rot":{"x":-2,"y":0,"z":0}}],
         ["bigwing_03a_c", ["sunburst_wing_c"],[],{"pos":{"x":0,"y":0.03,"z":0.04},"rot":{"x":-2,"y":0,"z":0}}],
         ["bigwing_03a_d", ["sunburst_wing_b"],[],{"pos":{"x":0,"y":0.03,"z":0.04},"rot":{"x":-2,"y":0,"z":0}}],
         ["bigwing_03a_e", ["sunburst_wing_c"],[],{"pos":{"x":0,"y":0.03,"z":0.04},"rot":{"x":-2,"y":0,"z":0}}],
         ["bigwing_03a_d_upper", ["sunburst_wing_b"],[],{"pos":{"x":0,"y":0.03,"z":0.04},"rot":{"x":-2,"y":0,"z":0}}],
         ["bigwing_03a_e_upper", ["sunburst_wing_c"],[],{"pos":{"x":0,"y":0.03,"z":0.04},"rot":{"x":-2,"y":0,"z":0}}],
    ],
    "variables": [
        ["name", "type", "unit", "category", "default", "min", "max", "title", "description"],
        ["$spoiler_angle_R", "range", "Degrees", "Chassis", 8, 4, 20, "Rear Wing Angle", "Angle of attack of the rear wing", {"stepDis":0.5}],
    ],
    "trunkCoupler":{
        "$+openForceMagnitude":  40,
        "$+openForceDuration":   0,
        "$+closeForceMagnitude": -10,
        "$+closeForceDuration":  0,
    }
    "nodes": [
         ["id", "posX", "posY", "posZ"],
         //--TRUNK--
         {"nodeMaterial":"|NM_PLASTIC"},
         {"frictionCoef":0.5},
         {"group":"sunburst_wing_b"},
         {"collision":true},
         {"nodeWeight":0.3},
         {"selfCollision":true},
         //left wing support lower
         ["sp1l", 0.2, 1.85, 0.92],
         ["sp2l", 0.2, 2.06, 0.88],

         //left wing support upper
         ["sp3l", 0.2, 1.89, 1.09],
         ["sp4l", 0.2, 2.065, 1.09],
         {"group":"sunburst_wing_c"},
         //right wing support lower
         ["sp1r", -0.2, 1.85, 0.92],
         ["sp2r", -0.2, 2.06, 0.88],

         //right wing support upper
         ["sp3r", -0.2, 1.89, 1.09],
         ["sp4r", -0.2, 2.065, 1.09],

         //wing
         {"nodeWeight":0.2},
         {"group":"sunburst_wing_a"},
         ["wing1rr2", -0.73, 1.82, 1.14, {"group":"","selfCollision":false,"collision":false}],
         ["wing1rr", -0.73, 1.82, 1.14],
         ["wing1r", -0.37, 1.82, 1.12],
         ["wing1", 0.0, 1.82, 1.12],
         ["wing1l", 0.37, 1.82, 1.12],
         ["wing1ll", 0.73, 1.82, 1.14],
         ["wing1ll2", 0.73, 1.82, 1.14, {"group":"","selfCollision":false,"collision":false}],

         ["wing2ll2", 0.73, 2.13, 1.18, {"group":"","selfCollision":false,"collision":false}],
         ["wing2ll", 0.73, 2.13, 1.18],
         ["wing2l", 0.37, 2.11, 1.16],
         ["wing2", 0.0, 2.11, 1.16],
         ["wing2r", -0.37, 2.11, 1.16],
         ["wing2rr", -0.73, 2.13, 1.18],
         ["wing2rr2", -0.73, 2.13, 1.18, {"group":"","selfCollision":false,"collision":false}],

         ["wing3rr", -0.73, 1.82, 1.045],
         ["wing3ll", 0.73, 1.82, 1.045],
         ["wing4rr", -0.73, 2.11, 1.03],
         ["wing4ll", 0.73, 2.11, 1.03],

         {"collision":false},
         {"selfCollision":false},
         {"nodeWeight":0.25},
         {"group":""},
         ["wing0", 0, 1.95, 0.9],
         {"group":""},
    ],
    "beams": [
          ["id1:", "id2:"],
          {"beamPrecompression":1, "beamType":"|NORMAL", "beamLongBound":1.0, "beamShortBound":1.0},
          //wing reinf
          {"beamSpring":141000,"beamDamp":25},
          {"beamDeform":1000,"beamStrength":"FLT_MAX"},
          ["wing2", "wing0"],
          ["wing1", "wing0"],
          ["wing2l", "wing0"],
          ["wing1l", "wing0"],
          ["wing2r", "wing0"],
          ["wing1r", "wing0"],
          ["wing1rr", "wing0"],
          ["wing2rr2", "wing0"],
          ["wing1ll", "wing0"],
          ["wing2ll", "wing0"],

          //left wing support
          {"beamSpring":351000,"beamDamp":20},
          {"beamDeform":10000,"beamStrength":"FLT_MAX"},
          ["sp1l","sp2l"],
          ["sp1l","sp3l"],
          ["sp2l","sp3l"],
          ["sp3l","sp4l"],

          //right wing support
          ["sp1r","sp2r"],
          ["sp1r","sp3r"],
          ["sp2r","sp3r"],
          ["sp3r","sp4r"],

          //cross
          {"beamSpring":201000,"beamDamp":20},
          ["sp3l","sp1r"],
          ["sp3r","sp1l"],

          ["sp4r","sp4l"],
          ["sp4r","sp3l"],
          ["sp3r","sp4l"],

          ["sp3l", "sp2r"],
          ["sp3r", "sp2l"],

          ["sp1r", "sp1l"],
          ["sp3r", "sp3l"],
          ["sp2l","sp2r"],

          ["sp2l", "sp1r"],
          ["sp2r", "sp1l"],

          //left wing
          {"beamSpring":151000,"beamDamp":20},
          {"beamDeform":4000,"beamStrength":25000},
          ["wing1l","wing1"],
          ["wing1l","wing2"],
          ["wing1l","wing2l"],
          ["wing1l","wing1ll"],
          ["wing1l","wing2ll"],
          ["wing1l","wing3ll"],
          ["wing1l","wing4ll"],

          ["wing1","wing2"],
          ["wing1","wing2l"],
          ["wing2","wing2l"],

          ["wing2l","wing1ll"],
          ["wing2l","wing2ll"],
          ["wing2l","wing3ll"],
          ["wing2l","wing4ll"],

          //side plate
          ["wing1ll2","wing2ll2"],
          ["wing1ll2","wing3ll"],
          ["wing1ll2","wing4ll"],

          ["wing2ll2","wing3ll"],
          ["wing2ll2","wing4ll"],

          ["wing3ll","wing4ll"],

          //right wing
          ["wing1r","wing1"],
          ["wing1r","wing2"],
          ["wing1r","wing2r"],
          ["wing1r","wing1rr"],
          ["wing1r","wing2rr"],
          ["wing1r","wing3rr"],
          ["wing1r","wing4rr"],

          ["wing1","wing2r"],
          ["wing2","wing2r"],
          ["wing2r","wing1rr"],
          ["wing2r","wing2rr"],
          ["wing2r","wing3rr"],
          ["wing2r","wing4rr"],

          //side plate
          ["wing1rr2","wing2rr2"],
          ["wing1rr2","wing3rr"],
          ["wing1rr2","wing4rr"],

          ["wing2rr2","wing3rr"],
          ["wing2rr2","wing4rr"],

          ["wing3rr","wing4rr"],

          //0 length beam to prevent aero normals from being bent
          {"beamSpring":51000,"beamDamp":25},
          {"beamDeform":"FLT_MAX","beamStrength":"FLT_MAX"},
          ["wing2ll","wing2ll2", {"optional":true}],
          ["wing1ll","wing1ll2", {"optional":true}],

          ["wing2rr","wing2rr2", {"optional":true}],
          ["wing1rr","wing1rr2", {"optional":true}],

          //cross-wing stiffeners
          {"beamDeform":1000,"beamStrength":21000},
          {"beamSpring":51000,"beamDamp":20},
          ["wing3rr","wing3ll"],
          ["wing3rr","wing4ll"],

          ["wing4rr","wing3ll"],
          ["wing4rr","wing4ll"],

          ["wing1","wing1ll"],
          ["wing1","wing2ll"],
          ["wing1","wing3ll"],
          ["wing1","wing4ll"],

          ["wing2","wing1ll"],
          ["wing2","wing2ll"],
          ["wing2","wing3ll"],
          ["wing2","wing4ll"],

          ["wing1","wing1rr"],
          ["wing1","wing2rr"],
          ["wing1","wing3rr"],
          ["wing1","wing4rr"],

          ["wing2","wing1rr"],
          ["wing2","wing2rr"],
          ["wing2","wing3rr"],
          ["wing2","wing4rr"],

          //left support trunk attach
          {"breakGroup":"spoiler_trunk_L"},
          {"beamSpring":501000,"beamDamp":25},
          {"beamDeform":2000,"beamStrength":5600},
          ["sp1l","t1"],
          ["sp1l","t3"],
          ["sp1l","t3l"],
          ["sp1l","t2l"],
          ["sp1l","t5"],

          ["sp2l","t1"],
          ["sp2l","t3"],
          ["sp2l","t3l"],
          ["sp2l","t2l"],
          ["sp2l","t5"],

          ["sp2l", "t4l"],
          ["sp2l", "t4"],
          ["sp1l", "t4l"],
          ["sp1l", "t4"],

          //right support trunk attach
          {"breakGroup":"spoiler_trunk_R"},
          ["sp1r","t1"],
          ["sp1r","t3"],
          ["sp1r","t3r"],
          ["sp1r","t2r"],
          ["sp1r","t5"],

          ["sp2r","t1"],
          ["sp2r","t3"],
          ["sp2r","t3r"],
          ["sp2r","t2r"],
          ["sp2r","t5"],

          ["sp2r", "t4r"],
          ["sp2r", "t4"],
          ["sp1r", "t4r"],
          ["sp1r", "t4"],

          //left support trunk rigidifiers
          {"beamSpring":101000,"beamDamp":40},
          {"beamDeform":2000,"beamStrength":5600},
          {"breakGroup":"spoiler_trunk_L"},
          ["sp3l","t1"],
          ["sp3l","t3"],
          ["sp3l","t3l"],
          ["sp3l","t2l"],

          //right support trunk rigidifiers
          {"breakGroup":"spoiler_trunk_R"},
          ["sp3r","t1"],
          ["sp3r","t3"],
          ["sp3r","t3r"],
          ["sp3r","t2r"],
          {"breakGroup":""},

          //left support wing attach
          {"beamSpring":301000,"beamDamp":25},
          {"beamDeform":5000,"beamStrength":"FLT_MAX"},
          //{"breakGroup":"spoiler_L"},
          ["sp3l","wing1l"],
          ["sp3l","wing2l"],
          ["sp3l","wing1ll"],
          ["sp3l","wing2ll"],

          ["sp4l","wing1l"],
          ["sp4l","wing2l"],
          ["sp4l","wing1ll"],
          ["sp4l","wing2ll"],

          ["sp3l","wing1"],
          ["sp3l","wing2"],
          ["sp4l","wing1"],
          ["sp4l","wing2"],

          //["sp3l","wing3ll"],
          //["sp3l","wing4ll"],
          //["sp4l","wing3ll"],
          //["sp4l","wing4ll"],

          ["sp3l","wing0"],
          ["sp4l","wing0"],
          //right support wing attach
          //{"breakGroup":"spoiler_R"},
          ["sp3r","wing1r"],
          ["sp3r","wing2r"],
          ["sp3r","wing1rr"],
          ["sp3r","wing2rr"],

          ["sp4r","wing1r"],
          ["sp4r","wing2r"],
          ["sp4r","wing1rr"],
          ["sp4r","wing2rr"],

          ["sp3r","wing1"],
          ["sp3r","wing2"],
          ["sp4r","wing1"],
          ["sp4r","wing2"],

          //["sp3r","wing3rr"],
          //["sp3r","wing4rr"],
          //["sp4r","wing3rr"],
          //["sp4r","wing4rr"],

          ["sp3r","wing0"],
          ["sp4r","wing0"],


          {"beamDeform":1500,"beamStrength":"FLT_MAX"},
          {"beamSpring":81000,"beamDamp":50},
            ["wing1ll", "sp1l"],
            ["wing1rr", "sp1r"],

          //damp help
          {"beamSpring":0,"beamDamp":45},
          {"breakGroup":["spoiler_L","spoiler_trunk_L"]},
          {"breakGroupType":1},
          ["wing4rr", "t1rr"],
          ["wing4rr", "t2rr"],
          ["wing3rr", "t2rr"],
          {"breakGroup":["spoiler_R","spoiler_trunk_R"]},
          ["wing4ll", "t1ll"],
          ["wing4ll", "t2ll"],
          ["wing3ll", "t2ll"],
          {"breakGroupType":0},
          {"breakGroup":""},

          //height-adjustable struts
          {"beamSpring":1001000,"beamDamp":30},
          {"beamDeform":5000,"beamStrength":25000},
          {"breakGroup":"spoiler_R"},
          {"disableMeshBreaking":true},
          ["sp2r","sp4r", {"beamPrecompression":"$=$spoiler_angle_R/64+0.875","beamPrecompressionTime":0.5}],
          {"breakGroup":"spoiler_L"},
          ["sp2l","sp4l", {"beamPrecompression":"$=$spoiler_angle_R/64+0.875","beamPrecompressionTime":0.5}],
          {"disableMeshBreaking":false},
          {"breakGroup":""},
    ],
    "triangles": [
        ["id1:","id2:","id3:"],
        {"dragCoef":50},
        {"groundModel":"plastic"},
        ["wing1ll","wing2ll","wing1l", {"liftCoef":120, "stallAngle":0.4}],
        ["wing2ll","wing2l","wing1l", {"liftCoef":120, "stallAngle":0.4}],

        ["wing1l","wing2l","wing2", {"liftCoef":120, "stallAngle":0.4}],
        ["wing1l","wing2","wing1", {"liftCoef":120, "stallAngle":0.4}],

        ["wing1","wing2","wing1r", {"liftCoef":120, "stallAngle":0.4}],
        ["wing2","wing2r","wing1r", {"liftCoef":120, "stallAngle":0.4}],

        ["wing1r","wing2r","wing2rr", {"liftCoef":120, "stallAngle":0.4}],
        ["wing1r","wing2rr","wing1rr", {"liftCoef":120, "stallAngle":0.4}],

        //side
        {"dragCoef":30},
        ["wing2ll2","wing1ll2","wing3ll"],
        ["wing2ll2","wing3ll","wing4ll"],
        ["wing2rr2","wing3rr","wing1rr2"],
        ["wing2rr2","wing4rr","wing3rr"],
    ],
},
}