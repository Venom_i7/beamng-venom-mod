{
"bolide_brake_F": {
    "information":{
        "authors":"BeamNG",
        "name":"Front Disc Brakes",
        "value":700,
    },
    "slotType" : "bolide_brake_F",
    "slots": [
        ["type", "default", "description"],
        ["brakepad_F","brakepad_F_sport", "Front Brake Pads", {"coreSlot":true}],
    ],
    "flexbodies": [
        ["mesh", "[group]:", "nonFlexMaterials"],
        //brake discs
        ["brake_disc_plain",          ["wheel_FR","wheelhub_FR"],    [],  {"pos":{"x":-0.775, "y":-1.28, "z":0.345}, "scale":{"x":0.9, "y":0.95, "z":0.95},  "rot":{"x":0,   "y":0,  "z":0  }}],
        ["brake_disc_plain",          ["wheel_FL","wheelhub_FL"],    [],  {"pos":{"x": 0.775, "y":-1.28, "z":0.345}, "scale":{"x":0.9, "y":0.95, "z":0.95},  "rot":{"x":180, "y":0,  "z":0  }}],
        ["brake_caliper_4pot_yellow", ["bolide_hub_F"], [], {"pos":{"x":-0.775, "y":-1.285, "z":0.345}, "scale":{"x":0.9, "y":0.85, "z":0.85},  "rot":{"x":-15, "y":0,  "z":0  }}],
        ["brake_caliper_4pot_yellow", ["bolide_hub_F"], [], {"pos":{"x": 0.775, "y":-1.285, "z":0.345}, "scale":{"x":0.9, "y":0.85, "z":0.85},  "rot":{"x":15,  "y":180,"z":0  }}],
    ],
    "pressureWheels": [
        ["name","hubGroup","group","node1:","node2:","nodeS","nodeArm:","wheelDir"],
        {"brakeTorque":"$=$brakestrength*1650"},
        {"brakeInputSplit":1},
        {"brakeSplitCoef":1},
        {"parkingTorque":0},
        {"brakeSpring":125},

        //brake thermals
        {"enableBrakeThermals":true},
        {"brakeDiameter":0.305},
        {"brakeMass":8.25},
        {"brakeType":"vented-disc"},
        {"rotorMaterial":"steel"},
        {"brakeVentingCoef":1.0},
        //brake sounds
        {"squealCoefNatural": 0.0, "squealCoefLowSpeed": 0.0}
    ],
},
"bolide_brake_R": {
    "information":{
        "authors":"BeamNG",
        "name":"Rear Disc Brakes",
        "value":600,
    },
    "slotType" : "bolide_brake_R",
    "slots": [
        ["type", "default", "description"],
        ["brakepad_R","brakepad_R_sport", "Rear Brake Pads", {"coreSlot":true}],
    ],
    "flexbodies": [
        ["mesh", "[group]:", "nonFlexMaterials"],
        //brake discs
        ["brake_disc_plain",          ["wheel_RR","wheelhub_RR"],    [],                           {"pos":{"x":-0.765, "y":1.25, "z":0.345}, "scale":{"x":0.9, "y":0.95,  "z":0.95},  "rot":{"x":0,   "y":0,  "z":0  }}],
        ["brake_disc_plain",          ["wheel_RL","wheelhub_RL"],    [],                           {"pos":{"x": 0.765, "y":1.25, "z":0.345}, "scale":{"x":0.9, "y":0.95,  "z":0.95},  "rot":{"x":180, "y":0,  "z":0  }}],
        ["brake_caliper_4pot_yellow", ["bolide_hub_R"], [], {"pos":{"x":-0.765, "y":1.25, "z":0.345}, "scale":{"x":0.9, "y":0.85, "z":0.85}, "rot":{"x":180, "y":0,  "z":0  }}],
        ["brake_caliper_4pot_yellow", ["bolide_hub_R"], [], {"pos":{"x": 0.765, "y":1.25, "z":0.345}, "scale":{"x":0.9, "y":0.85, "z":0.85}, "rot":{"x":180, "y":180,"z":0  }}],
    ],
    "pressureWheels": [
        ["name","hubGroup","group","node1:","node2:","nodeS","nodeArm:","wheelDir"],
        {"brakeTorque":"$=$brakestrength*1100"},
        {"brakeInputSplit":1},
        {"brakeSplitCoef":1},
        {"parkingTorque":1400},
        {"brakeSpring":125},

        //brake thermals
        {"enableBrakeThermals":true},
        {"brakeDiameter":0.305},
        {"brakeMass":7.75},
        {"brakeType":"vented-disc"},
        {"rotorMaterial":"steel"},
        {"brakeVentingCoef":0.8},
        //brake sounds
        {"squealCoefNatural": 0.0, "squealCoefLowSpeed": 0.0}
    ],
},
"bolide_brake_RR": {
    "information":{
        "authors":"BeamNG",
        "name":"Rear Rear Disc Brakes",
        "value":600,
    },
    "slotType" : "bolide_brake_RR",
    "slots": [
        ["type", "default", "description"],
        ["brakepad_RR","brakepad_RR_sport", "Rear Rear Brake Pads", {"coreSlot":true}],
    ],
    "flexbodies": [
        ["mesh", "[group]:", "nonFlexMaterials"],
        //brake discs
        ["brake_disc_plain",          ["wheel_RRR","wheelhub_RRR"],    [],                           {"pos":{"x":-0.765, "y":2.04, "z":0.345}, "scale":{"x":0.9, "y":0.95,  "z":0.95},  "rot":{"x":0,   "y":0,  "z":0  }}],
        ["brake_disc_plain",          ["wheel_RRL","wheelhub_RRL"],    [],                           {"pos":{"x": 0.765, "y":2.04, "z":0.345}, "scale":{"x":0.9, "y":0.95,  "z":0.95},  "rot":{"x":180, "y":0,  "z":0  }}],
        ["brake_caliper_4pot_yellow", ["bolide_hub_RR"], [], {"pos":{"x":-0.765, "y":2.04, "z":0.345}, "scale":{"x":0.9, "y":0.85, "z":0.85}, "rot":{"x":180, "y":0,  "z":0  }}],
        ["brake_caliper_4pot_yellow", ["bolide_hub_RR"], [], {"pos":{"x": 0.765, "y":2.04, "z":0.345}, "scale":{"x":0.9, "y":0.85, "z":0.85}, "rot":{"x":180, "y":180,"z":0  }}],
    ],
    "pressureWheels": [
        ["name","hubGroup","group","node1:","node2:","nodeS","nodeArm:","wheelDir"],
        {"brakeTorque":"$=$brakestrength*1100"},
        {"brakeInputSplit":1},
        {"brakeSplitCoef":1},
        {"parkingTorque":1400},
        {"brakeSpring":125},

        //brake thermals
        {"enableBrakeThermals":true},
        {"brakeDiameter":0.305},
        {"brakeMass":7.75},
        {"brakeType":"vented-disc"},
        {"rotorMaterial":"steel"},
        {"brakeVentingCoef":0.8},
        //brake sounds
        {"squealCoefNatural": 0.0, "squealCoefLowSpeed": 0.0}
    ],
},
"bolide_brake_F_race": {
    "information":{
        "authors":"BeamNG",
        "name":"Race Front Disc Brakes",
        "value":1400,
    },
    "slotType" : "bolide_brake_F",
    "slots": [
        ["type", "default", "description"],
        ["brakepad_F","brakepad_F_race", "Front Brake Pads", {"coreSlot":true}],
    ],
    "variables": [
        ["name", "type", "unit", "category", "default", "min", "max", "title", "description"],
        //["$braketorque_F", "range", "", "Brakes", 1700, 0, 2500, "Front Brake Torque", "Strength of the front brakes", {"minDis":0}]
        //["$braketorque", "range", "", "Brakes", 3800, 1000, 5000, "Max Brake Torque", "Scales the overall brake torque", {"minDis":0}]
        ["$brakebias", "range", "", "Brakes", 0.6, 0, 1, "Front/Rear Bias", "Percent of brake torque to the front wheels", {"minDis":0, "maxDis":100}],
    ],
    "flexbodies": [
        ["mesh", "[group]:", "nonFlexMaterials"],
        //brake discs
        ["brake_disc_drilled",             ["wheel_FR","wheelhub_FR"],    [],  {"pos":{"x":-0.775, "y":-1.28, "z":0.345},  "scale":{"x":0.9, "y":0.95, "z":0.95},  "rot":{"x":0,   "y":0,  "z":0  }}],
        ["brake_disc_drilled",             ["wheel_FL","wheelhub_FL"],    [],  {"pos":{"x": 0.775, "y":-1.28, "z":0.345},  "scale":{"x":0.9, "y":0.95, "z":0.95},  "rot":{"x":180, "y":0,  "z":0  }}],
        ["brake_caliper_4pot_red_rotopad", ["bolide_hub_F"], [], {"pos":{"x":-0.775, "y":-1.285, "z":0.345}, "scale":{"x":0.9, "y":0.95, "z":0.95},  "rot":{"x":-15, "y":0,  "z":0  }}],
        ["brake_caliper_4pot_red_rotopad", ["bolide_hub_F"], [], {"pos":{"x": 0.775, "y":-1.285, "z":0.345}, "scale":{"x":0.9, "y":0.95, "z":0.95},  "rot":{"x":15,  "y":180,"z":0  }}],
    ],
    "pressureWheels": [
        ["name","hubGroup","group","node1:","node2:","nodeS","nodeArm:","wheelDir"],
        {"brakeTorque":"$=$brakestrength*3800*$brakebias"},
        {"brakeInputSplit":1},
        {"brakeSplitCoef":1},
        {"parkingTorque":0},
        {"brakeSpring":125},

        //brake thermals
        {"enableBrakeThermals":true},
        {"brakeDiameter":0.310},
        {"brakeMass":10.5},
        {"brakeType":"vented-disc"},
        {"rotorMaterial":"steel"},
        {"brakeVentingCoef":1.4},
        //brake sounds
        {"squealCoefNatural": 1.0, "squealCoefLowSpeed": 1.0}
    ],
},
"bolide_brake_R_race": {
    "information":{
        "authors":"BeamNG",
        "name":"Race Rear Disc Brakes",
        "value":1300,
    },
    "slotType" : "bolide_brake_R",
    "slots": [
        ["type", "default", "description"],
        ["brakepad_R","brakepad_R_race", "Rear Brake Pads", {"coreSlot":true}],
    ],
    "variables": [
        ["name", "type", "unit", "category", "default", "min", "max", "title", "description"],
        //["$braketorque_R", "range", "", "Brakes", 1300, 0, 2500, "Rear Brake Torque", "Strength of the front brakes", {"minDis":0}]
    ],
    "flexbodies": [
        ["mesh", "[group]:", "nonFlexMaterials"],
        //brake discs
        ["brake_disc_drilled",             ["wheel_RR","wheelhub_RR"],    [],   {"pos":{"x":-0.765, "y":1.25, "z":0.345}, "scale":{"x":0.9, "y":0.92, "z":0.92},  "rot":{"x":0,   "y":0,  "z":0  }}],
        ["brake_disc_drilled",             ["wheel_RL","wheelhub_RL"],    [],   {"pos":{"x": 0.765, "y":1.25, "z":0.345}, "scale":{"x":0.9, "y":0.92, "z":0.92},  "rot":{"x":180, "y":0,  "z":0  }}],
        ["brake_caliper_4pot_red_rotopad", ["bolide_hub_R"],              [],   {"pos":{"x":-0.765, "y":1.25, "z":0.345}, "scale":{"x":0.9, "y":0.92, "z":0.92}, "rot":{"x":180, "y":0,  "z":0  }}],
        ["brake_caliper_4pot_red_rotopad", ["bolide_hub_R"],              [],   {"pos":{"x": 0.765, "y":1.25, "z":0.345}, "scale":{"x":0.9, "y":0.92, "z":0.92}, "rot":{"x":180, "y":180,"z":0  }}],
    ],
    "pressureWheels": [
        ["name","hubGroup","group","node1:","node2:","nodeS","nodeArm:","wheelDir"],
        //if no race front brake, use default value
        //{"brakeTorque":"$=case($brakebias == nil, $brakestrength*900, $brakestrength*3800*(1-$brakebias))"},
        {"brakeTorque":"$=$brakebias == nil and $brakestrength*900 or $brakestrength*3800*(1-$brakebias)"},
        {"brakeInputSplit":1},
        {"brakeSplitCoef":1},
        {"parkingTorque":1800},
        {"brakeSpring":125},

        //brake thermals
        {"enableBrakeThermals":true},
        {"brakeDiameter":0.300},
        {"brakeMass":9.0},
        {"brakeType":"vented-disc"},
        {"rotorMaterial":"steel"},
        {"brakeVentingCoef":1.0},
        //brake sounds
        {"squealCoefNatural": 0.0, "squealCoefLowSpeed": 0.0}
    ],
},
"bolide_brake_RR_race": {
    "information":{
        "authors":"BeamNG",
        "name":"Race Rear Rear Disc Brakes",
        "value":1300,
    },
    "slotType" : "bolide_brake_RR",
    "slots": [
        ["type", "default", "description"],
        ["brakepad_RR","brakepad_RR_race", "Rear Rear Brake Pads", {"coreSlot":true}],
    ],
    "variables": [
        ["name", "type", "unit", "category", "default", "min", "max", "title", "description"],
        //["$braketorque_R", "range", "", "Brakes", 1300, 0, 2500, "Rear Brake Torque", "Strength of the front brakes", {"minDis":0}]
    ],
    "flexbodies": [
        ["mesh", "[group]:", "nonFlexMaterials"],
        //brake discs
        ["brake_disc_drilled",             ["wheel_RRR","wheelhub_RRR"],    [],   {"pos":{"x":-0.765, "y":2.04, "z":0.345}, "scale":{"x":0.9, "y":0.92, "z":0.92},  "rot":{"x":0,   "y":0,  "z":0  }}],
        ["brake_disc_drilled",             ["wheel_RRL","wheelhub_RRL"],    [],   {"pos":{"x": 0.765, "y":2.04, "z":0.345}, "scale":{"x":0.9, "y":0.92, "z":0.92},  "rot":{"x":180, "y":0,  "z":0  }}],
        ["brake_caliper_4pot_red_rotopad", ["bolide_hub_RR"],              [],   {"pos":{"x":-0.765, "y":2.04, "z":0.345}, "scale":{"x":0.9, "y":0.92, "z":0.92}, "rot":{"x":180, "y":0,  "z":0  }}],
        ["brake_caliper_4pot_red_rotopad", ["bolide_hub_RR"],              [],   {"pos":{"x": 0.765, "y":2.04, "z":0.345}, "scale":{"x":0.9, "y":0.92, "z":0.92}, "rot":{"x":180, "y":180,"z":0  }}],
    ],
    "pressureWheels": [
        ["name","hubGroup","group","node1:","node2:","nodeS","nodeArm:","wheelDir"],
        //if no race front brake, use default value
        //{"brakeTorque":"$=case($brakebias == nil, $brakestrength*900, $brakestrength*3800*(1-$brakebias))"},
        {"brakeTorque":"$=$brakebias == nil and $brakestrength*900 or $brakestrength*3800*(1-$brakebias)"},
        {"brakeInputSplit":1},
        {"brakeSplitCoef":1},
        {"parkingTorque":1800},
        {"brakeSpring":125},

        //brake thermals
        {"enableBrakeThermals":true},
        {"brakeDiameter":0.300},
        {"brakeMass":9.0},
        {"brakeType":"vented-disc"},
        {"rotorMaterial":"steel"},
        {"brakeVentingCoef":1.0},
        //brake sounds
        {"squealCoefNatural": 0.0, "squealCoefLowSpeed": 0.0}
    ],
},
"bolide_race_brake_F": {
    "information":{
        "authors":"BeamNG",
        "name":"Race Front Disc Brakes",
        "value":1400,
    },
    "slotType" : "bolide_race_brake_F",
    "slots": [
        ["type", "default", "description"],
        ["brakepad_F","brakepad_F_race", "Front Brake Pads", {"coreSlot":true}],
    ],
    "variables": [
        ["name", "type", "unit", "category", "default", "min", "max", "title", "description"],
        //["$braketorque_F", "range", "", "Brakes", 1700, 0, 2500, "Front Brake Torque", "Strength of the front brakes", {"minDis":0}]
        //["$braketorque", "range", "", "Brakes", 3800, 1000, 5000, "Max Brake Torque", "Scales the overall brake torque", {"minDis":0}]
        ["$brakebias", "range", "", "Brakes", 0.6, 0, 1, "Front/Rear Bias", "Percent of brake torque to the front wheels", {"minDis":0, "maxDis":100}],
    ],
    "flexbodies": [
        ["mesh", "[group]:", "nonFlexMaterials"],
        //brake discs
        ["brake_disc_drilled",             ["wheel_FR","wheelhub_FR"],    [],  {"pos":{"x":-0.765, "y":-1.28, "z":0.395}, "scale":{"x":0.9, "y":0.95, "z":0.95},  "rot":{"x":0,   "y":0,  "z":0  }}],
        ["brake_disc_drilled",             ["wheel_FL","wheelhub_FL"],    [],  {"pos":{"x": 0.765, "y":-1.28, "z":0.395}, "scale":{"x":0.9, "y":0.95, "z":0.95},  "rot":{"x":180, "y":0,  "z":0  }}],
        ["brake_caliper_4pot_red_rotopad", ["bolide_hub_F"], [], {"pos":{"x":-0.765, "y":-1.28, "z":0.395}, "scale":{"x":0.9, "y":0.95, "z":0.95},  "rot":{"x":-15, "y":0,  "z":0  }}],
        ["brake_caliper_4pot_red_rotopad", ["bolide_hub_F"], [], {"pos":{"x": 0.765, "y":-1.28, "z":0.395}, "scale":{"x":0.9, "y":0.95, "z":0.95},  "rot":{"x":15,  "y":180,"z":0  }}],
    ],
    "pressureWheels": [
        ["name","hubGroup","group","node1:","node2:","nodeS","nodeArm:","wheelDir"],
        {"brakeTorque":"$=$brakestrength*3800*$brakebias"},
        {"brakeInputSplit":1},
        {"brakeSplitCoef":1},
        {"parkingTorque":0},
        {"brakeSpring":125},

        //brake thermals
        {"enableBrakeThermals":true},
        {"brakeDiameter":0.310},
        {"brakeMass":10.5},
        {"brakeType":"vented-disc"},
        {"rotorMaterial":"steel"},
        {"brakeVentingCoef":1.4},
        //brake sounds
        {"squealCoefNatural": 1.0, "squealCoefLowSpeed": 1.0}
    ],
},
"bolide_race_brake_R": {
    "information":{
        "authors":"BeamNG",
        "name":"Race Rear Disc Brakes",
        "value":1300,
    },
    "slotType" : "bolide_race_brake_R",
    "slots": [
        ["type", "default", "description"],
        ["brakepad_R","brakepad_R_race", "Rear Brake Pads", {"coreSlot":true}],
    ],
    "variables": [
        ["name", "type", "unit", "category", "default", "min", "max", "title", "description"],
        //["$braketorque_R", "range", "", "Brakes", 1300, 0, 2500, "Rear Brake Torque", "Strength of the front brakes", {"minDis":0}]
    ],
    "flexbodies": [
        ["mesh", "[group]:", "nonFlexMaterials"],
        //brake discs
        ["brake_disc_drilled",             ["wheel_RR","wheelhub_RR"],    [],  {"pos":{"x":-0.765, "y":1.25, "z":0.395}, "scale":{"x":0.9, "y":0.92, "z":0.92},  "rot":{"x":0,   "y":0,  "z":0  }}],
        ["brake_disc_drilled",             ["wheel_RL","wheelhub_RL"],    [],  {"pos":{"x": 0.765, "y":1.25, "z":0.395}, "scale":{"x":0.9, "y":0.92, "z":0.92},  "rot":{"x":180, "y":0,  "z":0  }}],
        ["brake_caliper_4pot_red_rotopad", ["bolide_hub_R"], [],               {"pos":{"x":-0.765, "y":1.25, "z":0.395}, "scale":{"x":0.9, "y":0.92, "z":0.92}, "rot":{"x":180, "y":0,  "z":0  }}],
        ["brake_caliper_4pot_red_rotopad", ["bolide_hub_R"], [],               {"pos":{"x": 0.765, "y":1.25, "z":0.395}, "scale":{"x":0.9, "y":0.92, "z":0.92}, "rot":{"x":180, "y":180,"z":0  }}],
    ],
    "pressureWheels": [
        ["name","hubGroup","group","node1:","node2:","nodeS","nodeArm:","wheelDir"],
        //if no race front brake, use default value
        //{"brakeTorque":"$=case($brakebias == nil, $brakestrength*900, $brakestrength*3800*(1-$brakebias))"},
        {"brakeTorque":"$=$brakebias == nil and $brakestrength*900 or $brakestrength*3800*(1-$brakebias)"},
        {"brakeInputSplit":1},
        {"brakeSplitCoef":1},
        {"parkingTorque":1800},
        {"brakeSpring":125},

        //brake thermals
        {"enableBrakeThermals":true},
        {"brakeDiameter":0.300},
        {"brakeMass":9.0},
        {"brakeType":"vented-disc"},
        {"rotorMaterial":"steel"},
        {"brakeVentingCoef":1.0},
        //brake sounds
        {"squealCoefNatural": 0.0, "squealCoefLowSpeed": 0.0}
    ],
},
}