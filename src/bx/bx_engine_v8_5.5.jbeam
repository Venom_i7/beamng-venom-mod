{
"bx_engine_v8_5.5": {
    "information":{
        "authors":"BeamNG",
        "name":"5.5L V8 Engine",
        "value":7900,
    },
    "slotType" : "bx_engine",
    "slots": [
        ["type", "default", "description"],
        ["bx_enginemounts","bx_enginemounts", "Engine Mounts", {"coreSlot":true}],
        ["pickup_oilpan_v8","pickup_oilpan_v8", "Oil Pan", {"coreSlot":true, "nodeMove":{"x":0.0,"y":0.1,"z":-0.16}}],
        ["bx_intake_v8","bx_intake_v8_stock", "Intake", {"coreSlot":true}],
        ["pickup_engine_v8_ecu","pickup_engine_v8_ecu", "Engine Management", {"coreSlot":true}],
        ["n2o_system","", "Nitrous Oxide System"],
        ["pickup_engine_v8_internals","pickup_engine_v8_internals", "Engine Long Block", {"coreSlot":true}],
        ["bx_transmission","bx_transmission_4A", "Transmission"],
        ["bx_radiator_v8","bx_radiator_v8", "Radiator"],
        ["bx_oilcooler","", "Oil Cooler"],
        ["bx_exhaust_v8","bx_exhaust_v8", "Exhaust"],
    ],
    "powertrain" : [
        ["type", "name", "inputName", "inputIndex"],
        ["combustionEngine", "mainEngine", "dummy", 0],
    ],
    "mainEngine": {
        "torque":[
            ["rpm", "torque"],
            [0, 0],
            [500, 165],
            [1000, 295],
            [2000, 396],
            [3000, 435],
            [4000, 395],
            [5000, 338],
            [6000, 257],
            [7000, 200],
            [8000, 85],
        ],

        //engine performance
        "idleRPM":750,
        //max physically capable
        "maxRPM":5800,
        "inertia":0.20,
        "friction":19,
        "dynamicFriction":0.035,
        "engineBrakeTorque":52,
        //"burnEfficiency":0.32,
        "burnEfficiency":[
            [0, 0.1],
            [0.05, 0.21],
            [0.4, 0.33],
            [0.7, 0.36],
            [1, 0.24],
        ],
        //fuel system
        "energyStorage": ["mainTank","auxTank"],
        "requiredEnergyType":"gasoline",

        //exhaust
        "instantAfterFireSound": "event:>Vehicle>Afterfire>v8_crossflow_03>single",
        "sustainedAfterFireSound": "event:>Vehicle>Afterfire>v8_crossflow_03>multi",
        "shiftAfterFireSound": "event:>Vehicle>Afterfire>v8_crossflow_03>shift",
        "instantAfterFireCoef": 0.8,
        "sustainedAfterFireCoef": 0.9,
        "particulates":0.05,
        "instantAfterFireVolumeCoef": 0.9,
        "shiftAfterFireVolumeCoef": 0.6,

        //cooling and oil system
        "thermalsEnabled":true,
        "engineBlockMaterial":"aluminum",
        "oilVolume":8,
        "engineBlockAirCoolingEfficiency":39,

        //engine durability
        "cylinderWallTemperatureDamageThreshold":150,
        "headGasketDamageThreshold":1500000,
        "pistonRingDamageThreshold":1500000,
        "connectingRodDamageThreshold":2000000,
        "maxTorqueRating": 590,
        "maxOverTorqueDamage": 500,

        //node beam interface
        "torqueReactionNodes:":["e1l","e2l","e4r"],
        "waterDamage": {"[engineGroup]:":["engine_intake"]},
        "radiator": {"[engineGroup]:":["radiator"]},
        "engineBlock": {"[engineGroup]:":["engine_block"]},
        "breakTriggerBeam":"engine",
        "uiName":"Engine",
        "soundConfig": "soundConfig",
        "soundConfigExhaust": "soundConfigExhaust",

        //starter motor
        "starterSample":"event:>Engine>Starter>v8_20xy_eng",
        "starterSampleExhaust":"event:>Engine>Starter>v8_20xy_exh",
        "shutOffSampleEngine":"event:>Engine>Shutoff>v8_20xy_eng",
        "shutOffSampleExhaust":"event:>Engine>Shutoff>v8_20xy_exh",
        "starterVolume":0.76,
        "starterVolumeExhaust":0.76,
        "shutOffVolumeEngine":0.76,
        "shutOffVolumeExhaust":0.76,
        "starterThrottleKillTime":0.84,
        "idleRPMStartRate":1.25,
        "idleRPMStartCoef":1,

        //engine deform groups
        "deformGroups":["mainEngine", "mainEngine_intake", "mainEngine_accessories"]
    },
    "soundConfig": {
        "sampleName": "V8_cross_engine",
        "intakeMuffling": 1,

        "mainGain": -2.5,
        "onLoadGain":1,
        "offLoadGain":0.67,

        "maxLoadMix": 0.75,
        "minLoadMix": 0,

        "lowShelfGain":6,
        "lowShelfFreq":120,

        "highShelfGain":4,
        "highShelfFreq":6000,

        "eqLowGain": 0,
        "eqLowFreq": 150,
        "eqLowWidth": 0.2,

        "eqHighGain": 0,
        "eqHighFreq": 2500,
        "eqHighWidth": 0.2,

        "fundamentalFrequencyCylinderCount":8,
        "eqFundamentalGain": 0,
    },
    "soundConfigExhaust": {
        "sampleName": "V8_cross_exhaust",

        "mainGain": -0.5,
        "onLoadGain":1,
        "offLoadGain":0.61,

        "maxLoadMix": 0.72,
        "minLoadMix": 0,

        "lowShelfGain":-12,
        "lowShelfFreq":90,

        "highShelfGain":4,
        "highShelfFreq":2400,

        "eqLowGain": 1.0,
        "eqLowFreq": 150,
        "eqLowWidth": 0.2,

        "eqHighGain": 0,
        "eqHighFreq": 2500,
        "eqHighWidth": 0.2,

        "fundamentalFrequencyCylinderCount":8,
        "eqFundamentalGain": -1,
    },
    "vehicleController": {
        "clutchLaunchStartRPM":1200,
        "clutchLaunchTargetRPM":1600,
        //**highShiftDown can be overwritten by automatic transmissions**
        "highShiftDownRPM":[0,0,0,2600,2950,3150,3400,3400],
        //**highShiftUp can be overwritten by intake modifications**
        "highShiftUpRPM":5600,
    },
    "flexbodies": [
        ["mesh", "[group]:", "nonFlexMaterials"],
        ["bx_engine_v8", ["bx_engine"]],
        //["bx_engbaycrap_custom", ["bx_body","bx_shocktop_F"]],
        {"deformGroup":"radhose_damage", "deformMaterialBase":"generic_perf_parts", "deformMaterialDamaged":"invis"},
        ["bx_radhose_v8", ["bx_engine", "bx_radiator"]],
        {"deformGroup":"", "deformMaterialBase":"", "deformMaterialDamaged":""},
    ],
        "props": [
        ["func",            "mesh",                   "idRef:", "idX:", "idY:", "baseRotation",          "rotation",            "translation",         "min", "max", "offset", "multiplier"],
        ["rpmspin",             "bx_supercharger_pulley",      "e3l","e3r","e4l",     {"x":-90, "y":0, "z":0}, {"x":0, "y":1, "z":0}, {"x":0, "y":0, "z":0}, -360,  360,   0,        1],
    ],

    "props": [
        ["func",            "mesh",                   "idRef:", "idX:", "idY:", "baseRotation",          "rotation",            "translation",         "min", "max", "offset", "multiplier"],
        ["rpmspin",             "bx_engine_v8_pulley1",      "e3l","e3r","e4l",     {"x":-90, "y":0, "z":0}, {"x":0, "y":1, "z":0}, {"x":0, "y":0, "z":0}, -360,  360,   0,        1],
        ["rpmspin",             "bx_engine_v8_pulley2",      "e3l","e3r","e4l",     {"x":-90, "y":0, "z":0}, {"x":0, "y":1, "z":0}, {"x":0, "y":0, "z":0}, -360,  360,   0,       -1],
        ["rpmspin",             "bx_engine_v8_pulley3",      "e3l","e3r","e4l",     {"x":-90, "y":0, "z":0}, {"x":0, "y":1, "z":0}, {"x":0, "y":0, "z":0}, -360,  360,   0,        1],
        ["rpmspin",             "bx_engine_v8_pulley4",      "e3l","e3r","e4l",     {"x":-90, "y":0, "z":0}, {"x":0, "y":1, "z":0}, {"x":0, "y":0, "z":0}, -360,  360,   0,        1],
        ["rpmspin",             "bx_engine_v8_pulley5",      "e3l","e3r","e4l",     {"x":-90, "y":0, "z":0}, {"x":0, "y":1, "z":0}, {"x":0, "y":0, "z":0}, -360,  360,   0,        1],
        ["rpmspin",             "bx_engine_v8_pulley6",      "e3l","e3r","e4l",     {"x":-90, "y":0, "z":0}, {"x":0, "y":1, "z":0}, {"x":0, "y":0, "z":0}, -1440,  1440, 0,        4],

    ],

    "nodes": [
         ["id", "posX", "posY", "posZ"],
         //5.5L V8 Engine
         {"frictionCoef":0.5},
         {"nodeMaterial":"|NM_METAL"},
         {"selfCollision":false},
         {"collision":true},
         {"group":"bx_engine"},
         {"nodeWeight":29.14},
         {"engineGroup":"engine_block"},
         ["e1r", -0.13, -1.0439, 0.2918, {"chemEnergy":1000,"burnRate":0.39,"flashPoint":700,"specHeat": 0.1,"selfIgnitionCoef":false,"smokePoint":650,"baseTemp":"thermals","conductionRadius":0.2}],
         ["e1l", 0.13, -1.0439, 0.2918, {"chemEnergy":1000,"burnRate":0.39,"flashPoint":700,"specHeat": 0.1,"selfIgnitionCoef":false,"smokePoint":650,"baseTemp":"thermals","conductionRadius":0.2, "isExhaust":"mainEngine"}],
         ["e2r", -0.13, -1.6339, 0.2918, {"isExhaust":"mainEngine"}],
         ["e2l", 0.13, -1.6339, 0.2918, {"isExhaust":"mainEngine"}],
         {"engineGroup":["engine_block","engine_intake"]},
         {"group":["bx_engine","bx_engine_intake"]},
         ["e3r", -0.32, -1.0439, 0.7118],
         ["e3l", 0.32, -1.0439, 0.7118],
         ["e4r", -0.32, -1.6339, 0.7118],
         ["e4l", 0.32, -1.6339, 0.7118],
         {"chemEnergy":false,"burnRate":false,"flashPoint":false,"specHeat":false,"smokePoint":false,"selfIgnitionCoef":false,"baseTemp":false,"conductionRadius":false},
         {"engineGroup":""},
         {"group":""},
         //engine mount nodes
         ["em1r", -0.27, -1.3939, 0.4718, {"nodeWeight":3.5}],
         ["em1l", 0.27, -1.3939, 0.4718, {"nodeWeight":3.5}],
    ],
    "beams": [
          ["id1:", "id2:"],
          {"beamPrecompression":1, "beamType":"|NORMAL", "beamLongBound":1.0, "beamShortBound":1.0},
          //--ENGINE CUBE--
          {"beamSpring":18800940,"beamDamp":470},
          {"beamDeform":315000,"beamStrength":"FLT_MAX"},
          {"deformGroup":"mainEngine", "deformationTriggerRatio":0.001}
          ["e1r","e1l"],
          ["e1r","e2r"],
          ["e1r","e2l"],
          ["e1r","e3r"],
          ["e1r","e3l"],
          ["e1r","e4r"],
          ["e1r","e4l"],
          ["e1l","e2r"],
          ["e1l","e2l"],
          ["e1l","e3r"],
          ["e1l","e3l"],
          ["e1l","e4r"],
          ["e1l","e4l"],
          ["e2r","e2l"],
          ["e2r","e3r"],
          ["e2r","e3l"],
          ["e2r","e4r"],
          ["e2r","e4l"],
          ["e2l","e3r"],
          ["e2l","e3l"],
          ["e2l","e4r"],
          ["e2l","e4l"],
          ["e3r","e3l"],
          ["e3r","e4r"],
          ["e3r","e4l"],
          ["e3l","e4r"],
          ["e3l","e4l"],
          ["e4r","e4l"],

          //engine mount nodes
          {"beamSpring":2956300,"beamDamp":130.43},
          {"beamDeform":63000,"beamStrength":"FLT_MAX"},
          ["em1r","e3l"],
          ["em1r","e3r"],
          ["em1r","e4l"],
          ["em1r","e4r"],
          ["em1r", "e1r"],
          ["em1r", "e1l"],
          ["em1r", "e2l"],
          ["em1r", "e2r"],

          ["em1l","e3l"],
          ["em1l","e3r"],
          ["em1l","e4l"],
          ["em1l","e4r"],
          ["em1l", "e1r"],
          ["em1l", "e1l"],
          ["em1l", "e2l"],
          ["em1l", "e2r"],
          {"deformGroup":""},

          //mesh break beams
          {"beamPrecompression":0.8, "beamType":"|SUPPORT","beamLongBound":0.8},
          {"beamSpring":9400,"beamDamp":0},
          {"beamDeform":350,"beamStrength":700},
          {"deformGroup":"engbay_break","deformationTriggerRatio":0.01},
          {"optional":true},
          ["e2r","rad2"],
          ["e2l","rad2"],
          ["e4r","rad2"],
          ["e4l","rad2"],
          {"optional":false},
          {"deformGroup":""},
          {"beamPrecompression":1, "beamType":"|NORMAL", "beamLongBound":1.0, "beamShortBound":1.0},
    ],
},
}
