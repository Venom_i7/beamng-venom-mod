{
"tire_R_225_45_15_altb_race": {
    "information":{
        "authors":"BeamNG",
        "name":"225/45R15 Race Rear Tires",
        "value":250,
    },
    "slotType" : "tire_R_15x8_altb",
    "flexbodies": [
        ["mesh", "[group]:", "nonFlexMaterials"],
        ["tire_01f_15x7_23",  ["wheel_RR","tire_RR"], [], {"pos":{"x":-0.48, "y":0.0, "z":0.0}, "rot":{"x":0, "y":0, "z":180}, "scale":{"x":1.13, "y":1, "z":1}}],
        ["tire_01f_15x7_23",  ["wheel_RL","tire_RL"], [], {"pos":{"x": 0.48, "y":0.0, "z":0.0}, "rot":{"x":0, "y":0, "z":0},   "scale":{"x":1.13, "y":1, "z":1}}],
    ],
    "variables": [
        ["name", "type", "unit", "category", "default", "min", "max", "title", "description"],
        ["$tirepressure_R", "range", "psi", "Wheels", 27, 0, 50, "Tire Pressure", "Relative to atmospheric pressure", {"subCategory":"Rear"}],
    ],
    "pressureWheels": [
        ["name","hubGroup","group","node1:","node2:","nodeS","nodeArm:","wheelDir"],
        {"disableMeshBreaking":false,"disableHubMeshBreaking":false},
        {"hasTire":true},
        {"enableTireReinfBeams":false},
        {"enableTireLbeams":true},
        {"enableTireSideReinfBeams":false},
        {"enableTreadReinfBeams":true},
        {"enableTirePeripheryReinfBeams":true},

        //general settings
        {"radius":0.29},
        {"tireWidth":0.20},

        //tire options
        {"wheelSideBeamSpring":"$=$tirepressure_R*550", "wheelSideBeamDamp":25},
        {"wheelSideBeamSpringExpansion":361000,"wheelSideBeamDampExpansion":25},
        {"wheelSideTransitionZone":0.12,"wheelSideBeamPrecompression":0.985},

        {"wheelReinfBeamSpring":30000,"wheelReinfBeamDamp":170},
        {"wheelReinfBeamDampCutoffHz":400,"wheelReinfBeamPrecompression":0.98},

        {"wheelTreadBeamSpring":75000,"wheelTreadBeamDamp":50},
        {"wheelTreadBeamDampCutoffHz":500,"wheelTreadBeamPrecompression":0.98},

        {"wheelTreadReinfBeamSpring":145000,"wheelTreadReinfBeamDamp":50},
        {"wheelTreadReinfBeamDampCutoffHz":500,"wheelTreadReinfBeamPrecompression":0.98},

        {"wheelPeripheryBeamSpring":71000,"wheelPeripheryBeamDamp":30},
        {"wheelPeripheryBeamDampCutoffHz":500,"wheelPeripheryBeamPrecompression":0.98},

        {"wheelPeripheryReinfBeamSpring":141000,"wheelPeripheryReinfBeamDamp":30},
        {"wheelPeripheryReinfBeamDampCutoffHz":500,"wheelPeripheryReinfBeamPrecompression":0.98},

        //general tire values
        {"nodeWeight":0.145},
        {"nodeMaterial":"|NM_RUBBER"},
        {"triangleCollision":false},
        {"pressurePSI":"$tirepressure_R"},
        {"dragCoef":5},

        //groundmodel friction multipliers
        {"frictionCoef":1.0},
        {"slidingFrictionCoef":1.00},
        {"stribeckExponent":2},
        {"treadCoef":0},

        //advanced friction values
        {"noLoadCoef":2.0},
        {"loadSensitivitySlope":0.000267},
        {"fullLoadCoef":0.65},
        {"softnessCoef":1},

        //deform values
        {"wheelSideBeamDeform":14000,"wheelSideBeamStrength":18000},
        {"wheelTreadBeamDeform":11000,"wheelTreadBeamStrength":14000},
        {"wheelPeripheryBeamDeform":45000,"wheelPeripheryBeamStrength":45000},
    ],
},
"tire_R_225_50_15_altb_race": {
    "information":{
        "authors":"BeamNG",
        "name":"225/50R15 Race Rear Tires",
        "value":250,
    },
    "slotType" : "tire_R_15x8_altb",
    "flexbodies": [
        ["mesh", "[group]:", "nonFlexMaterials"],
        ["tire_01f_15x8_24",  ["wheel_RR","tire_RR"], [], {"pos":{"x":-0.48, "y":0.0, "z":0.0}, "rot":{"x":0, "y":0, "z":180}, "scale":{"x":1.015, "y":0.996, "z":0.996}}],
        ["tire_01f_15x8_24",  ["wheel_RL","tire_RL"], [], {"pos":{"x": 0.48, "y":0.0, "z":0.0}, "rot":{"x":0, "y":0, "z":0},   "scale":{"x":1.015, "y":0.996, "z":0.996}}],
    ],
    "variables": [
        ["name", "type", "unit", "category", "default", "min", "max", "title", "description"],
        ["$tirepressure_R", "range", "psi", "Wheels", 27, 0, 50, "Tire Pressure", "Relative to atmospheric pressure", {"subCategory":"Rear"}],
    ],
    "pressureWheels": [
        ["name","hubGroup","group","node1:","node2:","nodeS","nodeArm:","wheelDir"],
        {"disableMeshBreaking":false,"disableHubMeshBreaking":false},
        {"hasTire":true},
        {"enableTireReinfBeams":false},
        {"enableTireLbeams":true},
        {"enableTireSideReinfBeams":false},
        {"enableTreadReinfBeams":true},
        {"enableTirePeripheryReinfBeams":true},

        //general settings
        {"radius":0.306},
        {"tireWidth":0.20},

        //tire options
        {"wheelSideBeamSpring":"$=$tirepressure_R*714","wheelSideBeamDamp":35},
        {"wheelSideBeamSpringExpansion":401000,"wheelSideBeamDampExpansion":35},
        {"wheelSideTransitionZone":0.12,"wheelSideBeamPrecompression":0.99},

        {"wheelReinfBeamSpring":30000,"wheelReinfBeamDamp":180},
        {"wheelReinfBeamDampCutoffHz":500,"wheelReinfBeamPrecompression":0.98},

        {"wheelTreadBeamSpring":71000,"wheelTreadBeamDamp":70},
        {"wheelTreadBeamDampCutoffHz":500,"wheelTreadBeamPrecompression":0.98},

        {"wheelTreadReinfBeamSpring":121000,"wheelTreadReinfBeamDamp":70},
        {"wheelTreadReinfBeamDampCutoffHz":500,"wheelTreadReinfBeamPrecompression":0.98},

        {"wheelPeripheryBeamSpring":51000,"wheelPeripheryBeamDamp":40},
        {"wheelPeripheryBeamDampCutoffHz":500,"wheelPeripheryBeamPrecompression":0.98},

        {"wheelPeripheryReinfBeamSpring":101000,"wheelPeripheryReinfBeamDamp":40},
        {"wheelPeripheryReinfBeamDampCutoffHz":500,"wheelPeripheryReinfBeamPrecompression":0.98},

        //general tire values
        {"nodeWeight":0.15},
        {"nodeMaterial":"|NM_RUBBER"},
        {"triangleCollision":false},
        {"pressurePSI":"$tirepressure_R"},
        {"dragCoef":5},

        //groundmodel friction multipliers
        {"frictionCoef":1.00},
        {"slidingFrictionCoef":1.00},
        {"stribeckExponent":1.9},
        {"treadCoef":0},

        //advanced friction values
        {"noLoadCoef":1.93},
        {"loadSensitivitySlope":0.000244},
        //{"noLoadCoef":1.89},
        //{"loadSensitivitySlope":0.00026},
        {"fullLoadCoef":0.65},
        {"softnessCoef":1},

        //deform values
        {"wheelSideBeamDeform":14000,"wheelSideBeamStrength":18000},
        {"wheelTreadBeamDeform":11000,"wheelTreadBeamStrength":14000},
        {"wheelPeripheryBeamDeform":45000,"wheelPeripheryBeamStrength":45000},
    ],
},
"tire_R_26_8_15_altb_race": {
    "information":{
        "authors":"BeamNG",
        "name":"26.0x8.0-15 Grip-All Race Rear Tires",
        "value":400,
    },
    "slotType" : "tire_R_15x8_altb",
    "flexbodies": [
        ["mesh", "[group]:", "nonFlexMaterials"],
        ["tire_01d_15x8_26", ["wheel_RR","tire_RR"], [], {"pos":{"x":-0.48, "y":-0.0, "z":0.0}, "rot":{"x":0, "y":0, "z":180}, "scale":{"x":1.035, "y":1.01, "z":1.01}}],
        ["tire_01d_15x8_26", ["wheel_RL","tire_RL"], [], {"pos":{"x": 0.48, "y":-0.0, "z":0.0}, "rot":{"x":0, "y":0, "z":0},   "scale":{"x":1.035, "y":1.01, "z":1.01}}],
    ],
    "variables": [
        ["name", "type", "unit", "category", "default", "min", "max", "title", "description"],
        ["$tirepressure_R", "range", "psi", "Wheels", 28, 0, 50, "Tire Pressure", "Relative to atmospheric pressure", {"subCategory":"Rear"}],
    ],
    "pressureWheels": [
        ["name","hubGroup","group","node1:","node2:","nodeS","nodeArm:","wheelDir"],
        {"disableMeshBreaking":false,"disableHubMeshBreaking":false},
        {"hasTire":true},
        {"enableTireReinfBeams":false},
        {"enableTireLbeams":true},
        {"enableTireSideReinfBeams":false},
        {"enableTreadReinfBeams":true},
        {"enableTirePeripheryReinfBeams":true},

        //general settings
        {"radius":0.33},
        {"tireWidth":0.215},

        //tire options
        {"wheelSideBeamSpring":"$=$tirepressure_R*750","wheelSideBeamDamp":35},
        {"wheelSideBeamSpringExpansion":431000,"wheelSideBeamDampExpansion":35},
        {"wheelSideTransitionZone":0.09,"wheelSideBeamPrecompression":0.985},

        {"wheelReinfBeamSpring":22000,"wheelReinfBeamDamp":190},
        {"wheelReinfBeamDampCutoffHz":500,"wheelReinfBeamPrecompression":0.98},

        {"wheelTreadBeamSpring":85000,"wheelTreadBeamDamp":80},
        {"wheelTreadBeamDampCutoffHz":500,"wheelTreadBeamPrecompression":0.98},

        {"wheelTreadReinfBeamSpring":170000,"wheelTreadReinfBeamDamp":60},
        {"wheelTreadReinfBeamDampCutoffHz":500,"wheelTreadReinfBeamPrecompression":0.98},

        {"wheelPeripheryBeamSpring":80000,"wheelPeripheryBeamDamp":40},
        {"wheelPeripheryBeamDampCutoffHz":500,"wheelPeripheryBeamPrecompression":0.98},

        {"wheelPeripheryReinfBeamSpring":160000,"wheelPeripheryReinfBeamDamp":35},
        {"wheelPeripheryReinfBeamDampCutoffHz":500,"wheelPeripheryReinfBeamPrecompression":0.98},

        //general tire values
        {"nodeWeight":0.17},
        {"nodeMaterial":"|NM_RUBBER"},
        {"triangleCollision":false},
        {"pressurePSI":"$tirepressure_R"},
        {"dragCoef":5},

        //groundmodel friction multipliers
        {"frictionCoef":1.0},
        {"slidingFrictionCoef":1.00},
        {"stribeckExponent":2},
        {"treadCoef":0},

        //advanced friction values
        {"noLoadCoef":2.07},
        {"loadSensitivitySlope":0.000258},
        {"fullLoadCoef":0.65},
        {"softnessCoef":1},

        //deform values
        {"wheelSideBeamDeform":15000,"wheelSideBeamStrength":20000},
        {"wheelTreadBeamDeform":11000,"wheelTreadBeamStrength":14000},
        {"wheelPeripheryBeamDeform":45000,"wheelPeripheryBeamStrength":45000},
    ],
},
"tire_R_26_8_15_altb_drag": {
    "information":{
        "authors":"BeamNG",
        "name":"26.0x8.0-15 Drag Rear Tires",
        "value":400,
    },
    "slotType" : "tire_R_15x8_altb",
    "flexbodies": [
        ["mesh", "[group]:", "nonFlexMaterials"],
        ["tire_01d_15x8_26", ["wheel_RR","tire_RR"], [], {"pos":{"x":-0.48, "y":-0.0, "z":0.0}, "rot":{"x":0, "y":0, "z":180}, "scale":{"x":1.01, "y":1.02, "z":1.02}}],
        ["tire_01d_15x8_26", ["wheel_RL","tire_RL"], [], {"pos":{"x": 0.48, "y":-0.0, "z":0.0}, "rot":{"x":0, "y":0, "z":0},   "scale":{"x":1.01, "y":1.02, "z":1.02}}],
    ],
    "variables": [
        ["name", "type", "unit", "category", "default", "min", "max", "title", "description"],
        ["$tirepressure_R", "range", "psi", "Wheels", 20, 0, 50, "Tire Pressure", "Relative to atmospheric pressure", {"subCategory":"Rear"}],
    ],
    "pressureWheels": [
        ["name","hubGroup","group","node1:","node2:","nodeS","nodeArm:","wheelDir"],
        {"disableMeshBreaking":false,"disableHubMeshBreaking":false},
        {"hasTire":true},
        {"enableTireReinfBeams":false},
        {"enableTireLbeams":true},
        {"enableTireSideReinfBeams":false},
        {"enableTreadReinfBeams":true},
        {"enableTirePeripheryReinfBeams":true},

        //general settings
        {"radius":0.33},
        {"tireWidth":0.205},

        //tire options
        {"wheelSideBeamSpring":"$=$tirepressure_R*550","wheelSideBeamDamp":30},
        {"wheelSideBeamSpringExpansion":361000,"wheelSideBeamDampExpansion":30},
        {"wheelSideTransitionZone":0.1,"wheelSideBeamPrecompression":0.99},

        {"wheelReinfBeamSpring":6000,"wheelReinfBeamDamp":160},
        {"wheelReinfBeamDampCutoffHz":500,"wheelReinfBeamPrecompression":0.99},

        {"wheelTreadBeamSpring":61000,"wheelTreadBeamDamp":55},
        {"wheelTreadBeamDampCutoffHz":500,"wheelTreadBeamPrecompression":0.99},

        {"wheelTreadReinfBeamSpring":121000,"wheelTreadReinfBeamDamp":55},
        {"wheelTreadReinfBeamDampCutoffHz":500,"wheelTreadReinfBeamPrecompression":0.99},

        {"wheelPeripheryBeamSpring":51000,"wheelPeripheryBeamDamp":55},
        {"wheelPeripheryBeamDampCutoffHz":500,"wheelPeripheryBeamPrecompression":0.99},

        {"wheelPeripheryReinfBeamSpring":51000,"wheelPeripheryReinfBeamDamp":55},
        {"wheelPeripheryReinfBeamDampCutoffHz":500,"wheelPeripheryReinfBeamPrecompression":0.99},

        //general tire values
        {"nodeWeight":0.125},
        {"nodeMaterial":"|NM_RUBBER"},
        {"triangleCollision":false},
        {"pressurePSI":"$tirepressure_R"},
        {"dragCoef":5},

        //groundmodel friction multipliers
        {"frictionCoef":1.3},
        {"slidingFrictionCoef":1.3},
        {"treadCoef":0},

        //advanced friction values
        {"noLoadCoef":2.1},
        {"loadSensitivitySlope":0.00048},
        {"fullLoadCoef":0.2},
        {"softnessCoef":1},

        //deform values
        {"wheelSideBeamDeform":28000,"wheelSideBeamStrength":32000},
        {"wheelSideReinfBeamDeform":28000,"wheelSideReinfBeamStrength":32000},
        {"wheelTreadBeamDeform":28000,"wheelTreadBeamStrength":32000},
        {"wheelPeripheryBeamDeform":70000,"wheelPeripheryBeamStrength":70000},
    ],
},
}