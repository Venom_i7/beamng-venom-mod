{
"sunburst_bigsplitter_01a_R": {
    "information":{
        "authors":"BeamNG",
        "name":"Nomi GTRX Front Right Splitter",
        "value":2200,
    },
    "slotType" : "sunburst_lip_FR",
    "flexbodies": [
         ["mesh", "[group]:", "nonFlexMaterials"],
         ["bigsplitter_01a_R", ["sunburst_splitter_R"],[],{"pos":{"x":0,"y":0,"z":0.0}}],
    ],
    "nodes": [
         ["id", "posX", "posY", "posZ"],
         //--splitter--
         {"nodeMaterial":"|NM_PLASTIC"},
         {"frictionCoef":0.5},
         {"group":"sunburst_splitter_R"},
         {"collision":true},
         {"nodeWeight":0.40},
         {"selfCollision":true},
         ["spl4r",-0.89, -1.31, 0.74],
         ["spl5r",-0.89, -1.65, 0.74],
         ["spl6r",-0.89, -1.66, 0.16],
         ["spl7r",-0.89, -2.15, 0.16],
         //rigidifier
         ["spl8r",-0.80, -1.73, 0.45,{"collision":false,"selfCollision":false}],
         {"group":""},
    ],
     "beams": [
          ["id1:", "id2:"],
           {"beamPrecompression":1, "beamType":"|NORMAL", "beamLongBound":1.0, "beamShortBound":1.0},
          //main shape
          {"beamSpring":365000,"beamDamp":50},
          {"beamDeform":14000,"beamStrength":18000},
          {"deformLimitExpansion":1.1},
          ["spl4r","spl5r"],
          ["spl6r","spl7r"],

          ["spl4r","spl6r"],
          ["spl5r","spl7r"],

          //cross
          {"deformLimitExpansion":""},
          ["spl4r","spl7r"],
          ["spl5r","spl6r"],
          //rigidifier
          {"beamSpring":265000,"beamDamp":50},
          {"beamDeform":3000,"beamStrength":18000},
          ["spl4r","spl8r"],
          ["spl5r","spl8r"],
          ["spl6r","spl8r"],
          ["spl7r","spl8r"],
          //attach
          {"beamSpring":125000,"beamDamp":150},
          {"beamDeform":3000,"beamStrength":4000},
          {"breakGroup":"splitter_R_a"},
          //to fender
          ["spl4r","fe5r"],
          ["spl4r","fe9r"],
          ["spl4r","fe6r"],
          ["spl4r","fe10r"],

          ["spl5r","fe6r"],
          ["spl5r","fe11r"],
          ["spl5r","fe10r"],

          ["spl8r","fe10r"],
          ["spl8r","fe12r"],

          //to bumper
          ["spl5r","fb2rr", {"optional":true}],
          ["spl7r","fb2rr", {"optional":true}],

          //to splitter
          {"breakGroup":"splitter_R_b"},
          {"optional":true},
          ["spl6r","spl1rr"],
          ["spl7r","spl1rr"],
          ["spl6r","spl2rr"],
          ["spl7r","spl2rr"],
          ["spl6r","spl1r"],
          ["spl7r","spl1r"],
          ["spl6r","spl2r"],
          ["spl7r","spl2r"],

          ["spl8r","spl1rr"],
          ["spl8r","spl2rr"],
          ["spl8r","spl3r"],
          {"optional":false},
          {"breakGroup":""},
          {"deformLimitExpansion":1.1},
    ],
    "triangles": [
        ["id1:","id2:","id3:"],
        {"dragCoef":10},
        {"groundModel":"plastic"},
        ["spl5r","spl6r","spl7r"],
        ["spl4r","spl6r","spl5r"],
    ],
},
"sunburst_bigsplitter_01a_L": {
    "information":{
        "authors":"BeamNG",
        "name":"Nomi GTRX Front Left Splitter",
        "value":2200,
    },
    "slotType" : "sunburst_lip_FL",
    "flexbodies": [
         ["mesh", "[group]:", "nonFlexMaterials"],
         ["bigsplitter_01a_L", ["sunburst_splitter_L"],[],{"pos":{"x":0,"y":0,"z":0.0}}],
    ],
    "nodes": [
         ["id", "posX", "posY", "posZ"],
         //--splitter--
         {"nodeMaterial":"|NM_PLASTIC"},
         {"frictionCoef":0.5},
         {"group":"sunburst_splitter_L"},
         {"collision":true},
         {"nodeWeight":0.40},
         {"selfCollision":true},
         ["spl4l", 0.89, -1.31, 0.74],
         ["spl5l", 0.89, -1.65, 0.74],
         ["spl6l", 0.89, -1.66, 0.16],
         ["spl7l", 0.89, -2.15, 0.16],
         //rigidifier
         ["spl8l", 0.80, -1.73, 0.45,{"collision":false,"selfCollision":false}],
         {"group":""},
    ],
     "beams": [
          ["id1:", "id2:"],
           {"beamPrecompression":1, "beamType":"|NORMAL", "beamLongBound":1.0, "beamShortBound":1.0},
          //main shape
          {"beamSpring":365000,"beamDamp":50},
          {"beamDeform":14000,"beamStrength":18000},
          {"deformLimitExpansion":1.1},
          ["spl4l","spl5l"],
          ["spl6l","spl7l"],

          ["spl4l","spl6l"],
          ["spl5l","spl7l"],

          //cross
          {"deformLimitExpansion":""},
          ["spl4l","spl7l"],
          ["spl5l","spl6l"],
          //rigidifier
          {"beamSpring":265000,"beamDamp":50},
          {"beamDeform":3000,"beamStrength":18000},
          ["spl4l","spl8l"],
          ["spl5l","spl8l"],
          ["spl6l","spl8l"],
          ["spl7l","spl8l"],
          //attach
          {"beamSpring":125000,"beamDamp":150},
          {"beamDeform":3000,"beamStrength":4000},
          {"breakGroup":"splitter_L_a"},
          //to fender
          ["spl4l","fe5l"],
          ["spl4l","fe9l"],
          ["spl4l","fe6l"],
          ["spl4l","fe10l"],

          ["spl5l","fe6l"],
          ["spl5l","fe11l"],
          ["spl5l","fe10l"],

          ["spl8l","fe10l"],
          ["spl8l","fe12l"],

          //to bumper
          ["spl5l","fb2ll", {"optional":true}],
          ["spl7l","fb2ll", {"optional":true}],

          //to splitter
          {"breakGroup":"splitter_L_b"},
          {"optional":true},
          ["spl6l","spl1ll"],
          ["spl7l","spl1ll"],
          ["spl6l","spl2ll"],
          ["spl7l","spl2ll"],
          ["spl6l","spl1l"],
          ["spl7l","spl1l"],
          ["spl6l","spl2l"],
          ["spl7l","spl2l"],

          ["spl8l","spl1ll"],
          ["spl8l","spl2ll"],
          ["spl8l","spl3l"],
          {"optional":false},
          {"breakGroup":""},
          {"deformLimitExpansion":1.1},
    ],
    "triangles": [
        ["id1:","id2:","id3:"],
        {"dragCoef":10},
        {"groundModel":"plastic"},
        ["spl6l","spl5l","spl7l"],
        ["spl4l","spl5l","spl6l"],
    ],
},
"sunburst_bigsplitter_01a": {
    "information":{
        "authors":"BeamNG",
        "name":"Nomi GTRX Front Splitter",
        "value":3600,
    },
    "slotType" : "sunburst_lip_F",
    "flexbodies": [
         ["mesh", "[group]:", "nonFlexMaterials"],
         ["bigsplitter_01a", ["sunburst_splitter"],[],{"pos":{"x":0,"y":0,"z":0.0}}],
         ["bigsplitter_01a_strut_R", ["sunburst_splitter","sunburst_bumper_F"],[],{"pos":{"x":0,"y":0,"z":0.0}}],
         ["bigsplitter_01a_strut_L", ["sunburst_splitter","sunburst_bumper_F"],[],{"pos":{"x":0,"y":0,"z":0.0}}],
    ],
    "variables": [
        ["name", "type", "unit", "category", "default", "min", "max", "title", "description"],
        //["$splitter_angle_F", "range", "", "Chassis", 1.0, 1.0, 1.04, "Front Splitter Angle", "Adjust the angle of the front splitter", {"minDis":0}]
    ],
    "nodes": [
         ["id", "posX", "posY", "posZ"],
         //--splitter--
         {"nodeMaterial":"|NM_PLASTIC"},
         {"frictionCoef":0.5},
         {"group":"sunburst_splitter"},
         {"collision":true},
         {"nodeWeight":0.40},
         {"selfCollision":true},
         ["spl1rr",-0.87, -1.66, 0.17],
         ["spl1r",-0.43, -1.95, 0.17],
         ["spl1", 0.0, -1.98, 0.17],
         ["spl1l", 0.43, -1.95, 0.17],
         ["spl1ll", 0.87, -1.66, 0.17],

         ["spl2rr",-0.87, -2.15, 0.16],
         ["spl2r",-0.43, -2.35, 0.16],
         ["spl2", 0.0, -2.37, 0.16],
         ["spl2l", 0.43, -2.35, 0.16],
         ["spl2ll", 0.87, -2.15, 0.16],

         //rigidifier
         {"collision":false},
         {"selfCollision":false},
         ["spl3r",-0.30, -2.20, 0.30],
         ["spl3l", 0.30, -2.20, 0.30],
         {"group":""},
    ],
     "beams": [
          ["id1:", "id2:"],
           {"beamPrecompression":1, "beamType":"|NORMAL", "beamLongBound":1.0, "beamShortBound":1.0},
          //main shape
          {"beamSpring":401000,"beamDamp":50},
          {"beamDeform":14000,"beamStrength":18000},
          {"deformLimitExpansion":1.1},
          ["spl1rr","spl1r"],
          ["spl1r","spl1"],
          ["spl1l","spl1"],
          ["spl1ll","spl1l"],

          ["spl2rr","spl2r"],
          ["spl2r","spl2"],
          ["spl2l","spl2"],
          ["spl2ll","spl2l"],

          ["spl1rr","spl2rr"],
          ["spl1r","spl2r"],
          ["spl1","spl2"],
          ["spl1l","spl2l"],
          ["spl1ll","spl2ll"],

          //cross
          {"deformLimitExpansion":""},
          ["spl1rr","spl2r"],
          ["spl1r","spl2rr"],
          ["spl1r","spl2"],

          ["spl1","spl2r"],
          ["spl1","spl2l"],

          ["spl1ll","spl2l"],
          ["spl1l","spl2ll"],
          ["spl1l","spl2"],

          //rigids
          {"beamSpring":301000,"beamDamp":50},
          ["spl1rr","spl1"],
          ["spl1r","spl1l"],
          ["spl1ll","spl1"],

          ["spl2rr","spl2"],
          ["spl2r","spl2l"],
          ["spl2ll","spl2"],

          //rigidifier
          {"beamSpring":351000,"beamDamp":50},
          {"beamDeform":3000,"beamStrength":18000},
          ["spl1rr","spl3r"],
          ["spl1r","spl3r"],
          ["spl1","spl3r"],
          ["spl1l","spl3r"],

          ["spl2rr","spl3r"],
          ["spl2r","spl3r"],
          ["spl2","spl3r"],
          ["spl2l","spl3r"],

          ["spl1ll","spl3l"],
          ["spl1l","spl3l"],
          ["spl1","spl3l"],
          ["spl1r","spl3l"],

          ["spl2ll","spl3l"],
          ["spl2l","spl3l"],
          ["spl2","spl3l"],
          ["spl2r","spl3l"],

          ["spl3r","spl3l"],

          //attach
          {"beamSpring":301000,"beamDamp":50},
          {"beamDeform":3000,"beamStrength":4000},
          {"breakGroup":"splitter_R"},
          ["spl1rr","fb5rr"],
          ["spl2rr","fb5rr"],
          ["spl1rr","fb3rr"],
          ["spl2rr","fb3rr"],
          ["spl1rr","fb4rr"],
          ["spl2rr","fb2rr"],
          {"breakGroup":"splitter_L"},
          ["spl1ll","fb5ll"],
          ["spl2ll","fb5ll"],
          ["spl1ll","fb3ll"],
          ["spl2ll","fb3ll"],
          ["spl1ll","fb4ll"],
          ["spl2ll","fb2ll"],
          {"breakGroup":""},
          //middle
          ["spl3r","fb2r"],
          ["spl3l","fb2l"],
          ["spl3r","fb2"],
          ["spl3l","fb2"],

          //to body
          {"beamSpring":301000,"beamDamp":50},
          ["spl2r","f15r"],
          ["spl2l","f15l"],
          ["spl2r","f15"],
          ["spl2l","f15"],
          ["spl2l","f18l"],
          ["spl2r","f18"],
          ["spl2l","f18"],
          ["spl2r","f18r"],
          {"breakGroup":""},
          {"deformLimitExpansion":1.1},
    ],
    "triangles": [
        ["id1:","id2:","id3:"],
        {"dragCoef":0},
        {"groundModel":"plastic"},
        //bottom
        //r
        ["spl2rr","spl1rr","spl2r"],
        ["spl2r", "spl1rr","spl1r"],
        ["spl2r", "spl1r",  "spl2"],
        ["spl2",  "spl1r", "spl1"],
        //l
        ["spl1ll","spl2ll","spl2l"],
        ["spl1l", "spl1ll","spl2l"],
        ["spl1l", "spl2l",  "spl2"],
        ["spl1",  "spl1l",  "spl2"],
        //top
        {"dragCoef":40},
        {"breakGroup":"splitter_R"},
        ["spl5r","spl7r","fb2rr", {"liftCoef":350, "stallAngle":2,"optional":true}],
        ["fb2rr","spl2rr","spl2r", {"liftCoef":350, "stallAngle":2}],
        ["fb2rr","spl2r","fb2r", {"liftCoef":350, "stallAngle":2}],
        ["fb2r","spl2r","spl2", {"liftCoef":350, "stallAngle":2}],
        ["fb2r","spl2","fb2", {"liftCoef":350, "stallAngle":2}],
        {"breakGroup":"splitter_L"},
        ["fb2ll","spl2l","spl2ll", {"liftCoef":350, "stallAngle":2}],
        ["fb2l","spl2l","fb2ll", {"liftCoef":350, "stallAngle":2}],
        ["fb2l","spl2","spl2l", {"liftCoef":350, "stallAngle":2}],
        ["fb2","spl2","fb2l", {"liftCoef":350, "stallAngle":2}],
        ["spl7l","spl5l","fb2ll", {"liftCoef":350, "stallAngle":2,"optional":true}],
        {"breakGroup":"frontbumper_M"},
        ["f15r", "fb1r", "fb1", {"liftCoef":200, "stallAngle":0.7}],
        ["fb1", "f15", "f15r", {"liftCoef":200, "stallAngle":0.7}],
        ["fb1l", "f15l", "fb1", {"liftCoef":200, "stallAngle":0.7}],
        ["f15", "fb1", "f15l", {"liftCoef":200, "stallAngle":0.7}],
        {"group":""},
        {"breakGroup":""},
    ],
},
}