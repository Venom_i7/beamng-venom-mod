{
"tire_F_285_40_15_sport": {
    "information":{
        "authors":"BeamNG",
        "name":"285/40R15 Sport Front Tires",
        "value":190,
    },
    "slotType" : "tire_F_15x10",
    "flexbodies": [
        ["mesh", "[group]:", "nonFlexMaterials"],
        ["bolide_80s_285_15", ["wheel_FR","tire_FR"], [], {"pos":{"x":-0.532, "y":0.0, "z":0.0}, "rot":{"x":0, "y":0, "z":180}, "scale":{"x":1, "y":0.992, "z":0.992}}],
        ["bolide_80s_285_15", ["wheel_FL","tire_FL"], [], {"pos":{"x": 0.532, "y":0.0, "z":0.0}, "rot":{"x":0, "y":0, "z":0},   "scale":{"x":1, "y":0.992, "z":0.992}}],
    ],
    "variables": [
        ["name", "type", "unit", "category", "default", "min", "max", "title", "description"],
        ["$tirepressure_F", "range", "psi", "Wheels", 27, 0, 50, "Tire Pressure", "Relative to atmospheric pressure", {"subCategory":"Rear"}],
    ],
    "pressureWheels": [
        ["name","hubGroup","group","node1:","node2:","nodeS","nodeArm:","wheelDir"],
        {"disableMeshBreaking":false,"disableHubMeshBreaking":false},
        {"hasTire":true},
        {"enableTireReinfBeams":false},
        {"enableTireLbeams":true},
        {"enableTireSideReinfBeams":false},
        {"enableTreadReinfBeams":true},
        {"enableTirePeripheryReinfBeams":true},

        //general settings
        {"radius":0.305},
        {"tireWidth":0.275},
        {"numRays":16},
        //--Force 16 RAY WHEEL--

        //tire options
        {"wheelSideBeamSpring":"$=$tirepressure_F*371","wheelSideBeamDamp":35},
        {"wheelSideBeamSpringExpansion":481000,"wheelSideBeamDampExpansion":35},
        {"wheelSideTransitionZone":0.10,"wheelSideBeamPrecompression":0.985},

        {"wheelReinfBeamSpring":34000,"wheelReinfBeamDamp":200},
        {"wheelReinfBeamDampCutoffHz":500,"wheelReinfBeamPrecompression":0.98},

        {"wheelTreadBeamSpring":91000,"wheelTreadBeamDamp":95},
        {"wheelTreadBeamDampCutoffHz":500,"wheelTreadBeamPrecompression":0.98},

        {"wheelTreadReinfBeamSpring":181000,"wheelTreadReinfBeamDamp":65},
        {"wheelTreadReinfBeamDampCutoffHz":500,"wheelTreadReinfBeamPrecompression":0.98},

        {"wheelPeripheryBeamSpring":91000,"wheelPeripheryBeamDamp":45},
        {"wheelPeripheryBeamDampCutoffHz":500,"wheelPeripheryBeamPrecompression":0.98},

        {"wheelPeripheryReinfBeamSpring":181000,"wheelPeripheryReinfBeamDamp":45},
        {"wheelPeripheryReinfBeamDampCutoffHz":500,"wheelPeripheryReinfBeamPrecompression":0.98},

        //general tire values
        {"nodeWeight":0.175},
        {"nodeMaterial":"|NM_RUBBER"},
        {"triangleCollision":false},
        {"pressurePSI":"$tirepressure_F"},
        {"dragCoef":5},

        //groundmodel friction multipliers
        {"frictionCoef":1.0},
        {"slidingFrictionCoef":1.0},
        {"treadCoef":0.5},

        //advanced friction values
        {"noLoadCoef":1.40},
        {"loadSensitivitySlope":0.000132},
        {"fullLoadCoef":0.55},
        {"softnessCoef":0.8},

        //deform values
        {"wheelSideBeamDeform":15000,"wheelSideBeamStrength":20000},
        {"wheelTreadBeamDeform":11000,"wheelTreadBeamStrength":14000},
        {"wheelPeripheryBeamDeform":50000,"wheelPeripheryBeamStrength":50000},
    ],
},
"tire_F_275_50_15_sport": {
    "information":{
        "authors":"BeamNG",
        "name":"275/50R15 Sport Front Tires",
        "value":190,
    },
    "slotType" : "tire_F_15x10",
    "flexbodies": [
        ["mesh", "[group]:", "nonFlexMaterials"],
        ["tire_01a_15x10_25", ["wheel_FR","tire_FR"], [], {"pos":{"x":-0.53, "y":0.0, "z":0.0}, "rot":{"x":0, "y":0, "z":180}, "scale":{"x":1.125, "y":1.0, "z":1.0}}],
        ["tire_01a_15x10_25", ["wheel_FL","tire_FL"], [], {"pos":{"x": 0.53, "y":0.0, "z":0.0}, "rot":{"x":0, "y":0, "z":0},   "scale":{"x":1.125, "y":1.0, "z":1.0}}],
    ],
    "variables": [
        ["name", "type", "unit", "category", "default", "min", "max", "title", "description"],
        ["$tirepressure_F", "range", "psi", "Wheels", 27, 0, 50, "Tire Pressure", "Relative to atmospheric pressure", {"subCategory":"Rear"}],
    ],
    "pressureWheels": [
        ["name","hubGroup","group","node1:","node2:","nodeS","nodeArm:","wheelDir"],
        {"disableMeshBreaking":false,"disableHubMeshBreaking":false},
        {"hasTire":true},
        {"enableTireReinfBeams":false},
        {"enableTireLbeams":true},
        {"enableTireSideReinfBeams":false},
        {"enableTreadReinfBeams":true},
        {"enableTirePeripheryReinfBeams":true},

        //general settings
        {"radius":0.322},
        {"tireWidth":0.255},
        {"numRays":16},
        //--Force 16 RAY WHEEL--

        //tire options
        {"wheelSideBeamSpring":"$=$tirepressure_F*443","wheelSideBeamDamp":35},
        {"wheelSideBeamSpringExpansion":481000,"wheelSideBeamDampExpansion":35},
        {"wheelSideTransitionZone":0.10,"wheelSideBeamPrecompression":0.985},

        {"wheelReinfBeamSpring":25000,"wheelReinfBeamDamp":200},
        {"wheelReinfBeamDampCutoffHz":500,"wheelReinfBeamPrecompression":0.98},

        {"wheelTreadBeamSpring":85000,"wheelTreadBeamDamp":95},
        {"wheelTreadBeamDampCutoffHz":500,"wheelTreadBeamPrecompression":0.98},

        {"wheelTreadReinfBeamSpring":175000,"wheelTreadReinfBeamDamp":65},
        {"wheelTreadReinfBeamDampCutoffHz":500,"wheelTreadReinfBeamPrecompression":0.98},

        {"wheelPeripheryBeamSpring":85000,"wheelPeripheryBeamDamp":45},
        {"wheelPeripheryBeamDampCutoffHz":500,"wheelPeripheryBeamPrecompression":0.98},

        {"wheelPeripheryReinfBeamSpring":170000,"wheelPeripheryReinfBeamDamp":45},
        {"wheelPeripheryReinfBeamDampCutoffHz":500,"wheelPeripheryReinfBeamPrecompression":0.98},

        //general tire values
        {"nodeWeight":0.175},
        {"nodeMaterial":"|NM_RUBBER"},
        {"triangleCollision":false},
        {"pressurePSI":"$tirepressure_F"},
        {"dragCoef":5},

        //groundmodel friction multipliers
        {"frictionCoef":1.0},
        {"slidingFrictionCoef":1.0},
        {"treadCoef":0.5},

        //advanced friction values
        {"noLoadCoef":1.40},
        {"loadSensitivitySlope":0.000132},
        {"fullLoadCoef":0.55},
        {"softnessCoef":0.8},

        //deform values
        {"wheelSideBeamDeform":15000,"wheelSideBeamStrength":20000},
        {"wheelTreadBeamDeform":11000,"wheelTreadBeamStrength":14000},
        {"wheelPeripheryBeamDeform":50000,"wheelPeripheryBeamStrength":50000},
    ],
},
}