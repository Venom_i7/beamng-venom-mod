{"bolide_chassis":
{
    "information":{
        "authors":"BeamNG",
        "name":"Chassis",
        "value":6500,
    },

    "slotType" :"bolide_chassis",

    "slots":[
        ["type", "default", "description"],
        //bodywork
        ["bolide_body","bolide_body", "Body"],
        ["bolide_sideskirt_R","bolide_sideskirt_a_R", "Right Side Skirt"],
        ["bolide_sideskirt_L","bolide_sideskirt_a_L", "Left Side Skirt"],
        //subframes
        ["bolide_subframe_F","bolide_subframe_F", "Front Subframe"],
        ["bolide_subframe_R","bolide_subframe_R", "Rear Subframe"],
        //interior
        ["bolide_steer","bolide_steer", "Steering Wheel"],
        ["bolide_shifter","bolide_shifter_M", "Shifter"],
        ["bolide_pedals","bolide_pedals", "Pedals"],
        ["bolide_seat_FL","bolide_seat_FL", "Driver Seat"],
        ["bolide_seat_FR","bolide_seat_FR", "Passenger Seat"],
    ],
    "flexbodies":[
        ["mesh", "[group]:", "nonFlexMaterials"],
         //body
         ["bolide_chassis", ["bolide_chassis"]],
         ["bolide_intbase", ["bolide_chassis", "bolide_dash"]],
    ],

    "refNodes":[
        ["ref:", "back:", "left:", "up:", "leftCorner:", "rightCorner:"],
        ["f2r", "f3r", "f2l", "ref1r", "f1ll", "f1rr"],
    ],
    "cameraChase":{
        "distance":4.8,
        "distanceMin":2,
        "defaultRotation":{"x":0,"y":-12,"z":0},
        "offset":{"x":0.35, "y":0.3, "z":1.2},
        "fov":65,
    },
    "cameraExternal":{
        "distance":4.8,
        "distanceMin":2,
        "offset":{"x":0.35, "y":0.3, "z":0.3},
        "fov":65,
    },
    "camerasInternal":[
        ["type", "x", "y", "z", "fov", "id1:", "id2:", "id3:", "id4:", "id5:", "id6:"],
        {"nodeWeight":1.9},
        {"selfCollision":false},
        {"collision":false},
        {"beamSpring":60000,"beamDamp":300},
        //hood cam
        ["hood", 0.0, -0.61, 1.01, 65, "f1r","f1l","f2r","f2l","f6r","f6l", {"beamDeform":5001000,"beamStrength":"FLT_MAX"}],
        {"beamSpring":1100, "beamDamp":150},
        {"selfCollision":true},
        {"collision":true},
        //dash cam
        ["dash", 0.378, 0.28, 1.0, 55, "f1r","f1ll","f2r","f2ll","f3r","f3ll", {"beamDeform":5001000,"beamStrength":"FLT_MAX"}],
    ],
   "torsionbars":[
       ["id1:", "id2:", "id3:", "id4:"],
        //rigidify firewalls
        {"spring":1000000, "damp":50, "deform":10000, "strength":100000},
        ["f6l", "f1l", "f1r", "f3r"],
        ["f6r", "f1r", "f1l", "f3l"],

        ["f9l", "f3l", "f3r", "f1r"],
        ["f9r", "f3r", "f3l", "f1l"],

        {"optional":true}
        //front subframe
        ["f6l", "f1l", "f1r", "fx8r"],
        ["f6r", "f1r", "f1l", "fx8l"],

        ["f3l", "f1l", "f1r", "fx9r"],
        ["f3r", "f1r", "f1l", "fx9l"],
        //rear subframe
        ["f9l", "f3l", "f3r", "rx2r"],
        ["f9r", "f3r", "f3l", "rx2l"],

        ["f1l", "f3l", "f3r", "rx3r"],
        ["f1r", "f3r", "f3l", "rx3l"],
        {"optional":false}

        //rigify the column
        {"spring":20000, "damp":1, "deform":18000, "strength":28000},
        ["int_strw", "dshl", "dsh", "f5l"],
    ],
    "nodes":[
         ["id", "posX", "posY", "posZ"],
         {"frictionCoef":0.5},
         {"nodeMaterial":"|NM_METAL"},

         //--CHASSIS--
         {"selfCollision":true},
         {"collision":true},

         //floor
         {"nodeWeight":3.3},
         {"group":["bolide_chassis","bolide_floor"]},
         ["f1rr",-0.785,-0.709,0.193],
         {"chemEnergy":1000,"burnRate":0.52,"flashPoint":300,"specHeat":0.8,"smokePoint":150,"selfIgnitionCoef":false},
         ["f1r",-0.35,-0.911,0.157, {"couplerTag":"subframe_F_2r", "couplerStrength":190000, "couplerRadius":0.1, "couplerStartRadius":0.1, "couplerLock":true, "breakGroup":"subframe_FR1"}],
         ["f1l",0.35,-0.911,0.157, {"couplerTag":"subframe_F_2l", "couplerStrength":190000, "couplerRadius":0.1, "couplerStartRadius":0.1, "couplerLock":true, "breakGroup":"subframe_FL1"}],
         {"chemEnergy":false,"burnRate":false,"flashPoint":false,"specHeat":false,"smokePoint":false,"selfIgnitionCoef":false},
         ["f1ll",0.785,-0.709,0.193],

         ["f2rr",-0.785,-0.25,0.193],
         {"chemEnergy":1000,"burnRate":0.52,"flashPoint":300,"specHeat":0.8,"smokePoint":150,"selfIgnitionCoef":false},
         ["f2r",-0.35,-0.25,0.157],
         ["f2l",0.35,-0.25,0.157],
         {"chemEnergy":false,"burnRate":false,"flashPoint":false,"specHeat":false,"smokePoint":false,"selfIgnitionCoef":false},
         ["f2ll",0.785,-0.25,0.193],

         ["f3rr",-0.785,0.31,0.193, {"couplerTag":"subframe_R_1rr", "couplerStrength":160000, "couplerRadius":0.1, "couplerStartRadius":0.1, "couplerLock":true, "breakGroup":"subframe_RR1"}],
         {"chemEnergy":1000,"burnRate":0.52,"flashPoint":300,"specHeat":0.8,"smokePoint":150,"selfIgnitionCoef":false},
         ["f3r",-0.35,0.338,0.157, {"couplerTag":"subframe_R_1r", "couplerStrength":220000, "couplerRadius":0.1, "couplerStartRadius":0.1, "couplerLock":true, "breakGroup":"subframe_RR1"}],
         ["f3l",0.35,0.338,0.157, {"couplerTag":"subframe_R_1l", "couplerStrength":220000, "couplerRadius":0.1, "couplerStartRadius":0.1, "couplerLock":true, "breakGroup":"subframe_RL1"}],
         {"chemEnergy":false,"burnRate":false,"flashPoint":false,"specHeat":false,"smokePoint":false,"selfIgnitionCoef":false},
         ["f3ll",0.785,0.31,0.193, {"couplerTag":"subframe_R_1ll", "couplerStrength":160000, "couplerRadius":0.1, "couplerStartRadius":0.1, "couplerLock":true, "breakGroup":"subframe_RL1"}],

         //front firewall
         {"nodeWeight":3.0},
         {"group":"bolide_chassis"},
         ["f5rr",-0.785,-0.773,0.446],
         ["f5r",-0.404,-0.938,0.428, {"couplerTag":"subframe_F_1r", "couplerStrength":190000, "couplerRadius":0.1, "couplerStartRadius":0.1, "couplerLock":true, "breakGroup":"subframe_FR2"}],
         ["f5l",0.404,-0.938,0.428, {"couplerTag":"subframe_F_1l", "couplerStrength":190000, "couplerRadius":0.1, "couplerStartRadius":0.1, "couplerLock":true, "breakGroup":"subframe_FL2"}],
         ["f5ll",0.785,-0.773,0.446],

         //relocated by body, weight added by glass
         ["f6rr",-0.785,-0.836,0.699, {"group":["bolide_chassis","bolide_windshield"]}],
         ["f6r",-0.445,-0.961,0.664, {"group":["bolide_chassis","bolide_windshield"]}],
         ["f6l",0.445,-0.961,0.664, {"group":["bolide_chassis","bolide_windshield"]}],
         ["f6ll",0.785,-0.836,0.699, {"group":["bolide_chassis","bolide_windshield"]}],

         //thickness
         ["f7rr",-0.785,-0.25,0.348],
         ["f7",0.0,-0.25,0.44, {"nodeWeight":5.5}],
         ["f7ll",0.785,-0.25,0.348],

         //rear firewall
         ["f8rr",-0.785,0.383,0.454, {"selfCollision":false}],
         ["f8ll",0.785,0.383,0.454, {"selfCollision":false}],

         ["f9rr",-0.785,0.456,0.715],
         ["f9r",-0.483,0.485,0.681, {"couplerTag":"subframe_R_2r", "couplerStrength":190000, "couplerRadius":0.1, "couplerStartRadius":0.1, "couplerLock":true, "breakGroup":"subframe_RR2"}],
         ["f9l",0.483,0.485,0.681,  {"couplerTag":"subframe_R_2l", "couplerStrength":190000, "couplerRadius":0.1, "couplerStartRadius":0.1, "couplerLock":true, "breakGroup":"subframe_RL2"}],
         ["f9ll",0.785,0.456,0.715],

         //up refnode
         {"nodeWeight":1.0},
         ["ref1r",-0.35,-0.25,0.44, {"group":""}],

         //dash
         {"group":"bolide_dash"},
         {"nodeWeight":1.0},
         ["dshr",-0.721,-0.544,0.722],
         ["dsh",0.0,-0.544,0.722],
         ["dshl",0.721,-0.544,0.722],
         ["int_strw",  0.375, -0.347, 0.7075],
         {"group":""},
         {"nodeWeight":0.2},
         ["int_pbrk", -0.05, 0.00, 0.39],
         ["int_shft", 0.02, -0.17, 0.37],
        ],

    "beams":[
          ["id1:", "id2:"],
          //--EXPANSION DEFORM LIMIT DEFAULT--
          {"deformLimitExpansion":1.2},

          //--MISC--
          {"beamPrecompression":1, "beamType":"|NORMAL", "beamLongBound":1, "beamShortBound":1},

          //up refnode
          {"beamSpring":501000,"beamDamp":500},
          {"beamDeform":30000,"beamStrength":"FLT_MAX"},
          {"deformLimitExpansion":""},
          ["ref1r", "f2r"],
          ["ref1r", "f7"],
          ["ref1r", "f2l"],
          ["ref1r", "f1r"],
          ["ref1r", "f3r"],
          ["ref1r", "f2rr"],

          //breakable beams
          {"beamSpring":1001000,"beamDamp":132},
          {"beamDeform":6500,"beamStrength":19500},
          {"disableMeshBreaking":true,"disableTriangleBreaking":true},
          {"deformLimitExpansion":""},
          ["f6ll", "f3ll"],
          ["f6rr", "f3rr"],
          //{"beamDeform":14500,"beamStrength":39500},
          ["f9ll", "f1ll"],
          ["f9rr", "f1rr"],


          //torsion bar break beams
          {"beamSpring":1000, "beamDamp":1},
          {"beamDeform":150, "beamStrength":5550},
          {"optional":true},
          ["f6l", "f1l"],
          ["f6r", "f1r"],
          ["f1r", "f3r"],
          ["f1l", "f3l"],

          ["f1r", "fx9r", {"breakGroup":"subframe_FR1", "beamStrength":"FLT_MAX"}],
          ["f1l", "fx9l", {"breakGroup":"subframe_FL1", "beamStrength":"FLT_MAX"}],

          ["rx2r", "f3r", {"breakGroup":"subframe_RR1", "beamStrength":"FLT_MAX"}],
          ["rx2l", "f3l", {"breakGroup":"subframe_RL1", "beamStrength":"FLT_MAX"}],
          {"optional":false},
          {"disableMeshBreaking":false,"disableTriangleBreaking":false},

          //--CHASSIS--

          //enticer
          {"beamSpring":801000,"beamDamp":125},
          {"beamDeform":35000,"beamStrength":"FLT_MAX"},
          {"deformLimitExpansion":""},
          ["f6rr", "f3rr", {"optional":true}],
          ["f6ll", "f3ll", {"optional":true}],

          //floor main shape
          {"deformLimitExpansion":1.2},
          {"beamSpring":4001000,"beamDamp":70},
          {"beamDeform":40000,"beamStrength":"FLT_MAX"},
          //widthwise
          ["f1rr", "f1r"],
          ["f1r", "f1l"],
          ["f1l", "f1ll"],
          ["f2rr", "f2r"],
          ["f2r", "f2l"],
          ["f2l", "f2ll"],
          ["f3rr", "f3r"],
          ["f3r", "f3l"],
          ["f3l", "f3ll"],
          //lengthwise
          {"beamDeform":35000,"beamStrength":"FLT_MAX"},
          ["f1rr", "f2rr"],
          ["f2rr", "f3rr"],
          ["f1r", "f2r"],
          ["f2r", "f3r"],
          ["f1l", "f2l"],
          ["f2l", "f3l"],
          ["f1ll", "f2ll"],
          ["f2ll", "f3ll"],

          //surficial crossing
          {"deformLimitExpansion":""},
          {"beamDeform":25000,"beamStrength":"FLT_MAX"},
          ["f2rr", "f1r"],
          ["f2r", "f1rr"],
          ["f1r", "f2l"],
          ["f1l", "f2r"],
          ["f1l", "f2ll"],
          ["f1ll", "f2l"],
          ["f2l", "f3ll"],
          ["f2ll", "f3l"],
          ["f3l", "f2r"],
          ["f2l", "f3r"],
          ["f3r", "f2rr"],
          ["f2r", "f3rr"],

          //rigids
          {"beamDeform":18000,"beamStrength":"FLT_MAX"},
          //lengthwise
          ["f3rr", "f1rr"],
          ["f3r", "f1r"],
          ["f3l", "f1l"],
          ["f3ll", "f1ll"],
          //widthwise
          ["f1ll", "f1r"],
          ["f1l", "f1rr"],
          ["f2ll", "f2rr"],
          ["f2l", "f2rr"],
          ["f3ll", "f3r"],
          ["f3l", "f3rr"],

          //front firewall main shape
          {"deformLimitExpansion":1.2},
          {"beamSpring":4501000,"beamDamp":100},
          {"beamDeform":30000,"beamStrength":"FLT_MAX"},
          //vertical
          ["f1l", "f5l"],
          ["f5l", "f6l"],
          ["f1ll", "f5ll"],
          ["f5ll", "f6ll"],
          ["f1r", "f5r"],
          ["f5r", "f6r"],
          ["f1rr", "f5rr"],
          ["f5rr", "f6rr"],
          //widthwise
          {"beamDeform":20000,"beamStrength":"FLT_MAX"},
          ["f5rr", "f5r"],
          ["f5r", "f5l"],
          ["f5l", "f5ll"],
          ["f6rr", "f6r"],
          ["f6r", "f6l"],
          ["f6l", "f6ll"],

          //surficial crossing
          {"deformLimitExpansion":""},
          {"beamDeform":15000,"beamStrength":"FLT_MAX"},
          ["f5rr", "f1r"],
          ["f1rr", "f5r"],
          ["f5r", "f6rr"],
          ["f5rr", "f6r"],
          ["f5r", "f1l"],
          ["f1r", "f5l"],
          ["f5l", "f6r"],
          ["f5r", "f6l"],
          ["f6l", "f5ll"],
          ["f6ll", "f5l"],
          ["f5l", "f1ll"],
          ["f1l", "f5ll"],

          //rigids
          {"beamDeform":8000,"beamStrength":"FLT_MAX"},
          //widthwise
          ["f5rr", "f5l"],
          ["f5ll", "f5r"],
          ["f6rr", "f6l"],
          ["f6ll", "f6r"],
          //vertical
          {"beamDeform":12000,"beamStrength":"FLT_MAX"},
          ["f1rr", "f6rr"],
          ["f1r", "f6r"],
          ["f1l", "f6l"],
          ["f1ll", "f6ll"],
          //corner to floor
          {"beamDeform":9000,"beamStrength":"FLT_MAX"},
          ["f6r", "f2r"],
          ["f6l", "f2l"],
          ["f5l", "f2l"],
          ["f5r", "f2r"],

          //rear firewall main shape
          {"deformLimitExpansion":1.2},
          {"beamSpring":4501000,"beamDamp":100},
          {"beamDeform":30000,"beamStrength":"FLT_MAX"},
          //vertical
          ["f3rr", "f8rr"],
          ["f8rr", "f9rr"],
          ["f3r", "f9r"],
          ["f3l", "f9l"],
          ["f3ll", "f8ll"],
          ["f8ll", "f9ll"],
          //widthwise
          {"beamDeform":20000,"beamStrength":"FLT_MAX"},
          ["f9rr", "f9r"],
          ["f9r", "f9l"],
          ["f9l", "f9ll"],

          //surficial crossing
          {"deformLimitExpansion":""},
          {"beamDeform":15000,"beamStrength":"FLT_MAX"},
          ["f8rr", "f3r"],
          ["f8rr", "f9r"],
          ["f8ll", "f3l"],
          ["f8ll", "f9l"],
          ["f9ll", "f3l"],
          ["f3ll", "f9l"],
          ["f9rr", "f3r"],
          ["f3rr", "f9r"],
          ["f3r", "f9l"],
          ["f3l", "f9r"],

          //rigids
          {"beamDeform":8000,"beamStrength":"FLT_MAX"},
          //widthwise
          ["f9rr", "f9l"],
          ["f9ll", "f9r"],
          //vertical
          {"beamDeform":12000,"beamStrength":"FLT_MAX"},
          ["f3rr", "f9rr"],
          ["f3ll", "f9ll"],
          //corner to floor
          {"beamDeform":9000,"beamStrength":"FLT_MAX"},
          ["f9l", "f2l"],
          ["f9r", "f2r"],

          //chassis thickness main shape
          {"deformLimitExpansion":1.2},
          {"beamSpring":4501000,"beamDamp":100},
          {"beamDeform":30000,"beamStrength":"FLT_MAX"},
          //vertical
          ["f7", "f2r"],
          ["f7", "f2l"],
          //lengthwise
          {"beamDeform":20000,"beamStrength":"FLT_MAX"},
          {"deformLimitExpansion":""},
          ["f7", "f3l"],
          ["f7", "f3r"],
          ["f7", "f1l"],
          ["f7", "f1r"],

          //rigids
          {"beamDeform":15000,"beamStrength":"FLT_MAX"},
          //corner
          ["f7", "f5l"],
          ["f7", "f5r"],
          ["f7", "f9l"],
          ["f7", "f9r"],
          //widthwise
          ["f7", "f2ll"],
          ["f7", "f2rr"],

          //tub sides main shape
          {"deformLimitExpansion":1.2},
          {"beamSpring":4501000,"beamDamp":100},
          {"beamDeform":30000,"beamStrength":"FLT_MAX"},
          //vertical
          ["f2ll", "f7ll"],
          ["f2rr", "f7rr"],
          //lengthwise
          {"beamDeform":20000,"beamStrength":"FLT_MAX"},
          ["f5rr", "f7rr"],
          ["f7rr", "f8rr"],
          ["f5ll", "f7ll"],
          ["f7ll", "f8ll"],

          //surficial crossing
          {"deformLimitExpansion":""},
          {"beamDeform":15000,"beamStrength":"FLT_MAX"},
          ["f5rr", "f2rr"],
          ["f7rr", "f1rr"],
          ["f7rr", "f3rr"],
          ["f2rr", "f8rr"],
          ["f1ll", "f7ll"],
          ["f2ll", "f5ll"],
          ["f2ll", "f8ll"],
          ["f3ll", "f7ll"],

          //rigids
          {"beamDeform":12000,"beamStrength":"FLT_MAX"},
          //lengthwise
          ["f5ll", "f8ll"],
          ["f5rr", "f8rr"],
          //corner
          ["f6ll", "f7ll"],
          ["f6rr", "f7rr"],
          ["f9ll", "f7ll"],
          ["f9rr", "f7rr"],
          ["f7rr", "f2r"],
          ["f7ll", "f2l"],

          //--DASHBOARD--
          {"beamSpring":1501000,"beamDamp":100},
          {"beamDeform":12000,"beamStrength":"FLT_MAX"},
          {"deformLimitExpansion":""},
          ["dsh", "dshl"],
          ["dsh", "dshr"],
          ["dshl", "f6ll"],
          ["dsh", "f6l"],
          ["dsh", "f6r"],
          ["dshr", "f6rr"],
          ["dshr", "f6r"],
          ["dshl", "f6l"],
          ["dshl", "f1ll"],
          ["dshr", "f1rr"],
          ["dsh", "f5l"],
          ["dsh", "f5r"],
          ["dsh", "f7"],
          ["dshr", "f5r"],
          ["dshl", "f5l"],
          ["dsh", "f1l"],
          ["dsh", "f1r"],
          ["dshl", "f1l"],
          ["dshr", "f1r"],
          ["dshr", "f7"],
          ["dshl", "f7"],
          ["dsh","int_strw"],
          ["dshl","int_strw"],
          {"beamSpring":160100,"beamDamp":142.73},
          {"beamDeform":70300,"beamStrength":"FLT_MAX"},
          ["int_pbrk","f5l"],
          ["int_pbrk","f5r"],
          ["int_pbrk","f2r"],
          ["int_pbrk","f2l"],
          ["int_shft","f5l"],
          ["int_shft","f5r"],
          ["int_shft","f2r"],
          ["int_shft","f2l"],
          //bleed through
          {"deformLimitExpansion":1.2},
    ],

    "triangles":[
        ["id1:", "id2:", "id3:"],
        {"groundModel":"metal"},

        //Dashboard
        {"dragCoef":0},
        ["dshl", "f6ll", "f5ll"],
        ["f6ll", "dshl", "f6l"],
        ["f6l", "dshl", "dsh"],
        ["dsh", "f6r", "f6l"],
        ["dsh", "dshr", "f6r"],
        ["f6r", "dshr", "f6rr"],
        ["dshr", "f5rr", "f6rr"],
        ["dshl", "f5ll", "f5l"],
        ["f5l", "dsh", "dshl"],
        ["f5l", "f5r", "dsh"],
        ["dsh", "f5r", "dshr"],
        ["dshr", "f5r", "f5rr"],

        {"dragCoef":15},
        //underside
        {"group":"bolide_tub"},
        ["f1rr", "f2r", "f1r"],
        ["f1r", "f2r", "f1l"],
        ["f1l", "f2l", "f1ll"],
        ["f1l", "f2r", "f2l"],
        ["f2l", "f2ll", "f1ll"],
        ["f1rr", "f2rr", "f2r"],

        {"dragCoef":0},
        //fix downforce issue
        ["f2rr", "f3rr", "f2r"],
        ["f2r", "f3rr", "f3r"],
        ["f2r", "f3r", "f2l"],
        ["f2l", "f3r", "f3l"],
        ["f3l", "f2ll", "f2l"],
        ["f3l", "f3ll", "f2ll"],

        //front firewall
        {"dragCoef":5},
        {"group":"bolide_firewall_F"},
        ["f5rr", "f1rr", "f1r"],
        ["f1l", "f1ll", "f5ll"],
        ["f5l", "f1l", "f5ll"],
        ["f5rr", "f1r", "f5r"],
        ["f6r", "f5rr", "f5r"],
        ["f6l", "f5l", "f5ll"],
        ["f6l", "f5ll", "f6ll"],
        ["f6rr", "f5rr", "f6r"],
        ["f6r", "f5r", "f5l"],
        ["f5l", "f6l", "f6r"],
        ["f5r", "f1r", "f1l"],
        ["f1l", "f5l", "f5r"],

        //rear firewall
        {"dragCoef":10},
        {"group":"bolide_firewall_R"},
        ["f8ll", "f3ll", "f3l"],
        ["f9ll", "f8ll", "f9l"],
        ["f9l", "f8ll", "f3l"],
        ["f8rr", "f3r", "f3rr"],
        ["f9r", "f8rr", "f9rr"],
        ["f9r", "f3r", "f8rr"],
        ["f9r", "f3l", "f3r"],
        ["f9l", "f3l", "f9r"],


        //sides
        {"dragCoef":7},
        {"group":"bolide_tubside_R"},
        ["f1rr", "f5rr", "f7rr"],
        ["f1rr", "f7rr", "f2rr"],
        ["f2rr", "f7rr", "f3rr"],
        ["f3rr", "f7rr", "f8rr"],
        {"group":"bolide_tubside_L"},
        ["f5ll", "f1ll", "f7ll"],
        ["f7ll", "f1ll", "f2ll"],
        ["f7ll", "f2ll", "f3ll"],
        ["f7ll", "f3ll", "f8ll"],
        {"group":""},
        ],
},
}