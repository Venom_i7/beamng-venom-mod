{
"tire_F_245_35_17_alt_drift": {
    "information":{
        "authors":"BeamNG",
        "name":"245/35R17 Drift Front Tires",
        "value":200,
    },
    "slotType" : "tire_F_17x9_alt",
    "flexbodies": [
        ["mesh", "[group]:", "nonFlexMaterials"],
        ["tire_01f_17x9_24", ["wheel_FR","tire_FR"], [], {"pos":{"x":-0.48, "y":0.0, "z":0.0}, "rot":{"x":0, "y":0, "z":180}, "scale":{"x":0.99, "y":0.995, "z":0.995}}],
        ["tire_01f_17x9_24", ["wheel_FL","tire_FL"], [], {"pos":{"x": 0.48, "y":0.0, "z":0.0}, "rot":{"x":0, "y":0, "z":0},   "scale":{"x":0.99, "y":0.995, "z":0.995}}],
    ],
    "variables": [
        ["name", "type", "unit", "category", "default", "min", "max", "title", "description"],
        ["$tirepressure_F", "range", "psi", "Wheels", 28, 0, 50, "Tire Pressure", "Relative to atmospheric pressure", {"subCategory":"Front"}],
    ],
    "pressureWheels": [
        ["name","hubGroup","group","node1:","node2:","nodeS","nodeArm:","wheelDir"],
        {"disableMeshBreaking":false,"disableHubMeshBreaking":false},
        {"hasTire":true},
        {"enableTireReinfBeams":false},
        {"enableTireLbeams":true},
        {"enableTireSideReinfBeams":false},
        {"enableTreadReinfBeams":true},
        {"enableTirePeripheryReinfBeams":true},

        //general settings
        {"radius":0.305},
        {"tireWidth":0.225},

        //tire options
        {"wheelSideBeamSpring":"$=$tirepressure_F*520","wheelSideBeamDamp":25},
        {"wheelSideBeamSpringExpansion":441000,"wheelSideBeamDampExpansion":25},
        {"wheelSideTransitionZone":0.1,"wheelSideBeamPrecompression":0.99},

        {"wheelReinfBeamSpring":24000,"wheelReinfBeamDamp":180},
        {"wheelReinfBeamDampCutoffHz":500,"wheelReinfBeamPrecompression":0.99},

        {"wheelTreadBeamSpring":90000,"wheelTreadBeamDamp":75},
        {"wheelTreadBeamDampCutoffHz":500,"wheelTreadBeamPrecompression":0.99},

        {"wheelTreadReinfBeamSpring":160000,"wheelTreadReinfBeamDamp":60},
        {"wheelTreadReinfBeamDampCutoffHz":500,"wheelTreadReinfBeamPrecompression":0.99},

        {"wheelPeripheryBeamSpring":90000,"wheelPeripheryBeamDamp":35},
        {"wheelPeripheryBeamDampCutoffHz":500,"wheelPeripheryBeamPrecompression":0.99},

        {"wheelPeripheryReinfBeamSpring":160000,"wheelPeripheryReinfBeamDamp":35},
        {"wheelPeripheryReinfBeamDampCutoffHz":500,"wheelPeripheryReinfBeamPrecompression":0.99},

        //general tire values
        {"nodeWeight":0.175},
        {"nodeMaterial":"|NM_RUBBER"},
        {"triangleCollision":false},
        {"pressurePSI":"$tirepressure_F"},
        {"dragCoef":5},

        //groundmodel friction multipliers
        {"frictionCoef":1},
        {"slidingFrictionCoef":1},
        {"treadCoef":0.5},

        //advanced friction values
        {"noLoadCoef":1.34},
        {"loadSensitivitySlope":0.000173},
        {"fullLoadCoef":0.4},
        {"softnessCoef":0.5},

        //deform values
        {"wheelSideBeamDeform":15000,"wheelSideBeamStrength":20000},
        {"wheelTreadBeamDeform":11000,"wheelTreadBeamStrength":14000},
        {"wheelPeripheryBeamDeform":50000,"wheelPeripheryBeamStrength":50000},
    ],
},
}