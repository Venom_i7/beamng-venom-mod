{
  "pickup_engine_v8_7.5_venom": {
    "information": {
      "authors": "Venom",
      "name": "7.5L V8 Venom Engine",
      "value": 15700
    },
    "slotType": "pickup_engine",
    "slots": [
      ["type", "default", "description"],
      ["pickup_enginemounts", "pickup_enginemounts_heavy", "Engine Mounts", { "coreSlot": true }],
      ["pickup_oilpan_v8", "pickup_oilpan_v8", "Oil Pan", { "coreSlot": true }],
      ["pickup_intake_v8", "pickup_intake_v8", "Intake", { "coreSlot": true }],
      ["pickup_header_v8", "pickup_header_v8", "Exhaust Manifolds", { "coreSlot": true }],
      ["pickup_engine_v8_ecu", "pickup_engine_ecu_v8_venom", "Engine Management", { "coreSlot": true }],
      ["pickup_engine_v8_internals", "pickup_engine_v8_internals_heavy", "Engine Long Block", { "coreSlot": true }],
      ["pickup_transmission", "pickup_transmission_4A", "Transmission"],
      ["pickup_oilcooler", "", "Oli Cooler"],
      ["n2o_system", "", "Nitrous Oxide System"]
    ],
    "powertrain": [
      ["type", "name", "inputName", "inputIndex"],
      ["combustionEngine", "mainEngine", "dummy", 0]
    ],
    "mainEngine": {
      "torque": [
        ["rpm", "torque"],
        [0, 0],
        [500, 270],
        [1000, 410],
        [2000, 515],
        [3000, 560],
        [4000, 580],
        [5000, 600],
        [6000, 620],
        [7000, 640],
        [8000, 650],
        [9000, 650],
        [10000, 650],
        [11000, 615],
        [12000, 575],
        [13000, 525],
        [14000, 432],
        [15000, 333]
      ],

      //engine performance
      "idleRPM": 600,
      //max physically capable
      "maxRPM": 10500,
      "inertia": 0.15,
      "friction": 7,
      "dynamicFriction": 0.007,
      "engineBrakeTorque": 92,
      //"burnEfficiency":0.32,
      "burnEfficiency": [
        [0, 0.25],
        [0.05, 0.77],
        [0.4, 0.89],
        [0.7, 0.96],
        [1, 0.99]
      ],
      //fuel system
      "energyStorage": ["mainTank", "auxTank"],
      "requiredEnergyType": "gasoline",

      //exhaust
      "instantAfterFireSound": "event:>Vehicle>Afterfire>01_Single_EQ1",
      "sustainedAfterFireSound": "event:>Vehicle>Afterfire>01_Multi_EQ1",
      "shiftAfterFireSound": "event:>Vehicle>Afterfire>01_Shift_EQ1",
      "particulates": 0.25,
      "instantAfterFireCoef": 0.75,
      "sustainedAfterFireCoef": 0.75,
      "instantAfterFireVolumeCoef": 1.3,
      "sustainedAfterFireVolumeCoef": 0.95,
      "shiftAfterFireVolumeCoef": 1.1,

      //cooling and oil system
      "thermalsEnabled": true,
      "engineBlockMaterial": "aluminum",
      "oilVolume": 12,
      "engineBlockAirCoolingEfficiency": 75,

      //engine durability
      "cylinderWallTemperatureDamageThreshold": 989900,
      "headGasketDamageThreshold": 98997797,
      "pistonRingDamageThreshold": 98997797,
      "connectingRodDamageThreshold": 98997797,
      "maxTorqueRating": 8500,
      "maxOverTorqueDamage": 8450,

      //node beam interface
      "torqueReactionNodes:": ["e1l", "e2l", "e4r"],
      "waterDamage": { "[engineGroup]:": ["engine_intake"] },
      "radiator": { "[engineGroup]:": ["radiator"] },
      "engineBlock": { "[engineGroup]:": ["engine_block"] },
      "breakTriggerBeam": "engine",
      "uiName": "Engine",
      "soundConfig": "soundConfig",
      "soundConfigExhaust": "soundConfigExhaust",

      //starter motor
      "starterSample": "event:>Engine>Starter>v8_20xy_eng",
      "starterSampleExhaust": "event:>Engine>Starter>v8_20xy_exh",
      "shutOffSampleEngine": "event:>Engine>Shutoff>v8_20xy_eng",
      "shutOffSampleExhaust": "event:>Engine>Shutoff>v8_20xy_exh",
      "starterTorque": 750,
      "starterVolume": 0.76,
      "starterVolumeExhaust": 0.76,
      "shutOffVolumeEngine": 0.76,
      "shutOffVolumeExhaust": 0.76,
      "starterThrottleKillTime": 0.84,
      "idleRPMStartRate": 1.25,
      "idleRPMStartCoef": 1,

      //engine deform groups
      "deformGroups": ["mainEngine", "mainEngine_intake", "mainEngine_accessories"]
    },
    "soundConfig": {
      "sampleName": "V8_cross_engine",
      "intakeMuffling": 1,

      "mainGain": -2.0,
      "onLoadGain": 1,
      "offLoadGain": 0.67,

      "maxLoadMix": 0.75,
      "minLoadMix": 0,

      "lowShelfGain": 6,
      "lowShelfFreq": 120,

      "highShelfGain": 4,
      "highShelfFreq": 6000,

      "eqLowGain": 0,
      "eqLowFreq": 150,
      "eqLowWidth": 0.2,

      "eqHighGain": 0,
      "eqHighFreq": 2500,
      "eqHighWidth": 0.2,

      "fundamentalFrequencyCylinderCount": 8,
      "eqFundamentalGain": -2
    },
    "soundConfigExhaust": {
      "sampleName": "V8_cross_exhaust",

      "mainGain": 0.0,
      "onLoadGain": 1,
      "offLoadGain": 0.64,

      "maxLoadMix": 0.75,
      "minLoadMix": 0,

      "lowShelfGain": -11,
      "lowShelfFreq": 90,

      "highShelfGain": 5,
      "highShelfFreq": 2400,

      "eqLowGain": 2,
      "eqLowFreq": 150,
      "eqLowWidth": 0.2,

      "eqHighGain": 0,
      "eqHighFreq": 2500,
      "eqHighWidth": 0.2,

      "fundamentalFrequencyCylinderCount": 8,
      "eqFundamentalGain": 0
    },
    "vehicleController": {
      "clutchLaunchStartRPM": 1200,
      "clutchLaunchTargetRPM": 1600,
      //**highShiftDown can be overwritten by automatic transmissions**
      "highShiftDownRPM": [0, 0, 0, 2600, 2950, 3150, 3400, 3400],
      //**highShiftUp can be overwritten by intake modifications**
      "highShiftUpRPM": 7500
    },
    "flexbodies": [
      ["mesh", "[group]:", "nonFlexMaterials"],
      ["gavril_v8_pickup", ["pickup_engine"], [], { "pos": { "x": 0, "y": 0, "z": 0 }, "rot": { "x": 0, "y": 0, "z": 0 }, "scale": { "x": 1, "y": 1, "z": 1 } }],
      ["gavril_v8_pickup_gasparts", ["pickup_engine"], [], { "pos": { "x": 0, "y": 0, "z": 0 }, "rot": { "x": 0, "y": 0, "z": 0 }, "scale": { "x": 1, "y": 1, "z": 1 } }],
      { "deformGroup": "engbay_break", "deformMaterialBase": "pickup", "deformMaterialDamaged": "invis" },
      ["gavril_v8_pickup_radhose", ["pickup_engine", "pickup_radsupport", "roamer_radsupport"]],
      { "deformGroup": "", "deformMaterialBase": "", "deformMaterialDamaged": "" }
    ],

    "props": [
      ["func", "mesh", "idRef:", "idX:", "idY:", "baseRotation", "rotation", "translation", "min", "max", "offset", "multiplier"],
      ["rpmspin", "gavril_v8_pickup_pulley1", "e3l", "e3r", "e4l", { "x": -90, "y": 0, "z": 0 }, { "x": 0, "y": 1, "z": 0 }, { "x": 0, "y": 0, "z": 0 }, -360, 360, 0, 1],
      ["rpmspin", "gavril_v8_pickup_pulley2", "e3l", "e3r", "e4l", { "x": -90, "y": 0, "z": 0 }, { "x": 0, "y": 1, "z": 0 }, { "x": 0, "y": 0, "z": 0 }, -360, 360, 0, -1],
      ["rpmspin", "gavril_v8_pickup_pulley3", "e3l", "e3r", "e4l", { "x": -90, "y": 0, "z": 0 }, { "x": 0, "y": 1, "z": 0 }, { "x": 0, "y": 0, "z": 0 }, -360, 360, 0, 1],
      ["rpmspin", "gavril_v8_pickup_pulley4", "e3l", "e3r", "e4l", { "x": -90, "y": 0, "z": 0 }, { "x": 0, "y": 1, "z": 0 }, { "x": 0, "y": 0, "z": 0 }, -360, 360, 0, 1],
      ["rpmspin", "gavril_v8_pickup_pulley5", "e3l", "e3r", "e4l", { "x": -90, "y": 0, "z": 0 }, { "x": 0, "y": 1, "z": 0 }, { "x": 0, "y": 0, "z": 0 }, -360, 360, 0, 1],
      ["rpmspin", "gavril_v8_pickup_pulley6", "e3l", "e3r", "e4l", { "x": -90, "y": 0, "z": 0 }, { "x": 0, "y": 1, "z": 0 }, { "x": 0, "y": 0, "z": 0 }, -1440, 1440, 0, 4]
    ],

    "nodes": [
      ["id", "posX", "posY", "posZ"],
      { "selfCollision": false },
      { "collision": true },
      //--7.0L V8 Engine-
      { "frictionCoef": 0.5 },
      { "nodeMaterial": "|NM_METAL" },
      { "nodeWeight": 32.4 },
      { "group": "pickup_engine" },
      { "engineGroup": "engine_block" },
      [
        "e1r",
        -0.13,
        -1.14,
        0.43,
        {
          "chemEnergy": 1000,
          "burnRate": 0.39,
          "flashPoint": 800,
          "specHeat": 0.1,
          "selfIgnitionCoef": false,
          "smokePoint": 650,
          "baseTemp": "thermals",
          "conductionRadius": 0.2
        }
      ],
      [
        "e1l",
        0.13,
        -1.14,
        0.43,
        {
          "chemEnergy": 1000,
          "burnRate": 0.39,
          "flashPoint": 800,
          "specHeat": 0.1,
          "selfIgnitionCoef": false,
          "smokePoint": 650,
          "baseTemp": "thermals",
          "conductionRadius": 0.2
        }
      ],
      ["e2r", -0.13, -1.74, 0.43, { "isExhaust": "mainEngine" }],
      ["e2l", 0.13, -1.74, 0.43, { "isExhaust": "mainEngine" }],
      { "engineGroup": ["engine_block", "engine_intake"] },
      { "group": ["pickup_engine", "pickup_engine_intake"] },
      ["e3r", -0.32, -1.14, 1.0],
      ["e3l", 0.32, -1.14, 1.0],
      ["e4r", -0.32, -1.74, 1.0, { "selfCollision": true }],
      ["e4l", 0.32, -1.74, 1.0, { "selfCollision": true }],
      {
        "chemEnergy": false,
        "burnRate": false,
        "flashPoint": false,
        "specHeat": false,
        "smokePoint": false,
        "selfIgnitionCoef": false,
        "baseTemp": false,
        "conductionRadius": false
      },
      { "engineGroup": "" },
      { "group": "" },
      //engine mount nodes
      ["em1r", -0.27, -1.5, 0.7, { "nodeWeight": 3 }],
      ["em1l", 0.27, -1.5, 0.7, { "nodeWeight": 3 }]
    ],
    "beams": [
      ["id1:", "id2:"],
      { "beamPrecompression": 1, "beamType": "|NORMAL", "beamLongBound": 1.0, "beamShortBound": 1.0 },
      //--ENGINE CUBE--
      { "beamSpring": 18800940, "beamDamp": 470 },
      { "beamDeform": 315000, "beamStrength": "FLT_MAX" },
      { "deformGroup": "mainEngine", "deformationTriggerRatio": 0.001 },
      ["e1r", "e1l"],
      ["e1r", "e2r"],
      ["e1r", "e2l"],
      ["e1r", "e3r"],
      ["e1r", "e3l"],
      ["e1r", "e4r"],
      ["e1r", "e4l"],
      ["e1l", "e2r"],
      ["e1l", "e2l"],
      ["e1l", "e3r"],
      ["e1l", "e3l"],
      ["e1l", "e4r"],
      ["e1l", "e4l"],
      ["e2r", "e2l"],
      ["e2r", "e3r"],
      ["e2r", "e3l"],
      ["e2r", "e4r"],
      ["e2r", "e4l"],
      ["e2l", "e3r"],
      ["e2l", "e3l"],
      ["e2l", "e4r"],
      ["e2l", "e4l"],
      ["e3r", "e3l"],
      ["e3r", "e4r"],
      ["e3r", "e4l"],
      ["e3l", "e4r"],
      ["e3l", "e4l"],
      ["e4r", "e4l"],

      //engine mount nodes
      { "beamSpring": 2956300, "beamDamp": 130.43 },
      { "beamDeform": 63000, "beamStrength": "FLT_MAX" },
      ["em1r", "e3l"],
      ["em1r", "e3r"],
      ["em1r", "e4l"],
      ["em1r", "e4r"],
      ["em1r", "e1r"],
      ["em1r", "e1l"],
      ["em1r", "e2l"],
      ["em1r", "e2r"],

      ["em1l", "e3l"],
      ["em1l", "e3r"],
      ["em1l", "e4l"],
      ["em1l", "e4r"],
      ["em1l", "e1r"],
      ["em1l", "e1l"],
      ["em1l", "e2l"],
      ["em1l", "e2r"],
      { "deformGroup": "" },

      //mesh break beams
      { "beamPrecompression": 0.8, "beamType": "|SUPPORT", "beamLongBound": 0.8 },
      { "beamSpring": 9400, "beamDamp": 0 },
      { "beamDeform": 350, "beamStrength": 700 },
      { "deformGroup": "engbay_break", "deformationTriggerRatio": 0.01 },
      { "optional": true },
      ["e2r", "rad2"],
      ["e2l", "rad2"],
      ["e4r", "rad2"],
      ["e4l", "rad2"],
      { "optional": false },
      { "deformGroup": "" },
      { "beamPrecompression": 1, "beamType": "|NORMAL", "beamLongBound": 1.0, "beamShortBound": 1.0 }
    ]
  },
  "pickup_engine_v8_internals_venom": {
    "information": {
      "authors": "Venom",
      "name": "Venom Billet Long Block",
      "value": 17000
    },
    "slotType": "pickup_engine_v8_internals",
    "mainEngine": {
      "torqueModMult": [
        ["rpm", "torque"],
        [0, 1.0],
        [500, 1.3],
        [1000, 1.15],
        [2000, 1.135],
        [3000, 1.19],
        [4000, 1.23],
        [5000, 1.25],
        [6000, 1.24],
        [7000, 1.23],
        [8000, 1.24],
        [9000, 1.26],
        [10000, 1.275],
        [11000, 1.36],
        [12000, 1.46],
        [13000, 1.61],
        [14000, 1.97],
        [15000, 2.57]
      ],
      //max rpm physically capable of
      "$+maxRPM": 5000,
      "$*friction": 0.77,
      "$*dynamicFriction": 0.77,
      "$*inertia": 1.01,
      "$*engineBrakeTorque": 1.5,
      //engine durability
      "$+cylinderWallTemperatureDamageThreshold": 9899,
      "$+headGasketDamageThreshold": 9899777,
      "$+pistonRingDamageThreshold": 9899777,
      "$+connectingRodDamageThreshold": 9899777,
      "$*maxTorqueRating": 29.87,
      "$*maxOverTorqueDamage": 29.87
    }
  },
  "pickup_intake_v8_venom": {
    "information": {
      "authors": "Venom",
      "name": "Venom Race Intake",
      "value": 12000
    },
    "slotType": "pickup_intake_v8",
    "flexbodies": [
      ["mesh", "[group]:", "nonFlexMaterials"],
      [
        "pickup_intake_race",
        ["pickup_engine_intake", "pickup_airbox"],
        [],
        { "pos": { "x": 0, "y": 0, "z": 0 }, "rot": { "x": 0, "y": 0, "z": 0 }, "scale": { "x": 1, "y": 1, "z": 1 } }
      ],
      ["pickup_intake_stock_belts", ["pickup_engine"], [], { "pos": { "x": 0, "y": 0, "z": 0 }, "rot": { "x": 0, "y": 0, "z": 0 }, "scale": { "x": 1, "y": 1, "z": 1 } }]
    ],
    "mainEngine": {
      "torqueModIntake": [
        ["rpm", "torque"],
        [0, 0],
        [1000, 100],
        [2000, 110],
        [3000, 120],
        [4000, 130],
        [5000, 140],
        [6000, 150],
        [6500, 160],
        [7000, 170],
        [8000, 180],
        [9000, 190],
        [10000, 200],
        [11000, 210],
        [12000, 220],
        [13000, 230],
        [14000, 240],
        [15000, 250],
        [16000, 260],
        [16000, 270],
        [17000, 280],
        [18000, 290],
        [19000, 300],
        [20000, 310]
      ],
      "$+particulates": 0.03
    },
    "soundConfig": {
      "$+maxLoadMix": 0.2,
      "$+intakeMuffling": -1,
      "$+mainGain": 1,
      "$+offLoadGain": -0.05
    },
    "soundConfigExhaust": {
      "$+maxLoadMix": 0.2,
      "$+minLoadMix": 0.0,
      "$+mainGain": 0.5,
      "$+offLoadGain": -0.05
    },
    "vehicleController": {
      "revMatchThrottle": 0.15
    },
    "nodes": [
      ["id", "posX", "posY", "posZ"],
      { "selfCollision": false },
      { "collision": true },
      //--Stock Intake
      { "frictionCoef": 0.5 },
      { "nodeMaterial": "|NM_METAL" },
      { "nodeWeight": 3.3 },
      { "group": ["pickup_engine_intake"] },
      ["intake1", 0.11, -1.35, 1.04],
      { "group": ["pickup_airbox"] },
      { "nodeWeight": 1.88 },
      { "selfCollision": true },
      ["intake2", 0.6, -1.94, 1.04],
      { "engineGroup": "" },
      { "group": "" }
    ],
    "beams": [
      ["id1:", "id2:"],
      { "beamPrecompression": 1, "beamType": "|NORMAL", "beamLongBound": 1.0, "beamShortBound": 1.0 },
      //--Stock Intake
      { "beamSpring": 1880940, "beamDamp": 235 },
      { "beamDeform": 105000, "beamStrength": "FLT_MAX" },
      { "deformGroup": "mainEngine_intake", "deformationTriggerRatio": 0.01 },
      ["intake1", "e3r"],
      ["intake1", "e3l"],
      ["intake1", "e4r"],
      ["intake1", "e4l"],
      ["intake1", "e2l"],
      ["intake1", "e2r"],
      { "deformGroup": "" },

      { "beamSpring": 120940, "beamDamp": 141 },
      { "beamDeform": 6150, "beamStrength": "FLT_MAX" },
      ["intake2", "e4l"],
      ["intake2", "e3l"],
      ["intake2", "e2l"],
      ["intake2", "e1l"],
      ["intake2", "e4r"],
      { "optional": true },
      { "beamDeform": 9150, "beamStrength": 9900 },
      ["intake2", "s2l"],
      ["intake2", "s2ll"],
      ["intake2", "s1l"],
      ["intake2", "s1ll"],
      { "deformGroup": "" },
      { "optional": false },

      //mesh break beams
      { "beamPrecompression": 0.8, "beamType": "|SUPPORT", "beamLongBound": 0.8 },
      { "beamSpring": 9400, "beamDamp": 0 },
      { "beamDeform": 350, "beamStrength": 700 },
      ["intake1", "intake2"],
      ["intake2", "e4l"],
      { "deformGroup": "" },
      { "beamPrecompression": 1, "beamType": "|NORMAL", "beamLongBound": 1.0, "beamShortBound": 1.0 }
    ]
  },
  "pickup_supercharger_v8_venom": {
    "information": {
      "authors": "BeamNG",
      "name": "Venom Supercharger",
      "value": 27000
    },
    "slotType": "pickup_intake_v8",
    "variables": [
      ["name", "type", "unit", "category", "default", "min", "max", "title", "description"],
      ["$gearRatioConfig", "range", "ratio", "Engine", 3, 1, 7, "Supercharger Gear Ratio", "Supercharger Gear Ratio Per Engine RPM", { "stepDis": 0.25 }],
      ["$boostRate", "range", "PSI", "Engine", 7.5, 1, 25, "Supercharger Boost Per 1000 RPM", "Supercharger Boost Per 1000 RPM", { "stepDis": 0.25 }]
    ],
    "supercharger": {
      "gearRatio": "$gearRatioConfig",
      "clutchEngageRPM": 0,
      "pressureRatePSI": 1770,
      "pressurePSIPer1kRPM": "$boostRate",
      "crankLossPer1kRPM": 0.007,
      "type": "roots",
      "whineLoopEvent": "event:>Vehicle>Forced_Induction>Supercharger_02>supercharger",
      "whineVolumePerPSI": 0.0015,
      "whinePitchPer10kRPM": 0.01,
      "boostController": [
        //throttle in %, factor
        [0, 0.35],
        [100, 1]
      ]
    },
    "mainEngine": {
      //supercharger name
      "supercharger": "supercharger",
      "$*instantAfterFireCoef": 3,
      "$*sustainedAfterFireCoef": 2.25,

      //supercharger deform groups
      "deformGroups_supercharger": ["mainEngine_supercharger"]
    },
    "soundConfig": {
      "$+maxLoadMix": 0.2,
      "$+intakeMuffling": -1,
      "$+mainGain": 1,
      "$+offLoadGain": -0.05
    },
    "soundConfigExhaust": {
      "$+maxLoadMix": 0.2,
      "$+minLoadMix": 0.0,
      "$+mainGain": 2.0,
      "$+offLoadGain": -0.05
    },
    "vehicleController": {
      "revMatchThrottle": 0.25
    },
    "flexbodies": [
      ["mesh", "[group]:", "nonFlexMaterials"],
      ["pickup_supercharger", ["pickup_engine"], [], { "pos": { "x": 0, "y": 0, "z": 0 }, "rot": { "x": 0, "y": 0, "z": 0 }, "scale": { "x": 1, "y": 1, "z": 1 } }],
      ["pickup_supercharger_belts", ["pickup_engine"], [], { "pos": { "x": 0, "y": 0, "z": 0 }, "rot": { "x": 0, "y": 0, "z": 0 }, "scale": { "x": 1, "y": 1, "z": 1 } }]
    ],
    "props": [
      ["func", "mesh", "idRef:", "idX:", "idY:", "baseRotation", "rotation", "translation", "min", "max", "offset", "multiplier"],
      ["rpmspin", "pickup_supercharger_pulley", "e3l", "e3r", "e4l", { "x": -90, "y": 0, "z": 0 }, { "x": 0, "y": 1, "z": 0 }, { "x": 0, "y": 0, "z": 0 }, -360, 360, 0, 1]
    ],
    "nodes": [
      ["id", "posX", "posY", "posZ"],
      { "selfCollision": false },
      { "collision": true },
      //--Supercharger
      { "frictionCoef": 0.5 },
      { "nodeMaterial": "|NM_METAL" },
      { "nodeWeight": 3.76 },
      { "group": ["pickup_engine", "pickup_engine_intake"] },
      ["sc1r", -0.12, -1.21, 1.12],
      ["sc1l", 0.12, -1.21, 1.12],
      ["sc2r", -0.12, -1.75, 1.12],
      ["sc2l", 0.12, -1.75, 1.12],
      { "group": ["pickup_engine_intake"] },
      { "nodeWeight": 1.88 },
      { "selfCollision": true },
      ["intake2", 0.51, -1.82, 1.04],
      { "engineGroup": "" },
      { "group": "" }
    ],
    "beams": [
      ["id1:", "id2:"],
      { "beamPrecompression": 1, "beamType": "|NORMAL", "beamLongBound": 1.0, "beamShortBound": 1.0 },
      //--Supercharger
      { "beamSpring": 4700940, "beamDamp": 235 },
      { "beamDeform": 105000, "beamStrength": "FLT_MAX" },
      { "deformGroup": "mainEngine_supercharger", "deformationTriggerRatio": 0.001 },
      ["sc1l", "sc1r"],
      ["sc2l", "sc2r"],
      ["sc1r", "sc2r"],
      ["sc1l", "sc2l"],
      ["sc1r", "sc2l"],
      ["sc1l", "sc2r"],
      ["sc2l", "e4l"],
      ["sc1l", "e3l"],
      ["sc1r", "e3r"],
      ["sc2r", "e4r"],
      ["sc2l", "e3l"],
      ["sc1l", "e4l"],
      ["sc2r", "e3r"],
      ["sc1r", "e4r"],
      ["sc2r", "e4l"],
      ["sc2l", "e4r"],
      ["sc1r", "e3l"],
      ["sc1l", "e3r"],

      ["sc1l", "e1l"],
      ["sc1r", "e1r"],
      ["sc2r", "e2r"],
      ["sc2l", "e2l"],
      { "deformGroup": "" },

      { "beamSpring": 94940, "beamDamp": 470 },
      { "beamDeform": 6150, "beamStrength": "FLT_MAX" },
      //{"deformGroup":"mainEngine_intake", "deformationTriggerRatio":0.01}
      ["intake2", "e3l"],
      ["intake2", "e4r"],
      ["intake2", "sc1l"],
      ["intake2", "sc2l"],
      { "deformGroup": "" },

      //mesh break beams
      { "beamPrecompression": 0.85, "beamType": "|SUPPORT", "beamLongBound": 0.7 },
      { "beamSpring": 18800, "beamDamp": 0 },
      { "beamDeform": 350, "beamStrength": 700 },
      ["intake2", "e4l"],
      ["intake2", "e4r"],
      ["intake2", "e3l"],
      { "deformGroup": "" },
      { "beamPrecompression": 1, "beamType": "|NORMAL", "beamLongBound": 1.0, "beamShortBound": 1.0 }
    ],
    "triangles": [["id1:", "id2:", "id3:"], { "groundModel": "metal" }, { "dragCoef": 0 }, ["sc2r", "sc2l", "sc1l"], ["sc1l", "sc1r", "sc2r"]]
  },
  "pickup_engine_ecu_v8_venom": {
    "information": {
      "authors": "Venom",
      "name": "Venom ECU",
      "value": 17000
    },
    "slotType": "pickup_engine_v8_ecu",
    "variables": [
      ["name", "type", "unit", "category", "default", "min", "max", "title", "description"],
      ["$idleRPMConfig", "range", "rpm", "Engine", 700, 100, 3000, "RPM Idle", "Engine idle RPM", { "stepDis": 50 }],
      ["$revLimiterRPM", "range", "rpm", "Engine", 8000, 1500, 15000, "RPM Limit", "RPM where the rev limiter prevents further revving", { "stepDis": 50 }],
      ["$revLimiterCutTime", "range", "s", "Engine", 0.15, 0.01, 0.5, "RPM Limit Cut Time", "How fast the rev limiter cycles", { "stepDis": 0.01 }],
      ["$venomFineTuneLowRPM", "range", "nm", "Engine", 0, -300, 1000, "KuyPeD tuning low RPM", "KuyPeD fine ecu torque low RPM (1000-7000) gain tuning", { "stepDis": 1 }],
      ["$venomFineTuneHighRPM", "range", "nm", "Engine", 0, -300, 1500, "KuyPeD tuning high RPM", "KuyPeD fine ecu torque high RPM (7000-15000) gain tuning", { "stepDis": 1 }]
    ],
    "controller": [["fileName"], ["twoStepLaunch", { "rpmLimit": 4000 }]],
    "mainEngine": {
      "torqueModIntake": [
        ["rpm", "torque"],
        [500, "$=$venomFineTuneLowRPM*1.00"],
        [1000, "$=$venomFineTuneLowRPM*1.01"],
        [2000, "$=$venomFineTuneLowRPM*1.02"],
        [2500, "$=$venomFineTuneLowRPM*1.03"],
        [3000, "$=$venomFineTuneLowRPM*1.04"],
        [4000, "$=$venomFineTuneLowRPM*1.05"],
        [5000, "$=$venomFineTuneLowRPM*1.06"],
        [6000, "$=$venomFineTuneLowRPM*1.07"],
        [7000, "$=$venomFineTuneLowRPM*1.08"],
        [8000, "$=$venomFineTuneHighRPM*1.00"],
        [9000, "$=$venomFineTuneHighRPM*1.01"],
        [10000, "$=$venomFineTuneHighRPM*1.02"],
        [11000, "$=$venomFineTuneHighRPM*1.03"],
        [12000, "$=$venomFineTuneHighRPM*1.04"],
        [13000, "$=$venomFineTuneHighRPM*1.05"],
        [14000, "$=$venomFineTuneHighRPM*1.06"],
        [15000, "$=$venomFineTuneHighRPM*1.07"],
        [16000, "$=$venomFineTuneHighRPM*1.08"],
        [17000, "$=$venomFineTuneHighRPM*1.09"],
        [18000, "$=$venomFineTuneHighRPM*1.10"],
        [19000, "$=$venomFineTuneHighRPM*1.11"],
        [20000, "$=$venomFineTuneHighRPM*1.12"]
      ],
      "idleRPM": "$idleRPMConfig",
      "revLimiterRPM": "$revLimiterRPM",
      "revLimiterType": "timeBased",
      "revLimiterCutTime": "$revLimiterCutTime"
    },
    "vehicleController": {
      "highShiftUpRPM": "$=$revLimiterRPM - 200"
    }
  },
  "pickup_transmission_8A_venom": {
    "information": {
      "authors": "Venom",
      "name": "8-Speed Venom Super Hi-Speed AT",
      "value": 30000
    },
    "slotType": "pickup_transmission",
    "slots": [
      ["type", "default", "description"],
      ["pickup_converter", "pickup_converter_heavy", "Torque Converter", { "coreSlot": true }],
      ["pickup_transfer_case", "pickup_transfer_case_4WD", "Transfer Case", { "coreSlot": true }]
    ],
    "variables": [
      ["name", "type", "unit", "category", "default", "min", "max", "title", "description"],
      ["$gear_R", "range", ":1", "Transmission", 3.35, 0.5, 5, "Reverse Gear Ratio", "Torque multiplication ratio", { "stepDis": 0.01 }],
      ["$gear_1", "range", ":1", "Transmission", 4.71, 0.5, 5, "1st Gear Ratio", "Torque multiplication ratio", { "stepDis": 0.01 }],
      ["$gear_2", "range", ":1", "Transmission", 3.14, 0.5, 5, "2nd Gear Ratio", "Torque multiplication ratio", { "stepDis": 0.01 }],
      ["$gear_3", "range", ":1", "Transmission", 2.11, 0.5, 5, "3rd Gear Ratio", "Torque multiplication ratio", { "stepDis": 0.01 }],
      ["$gear_4", "range", ":1", "Transmission", 1.67, 0.5, 5, "4th Gear Ratio", "Torque multiplication ratio", { "stepDis": 0.01 }],
      ["$gear_5", "range", ":1", "Transmission", 1.29, 0.5, 5, "5th Gear Ratio", "Torque multiplication ratio", { "stepDis": 0.01 }],
      ["$gear_6", "range", ":1", "Transmission", 1.0, 0.5, 5, "6th Gear Ratio", "Torque multiplication ratio", { "stepDis": 0.01 }],
      ["$gear_7", "range", ":1", "Transmission", 0.84, 0.5, 5, "7th Gear Ratio", "Torque multiplication ratio", { "stepDis": 0.01 }],
      ["$gear_8", "range", ":1", "Transmission", 0.67, 0.5, 5, "8th Gear Ratio", "Torque multiplication ratio", { "stepDis": 0.01 }]
    ],
    "powertrain": [
      ["type", "name", "inputName", "inputIndex"],
      ["torqueConverter", "torqueConverter", "mainEngine", 1],
      ["automaticGearbox", "gearbox", "torqueConverter", 1]
    ],
    "gearbox": {
      "uiName": "Gearbox",
      "gearRatios": ["$=-$gear_R", 0, "$gear_1", "$gear_2", "$gear_3", "$gear_4", "$gear_5", "$gear_6", "$gear_7", "$gear_8"],
      "parkLockTorque": 17000,
      "oneWayViscousCoef": 10,
      "gearChangeTime": 0.01,
      "shiftEfficiency": 0.997,
      "friction": 4.375,
      "dynamicFriction": 0.00117,
      "torqueLossCoef": 0.003,
      "gearboxNode:": ["tra1"]
    },
    "vehicleController": {
      "automaticModes": "PRNDSM",
      "shiftDownRPMOffsetCoef": 1.15,
      "calculateOptimalLoadShiftPoints": true,
      "transmissionGearChangeDelay": 0.1,
      "aggressionHoldOffThrottleDelay": 2.5,
      "aggressionSmoothingUp": 2,
      "aggressionSmoothingDown": 0.2,
      "gearboxDecisionSmoothingUp": 2,
      "gearboxDecisionSmoothingDown": 2,
      "lowShiftDownRPM": 1300,
      "lowShiftUpRPM": 2100,
      "maxGearChangeTime": 0.1,
      "minGearChangeTime": 0.1,
      "sportGearChangeTime": 0.02,
      "throttleCoefWhileShifting": 0.35
    },
    "flexbodies": [
      ["mesh", "[group]:", "nonFlexMaterials"],
      ["gavril_v8_pickup_transmission", ["pickup_engine", "pickup_transmission"]]
    ],
    "nodes": [
      ["id", "posX", "posY", "posZ"],
      { "selfCollision": false },
      { "collision": true },
      //--6 Speed Auto-
      { "frictionCoef": 0.5 },
      { "nodeMaterial": "|NM_METAL" },
      { "nodeWeight": 37.6 },
      { "group": "pickup_transmission" },
      ["tra1", 0.0, -0.41, 0.46],
      { "group": "" }
    ],
    "beams": [
      ["id1:", "id2:"],
      { "beamPrecompression": 1, "beamType": "|NORMAL", "beamLongBound": 1.0, "beamShortBound": 1.0 },
      //--TRANSMISSION CONE--
      { "beamSpring": 18800940, "beamDamp": 470 },
      { "beamDeform": 175000, "beamStrength": "FLT_MAX" },
      ["tra1", "e1r"],
      ["tra1", "e3r"],
      ["tra1", "e1l"],
      ["tra1", "e3l"],
      { "beamPrecompression": 1, "beamType": "|NORMAL", "beamLongBound": 1.0, "beamShortBound": 1.0 }
    ]
  },
  "pickup_converter_venom": {
    "information": {
      "authors": "BeamNG",
      "name": "Venom Converter",
      "value": 4500
    },
    "slotType": "pickup_converter",
    "torqueConverter": {
      "uiName": "Torque Converter",
      "converterDiameter": 0.45,
      "converterStiffness": 23,
      "couplingAVRatio": 1,
      "stallTorqueRatio": 0.75,
      "additionalEngineInertia": 0.32
    }
  },
  "pickup_transfer_case_AWD_venom": {
    "information": {
      "authors": "Venom",
      "name": "AWD Venom Transfer Case",
      "value": 7750
    },
    "slotType": "pickup_transfer_case",
    "variables": [
      ["name", "type", "unit", "category", "default", "min", "max", "title", "description"],
      [
        "$difftorquesplit_C",
        "range",
        "%",
        "Differentials",
        0.8,
        0.0,
        1.0,
        "Base Torque Split",
        "Percent torque to rear wheels",
        { "minDis": 0, "maxDis": 100, "stepDis": 1, "subCategory": "Center" }
      ]
    ],
    "powertrain": [
      ["type", "name", "inputName", "inputIndex"],
      //lsd center diff
      [
        "differential",
        "transfercase",
        "gearbox",
        1,
        {
          "diffType": "lsd",
          "lsdPreload": 50,
          "lsdLockCoef": 0.15,
          "lsdRevLockCoef": 0.15,
          "diffTorqueSplit": "$difftorquesplit_C",
          "friction": 0.44,
          "dynamicFriction": 0.00048,
          "torqueLossCoef": 0.0075,
          "uiName": "Center Differential",
          "defaultVirtualInertia": 0.1,
          "speedLimitCoef": 0.1
        }
      ],
      //non disconnecting front shaft
      ["shaft", "transfercase_F", "transfercase", 2, { "friction": 0.44, "dynamicFriction": 0.00048, "uiName": "Front Output Shaft" }]
    ],
    "flexbodies": [
      ["mesh", "[group]:", "nonFlexMaterials"],
      ["pickup_transfer_case", ["pickup_transmission", "pickup_engine"]]
    ],
    "nodes": [
      ["id", "posX", "posY", "posZ"],
      { "selfCollision": false },
      { "collision": true },
      { "frictionCoef": 0.5 },
      { "nodeMaterial": "|NM_METAL" },
      { "nodeWeight": 18 },
      { "group": "pickup_transmission" },
      ["tra2", -0.12, -0.78, 0.425],
      ["tra3", 0.18, -0.78, 0.425],
      { "group": "" }
    ],
    "beams": [
      ["id1:", "id2:"],
      { "beamPrecompression": 1, "beamType": "|NORMAL", "beamLongBound": 1.0, "beamShortBound": 1.0 },
      { "beamSpring": 12600840, "beamDamp": 390 },
      { "beamDeform": 99750, "beamStrength": "FLT_MAX" },
      ["tra2", "tra1"],

      ["tra2", "e1r"],
      ["tra2", "e1l"],
      ["tra2", "e3r"],
      ["tra2", "e3l"],

      ["tra3", "e1r"],
      ["tra3", "e1l"],
      ["tra3", "e3r"],
      ["tra3", "e3l"],

      ["tra3", "tra2"],
      ["tra3", "tra1"],
      { "beamPrecompression": 1, "beamType": "|NORMAL", "beamLongBound": 1.0, "beamShortBound": 1.0 }
    ]
  },
  "pickup_oilpan_v8_venom": {
    "information": {
      "authors": "Venom",
      "name": "Venom Oil Pan",
      "value": 7000
    },
    "slotType": "pickup_oilpan_v8",
    "mainEngine": {
      //engine durability
      "oilpanMaximumSafeG": 20,

      //node beam interface
      "oilpanNodes:": ["oilpan", "oilref"],

      //engine deform groups
      "deformGroups_oilPan": ["oilpan_damage"]
    },
    "nodes": [
      ["id", "posX", "posY", "posZ"],
      { "selfCollision": false },
      { "collision": true },
      { "frictionCoef": 0.5 },
      { "nodeMaterial": "|NM_METAL" },

      //oil pan node
      { "group": "" },
      { "nodeWeight": 2 },
      ["oilpan", 0.0, -1.36, 0.42],
      ["oilref", 0.0, -1.36, 0.92, { "nodeWeight": 1, "collision": false }]
    ],
    "beams": [
      ["id1:", "id2:"],
      { "beamPrecompression": 1, "beamType": "|NORMAL", "beamLongBound": 1.0, "beamShortBound": 1.0 },

      //oilpan node
      { "beamSpring": 1501000, "beamDamp": 250 },
      { "beamDeform": 8000, "beamStrength": "FLT_MAX" },
      { "deformGroup": "oilpan_damage", "deformationTriggerRatio": 0.005 },
      ["oilpan", "e1r"],
      ["oilpan", "e1l"],
      ["oilpan", "e2r"],
      ["oilpan", "e2l"],
      ["oilpan", "e3r"],
      ["oilpan", "e3l"],
      ["oilpan", "e4r"],
      ["oilpan", "e4l"],
      { "deformGroup": "" },

      //oil ref
      { "beamSpring": 1001000, "beamDamp": 150 },
      { "beamDeform": 25000, "beamStrength": "FLT_MAX" },
      ["oilref", "e1r"],
      ["oilref", "e1l"],
      ["oilref", "e2r"],
      ["oilref", "e2l"],
      ["oilref", "e3r"],
      ["oilref", "e3l"],
      ["oilref", "e4r"],
      ["oilref", "e4l"],

      { "beamPrecompression": 1, "beamType": "|NORMAL", "beamLongBound": 1.0, "beamShortBound": 1.0 }
    ]
  },
  "pickup_oilcooler": {
    "information": {
      "authors": "Venom",
      "name": "Venom Oil Cooler",
      "value": 2500
    },
    "slotType": "pickup_oilcooler",
    "mainEngine": {
      "oilRadiatorArea": 0.95,
      "oilRadiatorEffectiveness": 98990,
      "oilThermostatTemperature": 105
    }
  }
}
