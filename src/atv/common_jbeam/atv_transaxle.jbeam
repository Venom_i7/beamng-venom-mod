{
"atv_transmission": {
    "information":{
        "authors":"BeamNG",
        "name":"Single Range Transmission",
        "value":1200,
    },
    "slotType" : "atv_transmission",
    "slots": [
        ["type", "default", "description"],
        ["atv_brakes","atv_brakes", "Brakes"],
        ["atv_diff_ratio","atv_diff_ratio_3_3", "Final Drive"],
    ],
    "flexbodies": [
        ["mesh", "[group]:", "nonFlexMaterials"],
        ["atv_instruments_clear", ["atv_body"]],
        {"deformGroup":""},
    ],
    "powertrain": [
        ["type", "name", "inputName", "inputIndex"],
        ["centrifugalClutch", "clutch", "mainEngine", 1 {"uiName":"Centrifugal Clutch",}],
        ["cvtGearbox", "gearbox", "clutch", 1],
        ["differential", "differential_F", "gearbox", 1, {"diffType":"open", "uiName":"Front Differential","defaultVirtualInertia":0.25}],
        ["differential", "differential_right", "differential_F", 1, {"diffType":"locked", "lockTorque":50000, "gearRatio": 1.0, "uiName":"Right Brake Differential","defaultVirtualInertia":0.25}],
        ["differential", "differential_left", "differential_F", 2, {"diffType":"locked", "lockTorque":50000, "gearRatio": 1.0, "uiName":"Left Brake Differential","defaultVirtualInertia":0.25}],
        ["shaft", "driveshaft_right", "differential_right", 1, {"deformGroup":"driveshaft_right", "breakTriggerBeam":"driveshaft_right", "uiName":"Right Driveshaft", "electricsName":"driveshaft", "friction":0.33, "dynamicFriction":0.0037}],
        ["shaft", "driveshaft_left", "differential_left", 1, {"deformGroup":"driveshaft_left", "breakTriggerBeam":"driveshaft_left", "uiName":"Left Driveshaft", "electricsName":"driveshaft", "friction":0.33, "dynamicFriction":0.0037}],
    ],
    "differential_F": {
        "friction": 1.98,
        "dynamicFriction": 0.00087,
        "torqueLossCoef": 0.016,
    },
    "clutch": {
        "engageRPMStart":1300,
        "engageRPMEnd":2000,
        "additionalEngineInertia":0.04,
    }
    "gearbox": {
        "uiName":"Gearbox",
        "minGearRatio":2.54,
        "maxGearRatio":5.54,
        "friction": 0.92,
        "dynamicFriction": 0.00091,
        "torqueLossCoef": 0.019,
        "oneWayViscousCoef":0,
        "gearboxNode:":["tra1"],
    },
    "vehicleController": {
        "automaticModes":"RND",
        "cvtHighRPM":3500,
        "cvtLowRPM":3000,
        //"aggressionSmoothingDown":0.5,
        "cvtGearRatioSmoothingIn":20,
        "cvtGearRatioSmoothingOut":10,
        "cvtAggression":0.4,
        "aggressionHoldOffThrottleDelay":0,
    },
    "props": [
        ["func",    "mesh",          "idRef:","idX:","idY:", "baseRotation",        "rotation",        "translation",        "min", "max", "offset", "multiplier"],
        //shifter
       // ["gear_A",  "atv_shifter", "f7r","f7l","f8r",      {"x":90, "y":0, "z":0},  {"x":0, "y":0, "z":0.1}, {"x":0, "y":0.14, "z":0},  0, 1, -0.12, 1],
    ],
    "nodes": [
        ["id", "posX", "posY", "posZ"],
        //--CVT--
        {"selfCollision":false},
        {"collision":true},
        {"nodeMaterial":"|NM_METAL"},
        {"frictionCoef":0.5},
        {"group":"atv_transmission"},
        {"nodeWeight":4.5},
        ["tra1", 0.2, -1.12, 0.45],
        {"group":""},
    ],

    "beams": [
        ["id1:", "id2:"],
        //--TRANSMISSION CONE--
        {"beamType":"|NORMAL", "beamLongBound":1.0, "beamShortBound":1.0},
        {"beamSpring":12001000,"beamDamp":400},
        {"beamDeform":250000,"beamStrength":"FLT_MAX"},
        ["tra1","e1l"],
        ["tra1","e2l"],
        ["tra1","e4l"],
        ["tra1","e3l"],

        {"beamSpring":21000,"beamDamp":50},
        {"beamDeform":1000,"beamStrength":1500},
        ["e2l","b11l", {"name":"driveshaft_right"}],
        ["e1l","b11r", {"name":"driveshaft_left"}],
        {"breakGroup":""},
        {"beamPrecompression":1, "beamType":"|NORMAL", "beamLongBound":1.0, "beamShortBound":1.0},
    ],
},
"atv_transmission_highlow": {
    "information":{
        "authors":"BeamNG",
        "name":"High/Low Range Transmission",
        "value":1200,
    },
    "slotType" : "atv_transmission",
    "slots": [
        ["type", "default", "description"],
        ["atv_brakes","atv_brakes", "Brakes"],
        ["atv_diff_ratio","atv_diff_ratio_3_3", "Final Drive"],
    ],
    "flexbodies": [
        ["mesh", "[group]:", "nonFlexMaterials"],
        ["atv_instruments", ["atv_body"]],
        {"deformGroup":""},
    ],
    "controller": [
        ["fileName"],
        ["4wd", {"rangeBoxName":"rangebox"}],
    ],
    "powertrain": [
        ["type", "name", "inputName", "inputIndex"],
        ["centrifugalClutch", "clutch", "mainEngine", 1 {"uiName":"Centrifugal Clutch",}],
        ["cvtGearbox", "gearbox", "clutch", 1],
        ["rangeBox", "rangebox", "gearbox", 1, {"gearRatios":[1,1.7], "uiName":"Rangebox"}],
        ["differential", "differential_F", "rangebox", 1, {"diffType":"open", "uiName":"Front Differential","defaultVirtualInertia":0.25}],
        ["differential", "differential_right", "differential_F", 1, {"diffType":"locked", "lockTorque":50000, "gearRatio": 1.0, "uiName":"Right Brake Differential","defaultVirtualInertia":0.25}],
        ["differential", "differential_left", "differential_F", 2, {"diffType":"locked", "lockTorque":50000, "gearRatio": 1.0, "uiName":"Left Brake Differential","defaultVirtualInertia":0.25}],
        ["shaft", "driveshaft_right", "differential_right", 1, {"deformGroup":"driveshaft_right", "breakTriggerBeam":"driveshaft_right", "uiName":"Right Driveshaft", "electricsName":"driveshaft", "friction":0.33, "dynamicFriction":0.0037}],
        ["shaft", "driveshaft_left", "differential_left", 1, {"deformGroup":"driveshaft_left", "breakTriggerBeam":"driveshaft_left", "uiName":"Left Driveshaft", "electricsName":"driveshaft", "friction":0.33, "dynamicFriction":0.0037}],
    ],
    "differential_F": {
        "friction": 1.98,
        "dynamicFriction": 0.00087,
        "torqueLossCoef": 0.016,
    },
    "clutch": {
        "engageRPMStart":1300,
        "engageRPMEnd":2000,
        "additionalEngineInertia":0.04,
    }
    "gearbox": {
        "uiName":"Gearbox",
        "minGearRatio":2.54,
        "maxGearRatio":5.54,
        "friction": 0.92,
        "dynamicFriction": 0.00091,
        "torqueLossCoef": 0.019,
        "oneWayViscousCoef":0,
        "gearboxNode:":["tra1"],
    },
    "vehicleController": {
        "automaticModes":"RND",
        "cvtHighRPM":3500,
        "cvtLowRPM":3000,
        //"aggressionSmoothingDown":0.5,
        "cvtGearRatioSmoothingIn":20,
        "cvtGearRatioSmoothingOut":10,
        "cvtAggression":0.4,
        "aggressionHoldOffThrottleDelay":0,
    },
    "props": [
        ["func",    "mesh",          "idRef:","idX:","idY:", "baseRotation",        "rotation",        "translation",        "min", "max", "offset", "multiplier"],
        //shifter
       // ["gear_A",  "atv_shifter", "f7r","f7l","f8r",      {"x":90, "y":0, "z":0},  {"x":0, "y":0, "z":0.1}, {"x":0, "y":0.14, "z":0},  0, 1, -0.12, 1],
    ],
    "nodes": [
        ["id", "posX", "posY", "posZ"],
        //--CVT--
        {"selfCollision":false},
        {"collision":true},
        {"nodeMaterial":"|NM_METAL"},
        {"frictionCoef":0.5},
        {"group":"atv_transmission"},
        {"nodeWeight":4.5},
        ["tra1", 0.2, -1.12, 0.45],
        {"group":""},
    ],
    "beams": [
        ["id1:", "id2:"],
        //--TRANSMISSION CONE--
        {"beamType":"|NORMAL", "beamLongBound":1.0, "beamShortBound":1.0},
        {"beamSpring":12001000,"beamDamp":400},
        {"beamDeform":250000,"beamStrength":"FLT_MAX"},
        ["tra1","e1l"],
        ["tra1","e2l"],
        ["tra1","e4l"],
        ["tra1","e3l"],

        {"beamSpring":21000,"beamDamp":50},
        {"beamDeform":1000,"beamStrength":1500},
        ["e2l","b11l", {"name":"driveshaft_right"}],
        ["e1l","b11r", {"name":"driveshaft_left"}],
        {"breakGroup":""},
        {"beamPrecompression":1, "beamType":"|NORMAL", "beamLongBound":1.0, "beamShortBound":1.0},
    ],
},
"atv_diff_ratio_3_3": {
    "information":{
        "authors":"BeamNG",
        "name":"3.30:1 Final Drive",
        "value":150,
    },
    "slotType" : "atv_diff_ratio",
    "differential_F" : {
        "gearRatio":3.3,
    },
},
"atv_diff_ratio_2_65": {
    "information":{
        "authors":"BeamNG",
        "name":"2.65:1 Final Drive",
        "value":150,
    },
    "slotType" : "atv_diff_ratio",
    "differential_F" : {
        "gearRatio":2.65,
    },
},
"atv_diff_ratio_2_25": {
    "information":{
        "authors":"BeamNG",
        "name":"2.25:1 Final Drive",
        "value":150,
    },
    "slotType" : "atv_diff_ratio",
    "differential_F" : {
        "gearRatio":2.25,
    },
},
}