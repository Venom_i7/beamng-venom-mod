{
"covet_transmission_4A": {
    "information":{
        "authors":"BeamNG",
        "name":"4-Speed Automatic Transmission",
        "value":1200,
    },

    "slotType" : "covet_transmission",
    "slots": [
        ["type", "default", "description"],
        ["covet_converter","covet_converter","Torque Converter", {"coreSlot":true}],
        ["covet_differential_F","covet_differential_F", "Front Differential"],
    ],
    "powertrain": [
        ["type", "name", "inputName", "inputIndex"],
        ["torqueConverter", "torqueConverter", "mainEngine", 1],
        ["automaticGearbox", "gearbox", "torqueConverter", 1],
    ],
    "gearbox": {
        "uiName":"Gearbox",
        "gearRatios":[-2.32, 0, 3.032, 1.7298, 1.116, 0.75888],
        "parkLockTorque":1500,
        "oneWayViscousCoef":22,
        "friction": 0.74,
        "dynamicFriction": 0.00073,
        "torqueLossCoef": 0.017,
        "gearboxNode:":["tra1"],
    },
    "torqueConverter": {
        "uiName":"Torque Converter",
    },
    "vehicleController": {
        "automaticModes":"PRND21",
        "useSmartAggressionCalculation":false,
        "calculateOptimalLoadShiftPoints": true,
        "transmissionGearChangeDelay":1.55,
        "gearboxDecisionSmoothingUp":1.1,
        "gearboxDecisionSmoothingDown":0.3,
        "lowShiftDownRPM":1500,
        "lowShiftUpRPM":[0,0,1800,2500,2700],
        "wheelSlipUpThreshold":40000,
        "shiftDownRPMOffsetCoef":1.4,
    },
    "flexbodies": [
        ["mesh", "[group]:", "nonFlexMaterials"],
        ["covet_transmission_fwd", ["covet_engine","covet_transmission"]],
    ],
    "nodes": [
        ["id", "posX", "posY", "posZ"],
        //--4 Speed Auto--
        {"selfCollision":false},
        {"collision":true},
        {"nodeMaterial":"|NM_METAL"},
        {"frictionCoef":0.5},
        {"group":"covet_transmission"},
        {"nodeWeight":8},
        ["tra1", -0.37, -1.43, 0.39],
        {"group":"covet_differential"},
        ["tra2", -0.0025, -1.2777, 0.27917, {"group":"covet_halfshaft_L"}],
        ["tra3", -0.25, -1.2777, 0.27917, {"group":"covet_halfshaft_R"}],
        {"group":""},
        //transmission mount node
        ["em1r", -0.38, -1.4, 0.45, {"nodeWeight":3}],
    ],
    "beams": [
          ["id1:", "id2:"],
          //--TRANSMISSION CONE--
          {"beamType":"|NORMAL", "beamLongBound":1.0, "beamShortBound":1.0},
          {"beamSpring":8001000,"beamDamp":250},
          {"beamDeform":250000,"beamStrength":"FLT_MAX"},
          ["tra1","e1r"],
          ["tra1","e3r"],
          ["tra1","e1l"],
          ["tra1","e3l"],
          ["tra1","tra2"],
          ["tra1","tra3"],
          ["tra2","tra3"],
          ["tra2","e2l"],
          ["tra3","e2l"],
          ["tra2","e1l"],
          ["tra3","e1l"],
          ["tra2","e3l"],
          ["tra3","e3l"],
          ["tra2","e4l"],
          ["tra3","e4l"],

          //transmission mount node
          {"beamSpring":4400000,"beamDamp":150},
          {"beamDeform":90000,"beamStrength":"FLT_MAX"},
          ["em1r","e3l"],
          ["em1r","e3r"],
          ["em1r","e4l"],
          ["em1r","e4r"],
          ["em1r", "e1r"],
          ["em1r", "e1l"],
          ["em1r", "e2l"],
          ["em1r", "e2r"],
          ["em1r", "tra1"],
          {"beamPrecompression":1, "beamType":"|NORMAL", "beamLongBound":1.0, "beamShortBound":1.0},
    ],
},
"covet_transmission_4M": {
    "information":{
        "authors":"BeamNG",
        "name":"4-Speed Manual Transmission",
        "value":350,
    },

    "slotType" : "covet_transmission",

    "slots": [
        ["type", "default", "description"],
        ["covet_flywheel","covet_flywheel","Flywheel", {"coreSlot":true}],
        ["covet_differential_F","covet_differential_F", "Front Differential"],
    ],
    "powertrain": [
        ["type", "name", "inputName", "inputIndex"],
        ["frictionClutch", "clutch", "mainEngine", 1],
        ["manualGearbox", "gearbox", "clutch", 1],
    ],
    "gearbox": {
        "uiName":"Gearbox",
        "gearRatios":[-3.545, 0, 3.727, 2.048, 1.321, 0.903],
        "friction": 0.61,
        "dynamicFriction": 0.00062,
        "torqueLossCoef": 0.0155,
        "gearboxNode:":["tra1"],

        "gearWhineCoefsInput":  [0.60, 0.00, 0.12, 0.12, 0.12, 0.12, 0.12, 0.12, 0.12, 0.12],
        "gearWhineCoefsOutput": [0.00, 0.00, 0.24, 0.24, 0.24, 0.24, 0.24, 0.24, 0.24, 0.24],
        "gearWhineInputEvent": "event:>Vehicle>Transmission>helical_01>twine_in",
        "gearWhineOutputEvent": "event:>Vehicle>Transmission>helical_01>twine_out",

        "forwardInputPitchCoef":1.05
        "forwardOutputPitchCoef":1.05
        //"reverseInputPitchCoef":0.7
        //"reverseOutputPitchCoef":0.7

        //"gearWhineInputPitchCoefSmoothing":50
        //"gearWhineOutputPitchCoefSmoothing":50
        //"gearWhineInputVolumeCoefSmoothing":10
        //"gearWhineOutputVolumeCoefSmoothing":10

        //"gearWhineFixedCoefOutput": 0.7
        //"gearWhineFixedCoefInput": 0.4
    },
    "vehicleController": {
        "calculateOptimalLoadShiftPoints": true,
        "shiftDownRPMOffsetCoef":1.4,
        "lowShiftDownRPM":[0,0,0,1500,1700,1600],
        "lowShiftUpRPM":[0,0,3400,2800,2700],
        "wheelSlipUpThreshold":12000,
    },
    "flexbodies": [
        ["mesh", "[group]:", "nonFlexMaterials"],
        ["covet_transmission_fwd", ["covet_engine","covet_transmission"]],
    ],
    "nodes": [
        ["id", "posX", "posY", "posZ"],
        //--5 Speed Manual--
        {"selfCollision":false},
        {"collision":true},
        {"nodeMaterial":"|NM_METAL"},
        {"frictionCoef":0.5},
        {"group":"covet_transmission"},
        {"nodeWeight":6},
        ["tra1", -0.37, -1.43, 0.39],
        {"group":"covet_differential"},
        ["tra2", -0.25, -1.25, 0.325, {"group":"covet_halfshaft_L"}],
        ["tra3", -0.25, -1.2777, 0.27917, {"group":"covet_halfshaft_R"}],
        {"group":""},
        //transmission mount node
        ["em1r", -0.38, -1.4, 0.45, {"nodeWeight":3}],
    ],
    "beams": [
          ["id1:", "id2:"],
          //--TRANSMISSION CONE--
          {"beamType":"|NORMAL", "beamLongBound":1.0, "beamShortBound":1.0},
          {"beamSpring":8001000,"beamDamp":250},
          {"beamDeform":250000,"beamStrength":"FLT_MAX"},
          ["tra1","e1r"],
          ["tra1","e3r"],
          ["tra1","e1l"],
          ["tra1","e3l"],
          ["tra1","tra2"],
          ["tra1","tra3"],
          ["tra2","tra3"],
          ["tra2","e2l"],
          ["tra3","e2l"],
          ["tra2","e1l"],
          ["tra3","e1l"],
          ["tra2","e3l"],
          ["tra3","e3l"],
          ["tra2","e4l"],
          ["tra3","e4l"],

          //transmission mount node
          {"beamSpring":4400000,"beamDamp":150},
          {"beamDeform":90000,"beamStrength":"FLT_MAX"},
          ["em1r","e3l"],
          ["em1r","e3r"],
          ["em1r","e4l"],
          ["em1r","e4r"],
          ["em1r", "e1r"],
          ["em1r", "e1l"],
          ["em1r", "e2l"],
          ["em1r", "e2r"],
          ["em1r", "tra1"],
          {"beamPrecompression":1, "beamType":"|NORMAL", "beamLongBound":1.0, "beamShortBound":1.0},
    ],
},
"covet_transmission_5M": {
    "information":{
        "authors":"BeamNG",
        "name":"5-Speed Manual Transmission",
        "value":550,
    },

    "slotType" : "covet_transmission",
    "slots": [
        ["type", "default","description"],
        ["covet_flywheel","covet_flywheel","Flywheel", {"coreSlot":true}],
        ["covet_differential_F","covet_differential_F", "Front Differential"],
    ],
    "powertrain": [
        ["type", "name", "inputName", "inputIndex"],
        ["frictionClutch", "clutch", "mainEngine", 1],
        ["manualGearbox", "gearbox", "clutch", 1],
    ],
    "gearbox": {
        "uiName":"Gearbox",
        "gearRatios":[-3.546, 0, 3.365, 1.864, 1.322, 0.967, 0.795],
        "friction": 0.61,
        "dynamicFriction": 0.00062,
        "torqueLossCoef": 0.0155,
        "gearboxNode:":["tra1"],

        "gearWhineCoefsInput":  [0.60, 0.00, 0.12, 0.12, 0.12, 0.12, 0.12, 0.12, 0.12, 0.12],
        "gearWhineCoefsOutput": [0.00, 0.00, 0.24, 0.24, 0.24, 0.24, 0.24, 0.24, 0.24, 0.24],
        "gearWhineInputEvent": "event:>Vehicle>Transmission>helical_01>twine_in",
        "gearWhineOutputEvent": "event:>Vehicle>Transmission>helical_01>twine_out",

        "forwardInputPitchCoef":1.05
        "forwardOutputPitchCoef":1.05
        //"reverseInputPitchCoef":0.7
        //"reverseOutputPitchCoef":0.7

        //"gearWhineInputPitchCoefSmoothing":50
        //"gearWhineOutputPitchCoefSmoothing":50
        //"gearWhineInputVolumeCoefSmoothing":10
        //"gearWhineOutputVolumeCoefSmoothing":10

        //"gearWhineFixedCoefOutput": 0.7
        //"gearWhineFixedCoefInput": 0.4
    },
    "vehicleController": {
        "calculateOptimalLoadShiftPoints": true,
        //"shiftDownRPMOffsetCoef":1.25,
        //"aggressionSmoothingDown":0.05
        "lowShiftDownRPM":[0,0,0,1600,2000,1800,1800],
        "lowShiftUpRPM":[0,0,3400,3200,3000,3000],
        "wheelSlipUpThreshold":12000,
    },
    "flexbodies": [
        ["mesh", "[group]:", "nonFlexMaterials"],
        ["covet_transmission_fwd", ["covet_engine","covet_transmission"]],
    ],
    "nodes": [
        ["id", "posX", "posY", "posZ"],
        //--5 Speed Manual--
        {"selfCollision":false},
        {"collision":true},
        {"nodeMaterial":"|NM_METAL"},
        {"frictionCoef":0.5},
        {"group":"covet_transmission"},
        {"nodeWeight":6.5},
        ["tra1", -0.37, -1.43, 0.39],
        {"group":"covet_differential"},
        ["tra2", -0.0025, -1.2777, 0.27917, {"group":"covet_halfshaft_L"}],
        ["tra3", -0.25, -1.2777, 0.27917, {"group":"covet_halfshaft_R"}],
        {"group":""},
        //transmission mount node
        ["em1r", -0.38, -1.4, 0.45, {"nodeWeight":3}],
    ],
    "beams": [
          ["id1:", "id2:"],
          //--TRANSMISSION CONE--
          {"beamType":"|NORMAL", "beamLongBound":1.0, "beamShortBound":1.0},
          {"beamSpring":8001000,"beamDamp":250},
          {"beamDeform":250000,"beamStrength":"FLT_MAX"},
          ["tra1","e1r"],
          ["tra1","e3r"],
          ["tra1","e1l"],
          ["tra1","e3l"],
          ["tra1","tra2"],
          ["tra1","tra3"],
          ["tra2","tra3"],
          ["tra2","e2l"],
          ["tra3","e2l"],
          ["tra2","e1l"],
          ["tra3","e1l"],
          ["tra2","e3l"],
          ["tra3","e3l"],
          ["tra2","e4l"],
          ["tra3","e4l"],

          //transmission mount node
          {"beamSpring":4400000,"beamDamp":150},
          {"beamDeform":90000,"beamStrength":"FLT_MAX"},
          ["em1r","e3l"],
          ["em1r","e3r"],
          ["em1r","e4l"],
          ["em1r","e4r"],
          ["em1r", "e1r"],
          ["em1r", "e1l"],
          ["em1r", "e2l"],
          ["em1r", "e2r"],
          ["em1r", "tra1"],
          {"beamPrecompression":1, "beamType":"|NORMAL", "beamLongBound":1.0, "beamShortBound":1.0},
    ],
},
"covet_transmission_5M_ee": {
    "information":{
        "authors":"BeamNG",
        "name":"5-Speed High-Efficiency Manual Transmission",
        "value":600
    },

    "slotType" : "covet_transmission",
    "slots": [
        ["type", "default","description"],
        ["covet_flywheel","covet_flywheel","Flywheel", {"coreSlot":true}],
        ["covet_differential_F","covet_differential_F", "Front Differential"],
    ],
    "powertrain": [
        ["type", "name", "inputName", "inputIndex"],
        ["frictionClutch", "clutch", "mainEngine", 1],
        ["manualGearbox", "gearbox", "clutch", 1],
    ],
    "gearbox": {
        "uiName":"Gearbox",
        "gearRatios":[-3.546, 0, 3.805, 2.048, 1.322, 0.967, 0.795],
        "friction":0.92,
        "dynamicFriction":0.0009,
        "torqueLossCoef": 0.015,
        "gearboxNode:":["ta1"],

        "gearWhineCoefsInput":  [0.60, 0.00, 0.12, 0.12, 0.12, 0.12, 0.12, 0.12, 0.12, 0.12],
        "gearWhineCoefsOutput": [0.00, 0.00, 0.24, 0.24, 0.24, 0.24, 0.24, 0.24, 0.24, 0.24],
        "gearWhineInputEvent": "event:>Vehicle>Transmission>helical_01>twine_in",
        "gearWhineOutputEvent": "event:>Vehicle>Transmission>helical_01>twine_out",

        //"forwardInputPitchCoef":1
        //"forwardOutputPitchCoef":1
        //"reverseInputPitchCoef":0.7
        //"reverseOutputPitchCoef":0.7

        //"gearWhineInputPitchCoefSmoothing":50
        //"gearWhineOutputPitchCoefSmoothing":50
        //"gearWhineInputVolumeCoefSmoothing":10
        //"gearWhineOutputVolumeCoefSmoothing":10

        //"gearWhineFixedCoefOutput": 0.7
        //"gearWhineFixedCoefInput": 0.4
    },
    "vehicleController": {
        "calculateOptimalLoadShiftPoints": true,
        //"shiftDownRPMOffsetCoef":1.25,
        //"aggressionSmoothingDown":0.05
        "lowShiftDownRPM":[0,0,0,1600,2000,1800,1800],
        "lowShiftUpRPM":[0,0,3400,3200,3000,3000],
        "wheelSlipUpThreshold":12000,
    },
    "flexbodies": [
        ["mesh", "[group]:", "nonFlexMaterials"],
        ["covet_transmission_fwd", ["covet_engine","covet_transmission"]],
    ],
    "nodes": [
        ["id", "posX", "posY", "posZ"],
        //--5 Speed Manual--
        {"selfCollision":false},
        {"collision":true},
        {"nodeMaterial":"|NM_METAL"},
        {"frictionCoef":0.5},
        {"group":"covet_transmission"},
        {"nodeWeight":6.5},
        ["tra1", -0.37, -1.43, 0.39],
        {"group":"covet_differential"},
        ["tra2", -0.25, -1.25, 0.325, {"group":"covet_halfshaft_L"}],
        ["tra3", -0.25, -1.2777, 0.27917, {"group":"covet_halfshaft_R"}],
        {"group":""},
        //transmission mount node
        ["em1r", -0.38, -1.4, 0.45, {"nodeWeight":3}],
    ],
    "beams": [
          ["id1:", "id2:"],
          //--TRANSMISSION CONE--
          {"beamType":"|NORMAL", "beamLongBound":1.0, "beamShortBound":1.0},
          {"beamSpring":8001000,"beamDamp":250},
          {"beamDeform":250000,"beamStrength":"FLT_MAX"},
          ["tra1","e1r"],
          ["tra1","e3r"],
          ["tra1","e1l"],
          ["tra1","e3l"],
          ["tra1","tra2"],
          ["tra1","tra3"],
          ["tra2","tra3"],
          ["tra2","e2l"],
          ["tra3","e2l"],
          ["tra2","e1l"],
          ["tra3","e1l"],
          ["tra2","e3l"],
          ["tra3","e3l"],
          ["tra2","e4l"],
          ["tra3","e4l"],

          //transmission mount node
          {"beamSpring":4400000,"beamDamp":150},
          {"beamDeform":90000,"beamStrength":"FLT_MAX"},
          ["em1r","e3l"],
          ["em1r","e3r"],
          ["em1r","e4l"],
          ["em1r","e4r"],
          ["em1r", "e1r"],
          ["em1r", "e1l"],
          ["em1r", "e2l"],
          ["em1r", "e2r"],
          ["em1r", "tra1"],
          {"beamPrecompression":1, "beamType":"|NORMAL", "beamLongBound":1.0, "beamShortBound":1.0},
    ],
},
"covet_transmission_5M_sport": {
    "information":{
        "authors":"BeamNG",
        "name":"5-Speed Sport Manual Transmission",
        "value":1150,
    },

    "slotType" : "covet_transmission",
    "slots": [
        ["type", "default","description"],
        ["covet_flywheel","covet_flywheel","Flywheel", {"coreSlot":true}],
        ["covet_differential_F","covet_differential_F", "Front Differential"],
    ],
    "powertrain": [
        ["type", "name", "inputName", "inputIndex"],
        ["frictionClutch", "clutch", "mainEngine", 1],
        ["manualGearbox", "gearbox", "clutch", 1],
    ],
    "gearbox": {
        "uiName":"Gearbox",
        "gearRatios":[-3.546, 0, 3.365, 1.864, 1.322, 1.029, 0.821],
        "friction": 0.61,
        "dynamicFriction": 0.00062,
        "torqueLossCoef": 0.0155,
        "gearboxNode:":["tra1"],

        "gearWhineCoefsInput":  [0.60, 0.00, 0.15, 0.15, 0.15, 0.15, 0.15, 0.15, 0.15, 0.15],
        "gearWhineCoefsOutput": [0.00, 0.00, 0.30, 0.30, 0.30, 0.30, 0.30, 0.30, 0.30, 0.30],
        "gearWhineInputEvent": "event:>Vehicle>Transmission>helical_01>twine_in_tuned",
        "gearWhineOutputEvent": "event:>Vehicle>Transmission>helical_01>twine_out_tuned",

        "forwardInputPitchCoef":1.05
        "forwardOutputPitchCoef":1.05
        //"reverseInputPitchCoef":0.7
        //"reverseOutputPitchCoef":0.7

        //"gearWhineInputPitchCoefSmoothing":50
        //"gearWhineOutputPitchCoefSmoothing":50
        //"gearWhineInputVolumeCoefSmoothing":10
        //"gearWhineOutputVolumeCoefSmoothing":10

        //"gearWhineFixedCoefOutput": 0.7
        //"gearWhineFixedCoefInput": 0.4
    },
    "vehicleController": {
        "calculateOptimalLoadShiftPoints": true,
        //"shiftDownRPMOffsetCoef":1.25,
        //"aggressionSmoothingDown":0.05
        "lowShiftDownRPM":[0,0,0,1600,2000,1800,1800],
        "lowShiftUpRPM":[0,0,3400,3200,3000,3000],
        "wheelSlipUpThreshold":12000,
    },
    "flexbodies": [
        ["mesh", "[group]:", "nonFlexMaterials"],
        ["covet_transmission_fwd", ["covet_engine","covet_transmission"]],
    ],
    "nodes": [
        ["id", "posX", "posY", "posZ"],
        //--5 Speed Manual--
        {"selfCollision":false},
        {"collision":true},
        {"nodeMaterial":"|NM_METAL"},
        {"frictionCoef":0.5},
        {"group":"covet_transmission"},
        {"nodeWeight":6.5},
        ["tra1", -0.37, -1.43, 0.39],
        {"group":"covet_differential"},
        ["tra2", -0.0025, -1.2777, 0.27917, {"group":"covet_halfshaft_L"}],
        ["tra3", -0.25, -1.2777, 0.27917, {"group":"covet_halfshaft_R"}],
        {"group":""},
        //transmission mount node
        ["em1r", -0.38, -1.4, 0.45, {"nodeWeight":3}],
    ],
    "beams": [
          ["id1:", "id2:"],
          //--TRANSMISSION CONE--
          {"beamType":"|NORMAL", "beamLongBound":1.0, "beamShortBound":1.0},
          {"beamSpring":8001000,"beamDamp":250},
          {"beamDeform":250000,"beamStrength":"FLT_MAX"},
          ["tra1","e1r"],
          ["tra1","e3r"],
          ["tra1","e1l"],
          ["tra1","e3l"],
          ["tra1","tra2"],
          ["tra1","tra3"],
          ["tra2","tra3"],
          ["tra2","e2l"],
          ["tra3","e2l"],
          ["tra2","e1l"],
          ["tra3","e1l"],
          ["tra2","e3l"],
          ["tra3","e3l"],
          ["tra2","e4l"],
          ["tra3","e4l"],

          //transmission mount node
          {"beamSpring":4400000,"beamDamp":150},
          {"beamDeform":90000,"beamStrength":"FLT_MAX"},
          ["em1r","e3l"],
          ["em1r","e3r"],
          ["em1r","e4l"],
          ["em1r","e4r"],
          ["em1r", "e1r"],
          ["em1r", "e1l"],
          ["em1r", "e2l"],
          ["em1r", "e2r"],
          ["em1r", "tra1"],
          {"beamPrecompression":1, "beamType":"|NORMAL", "beamLongBound":1.0, "beamShortBound":1.0},
    ],
},
"covet_transmission_6M_race": {
    "information":{
        "authors":"BeamNG",
        "name":"Race 6-Speed Manual Transmission",
        "value":3200,
    },

    "slotType" : "covet_transmission",
    "slots": [
        ["type", "default","description"],
        ["covet_flywheel","covet_flywheel_race","Flywheel", {"coreSlot":true}],
        ["covet_differential_F","covet_differential_F", "Front Differential"],
    ],
    "variables": [
        ["name", "type", "unit", "category", "default", "min", "max", "title", "description"],
        ["$gear_R", "range", ":1", "Transmission", 3.58, 0.5, 5, "Reverse Gear Ratio", "Torque multiplication ratio", {"stepDis":0.01}],
        ["$gear_1", "range", ":1", "Transmission", 3.27, 0.5, 5, "1st Gear Ratio",     "Torque multiplication ratio", {"stepDis":0.01}],
        ["$gear_2", "range", ":1", "Transmission", 2.13, 0.5, 5, "2nd Gear Ratio",     "Torque multiplication ratio", {"stepDis":0.01}],
        ["$gear_3", "range", ":1", "Transmission", 1.52, 0.5, 5, "3rd Gear Ratio",     "Torque multiplication ratio", {"stepDis":0.01}],
        ["$gear_4", "range", ":1", "Transmission", 1.15, 0.5, 5, "4th Gear Ratio",     "Torque multiplication ratio", {"stepDis":0.01}],
        ["$gear_5", "range", ":1", "Transmission", 0.92, 0.5, 5, "5th Gear Ratio",     "Torque multiplication ratio", {"stepDis":0.01}],
        ["$gear_6", "range", ":1", "Transmission", 0.74, 0.5, 5, "6th Gear Ratio",     "Torque multiplication ratio", {"stepDis":0.01}],
    ],
    "powertrain": [
        ["type", "name", "inputName", "inputIndex"],
        ["frictionClutch", "clutch", "mainEngine", 1],
        ["manualGearbox", "gearbox", "clutch", 1],
    ],
    "gearbox": {
        "uiName":"Gearbox",
        "gearRatios":["$=-$gear_R", 0, "$gear_1", "$gear_2", "$gear_3", "$gear_4", "$gear_5", "$gear_6"],
        "friction": 0.76,
        "dynamicFriction": 0.00076,
        "torqueLossCoef": 0.013,
        "gearboxNode:":["tra1"],

        "gearWhineCoefsInput":  [0.66, 0.00, 0.33, 0.33, 0.33, 0.33, 0.33, 0.33, 0.33, 0.33],
        "gearWhineCoefsOutput": [0.00, 0.00, 0.66, 0.66, 0.66, 0.66, 0.66, 0.66, 0.66, 0.66],
        "gearWhineInputEvent": "event:>Vehicle>Transmission>straight_01>twine_in_race",
        "gearWhineOutputEvent": "event:>Vehicle>Transmission>straight_01>twine_out_race",

        "forwardInputPitchCoef":1.05
        "forwardOutputPitchCoef":1.05
        //"reverseInputPitchCoef":0.7
        //"reverseOutputPitchCoef":0.7

        //"gearWhineInputPitchCoefSmoothing":50
        //"gearWhineOutputPitchCoefSmoothing":50
        //"gearWhineInputVolumeCoefSmoothing":10
        //"gearWhineOutputVolumeCoefSmoothing":10

        //"gearWhineFixedCoefOutput": 0.7
        //"gearWhineFixedCoefInput": 0.4
    },
    "clutch": {
        "clutchFreePlay":0.2,
    },
    "vehicleController": {
        "transmissionShiftDelay":0.12,
        "shiftDownRPMOffsetCoef":1.12,
        "calculateOptimalLoadShiftPoints": true,
        //"aggressionSmoothingDown":0.05
        "aggressionHoldOffThrottleDelay":3,
        "lowShiftDownRPM":[0,0,0,2100,2300,2300,2300,2300],
        "lowShiftUpRPM":[0,0,4000,3800,3700,3550,3450],
        //"lowShiftDownRPM":[0,0,0,1600,1900,1800,1600,1500],
        //"lowShiftUpRPM":[0,0,3400,3000,2900,2800,2700],
        "wheelSlipUpThreshold":10000,
    },
    "flexbodies": [
        ["mesh", "[group]:", "nonFlexMaterials"],
        ["covet_transmission_fwd", ["covet_engine","covet_transmission"]],
    ],
    "nodes": [
        ["id", "posX", "posY", "posZ"],
        //--6 Speed Manual--
        {"selfCollision":false},
        {"collision":true},
        {"nodeMaterial":"|NM_METAL"},
        {"frictionCoef":0.5},
        {"group":"covet_transmission"},
        {"nodeWeight":7},
        ["tra1", 0.37, -1.43, 0.39],
        ["tra2", -0.0025, -1.2777, 0.27917, {"group":"covet_halfshaft_L"}],
        ["tra3", -0.25, -1.2777, 0.27917, {"group":"covet_halfshaft_R"}],
        {"group":""},
        //transmission mount node
        ["em1r", -0.38, -1.4, 0.45, {"nodeWeight":3}],
    ],
    "beams": [
          ["id1:", "id2:"],
          //--TRANSMISSION CONE--
          {"beamType":"|NORMAL", "beamLongBound":1.0, "beamShortBound":1.0},
          {"beamSpring":8001000,"beamDamp":250},
          {"beamDeform":250000,"beamStrength":"FLT_MAX"},
          ["tra1","e1r"],
          ["tra1","e3r"],
          ["tra1","e1l"],
          ["tra1","e3l"],
          ["tra1","tra2"],
          ["tra1","tra3"],
          ["tra2","tra3"],
          ["tra2","e2l"],
          ["tra3","e2l"],
          ["tra2","e1l"],
          ["tra3","e1l"],
          ["tra2","e3l"],
          ["tra3","e3l"],
          ["tra2","e4l"],
          ["tra3","e4l"],

           //transmission mount node
          {"beamSpring":4400000,"beamDamp":150},
          {"beamDeform":90000,"beamStrength":"FLT_MAX"},
          ["em1r","e3l"],
          ["em1r","e3r"],
          ["em1r","e4l"],
          ["em1r","e4r"],
          ["em1r", "e1r"],
          ["em1r", "e1l"],
          ["em1r", "e2l"],
          ["em1r", "e2r"],
          ["em1r", "tra1"],
          {"beamPrecompression":1, "beamType":"|NORMAL", "beamLongBound":1.0, "beamShortBound":1.0},
    ],
},
"covet_transmission_5M_sequential": {
    "information":{
        "authors":"BeamNG",
        "name":"Race 5-Speed Sequential Transmission",
        "value":6450,
    },

    "slotType" : "covet_transmission",
    "slots": [
        ["type", "default","description"],
        ["covet_flywheel","covet_flywheel_race","Flywheel", {"coreSlot":true}],
        ["covet_differential_F","covet_differential_F", "Front Differential"],
    ],
    "variables": [
        ["name", "type", "unit", "category", "default", "min", "max", "title", "description"],
        ["$gear_R", "range", ":1", "Transmission", 3.0, 0.5, 5, "Reverse Gear Ratio", "Torque multiplication ratio", {"stepDis":0.01}],
        ["$gear_1", "range", ":1", "Transmission", 3.31, 0.5, 5, "1st Gear Ratio",     "Torque multiplication ratio", {"stepDis":0.01}],
        ["$gear_2", "range", ":1", "Transmission", 1.81, 0.5, 5, "2nd Gear Ratio",     "Torque multiplication ratio", {"stepDis":0.01}],
        ["$gear_3", "range", ":1", "Transmission", 1.19, 0.5, 5, "3rd Gear Ratio",     "Torque multiplication ratio", {"stepDis":0.01}],
        ["$gear_4", "range", ":1", "Transmission", 0.87, 0.5, 5, "4th Gear Ratio",     "Torque multiplication ratio", {"stepDis":0.01}],
        ["$gear_5", "range", ":1", "Transmission", 0.71, 0.5, 5, "5th Gear Ratio",     "Torque multiplication ratio", {"stepDis":0.01}],
    ],
    "powertrain": [
        ["type", "name", "inputName", "inputIndex"],
        ["frictionClutch", "clutch", "mainEngine", 1],
        ["sequentialGearbox", "gearbox", "clutch", 1],
    ],
    "gearbox": {
        "uiName":"Gearbox",
        "gearRatios":["$=-$gear_R", 0, "$gear_1", "$gear_2", "$gear_3", "$gear_4", "$gear_5"],
        "friction": 0.76,
        "dynamicFriction": 0.00076,
        "torqueLossCoef": 0.012,
        "gearboxNode:":["tra1"],

        "gearWhineCoefsInput":  [0.66, 0.00, 0.33, 0.33, 0.33, 0.33, 0.33, 0.33, 0.33, 0.33],
        "gearWhineCoefsOutput": [0.00, 0.00, 0.66, 0.66, 0.66, 0.66, 0.66, 0.66, 0.66, 0.66],
        "gearWhineInputEvent": "event:>Vehicle>Transmission>straight_01>twine_in_race",
        "gearWhineOutputEvent": "event:>Vehicle>Transmission>straight_01>twine_out_race",

        //"forwardInputPitchCoef":1
        //"forwardOutputPitchCoef":1
        //"reverseInputPitchCoef":0.7
        //"reverseOutputPitchCoef":0.7

        //"gearWhineInputPitchCoefSmoothing":50
        //"gearWhineOutputPitchCoefSmoothing":50
        //"gearWhineInputVolumeCoefSmoothing":10
        //"gearWhineOutputVolumeCoefSmoothing":10

        //"gearWhineFixedCoefOutput": 0.7
        //"gearWhineFixedCoefInput": 0.4
    },
    "clutch": {
        "clutchFreePlay":0.75,
        "lockSpringCoef":0.3,
    },
    "vehicleController": {
        "calculateOptimalLoadShiftPoints": true,
        "shiftDownRPMOffsetCoef":1.17,
        //"aggressionSmoothingDown":0.05
        "aggressionHoldOffThrottleDelay":3,
        //"lowShiftDownRPM":[0,0,0,1600,1900,1800,1600,1500],
        //"lowShiftUpRPM":[0,0,3500,3300,3150,3000,2900],
        "lowShiftDownRPM":[0,0,0,2000,2600,2600,2600,2600],
        "lowShiftUpRPM":[0,0,4200,4200,4050,3850,3700],
        //"wheelSlipUpThreshold":100,
        "clutchLaunchStartRPM":3000,
        "clutchLaunchTargetRPM":3000,
    },
    "flexbodies": [
        ["mesh", "[group]:", "nonFlexMaterials"],
        ["covet_transmission_fwd", ["covet_engine","covet_transmission"]],
    ],
    "nodes": [
        ["id", "posX", "posY", "posZ"],
        //--6 Speed Manual--
        {"selfCollision":false},
        {"collision":true},
        {"nodeMaterial":"|NM_METAL"},
        {"frictionCoef":0.5},
        {"group":"covet_transmission"},
        {"nodeWeight":7},
        ["tra1", -0.37, -1.43, 0.39],
        {"group":"covet_differential"},
        ["tra2", -0.0025, -1.2777, 0.27917, {"group":"covet_halfshaft_L"}],
        ["tra3", -0.25, -1.2777, 0.27917, {"group":"covet_halfshaft_R"}],
        {"group":""},
        //transmission mount node
        ["em1r", -0.38, -1.4, 0.45, {"nodeWeight":3}],
    ],
    "beams": [
          ["id1:", "id2:"],
          //--TRANSMISSION CONE--
          {"beamType":"|NORMAL", "beamLongBound":1.0, "beamShortBound":1.0},
          {"beamSpring":8001000,"beamDamp":250},
          {"beamDeform":250000,"beamStrength":"FLT_MAX"},
          ["tra1","e1r"],
          ["tra1","e3r"],
          ["tra1","e1l"],
          ["tra1","e3l"],
          ["tra1","tra2"],
          ["tra1","tra3"],
          ["tra2","tra3"],
          ["tra2","e2l"],
          ["tra3","e2l"],
          ["tra2","e1l"],
          ["tra3","e1l"],
          ["tra2","e3l"],
          ["tra3","e3l"],
          ["tra2","e4l"],
          ["tra3","e4l"],

           //transmission mount node
          {"beamSpring":4400000,"beamDamp":150},
          {"beamDeform":90000,"beamStrength":"FLT_MAX"},
          ["em1r","e3l"],
          ["em1r","e3r"],
          ["em1r","e4l"],
          ["em1r","e4r"],
          ["em1r", "e1r"],
          ["em1r", "e1l"],
          ["em1r", "e2l"],
          ["em1r", "e2r"],
          ["em1r", "tra1"],
          {"beamPrecompression":1, "beamType":"|NORMAL", "beamLongBound":1.0, "beamShortBound":1.0},
    ],
},
"covet_converter": {
    "information":{
        "authors":"BeamNG",
        "name":"Locking Torque Converter",
        "value":150,
    },
    "slotType" : "covet_converter",
    "torqueConverter": {
        "uiName":"Torque Converter",
        "converterDiameter": 0.25,
        "converterStiffness":7,
        "couplingAVRatio":0.9,
        "stallTorqueRatio":1.5,
        "lockupClutchTorque":220,
        "additionalEngineInertia":0.10,
    },
    "vehicleController": {
        "torqueConverterLockupRPM":1300,
        "torqueConverterLockupMinGear":3,
    },
},
"covet_flywheel": {
    "information":{
        "authors":"BeamNG",
        "name":"Flywheel",
        "value":150,
    },
    "slotType" : "covet_flywheel",
    "clutch": {
        "uiName":"Clutch",
        "additionalEngineInertia":0.08,
        "clutchMass":4,
    },
},
"covet_flywheel_race": {
    "information":{
        "authors":"BeamNG",
        "name":"Ultra Light Flywheel",
        "value":600,
    },
    "slotType" : "covet_flywheel",
    "clutch": {
        "uiName":"Clutch",
        "additionalEngineInertia":0.01,
        "clutchMass":3,
    },
},
"covet_flywheel_light": {
    "information":{
        "authors":"BeamNG",
        "name":"Lightened Flywheel",
        "value":400,
    },
    "slotType" : "covet_flywheel",
    "clutch": {
        "uiName":"Clutch",
        "additionalEngineInertia":0.04,
        "clutchMass":3.4,
    },
},
}
