{
"vivace_brake_F": {
    "information":{
        "authors":"BeamNG",
        "name":"Front Disc Brakes",

        "value":210,
    },
    "slotType" : "vivace_brake_F",
    "slots": [
        ["type", "default", "description"],
        ["brakepad_F","brakepad_F_premium", "Front Brake Pads", {"coreSlot":true}],
    ],
    "flexbodies": [
        ["mesh", "[group]:", "nonFlexMaterials"],
        //brake discs
        ["brake_hub_5l",                 ["wheel_FR",    "wheelhub_FR"], [], {"pos":{"x":-0.79, "y":-1.342, "z":0.32}, "scale":{"x":0.9, "y":1.0,  "z":1.0},  "rot":{"x":0,   "y":0,   "z":0}}],
        ["brake_hub_5l",                 ["wheel_FL",    "wheelhub_FL"], [], {"pos":{"x": 0.79, "y":-1.342, "z":0.32}, "scale":{"x":0.9, "y":1.0,  "z":1.0},  "rot":{"x":0,   "y":0,   "z":180}}],
        ["brake_disc_plain",             ["wheel_FR",    "wheelhub_FR"], [], {"pos":{"x":-0.80, "y":-1.342, "z":0.32}, "scale":{"x":1.0, "y":1.0,  "z":1.0},  "rot":{"x":0,   "y":0,   "z":0}}],
        ["brake_disc_plain",             ["wheel_FL",    "wheelhub_FL"], [], {"pos":{"x": 0.80, "y":-1.342, "z":0.32}, "scale":{"x":1.0, "y":1.0,  "z":1.0},  "rot":{"x":180, "y":0,   "z":0}}],
        ["brake_caliper_standard_plain", ["vivace_hub_F","wheelhub_FR"], [], {"pos":{"x":-0.80, "y":-1.332, "z":0.32}, "scale":{"x":1.0, "y":0.95, "z":0.95}, "rot":{"x":0,   "y":0,   "z":0}}],
        ["brake_caliper_standard_plain", ["vivace_hub_F","wheelhub_FL"], [], {"pos":{"x": 0.80, "y":-1.332, "z":0.32}, "scale":{"x":1.0, "y":0.95, "z":0.95}, "rot":{"x":0,   "y":180, "z":0}}],
    ],
    "pressureWheels": [
        ["name","hubGroup","group","node1:","node2:","nodeS","nodeArm:","wheelDir"],
        //brakes
        {"brakeTorque":"$=$brakestrength*2500"},
        {"brakeInputSplit":1},
        {"brakeSplitCoef":1},
        {"parkingTorque":0},
        {"brakeSpring":125},

        //brake thermals
        {"enableBrakeThermals":true},
        {"brakeDiameter":0.280},
        {"brakeMass":6.7},
        {"brakeType":"vented-disc"},
        {"rotorMaterial":"steel"},
        {"brakeVentingCoef":1.0},
        //abs
        {"absSlipRatioTarget":0.2},
        //brake sounds
        {"squealCoefNatural": 0.0, "squealCoefLowSpeed": 0.0}
    ],
},
"vivace_brake_R": {
    "information":{
        "authors":"BeamNG",
        "name":"Rear Disc Brakes",
        "value":180,
    },
    "slotType" : "vivace_brake_R",
    "slots": [
        ["type", "default", "description"],
        ["brakepad_R","brakepad_R_premium", "Rear Brake Pads", {"coreSlot":true}],
    ],
    "flexbodies": [
        ["mesh", "[group]:", "nonFlexMaterials"],
        ["brake_hub_5l",                 ["wheel_RR",    "wheelhub_RR"], [], {"pos":{"x":-0.79, "y":1.338, "z":0.32}, "scale":{"x":0.9,  "y":1.0,  "z":1.0},  "rot":{"x":0,   "y":0,   "z":0}}],
        ["brake_hub_5l",                 ["wheel_RL",    "wheelhub_RL"], [], {"pos":{"x": 0.79, "y":1.338, "z":0.32}, "scale":{"x":0.9,  "y":1.0,  "z":1.0},  "rot":{"x":0,   "y":0,   "z":180}}],
        ["brake_disc_solid",             ["wheel_RR",    "wheelhub_RR"], [], {"pos":{"x":-0.80, "y":1.338, "z":0.32}, "scale":{"x":0.93, "y":0.93, "z":0.93}, "rot":{"x":0,   "y":0,   "z":0}}],
        ["brake_disc_solid",             ["wheel_RL",    "wheelhub_RL"], [], {"pos":{"x": 0.80, "y":1.338, "z":0.32}, "scale":{"x":0.93, "y":0.93, "z":0.93}, "rot":{"x":180, "y":0,   "z":0}}],
        ["brake_caliper_standard_plain", ["vivace_hub_R","wheelhub_RR"], [], {"pos":{"x":-0.80, "y":1.338, "z":0.32}, "scale":{"x":0.93, "y":0.85, "z":0.85}, "rot":{"x":0,   "y":0,   "z":0}}],
        ["brake_caliper_standard_plain", ["vivace_hub_R","wheelhub_RL"], [], {"pos":{"x": 0.80, "y":1.338, "z":0.32}, "scale":{"x":0.93, "y":0.85, "z":0.85}, "rot":{"x":0,   "y":180, "z":0}}],
    ],
    "pressureWheels": [
        ["name","hubGroup","group","node1:","node2:","nodeS","nodeArm:","wheelDir"],
        //brakes
        {"brakeTorque":"$=$brakestrength*1200"},
        {"brakeInputSplit":0.5},
        {"brakeSplitCoef":0.5},
        {"parkingTorque":1250},
        {"brakeSpring":125},

        //brake thermals
        {"enableBrakeThermals":true},
        {"brakeDiameter":0.260},
        {"brakeMass":4.5},
        {"brakeType":"disc"},
        {"rotorMaterial":"steel"},
        {"brakeVentingCoef":0.8},
        //abs
        {"absSlipRatioTarget":0.15},
        //brake sounds
        {"squealCoefNatural": 0.0, "squealCoefLowSpeed": 0.0}
    ],
},
"vivace_brake_F_sport": {
    "information":{
        "authors":"BeamNG",
        "name":"S-Sport Front Disc Brakes",
        "value":450,
    },
    "slotType" : "vivace_brake_F",
    "slots": [
        ["type", "default", "description"],
        ["brakepad_F","brakepad_F_sport", "Front Brake Pads", {"coreSlot":true}],
    ],
    "flexbodies": [
        ["mesh", "[group]:", "nonFlexMaterials"],
        //brake discs
        ["brake_hub_5l",             ["wheel_FR",    "wheelhub_FR"], [], {"pos":{"x":-0.79, "y":-1.342, "z":0.32}, "scale":{"x":0.9,  "y":1.0,  "z":1.0}, "rot":{"x":0,  "y":0,   "z":0}}],
        ["brake_hub_5l",             ["wheel_FL",    "wheelhub_FL"], [], {"pos":{"x": 0.79, "y":-1.342, "z":0.32}, "scale":{"x":0.9,  "y":1.0,  "z":1.0}, "rot":{"x":0,  "y":0,   "z":180}}],
        ["brake_disc_plain",         ["wheel_FR",    "wheelhub_FR"], [], {"pos":{"x":-0.80, "y":-1.342, "z":0.32}, "scale":{"x":1.1, "y":1.1, "z":1.1},   "rot":{"x":0,  "y":0,   "z":0}}],
        ["brake_disc_plain",         ["wheel_FL",    "wheelhub_FL"], [], {"pos":{"x": 0.80, "y":-1.342, "z":0.32}, "scale":{"x":1.1, "y":1.1, "z":1.1},   "rot":{"x":180,"y":0,   "z":0}}],
        ["brake_caliper_4pot_plain", ["vivace_hub_F","wheelhub_FR"], [], {"pos":{"x":-0.80, "y":-1.342, "z":0.32}, "scale":{"x":1.1, "y":1.0, "z":1.0}, "rot":{"x":0,  "y":0,   "z":0}}],
        ["brake_caliper_4pot_plain", ["vivace_hub_F","wheelhub_FL"], [], {"pos":{"x": 0.80, "y":-1.342, "z":0.32}, "scale":{"x":1.1, "y":1.0, "z":1.0}, "rot":{"x":0,  "y":180, "z":0}}],
    ],
    "pressureWheels": [
        ["name","hubGroup","group","node1:","node2:","nodeS","nodeArm:","wheelDir"],
        //brakes
        {"brakeTorque":"$=$brakestrength*3000"},
        {"brakeInputSplit":1},
        {"brakeSplitCoef":1},
        {"parkingTorque":0},
        {"brakeSpring":125},

        //brake thermals
        {"enableBrakeThermals":true},
        {"brakeDiameter":0.320},
        {"brakeMass":9.4},
        {"brakeType":"vented-disc"},
        {"rotorMaterial":"steel"},
        {"brakeVentingCoef":1.0},
        //abs
        {"absSlipRatioTarget":0.2},
        //brake sounds
        {"squealCoefNatural": 0.0, "squealCoefLowSpeed": 0.5}
    ],
},
"vivace_brake_R_sport": {
    "information":{
        "authors":"BeamNG",
        "name":"S-Sport Rear Disc Brakes",
        "value":400,
    },
    "slotType" : "vivace_brake_R",
    "slots": [
        ["type", "default", "description"],
        ["brakepad_R","brakepad_R_sport", "Rear Brake Pads", {"coreSlot":true}],
    ],
    "flexbodies": [
        ["mesh", "[group]:", "nonFlexMaterials"],
        ["brake_hub_5l",                 ["wheel_RR",    "wheelhub_RR"], [], {"pos":{"x":-0.79, "y":1.338, "z":0.32}, "scale":{"x":0.9,  "y":1.0,  "z":1.0},  "rot":{"x":0,   "y":0,   "z":0}}],
        ["brake_hub_5l",                 ["wheel_RL",    "wheelhub_RL"], [], {"pos":{"x": 0.79, "y":1.338, "z":0.32}, "scale":{"x":0.9,  "y":1.0,  "z":1.0},  "rot":{"x":0,   "y":0,   "z":180}}],
        ["brake_disc_solid",             ["wheel_RR",    "wheelhub_RR"], [], {"pos":{"x":-0.80, "y":1.338, "z":0.32}, "scale":{"x":1.03, "y":1.03, "z":1.03}, "rot":{"x":0,   "y":0,   "z":0}}],
        ["brake_disc_solid",             ["wheel_RL",    "wheelhub_RL"], [], {"pos":{"x": 0.80, "y":1.338, "z":0.32}, "scale":{"x":1.03, "y":1.03, "z":1.03}, "rot":{"x":180, "y":0,   "z":0}}],
        ["brake_caliper_standard_plain", ["vivace_hub_R","wheelhub_RR"], [], {"pos":{"x":-0.80, "y":1.333, "z":0.32}, "scale":{"x":1.03, "y":0.89, "z":0.89}, "rot":{"x":0,   "y":0,   "z":0}}],
        ["brake_caliper_standard_plain", ["vivace_hub_R","wheelhub_RL"], [], {"pos":{"x": 0.80, "y":1.333, "z":0.32}, "scale":{"x":1.03, "y":0.89, "z":0.89}, "rot":{"x":0,   "y":180, "z":0}}],
    ],
    "pressureWheels": [
        ["name","hubGroup","group","node1:","node2:","nodeS","nodeArm:","wheelDir"],
        //brakes
        {"brakeTorque":"$=$brakestrength*1300"},
        {"brakeInputSplit":0.5},
        {"brakeSplitCoef":0.4},
        {"parkingTorque":1300},
        {"brakeSpring":125},

        //brake thermals
        {"enableBrakeThermals":true},
        {"brakeDiameter":0.290},
        {"brakeMass":5.2},
        {"brakeType":"disc"},
        {"rotorMaterial":"steel"},
        {"brakeVentingCoef":0.8},
        //abs
        {"absSlipRatioTarget":0.15},
        //brake sounds
        {"squealCoefNatural": 0.0, "squealCoefLowSpeed": 0.0}
    ],
},
"vivace_brake_F_sport_r": {
    "information":{
        "authors":"BeamNG",
        "name":"R-Sport Front Disc Brakes",
        "value":750,
    },
    "slotType" : "vivace_brake_F",
    "slots": [
        ["type", "default", "description"],
        ["brakepad_F","brakepad_F_semi_race", "Front Brake Pads", {"coreSlot":true}],
    ],
    "flexbodies": [
        ["mesh", "[group]:", "nonFlexMaterials"],
        ["brake_hub_5l",           ["wheel_FR",    "wheelhub_FR"], [], {"pos":{"x":-0.79, "y":-1.342, "z":0.32}, "scale":{"x":0.9,  "y":1.0,  "z":1.0},  "rot":{"x":0,   "y":0,   "z":0}}],
        ["brake_hub_5l",           ["wheel_FL",    "wheelhub_FL"], [], {"pos":{"x": 0.79, "y":-1.342, "z":0.32}, "scale":{"x":0.9,  "y":1.0,  "z":1.0},  "rot":{"x":0,   "y":0,   "z":180}}],
        ["brake_disc_slotted",     ["wheel_FR",    "wheelhub_FR"], [], {"pos":{"x":-0.80, "y":-1.342, "z":0.32}, "scale":{"x":1.08, "y":1.08, "z":1.08}, "rot":{"x":0,   "y":0,   "z":0}}],
        ["brake_disc_slotted",     ["wheel_FL",    "wheelhub_FL"], [], {"pos":{"x": 0.80, "y":-1.342, "z":0.32}, "scale":{"x":1.08, "y":1.08, "z":1.08}, "rot":{"x":180, "y":0,   "z":0}}],
        ["brake_caliper_4pot_red", ["vivace_hub_F","wheelhub_FR"], [], {"pos":{"x":-0.80, "y":-1.344, "z":0.32}, "scale":{"x":1.2,  "y":1.1, "z":1.1},   "rot":{"x":0,   "y":0,   "z":0}}],
        ["brake_caliper_4pot_red", ["vivace_hub_F","wheelhub_FL"], [], {"pos":{"x": 0.80, "y":-1.344, "z":0.32}, "scale":{"x":1.2,  "y":1.1, "z":1.1},   "rot":{"x":0,   "y":180, "z":0}}],
    ],
    "pressureWheels": [
        ["name","hubGroup","group","node1:","node2:","nodeS","nodeArm:","wheelDir"],
        //brakes
        {"brakeTorque":"$=$brakestrength*3000"},
        {"brakeInputSplit":1},
        {"brakeSplitCoef":1},
        {"parkingTorque":0},
        {"brakeSpring":125},

        //brake thermals
        {"enableBrakeThermals":true},
        {"brakeDiameter":0.355},
        {"brakeMass":10.25},
        {"brakeType":"vented-disc"},
        {"rotorMaterial":"steel"},
        {"brakeVentingCoef":1.2},
        //abs
        {"absSlipRatioTarget":0.2},
        //brake sounds
        {"squealCoefNatural": 0.0, "squealCoefLowSpeed": 0.8}
    ],
},
"vivace_brake_R_sport_r": {
    "information":{
        "authors":"BeamNG",
        "name":"R-Sport Rear Disc Brakes",
        "value":500,
    },
    "slotType" : "vivace_brake_R",
    "slots": [
        ["type", "default", "description"],
        ["brakepad_R","brakepad_R_semi_race", "Rear Brake Pads", {"coreSlot":true}],
    ],
    "flexbodies": [
        ["mesh", "[group]:", "nonFlexMaterials"],
        ["brake_hub_5l",           ["wheel_RR",    "wheelhub_RR"], [], {"pos":{"x":-0.79, "y":1.338, "z":0.32}, "scale":{"x":0.9,  "y":1.0,  "z":1.0},  "rot":{"x":0,  "y":0,   "z":0}}],
        ["brake_hub_5l",           ["wheel_RL",    "wheelhub_RL"], [], {"pos":{"x": 0.79, "y":1.338, "z":0.32}, "scale":{"x":0.9,  "y":1.0,  "z":1.0},  "rot":{"x":0,  "y":0,   "z":180}}],
        ["brake_disc_slotted",     ["wheel_RR",    "wheelhub_RR"], [], {"pos":{"x":-0.80, "y":1.338, "z":0.32}, "scale":{"x":0.89, "y":0.89, "z":0.89}, "rot":{"x":0,  "y":0,   "z":0}}],
        ["brake_disc_slotted",     ["wheel_RL",    "wheelhub_RL"], [], {"pos":{"x": 0.80, "y":1.338, "z":0.32}, "scale":{"x":0.89, "y":0.89, "z":0.89}, "rot":{"x":180,"y":0,   "z":0}}],
        ["brake_caliper_4pot_red", ["vivace_hub_R","wheelhub_RR"], [], {"pos":{"x":-0.80, "y":1.340, "z":0.32}, "scale":{"x":0.95, "y":0.92, "z":0.92}, "rot":{"x":0,  "y":0,   "z":0}}],
        ["brake_caliper_4pot_red", ["vivace_hub_R","wheelhub_RL"], [], {"pos":{"x": 0.80, "y":1.340, "z":0.32}, "scale":{"x":0.95, "y":0.92, "z":0.92}, "rot":{"x":0,  "y":180, "z":0}}],
    ],
    "pressureWheels": [
        ["name","hubGroup","group","node1:","node2:","nodeS","nodeArm:","wheelDir"],
        //brakes
        {"brakeTorque":"$=$brakestrength*1400"},
        {"brakeInputSplit":0.5},
        {"brakeSplitCoef":0.4},
        {"parkingTorque":1400},
        {"brakeSpring":125},

        //brake thermals
        {"enableBrakeThermals":true},
        {"brakeDiameter":0.290},
        {"brakeMass":6.0},
        {"brakeType":"vented-disc"},
        {"rotorMaterial":"steel"},
        {"brakeVentingCoef":0.8},
        //abs
        {"absSlipRatioTarget":0.15},
        //brake sounds
        {"squealCoefNatural": 0.0, "squealCoefLowSpeed": 0.0}
    ],
},
"vivace_brake_F_race": {
    "information":{
        "authors":"BeamNG",
        "name":"Race Front Disc Brakes",
        "value":1650,
    },
    "slotType" : "vivace_brake_F",
    "slots": [
        ["type", "default", "description"],
        ["brakepad_F","brakepad_F_race", "Front Brake Pads", {"coreSlot":true}],
    ],
    "flexbodies": [
        ["mesh", "[group]:", "nonFlexMaterials"],
        ["brake_hub_5l",                   ["wheel_FR",    "wheelhub_FR"], [], {"pos":{"x":-0.79, "y":-1.342, "z":0.32}, "scale":{"x":0.9,  "y":1.0,  "z":1.0},  "rot":{"x":0,  "y":0,  "z":0}}],
        ["brake_hub_5l",                   ["wheel_FL",    "wheelhub_FL"], [], {"pos":{"x": 0.79, "y":-1.342, "z":0.32}, "scale":{"x":0.9,  "y":1.0,  "z":1.0},  "rot":{"x":0,  "y":0,  "z":180}}],
        ["brake_disc_drilled",             ["wheel_FR",    "wheelhub_FR"], [], {"pos":{"x":-0.80, "y":-1.342, "z":0.32}, "scale":{"x":1.13, "y":1.13, "z":1.13}, "rot":{"x":0,  "y":0,  "z":0}}],
        ["brake_disc_drilled",             ["wheel_FL",    "wheelhub_FL"], [], {"pos":{"x": 0.80, "y":-1.342, "z":0.32}, "scale":{"x":1.13, "y":1.13, "z":1.13}, "rot":{"x":180,"y":0,  "z":0}}],
        ["brake_caliper_6pot_red_rotopad", ["vivace_hub_F","wheelhub_FR"], [], {"pos":{"x":-0.80, "y":-1.344, "z":0.32}, "scale":{"x":1.30,  "y":1.15, "z":1.15}, "rot":{"x":0,  "y":0,  "z":0}}],
        ["brake_caliper_6pot_red_rotopad", ["vivace_hub_F","wheelhub_FL"], [], {"pos":{"x": 0.80, "y":-1.344, "z":0.32}, "scale":{"x":1.30,  "y":1.15, "z":1.15}, "rot":{"x":0,  "y":180,"z":0}}],
    ],
    "variables": [
        ["name", "type", "unit", "category", "default", "min", "max", "title", "description"],
        //["$braketorque", "range", "", "Brakes", 3800, 1000, 5000, "Max Brake Torque", "Scales the overall brake torque", {"minDis":0}]
        ["$brakebias", "range", "", "Brakes", 0.76, 0, 1, "Front/Rear Bias", "Percent of brake torque to the front wheels", {"minDis":0, "maxDis":100}],
    ],
    "pressureWheels": [
        ["name","hubGroup","group","node1:","node2:","nodeS","nodeArm:","wheelDir"],
        //brakes
        {"brakeTorque":"$=$brakestrength*4600*$brakebias"},
        {"brakeInputSplit":1},
        {"brakeSplitCoef":1},
        {"parkingTorque":0},
        {"brakeSpring":50},

        //brake thermals
        {"enableBrakeThermals":true},
        {"brakeDiameter":0.370},
        {"brakeMass":11},
        {"brakeType":"vented-disc"},
        {"rotorMaterial":"steel"},
        {"brakeVentingCoef":1.5},
        //abs
        {"absSlipRatioTarget":0.2},
        //brake sounds
        {"squealCoefNatural": 1.0, "squealCoefLowSpeed": 1.0}
    ],
},
"vivace_brake_R_race": {
    "information":{
        "authors":"BeamNG",
        "name":"Race Rear Disc Brakes",
        "value":1600,
    },
    "slotType" : "vivace_brake_R",
    "slots": [
        ["type", "default", "description"],
        ["brakepad_R","brakepad_R_race", "Rear Brake Pads", {"coreSlot":true}],
    ],
    "flexbodies": [
        ["mesh", "[group]:", "nonFlexMaterials"],
        ["brake_hub_5l",                   ["wheel_RR",    "wheelhub_RR"], [], {"pos":{"x":-0.79, "y":1.338, "z":0.32}, "scale":{"x":0.9,  "y":1.0,  "z":1.0},  "rot":{"x":0,   "y":0,   "z":0}}],
        ["brake_hub_5l",                   ["wheel_RL",    "wheelhub_RL"], [], {"pos":{"x": 0.79, "y":1.338, "z":0.32}, "scale":{"x":0.9,  "y":1.0,  "z":1.0},  "rot":{"x":0,   "y":0,   "z":180}}],
        ["brake_disc_drilled",             ["wheel_RR",    "wheelhub_RR"], [], {"pos":{"x":-0.80, "y":1.338, "z":0.32}, "scale":{"x":0.95, "y":0.95, "z":0.95}, "rot":{"x":0,   "y":0,   "z":0}}],
        ["brake_disc_drilled",             ["wheel_RL",    "wheelhub_RL"], [], {"pos":{"x": 0.80, "y":1.338, "z":0.32}, "scale":{"x":0.95, "y":0.95, "z":0.95}, "rot":{"x":180, "y":0,   "z":0}}],
        ["brake_caliper_4pot_red_rotopad", ["vivace_hub_R","wheelhub_RR"], [], {"pos":{"x":-0.80, "y":1.338, "z":0.32}, "scale":{"x":1.05, "y":0.98, "z":0.98}, "rot":{"x":0,   "y":0,   "z":0}}],
        ["brake_caliper_4pot_red_rotopad", ["vivace_hub_R","wheelhub_RL"], [], {"pos":{"x": 0.80, "y":1.338, "z":0.32}, "scale":{"x":1.05, "y":0.98, "z":0.98}, "rot":{"x":0,   "y":180, "z":0}}],
    ],
    "pressureWheels": [
        ["name","hubGroup","group","node1:","node2:","nodeS","nodeArm:","wheelDir"],
        //brakes
        //if no race front brake, use default value
        //{"brakeTorque":"$=case($brakebias == nil, $brakestrength*1000, $brakestrength*3800*(1-$brakebias))"},
        {"brakeTorque":"$=$brakebias == nil and $brakestrength*1000 or $brakestrength*4600*(1-$brakebias)"},
        {"brakeInputSplit":1},
        {"brakeSplitCoef":1},
        {"parkingTorque":3000},
        {"brakeSpring":50},

        //brake thermals
        {"enableBrakeThermals":true},
        {"brakeDiameter":0.310},
        {"brakeMass":6.5},
        {"brakeType":"vented-disc"},
        {"rotorMaterial":"steel"},
        {"brakeVentingCoef":1.0},
        //abs
        {"absSlipRatioTarget":0.15},
        //brake sounds
        {"squealCoefNatural": 0.0, "squealCoefLowSpeed": 0.0}
    ],
},

"vivace_brake_F_rally": {
    "information":{
        "authors":"BeamNG",
        "name":"Rally Front Disc Brakes",
        "value":1350,
    },
    "slotType" : "vivace_brake_F",
    "slots": [
        ["type", "default", "description"],
        ["brakepad_F","brakepad_F_semi_race", "Front Brake Pads", {"coreSlot":true}],
    ],
    "flexbodies": [
        ["mesh", "[group]:", "nonFlexMaterials"],
        ["brake_hub_5l",                   ["wheel_FR",    "wheelhub_FR"], [], {"pos":{"x":-0.79, "y":-1.342, "z":0.32}, "scale":{"x":0.9,  "y":1.0,  "z":1.0},  "rot":{"x":0,  "y":0,  "z":0}}],
        ["brake_hub_5l",                   ["wheel_FL",    "wheelhub_FL"], [], {"pos":{"x": 0.79, "y":-1.342, "z":0.32}, "scale":{"x":0.9,  "y":1.0,  "z":1.0},  "rot":{"x":0,  "y":0,  "z":180}}],
        ["brake_disc_slotted",             ["wheel_FR",    "wheelhub_FR"], [], {"pos":{"x":-0.80, "y":-1.342, "z":0.32}, "scale":{"x":1.00, "y":0.92, "z":0.92}, "rot":{"x":0,  "y":0,  "z":0}}],
        ["brake_disc_slotted",             ["wheel_FL",    "wheelhub_FL"], [], {"pos":{"x": 0.80, "y":-1.342, "z":0.32}, "scale":{"x":1.00, "y":0.92, "z":0.92}, "rot":{"x":180,"y":0,  "z":0}}],
        ["brake_caliper_4pot_red_rotopad", ["vivace_hub_F","wheelhub_FR"], [], {"pos":{"x":-0.80, "y":-1.344, "z":0.32}, "scale":{"x":1.05,  "y":0.93, "z":0.93},  "rot":{"x":0,  "y":0,  "z":0}}],
        ["brake_caliper_4pot_red_rotopad", ["vivace_hub_F","wheelhub_FL"], [], {"pos":{"x": 0.80, "y":-1.344, "z":0.32}, "scale":{"x":1.05,  "y":0.93, "z":0.93},  "rot":{"x":0,  "y":180,"z":0}}],
    ],
    "variables": [
        ["name", "type", "unit", "category", "default", "min", "max", "title", "description"],
        //["$braketorque", "range", "", "Brakes", 3800, 1000, 5000, "Max Brake Torque", "Scales the overall brake torque", {"minDis":0}]
        ["$brakebias", "range", "", "Brakes", 0.76, 0, 1, "Front/Rear Bias", "Percent of brake torque to the front wheels", {"minDis":0, "maxDis":100}],
    ],
    "pressureWheels": [
        ["name","hubGroup","group","node1:","node2:","nodeS","nodeArm:","wheelDir"],
        //brakes
        {"brakeTorque":"$=$brakestrength*3900*$brakebias"},
        {"brakeInputSplit":1},
        {"brakeSplitCoef":1},
        {"parkingTorque":0},
        {"brakeSpring":100},

        //brake thermals
        {"enableBrakeThermals":true},
        {"brakeDiameter":0.300},
        {"brakeMass":9.25},
        {"brakeType":"vented-disc"},
        {"rotorMaterial":"steel"},
        {"brakeVentingCoef":1.5},
        //abs
        {"absSlipRatioTarget":0.2},
        //brake sounds
        {"squealCoefNatural": 1.0, "squealCoefLowSpeed": 1.0}
    ],
},
"vivace_brake_R_rally": {
    "information":{
        "authors":"BeamNG",
        "name":"Rally Rear Disc Brakes",
        "value":1350,
    },
    "slotType" : "vivace_brake_R",
    "slots": [
        ["type", "default", "description"],
        ["brakepad_R","brakepad_R_semi_race", "Rear Brake Pads", {"coreSlot":true}],
    ],
    "flexbodies": [
        ["mesh", "[group]:", "nonFlexMaterials"],
        ["brake_hub_5l",                   ["wheel_RR",    "wheelhub_RR"], [], {"pos":{"x":-0.79, "y":1.338, "z":0.32}, "scale":{"x":0.9,  "y":1.0,  "z":1.0},  "rot":{"x":0,   "y":0,   "z":0}}],
        ["brake_hub_5l",                   ["wheel_RL",    "wheelhub_RL"], [], {"pos":{"x": 0.79, "y":1.338, "z":0.32}, "scale":{"x":0.9,  "y":1.0,  "z":1.0},  "rot":{"x":0,   "y":0,   "z":180}}],
        ["brake_disc_slotted",             ["wheel_RR",    "wheelhub_RR"], [], {"pos":{"x":-0.80, "y":1.338, "z":0.32}, "scale":{"x":1.00, "y":0.92, "z":0.92}, "rot":{"x":0,   "y":0,   "z":0}}],
        ["brake_disc_slotted",             ["wheel_RL",    "wheelhub_RL"], [], {"pos":{"x": 0.80, "y":1.338, "z":0.32}, "scale":{"x":1.00, "y":0.92, "z":0.92}, "rot":{"x":180, "y":0,   "z":0}}],
        ["brake_caliper_4pot_red_rotopad", ["vivace_hub_R","wheelhub_RR"], [], {"pos":{"x":-0.80, "y":1.338, "z":0.32}, "scale":{"x":1.05, "y":0.93, "z":0.93},   "rot":{"x":0,   "y":0,   "z":0}}],
        ["brake_caliper_4pot_red_rotopad", ["vivace_hub_R","wheelhub_RL"], [], {"pos":{"x": 0.80, "y":1.338, "z":0.32}, "scale":{"x":1.05, "y":0.93, "z":0.93},   "rot":{"x":0,   "y":180, "z":0}}],
    ],
    "pressureWheels": [
        ["name","hubGroup","group","node1:","node2:","nodeS","nodeArm:","wheelDir"],
        //brakes
        //if no race front brake, use default value
        //{"brakeTorque":"$=case($brakebias == nil, $brakestrength*1000, $brakestrength*3800*(1-$brakebias))"},
        {"brakeTorque":"$=$brakebias == nil and $brakestrength*1000 or $brakestrength*3900*(1-$brakebias)"},
        {"brakeInputSplit":1},
        {"brakeSplitCoef":1},
        {"parkingTorque":3000},
        {"brakeSpring":100},

        //brake thermals
        {"enableBrakeThermals":true},
        {"brakeDiameter":0.300},
        {"brakeMass":9.25},
        {"brakeType":"vented-disc"},
        {"rotorMaterial":"steel"},
        {"brakeVentingCoef":1.2},
        //abs
        {"absSlipRatioTarget":0.15},
        //brake sounds
        {"squealCoefNatural": 0.0, "squealCoefLowSpeed": 0.0}
    ],
},
}