{
"tire_F_255_60_17_offroad_sport": {
    "information":{
        "authors":"BeamNG",
        "name":"255/60R17 Sport Front Tires",
        "value":260,
    },
    "slotType" : "tire_F_17x9_offroad",
    "flexbodies": [
        ["mesh", "[group]:", "nonFlexMaterials"],
        ["tire_01a_17x10_29", ["wheel_FR","tire_FR"], [], {"pos":{"x":-0.56, "y":0.0, "z":0.0}, "rot":{"x":0, "y":0, "z":180}, "scale":{"x":0.96, "y":0.99, "z":0.99}}],
        ["tire_01a_17x10_29", ["wheel_FL","tire_FL"], [], {"pos":{"x": 0.56, "y":0.0, "z":0.0}, "rot":{"x":0, "y":0, "z":0},   "scale":{"x":0.96, "y":0.99, "z":0.99}}],
    ],
    "variables": [
        ["name", "type", "unit", "category", "default", "min", "max", "title", "description"],
        ["$tirepressure_F", "range", "psi", "Wheels", 30, 0, 50, "Tire Pressure", "Relative to atmospheric pressure", {"subCategory":"Front"}],
    ],
    "pressureWheels": [
        ["name","hubGroup","group","node1:","node2:","nodeS","nodeArm:","wheelDir"],
        {"disableMeshBreaking":false,"disableHubMeshBreaking":false},
        {"hasTire":true},
        {"enableTireReinfBeams":false},
        {"enableTireLbeams":true},
        {"enableTireSideReinfBeams":false},
        {"enableTreadReinfBeams":true},
        {"enableTirePeripheryReinfBeams":true},

        //general settings
        {"radius":0.37},
        {"tireWidth":0.225},

        //tire options
        {"wheelSideBeamSpring":"$=$tirepressure_F*800","wheelSideBeamDamp":35},
        {"wheelSideBeamSpringExpansion":511000,"wheelSideBeamDampExpansion":45},
        {"wheelSideTransitionZone":0.1,"wheelSideBeamPrecompression":0.985},

        {"wheelReinfBeamSpring":20000,"wheelReinfBeamDamp":210},
        {"wheelReinfBeamDampCutoffHz":500,"wheelReinfBeamPrecompression":0.985},

        {"wheelTreadBeamSpring":95000,"wheelTreadBeamDamp":95},
        {"wheelTreadBeamDampCutoffHz":500,"wheelTreadBeamPrecompression":0.985},

        {"wheelTreadReinfBeamSpring":165000,"wheelTreadReinfBeamDamp":75},
        {"wheelTreadReinfBeamDampCutoffHz":500,"wheelTreadReinfBeamPrecompression":0.985},

        {"wheelPeripheryBeamSpring":75000,"wheelPeripheryBeamDamp":45},
        {"wheelPeripheryBeamDampCutoffHz":500,"wheelPeripheryBeamPrecompression":0.985},

        {"wheelPeripheryReinfBeamSpring":115000,"wheelPeripheryReinfBeamDamp":35},
        {"wheelPeripheryReinfBeamDampCutoffHz":500,"wheelPeripheryReinfBeamPrecompression":0.985},

        //general tire values
        {"nodeWeight":0.2},
        {"nodeMaterial":"|NM_RUBBER"},
        {"triangleCollision":false},
        {"pressurePSI":"$tirepressure_F"},
        {"dragCoef":5},

        //groundmodel friction multipliers
        {"frictionCoef":1.0},
        {"slidingFrictionCoef":1.0},
        {"stribeckExponent":1.5},
        {"treadCoef":0.5},

        //advanced friction values
        {"noLoadCoef":1.43},
        {"loadSensitivitySlope":0.00012},
        {"fullLoadCoef":0.5},
        {"softnessCoef":0.7},

        //deform values
        {"wheelSideBeamDeform":22000,"wheelSideBeamStrength":30000},
        {"wheelTreadBeamDeform":17000,"wheelTreadBeamStrength":22000},
        {"wheelPeripheryBeamDeform":65000,"wheelPeripheryBeamStrength":65000},
    ],
},
}