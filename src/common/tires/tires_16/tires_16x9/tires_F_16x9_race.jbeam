{
"tire_F_245_45_16_race": {
    "information":{
        "authors":"BeamNG",
        "name":"245/45R16 Race Front Tires",
        "value":160,
    },
    "slotType" : "tire_F_16x9",
    "flexbodies": [
        ["mesh", "[group]:", "nonFlexMaterials"],
        ["tire_01f_15x7_23", ["wheel_FR","tire_FR"], [], {"pos":{"x":-0.485, "y":0.0, "z":0.0}, "rot":{"x":0, "y":0, "z":180}, "scale":{"x":1.25, "y":1.082, "z":1.082}}],
        ["tire_01f_15x7_23", ["wheel_FL","tire_FL"], [], {"pos":{"x": 0.485, "y":0.0, "z":0.0}, "rot":{"x":0, "y":0, "z":0},   "scale":{"x":1.25, "y":1.082, "z":1.082}}],
    ],
    "variables": [
        ["name", "type", "unit", "category", "default", "min", "max", "title", "description"],
        ["$tirepressure_F", "range", "psi", "Wheels", 25, 0, 50, "Tire Pressure", "Relative to atmospheric pressure", {"subCategory":"Front"}],
    ],
    "pressureWheels": [
        ["name","hubGroup","group","node1:","node2:","nodeS","nodeArm:","wheelDir"],
        {"disableMeshBreaking":false,"disableHubMeshBreaking":false},
        {"hasTire":true},
        {"enableTireReinfBeams":false},
        {"enableTireLbeams":true},
        {"enableTireSideReinfBeams":false},
        {"enableTreadReinfBeams":true},
        {"enableTirePeripheryReinfBeams":true},

        //general settings
        {"radius":0.312},
        {"tireWidth":0.225},

        //tire options
        {"wheelSideBeamSpring":"$=$tirepressure_F*714","wheelSideBeamDamp":35},
        {"wheelSideBeamSpringExpansion":411000,"wheelSideBeamDampExpansion":35},
        {"wheelSideTransitionZone":0.12,"wheelSideBeamPrecompression":0.99},

        {"wheelReinfBeamSpring":28000,"wheelReinfBeamDamp":190},
        {"wheelReinfBeamDampCutoffHz":500,"wheelReinfBeamPrecompression":0.98},

        {"wheelTreadBeamSpring":71000,"wheelTreadBeamDamp":80},
        {"wheelTreadBeamDampCutoffHz":500,"wheelTreadBeamPrecompression":0.98},

        {"wheelTreadReinfBeamSpring":121000,"wheelTreadReinfBeamDamp":80},
        {"wheelTreadReinfBeamDampCutoffHz":500,"wheelTreadReinfBeamPrecompression":0.98},

        {"wheelPeripheryBeamSpring":51000,"wheelPeripheryBeamDamp":40},
        {"wheelPeripheryBeamDampCutoffHz":500,"wheelPeripheryBeamPrecompression":0.98},

        {"wheelPeripheryReinfBeamSpring":101000,"wheelPeripheryReinfBeamDamp":40},
        {"wheelPeripheryReinfBeamDampCutoffHz":500,"wheelPeripheryReinfBeamPrecompression":0.98},

        //general tire values
        {"nodeWeight":0.17},
        {"nodeMaterial":"|NM_RUBBER"},
        {"triangleCollision":false},
        {"pressurePSI":"$tirepressure_F"},
        {"dragCoef":5},

        //groundmodel friction multipliers
        {"frictionCoef":1.0},
        {"slidingFrictionCoef":1.0},
        {"stribeckExponent":1.91},
        {"stribeckVelMult":1.2},
        {"treadCoef":0},

        //advanced friction values
        {"noLoadCoef":1.89},
        {"loadSensitivitySlope":0.00026},
        {"fullLoadCoef":0.75},
        {"softnessCoef":1},

        //deform values
        {"wheelSideBeamDeform":15000,"wheelSideBeamStrength":20000},
        {"wheelTreadBeamDeform":11000,"wheelTreadBeamStrength":14000},
        {"wheelPeripheryBeamDeform":50000,"wheelPeripheryBeamStrength":50000},
    ],
},
}