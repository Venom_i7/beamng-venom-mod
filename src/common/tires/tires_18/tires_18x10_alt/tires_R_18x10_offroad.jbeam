{
"tire_R_33_12_18_offroad": {
    "information":{
        "authors":"BeamNG",
        "name":"33x12.50R18 All-Terrain Rear Tires",
        "value":480,
    },
    "slotType" : "tire_R_18x10_alt",
    "flexbodies": [
        ["mesh", "[group]:", "nonFlexMaterials"],
        ["offroadtire_02a_18x10_33", ["wheel_RR","tire_RR"], [], {"pos":{"x":-0.52, "y":0.0, "z":0.0}, "rot":{"x":0, "y":0, "z":180}, "scale":{"x":1, "y":1, "z":1}}],
        ["offroadtire_02a_18x10_33", ["wheel_RL","tire_RL"], [], {"pos":{"x": 0.52, "y":0.0, "z":0.0}, "rot":{"x":0, "y":0, "z":0}, "scale":{"x":1, "y":1, "z":1}}],
    ],
    "variables": [
        ["name", "type", "unit", "category", "default", "min", "max", "title", "description"],
        ["$tirepressure_R", "range", "psi", "Wheels", 15, 0, 30, "Tire Pressure", "Relative to atmospheric pressure", {"subCategory":"Rear"}],
    ],
    "pressureWheels": [
        ["name","hubGroup","group","node1:","node2:","nodeS","nodeArm:","wheelDir"],
        {"disableMeshBreaking":false,"disableHubMeshBreaking":false},
        {"hasTire":true},
        {"enableTireReinfBeams":false},
        {"enableTireLbeams":true},
        {"enableTireSideReinfBeams":false},
        {"enableTreadReinfBeams":true},
        {"enableTirePeripheryReinfBeams":true},

        //general settings
        {"radius":0.415},
        {"tireWidth":0.265},

        //tire options
        {"wheelSideBeamSpring":"$=$tirepressure_R*800","wheelSideBeamDamp":60},
        {"wheelSideBeamSpringExpansion":661000,"wheelSideBeamDampExpansion":40},
        {"wheelSideTransitionZone":0.1,"wheelSideBeamPrecompression":0.99},

        {"wheelReinfBeamSpring":10000,"wheelReinfBeamDamp":300},
        {"wheelReinfBeamDampCutoffHz":500,"wheelReinfBeamPrecompression":0.99},

        {"wheelTreadBeamSpring":51000,"wheelTreadBeamDamp":100},
        {"wheelTreadBeamDampCutoffHz":500,"wheelTreadBeamPrecompression":0.99},

        {"wheelTreadReinfBeamSpring":101000,"wheelTreadReinfBeamDamp":80},
        {"wheelTreadReinfBeamDampCutoffHz":500,"wheelTreadReinfBeamPrecompression":0.99},

        {"wheelPeripheryBeamSpring":61000,"wheelPeripheryBeamDamp":60},
        {"wheelPeripheryBeamDampCutoffHz":500,"wheelPeripheryBeamPrecompression":0.99},

        {"wheelPeripheryReinfBeamSpring":121000,"wheelPeripheryReinfBeamDamp":40},
        {"wheelPeripheryReinfBeamDampCutoffHz":500,"wheelPeripheryReinfBeamPrecompression":0.99},

        //general tire values
        {"nodeWeight":0.3},
        {"nodeMaterial":"|NM_RUBBER"},
        {"pressurePSI":"$tirepressure_R"},
        {"dragCoef":8},

        //groundmodel friction multipliers
        {"frictionCoef":1.0},
        {"slidingFrictionCoef":1.0},
        {"treadCoef":0.85},

        //advanced friction values
        {"noLoadCoef":1.27},
        {"loadSensitivitySlope":0.000092},
        {"fullLoadCoef":0.5},
        {"softnessCoef":0.7},

        //deform values
        {"wheelSideBeamDeform":35000,"wheelSideBeamStrength":41000},
        {"wheelTreadBeamDeform":27000,"wheelTreadBeamStrength":36000},
        {"wheelPeripheryBeamDeform":80000,"wheelPeripheryBeamStrength":80000},
    ],
},
"tire_R_35_12_18_offroad": {
    "information":{
        "authors":"BeamNG",
        "name":"35x12.50R17 All-Terrain Rear Tires",
        "value":550,
    },
    "slotType" : "tire_R_18x10_alt",
    "flexbodies": [
        ["mesh", "[group]:", "nonFlexMaterials"],
        ["offroadtire_02a_18x10_35", ["wheel_RR","tire_RR"], [], {"pos":{"x":-0.52, "y":0.0, "z":0.0}, "rot":{"x":0, "y":0, "z":180}, "scale":{"x":1, "y":1, "z":1}}],
        ["offroadtire_02a_18x10_35", ["wheel_RL","tire_RL"], [], {"pos":{"x": 0.52, "y":0.0, "z":0.0}, "rot":{"x":0, "y":0, "z":0}, "scale":{"x":1, "y":1, "z":1}}],
    ],
    "variables": [
        ["name", "type", "unit", "category", "default", "min", "max", "title", "description"],
        ["$tirepressure_R", "range", "psi", "Wheels", 15, 0, 30, "Tire Pressure", "Relative to atmospheric pressure", {"subCategory":"Rear"}],
    ],
    "pressureWheels": [
        ["name","hubGroup","group","node1:","node2:","nodeS","nodeArm:","wheelDir"],
        {"disableMeshBreaking":false,"disableHubMeshBreaking":false},
        {"hasTire":true},
        {"enableTireReinfBeams":false},
        {"enableTireLbeams":true},
        {"enableTireSideReinfBeams":false},
        {"enableTreadReinfBeams":true},
        {"enableTirePeripheryReinfBeams":true},

        //general settings
        {"radius":0.445},
        {"tireWidth":0.265},

        //tire options
        {"wheelSideBeamSpring":"$=$tirepressure_R*700","wheelSideBeamDamp":60},
        {"wheelSideBeamSpringExpansion":701000,"wheelSideBeamDampExpansion":40},
        {"wheelSideTransitionZone":0.1,"wheelSideBeamPrecompression":0.99},

        {"wheelReinfBeamSpring":11000,"wheelReinfBeamDamp":350},
        {"wheelReinfBeamDampCutoffHz":500,"wheelReinfBeamPrecompression":0.99},

        {"wheelTreadBeamSpring":61000,"wheelTreadBeamDamp":120},
        {"wheelTreadBeamDampCutoffHz":500,"wheelTreadBeamPrecompression":0.99},

        {"wheelTreadReinfBeamSpring":121000,"wheelTreadReinfBeamDamp":90},
        {"wheelTreadReinfBeamDampCutoffHz":500,"wheelTreadReinfBeamPrecompression":0.99},

        {"wheelPeripheryBeamSpring":61000,"wheelPeripheryBeamDamp":70},
        {"wheelPeripheryBeamDampCutoffHz":500,"wheelPeripheryBeamPrecompression":0.99},

        {"wheelPeripheryReinfBeamSpring":121000,"wheelPeripheryReinfBeamDamp":45},
        {"wheelPeripheryReinfBeamDampCutoffHz":500,"wheelPeripheryReinfBeamPrecompression":0.99},

        //general tire values
        {"nodeWeight":0.34},
        {"nodeMaterial":"|NM_RUBBER"},
        {"pressurePSI":"$tirepressure_R"},
        {"dragCoef":8},

        //groundmodel friction multipliers
        {"frictionCoef":1.0},
        {"slidingFrictionCoef":1.0},
        {"treadCoef":0.85},

        //advanced friction values
        {"noLoadCoef":1.31},
        {"loadSensitivitySlope":0.000083},
        {"fullLoadCoef":0.5},
        {"softnessCoef":0.7},

        //deform values
        {"wheelSideBeamDeform":35000,"wheelSideBeamStrength":41000},
        {"wheelTreadBeamDeform":27000,"wheelTreadBeamStrength":36000},
        {"wheelPeripheryBeamDeform":80000,"wheelPeripheryBeamStrength":80000},
    ],
},
}