{
"tire_F_560_13": {
    "information":{
        "authors":"BeamNG",
        "name":"560-13 Bias Ply Front Tires",
        "value":70,
    },
    "slotType" : "tire_F_13x5",
    "flexbodies": [
        ["mesh", "[group]:", "nonFlexMaterials"],
        ["tire_03a_13x6_24", ["wheel_FR","tire_FR"], [], {"pos":{"x":-0.48, "y":0.0, "z":0.0}, "rot":{"x":0, "y":0, "z":180}, "scale":{"x":1.1, "y":1, "z":1}}],
        ["tire_03a_13x6_24", ["wheel_FL","tire_FL"], [], {"pos":{"x": 0.48, "y":0.0, "z":0.0}, "rot":{"x":0, "y":0, "z":0},   "scale":{"x":1.1, "y":1, "z":1}}],
    ],
    "variables": [
        ["name", "type", "unit", "category", "default", "min", "max", "title", "description"],
        ["$tirepressure_F", "range", "psi", "Wheels", 27, 0, 50, "Tire Pressure", "Relative to atmospheric pressure", {"subCategory":"Front"}],
    ],
    "pressureWheels": [
        ["name","hubGroup","group","node1:","node2:","nodeS","nodeArm:","wheelDir"],
        {"disableMeshBreaking":false,"disableHubMeshBreaking":false},
        {"hasTire":true},
        {"enableTireReinfBeams":false},
        {"enableTireLbeams":true},
        {"enableTireSideReinfBeams":false},
        {"enableTreadReinfBeams":true},
        {"enableTirePeripheryReinfBeams":true},

        //general settings
        {"radius":0.303},
        {"tireWidth":0.115},

        //tire options
        {"wheelSideBeamSpring":"$=$tirepressure_F*550","wheelSideBeamDamp":25},
        {"wheelSideBeamSpringExpansion":301000,"wheelSideBeamDampExpansion":25},
        {"wheelSideTransitionZone":0.1,"wheelSideBeamPrecompression":0.98},

        {"wheelReinfBeamSpring":5000,"wheelReinfBeamDamp":140},
        {"wheelReinfBeamDampCutoffHz":500,"wheelReinfBeamPrecompression":0.98},

        {"wheelTreadBeamSpring":61000,"wheelTreadBeamDamp":55},
        {"wheelTreadBeamDampCutoffHz":500,"wheelTreadBeamPrecompression":0.98},

        {"wheelTreadReinfBeamSpring":121000,"wheelTreadReinfBeamDamp":45},
        {"wheelTreadReinfBeamDampCutoffHz":500,"wheelTreadReinfBeamPrecompression":0.98},

        {"wheelPeripheryBeamSpring":31000,"wheelPeripheryBeamDamp":25},
        {"wheelPeripheryBeamDampCutoffHz":500,"wheelPeripheryBeamPrecompression":0.98},

        {"wheelPeripheryReinfBeamSpring":91000,"wheelPeripheryReinfBeamDamp":25},
        {"wheelPeripheryReinfBeamDampCutoffHz":500,"wheelPeripheryReinfBeamPrecompression":0.98},

        //general tire values
        {"nodeWeight":0.11},
        {"nodeMaterial":"|NM_RUBBER"},
        {"triangleCollision":false},
        {"pressurePSI":"$tirepressure_F"},
        {"dragCoef":5},

        //groundmodel friction multipliers
        {"frictionCoef":1.0},
        {"slidingFrictionCoef":0.98},
        {"treadCoef":0.7},

        //advanced friction values
        {"noLoadCoef":1.23},
        {"loadSensitivitySlope":0.00018},
        {"stribeckExponent":1.5},
        {"fullLoadCoef":0.4},
        {"softnessCoef":0.5},

        //deform values
        {"wheelSideBeamDeform":11000,"wheelSideBeamStrength":15000},
        {"wheelTreadBeamDeform":10000,"wheelTreadBeamStrength":13000},
        {"wheelPeripheryBeamDeform":40000,"wheelPeripheryBeamStrength":40000},
    ],
},
"tire_F_560_13_thinww": {
    "information":{
        "authors":"BeamNG",
        "name":"560-13 Bias Ply Thin Whitewall Front Tires",
        "value":70,
    },
    "slotType" : "tire_F_13x5",
    "flexbodies": [
        ["mesh", "[group]:", "nonFlexMaterials"],
        ["tire_03b_13x6_24", ["wheel_FR","tire_FR"], [], {"pos":{"x":-0.48, "y":0.0, "z":0.0}, "rot":{"x":0, "y":0, "z":180}, "scale":{"x":1.1, "y":1, "z":1}}],
        ["tire_03b_13x6_24", ["wheel_FL","tire_FL"], [], {"pos":{"x": 0.48, "y":0.0, "z":0.0}, "rot":{"x":0, "y":0, "z":0},   "scale":{"x":1.1, "y":1, "z":1}}],
    ],
    "variables": [
        ["name", "type", "unit", "category", "default", "min", "max", "title", "description"],
        ["$tirepressure_F", "range", "psi", "Wheels", 27, 0, 50, "Tire Pressure", "Relative to atmospheric pressure", {"subCategory":"Front"}],
    ],
    "pressureWheels": [
        ["name","hubGroup","group","node1:","node2:","nodeS","nodeArm:","wheelDir"],
        {"disableMeshBreaking":false,"disableHubMeshBreaking":false},
        {"hasTire":true},
        {"enableTireReinfBeams":false},
        {"enableTireLbeams":true},
        {"enableTireSideReinfBeams":false},
        {"enableTreadReinfBeams":true},
        {"enableTirePeripheryReinfBeams":true},

        //general settings
        {"radius":0.303},
        {"tireWidth":0.115},

        //tire options
        {"wheelSideBeamSpring":"$=$tirepressure_F*550","wheelSideBeamDamp":25},
        {"wheelSideBeamSpringExpansion":301000,"wheelSideBeamDampExpansion":25},
        {"wheelSideTransitionZone":0.1,"wheelSideBeamPrecompression":0.98},

        {"wheelReinfBeamSpring":5000,"wheelReinfBeamDamp":140},
        {"wheelReinfBeamDampCutoffHz":500,"wheelReinfBeamPrecompression":0.98},

        {"wheelTreadBeamSpring":61000,"wheelTreadBeamDamp":55},
        {"wheelTreadBeamDampCutoffHz":500,"wheelTreadBeamPrecompression":0.98},

        {"wheelTreadReinfBeamSpring":121000,"wheelTreadReinfBeamDamp":45},
        {"wheelTreadReinfBeamDampCutoffHz":500,"wheelTreadReinfBeamPrecompression":0.98},

        {"wheelPeripheryBeamSpring":31000,"wheelPeripheryBeamDamp":25},
        {"wheelPeripheryBeamDampCutoffHz":500,"wheelPeripheryBeamPrecompression":0.98},

        {"wheelPeripheryReinfBeamSpring":91000,"wheelPeripheryReinfBeamDamp":25},
        {"wheelPeripheryReinfBeamDampCutoffHz":500,"wheelPeripheryReinfBeamPrecompression":0.98},

        //general tire values
        {"nodeWeight":0.11},
        {"nodeMaterial":"|NM_RUBBER"},
        {"triangleCollision":false},
        {"pressurePSI":"$tirepressure_F"},
        {"dragCoef":5},

        //groundmodel friction multipliers
        {"frictionCoef":1.0},
        {"slidingFrictionCoef":0.98},
        {"treadCoef":0.7},

        //advanced friction values
        {"noLoadCoef":1.23},
        {"loadSensitivitySlope":0.00018},
        {"stribeckExponent":1.5},
        {"fullLoadCoef":0.4},
        {"softnessCoef":0.5},

        //deform values
        {"wheelSideBeamDeform":11000,"wheelSideBeamStrength":15000},
        {"wheelTreadBeamDeform":10000,"wheelTreadBeamStrength":13000},
        {"wheelPeripheryBeamDeform":40000,"wheelPeripheryBeamStrength":40000},
    ],
},
"tire_F_560_13_ww": {
    "information":{
        "authors":"BeamNG",
        "name":"560-13 Bias Ply Whitewall Front Tires",
        "value":70,
    },
    "slotType" : "tire_F_13x5",
    "flexbodies": [
        ["mesh", "[group]:", "nonFlexMaterials"],
        ["tire_03c_13x6_24", ["wheel_FR","tire_FR"], [], {"pos":{"x":-0.48, "y":0.0, "z":0.0}, "rot":{"x":0, "y":0, "z":180}, "scale":{"x":1.1, "y":1, "z":1}}],
        ["tire_03c_13x6_24", ["wheel_FL","tire_FL"], [], {"pos":{"x": 0.48, "y":0.0, "z":0.0}, "rot":{"x":0, "y":0, "z":0},   "scale":{"x":1.1, "y":1, "z":1}}],
    ],
    "variables": [
        ["name", "type", "unit", "category", "default", "min", "max", "title", "description"],
        ["$tirepressure_F", "range", "psi", "Wheels", 27, 0, 50, "Tire Pressure", "Relative to atmospheric pressure", {"subCategory":"Front"}],
    ],
    "pressureWheels": [
        ["name","hubGroup","group","node1:","node2:","nodeS","nodeArm:","wheelDir"],
        {"disableMeshBreaking":false,"disableHubMeshBreaking":false},
        {"hasTire":true},
        {"enableTireReinfBeams":false},
        {"enableTireLbeams":true},
        {"enableTireSideReinfBeams":false},
        {"enableTreadReinfBeams":true},
        {"enableTirePeripheryReinfBeams":true},

        //general settings
        {"radius":0.303},
        {"tireWidth":0.115},

        //tire options
        {"wheelSideBeamSpring":"$=$tirepressure_F*550","wheelSideBeamDamp":25},
        {"wheelSideBeamSpringExpansion":301000,"wheelSideBeamDampExpansion":25},
        {"wheelSideTransitionZone":0.1,"wheelSideBeamPrecompression":0.98},

        {"wheelReinfBeamSpring":5000,"wheelReinfBeamDamp":140},
        {"wheelReinfBeamDampCutoffHz":500,"wheelReinfBeamPrecompression":0.98},

        {"wheelTreadBeamSpring":61000,"wheelTreadBeamDamp":55},
        {"wheelTreadBeamDampCutoffHz":500,"wheelTreadBeamPrecompression":0.98},

        {"wheelTreadReinfBeamSpring":121000,"wheelTreadReinfBeamDamp":45},
        {"wheelTreadReinfBeamDampCutoffHz":500,"wheelTreadReinfBeamPrecompression":0.98},

        {"wheelPeripheryBeamSpring":31000,"wheelPeripheryBeamDamp":25},
        {"wheelPeripheryBeamDampCutoffHz":500,"wheelPeripheryBeamPrecompression":0.98},

        {"wheelPeripheryReinfBeamSpring":91000,"wheelPeripheryReinfBeamDamp":25},
        {"wheelPeripheryReinfBeamDampCutoffHz":500,"wheelPeripheryReinfBeamPrecompression":0.98},

        //general tire values
        {"nodeWeight":0.11},
        {"nodeMaterial":"|NM_RUBBER"},
        {"triangleCollision":false},
        {"pressurePSI":"$tirepressure_F"},
        {"dragCoef":5},

        //groundmodel friction multipliers
        {"frictionCoef":1.0},
        {"slidingFrictionCoef":0.98},
        {"treadCoef":0.7},

        //advanced friction values
        {"noLoadCoef":1.23},
        {"loadSensitivitySlope":0.00018},
        {"stribeckExponent":1.5},
        {"fullLoadCoef":0.4},
        {"softnessCoef":0.5},

        //deform values
        {"wheelSideBeamDeform":11000,"wheelSideBeamStrength":15000},
        {"wheelTreadBeamDeform":10000,"wheelTreadBeamStrength":13000},
        {"wheelPeripheryBeamDeform":40000,"wheelPeripheryBeamStrength":40000},
    ],
},
"tire_F_560_13_redline": {
    "information":{
        "authors":"BeamNG",
        "name":"560-13 Bias Ply Redline Front Tires",
        "value":70,
    },
    "slotType" : "tire_F_13x5",
    "flexbodies": [
        ["mesh", "[group]:", "nonFlexMaterials"],
        ["tire_03d_13x6_24", ["wheel_FR","tire_FR"], [], {"pos":{"x":-0.48, "y":0.0, "z":0.0}, "rot":{"x":0, "y":0, "z":180}, "scale":{"x":1.1, "y":1, "z":1}}],
        ["tire_03d_13x6_24", ["wheel_FL","tire_FL"], [], {"pos":{"x": 0.48, "y":0.0, "z":0.0}, "rot":{"x":0, "y":0, "z":0},   "scale":{"x":1.1, "y":1, "z":1}}],
    ],
    "variables": [
        ["name", "type", "unit", "category", "default", "min", "max", "title", "description"],
        ["$tirepressure_F", "range", "psi", "Wheels", 27, 0, 50, "Tire Pressure", "Relative to atmospheric pressure", {"subCategory":"Front"}],
    ],
    "pressureWheels": [
        ["name","hubGroup","group","node1:","node2:","nodeS","nodeArm:","wheelDir"],
        {"disableMeshBreaking":false,"disableHubMeshBreaking":false},
        {"hasTire":true},
        {"enableTireReinfBeams":false},
        {"enableTireLbeams":true},
        {"enableTireSideReinfBeams":false},
        {"enableTreadReinfBeams":true},
        {"enableTirePeripheryReinfBeams":true},

        //general settings
        {"radius":0.303},
        {"tireWidth":0.115},

        //tire options
        {"wheelSideBeamSpring":"$=$tirepressure_F*550","wheelSideBeamDamp":25},
        {"wheelSideBeamSpringExpansion":301000,"wheelSideBeamDampExpansion":25},
        {"wheelSideTransitionZone":0.1,"wheelSideBeamPrecompression":0.98},

        {"wheelReinfBeamSpring":5000,"wheelReinfBeamDamp":140},
        {"wheelReinfBeamDampCutoffHz":500,"wheelReinfBeamPrecompression":0.98},

        {"wheelTreadBeamSpring":61000,"wheelTreadBeamDamp":55},
        {"wheelTreadBeamDampCutoffHz":500,"wheelTreadBeamPrecompression":0.98},

        {"wheelTreadReinfBeamSpring":121000,"wheelTreadReinfBeamDamp":45},
        {"wheelTreadReinfBeamDampCutoffHz":500,"wheelTreadReinfBeamPrecompression":0.98},

        {"wheelPeripheryBeamSpring":31000,"wheelPeripheryBeamDamp":25},
        {"wheelPeripheryBeamDampCutoffHz":500,"wheelPeripheryBeamPrecompression":0.98},

        {"wheelPeripheryReinfBeamSpring":91000,"wheelPeripheryReinfBeamDamp":25},
        {"wheelPeripheryReinfBeamDampCutoffHz":500,"wheelPeripheryReinfBeamPrecompression":0.98},

        //general tire values
        {"nodeWeight":0.11},
        {"nodeMaterial":"|NM_RUBBER"},
        {"triangleCollision":false},
        {"pressurePSI":"$tirepressure_F"},
        {"dragCoef":5},

        //groundmodel friction multipliers
        {"frictionCoef":1.0},
        {"slidingFrictionCoef":0.98},
        {"treadCoef":0.7},

        //advanced friction values
        {"noLoadCoef":1.23},
        {"loadSensitivitySlope":0.00018},
        {"stribeckExponent":1.5},
        {"fullLoadCoef":0.4},
        {"softnessCoef":0.5},

        //deform values
        {"wheelSideBeamDeform":11000,"wheelSideBeamStrength":15000},
        {"wheelTreadBeamDeform":10000,"wheelTreadBeamStrength":13000},
        {"wheelPeripheryBeamDeform":40000,"wheelPeripheryBeamStrength":40000},
    ],
},
"tire_F_560_13_mixed": {
    "information":{
        "authors":"BeamNG",
        "name":"560-13 Bias Ply Two-Tone Style Front Tires",
        "value":70,
    },
    "slotType" : "tire_F_13x5",
    "flexbodies": [
        ["mesh", "[group]:", "nonFlexMaterials"],
        ["tire_03e_13x6_24", ["wheel_FR","tire_FR"], [], {"pos":{"x":-0.48, "y":0.0, "z":0.0}, "rot":{"x":0, "y":0, "z":180}, "scale":{"x":1.1, "y":1, "z":1}}],
        ["tire_03e_13x6_24", ["wheel_FL","tire_FL"], [], {"pos":{"x": 0.48, "y":0.0, "z":0.0}, "rot":{"x":0, "y":0, "z":0},   "scale":{"x":1.1, "y":1, "z":1}}],
    ],
    "variables": [
        ["name", "type", "unit", "category", "default", "min", "max", "title", "description"],
        ["$tirepressure_F", "range", "psi", "Wheels", 27, 0, 50, "Tire Pressure", "Relative to atmospheric pressure", {"subCategory":"Front"}],
    ],
    "pressureWheels": [
        ["name","hubGroup","group","node1:","node2:","nodeS","nodeArm:","wheelDir"],
        {"disableMeshBreaking":false,"disableHubMeshBreaking":false},
        {"hasTire":true},
        {"enableTireReinfBeams":false},
        {"enableTireLbeams":true},
        {"enableTireSideReinfBeams":false},
        {"enableTreadReinfBeams":true},
        {"enableTirePeripheryReinfBeams":true},

        //general settings
        {"radius":0.303},
        {"tireWidth":0.115},

        //tire options
        {"wheelSideBeamSpring":"$=$tirepressure_F*550","wheelSideBeamDamp":25},
        {"wheelSideBeamSpringExpansion":301000,"wheelSideBeamDampExpansion":25},
        {"wheelSideTransitionZone":0.1,"wheelSideBeamPrecompression":0.98},

        {"wheelReinfBeamSpring":5000,"wheelReinfBeamDamp":140},
        {"wheelReinfBeamDampCutoffHz":500,"wheelReinfBeamPrecompression":0.98},

        {"wheelTreadBeamSpring":61000,"wheelTreadBeamDamp":55},
        {"wheelTreadBeamDampCutoffHz":500,"wheelTreadBeamPrecompression":0.98},

        {"wheelTreadReinfBeamSpring":121000,"wheelTreadReinfBeamDamp":45},
        {"wheelTreadReinfBeamDampCutoffHz":500,"wheelTreadReinfBeamPrecompression":0.98},

        {"wheelPeripheryBeamSpring":31000,"wheelPeripheryBeamDamp":25},
        {"wheelPeripheryBeamDampCutoffHz":500,"wheelPeripheryBeamPrecompression":0.98},

        {"wheelPeripheryReinfBeamSpring":91000,"wheelPeripheryReinfBeamDamp":25},
        {"wheelPeripheryReinfBeamDampCutoffHz":500,"wheelPeripheryReinfBeamPrecompression":0.98},

        //general tire values
        {"nodeWeight":0.11},
        {"nodeMaterial":"|NM_RUBBER"},
        {"triangleCollision":false},
        {"pressurePSI":"$tirepressure_F"},
        {"dragCoef":5},

        //groundmodel friction multipliers
        {"frictionCoef":1.0},
        {"slidingFrictionCoef":0.98},
        {"treadCoef":0.7},

        //advanced friction values
        {"noLoadCoef":1.23},
        {"loadSensitivitySlope":0.00018},
        {"stribeckExponent":1.5},
        {"fullLoadCoef":0.4},
        {"softnessCoef":0.5},

        //deform values
        {"wheelSideBeamDeform":11000,"wheelSideBeamStrength":15000},
        {"wheelTreadBeamDeform":10000,"wheelTreadBeamStrength":13000},
        {"wheelPeripheryBeamDeform":40000,"wheelPeripheryBeamStrength":40000},
    ],
},
}
