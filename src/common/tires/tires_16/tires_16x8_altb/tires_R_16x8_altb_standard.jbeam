{
"tire_R_235_55_16_altb_standard": {
    "information":{
        "authors":"BeamNG",
        "name":"235/55R16 Standard Rear Tires",
        "value":140,
    },
    "slotType" : "tire_R_16x8_altb",
    "flexbodies": [
        ["mesh", "[group]:", "nonFlexMaterials"],
        ["tire_01a_16x7_26", ["wheel_RR","tire_RR"], [], {"pos":{"x":-0.52, "y":0.0, "z":0.0}, "rot":{"x":0, "y":0, "z":180}, "scale":{"x":1.1, "y":0.992, "z":0.992}}],
        ["tire_01a_16x7_26", ["wheel_RL","tire_RL"], [], {"pos":{"x": 0.52, "y":0.0, "z":0.0}, "rot":{"x":0, "y":0, "z":0},   "scale":{"x":1.1, "y":0.992, "z":0.992}}],
    ],
    "variables": [
        ["name", "type", "unit", "category", "default", "min", "max", "title", "description"],
        ["$tirepressure_R", "range", "psi", "Wheels", 30, 0, 50, "Tire Pressure", "Relative to atmospheric pressure", {"subCategory":"Rear"}],
    ],
    "pressureWheels": [
        ["name","hubGroup","group","node1:","node2:","nodeS","nodeArm:","wheelDir"],
        {"disableMeshBreaking":false,"disableHubMeshBreaking":false},
        {"hasTire":true},
        {"enableTireReinfBeams":false},
        {"enableTireLbeams":true},
        {"enableTireSideReinfBeams":false},
        {"enableTreadReinfBeams":true},
        {"enableTirePeripheryReinfBeams":true},

        //general settings
        {"radius":0.33},
        {"tireWidth":0.200},

        //tire options
        {"wheelSideBeamSpring":"$=$tirepressure_R*450","wheelSideBeamDamp":30},
        {"wheelSideBeamSpringExpansion":431000,"wheelSideBeamDampExpansion":30},
        {"wheelSideTransitionZone":0.09,"wheelSideBeamPrecompression":0.985},

        {"wheelReinfBeamSpring":15000,"wheelReinfBeamDamp":180},
        {"wheelReinfBeamDampCutoffHz":250,"wheelReinfBeamPrecompression":0.98},

        {"wheelTreadBeamSpring":65000,"wheelTreadBeamDamp":75},
        {"wheelTreadBeamDampCutoffHz":500,"wheelTreadBeamPrecompression":0.98},

        {"wheelTreadReinfBeamSpring":125000,"wheelTreadReinfBeamDamp":65},
        {"wheelTreadReinfBeamDampCutoffHz":500,"wheelTreadReinfBeamPrecompression":0.98},

        {"wheelPeripheryBeamSpring":50000,"wheelPeripheryBeamDamp":45},
        {"wheelPeripheryBeamDampCutoffHz":500,"wheelPeripheryBeamPrecompression":0.98},

        {"wheelPeripheryReinfBeamSpring":110000,"wheelPeripheryReinfBeamDamp":35},
        {"wheelPeripheryReinfBeamDampCutoffHz":500,"wheelPeripheryReinfBeamPrecompression":0.98},

        //general tire values
        {"nodeWeight":0.16},
        {"nodeMaterial":"|NM_RUBBER"},
        {"triangleCollision":false},
        {"pressurePSI":"$tirepressure_R"},
        {"dragCoef":5},

        //groundmodel friction multipliers
        {"frictionCoef":1.0},
        {"slidingFrictionCoef":1.0},
        {"treadCoef":0.7},

        //advanced friction values
        {"noLoadCoef":1.40},
        {"loadSensitivitySlope":0.00015},
        {"fullLoadCoef":0.5},
        {"softnessCoef":0.7},

        //deform values
        {"wheelSideBeamDeform":22000,"wheelSideBeamStrength":28000},
        {"wheelTreadBeamDeform":14000,"wheelTreadBeamStrength":18000},
        {"wheelPeripheryBeamDeform":55000,"wheelPeripheryBeamStrength":55000},
    ],
},
"tire_R_235_55_16_altb_standard_ww": {
    "information":{
        "authors":"BeamNG",
        "name":"235/55R16 Standard Thin Whitewall Rear Tires",
        "value":145,
    },
    "slotType" : "tire_R_16x8_altb",
    "flexbodies": [
        ["mesh", "[group]:", "nonFlexMaterials"],
        ["tire_01b_16x7_26", ["wheel_RR","tire_RR"], [], {"pos":{"x":-0.52, "y":0.0, "z":0.0}, "rot":{"x":0, "y":0, "z":180}, "scale":{"x":1.1, "y":0.992, "z":0.992}}],
        ["tire_01b_16x7_26", ["wheel_RL","tire_RL"], [], {"pos":{"x": 0.52, "y":0.0, "z":0.0}, "rot":{"x":0, "y":0, "z":0},   "scale":{"x":1.1, "y":0.992, "z":0.992}}],
    ],
    "variables": [
        ["name", "type", "unit", "category", "default", "min", "max", "title", "description"],
        ["$tirepressure_R", "range", "psi", "Wheels", 30, 0, 50, "Tire Pressure", "Relative to atmospheric pressure", {"subCategory":"Rear"}],
    ],
    "pressureWheels": [
        ["name","hubGroup","group","node1:","node2:","nodeS","nodeArm:","wheelDir"],
        {"disableMeshBreaking":false,"disableHubMeshBreaking":false},
        {"hasTire":true},
        {"enableTireReinfBeams":false},
        {"enableTireLbeams":true},
        {"enableTireSideReinfBeams":false},
        {"enableTreadReinfBeams":true},
        {"enableTirePeripheryReinfBeams":true},

        //general settings
        {"radius":0.33},
        {"tireWidth":0.200},

        //tire options
        {"wheelSideBeamSpring":"$=$tirepressure_R*450","wheelSideBeamDamp":30},
        {"wheelSideBeamSpringExpansion":431000,"wheelSideBeamDampExpansion":30},
        {"wheelSideTransitionZone":0.09,"wheelSideBeamPrecompression":0.985},

        {"wheelReinfBeamSpring":15000,"wheelReinfBeamDamp":180},
        {"wheelReinfBeamDampCutoffHz":250,"wheelReinfBeamPrecompression":0.98},

        {"wheelTreadBeamSpring":65000,"wheelTreadBeamDamp":75},
        {"wheelTreadBeamDampCutoffHz":500,"wheelTreadBeamPrecompression":0.98},

        {"wheelTreadReinfBeamSpring":125000,"wheelTreadReinfBeamDamp":65},
        {"wheelTreadReinfBeamDampCutoffHz":500,"wheelTreadReinfBeamPrecompression":0.98},

        {"wheelPeripheryBeamSpring":50000,"wheelPeripheryBeamDamp":45},
        {"wheelPeripheryBeamDampCutoffHz":500,"wheelPeripheryBeamPrecompression":0.98},

        {"wheelPeripheryReinfBeamSpring":110000,"wheelPeripheryReinfBeamDamp":35},
        {"wheelPeripheryReinfBeamDampCutoffHz":500,"wheelPeripheryReinfBeamPrecompression":0.98},

        //general tire values
        {"nodeWeight":0.16},
        {"nodeMaterial":"|NM_RUBBER"},
        {"triangleCollision":false},
        {"pressurePSI":"$tirepressure_R"},
        {"dragCoef":5},

        //groundmodel friction multipliers
        {"frictionCoef":1.0},
        {"slidingFrictionCoef":1.0},
        {"treadCoef":0.7},

        //advanced friction values
        {"noLoadCoef":1.40},
        {"loadSensitivitySlope":0.00015},
        {"fullLoadCoef":0.5},
        {"softnessCoef":0.7},

        //deform values
        {"wheelSideBeamDeform":22000,"wheelSideBeamStrength":28000},
        {"wheelTreadBeamDeform":14000,"wheelTreadBeamStrength":18000},
        {"wheelPeripheryBeamDeform":55000,"wheelPeripheryBeamStrength":55000},
    ],
},
}