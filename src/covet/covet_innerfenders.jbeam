{
"covet_innerfender_FR": {
    "information":{
        "authors":"BeamNG",
        "name":"Front Right Inner Fender",
        "value":235,
    },

    "slotType" : "covet_innerfender_FR",

    "flexbodies": [
        ["mesh", "[group]:", "nonFlexMaterials"],
        {"deformGroup":"innerfender_damage_R", "deformMaterialBase":"covet", "deformMaterialDamaged":"invis"},
        ["covet_innerfender_R", ["covet_innerfender_R","covet_fender_R_alt","covet_fender_R","covet_bumper_F"]],
        {"deformGroup":""},
    ],
    "nodes": [
        ["id", "posX", "posY", "posZ"],
        //--FENDER--
        {"nodeMaterial":"|NM_METAL"},
        {"frictionCoef":0.5},
        {"collision":true},
        {"selfCollision":true},
        {"nodeWeight":0.35},
        {"group":"covet_innerfender_R"},
        ["fb4rr", -0.79, -1.60, 0.235],
        //rigidifier
        {"selfCollision":false},
        {"group":"covet_innerfender_R"},
        ["ifer", -0.45, -1.25, 0.35],
        {"group":""},
    ],
    "beams": [
          ["id1:", "id2:"],
          {"beamPrecompression":1, "beamType":"|NORMAL", "beamLongBound":1.0, "beamShortBound":1.0},
          //--FENDER--
          {"beamSpring":101000,"beamDamp":25},
          {"beamDeform":3050,"beamStrength":4000},
          //inner fender main shape
          {"deformationTriggerRatio":0.1},
          {"deformLimitExpansion":1.1},
          {"disableMeshBreaking":true},
          {"optional":true}
          {"breakGroup":"ifender_c_R"},
          ["fe1r","fe3r"],
          ["fe3r","fe4r"],
          ["fe4r","fe5r"],
          ["fe5r","fb4rr",{"beamStrength":2500,"deformGroup":"innerfender_damage_R"}],
          {"optional":false}
          //rigidifier
          {"beamSpring":201000,"beamDamp":25},
          {"beamDeform":1200,"beamStrength":4000},
          //r
          ["fe1r","ifer"],
          ["fe3r","ifer"],
          ["fe4r","ifer"],
          ["fe5r","ifer"],
          ["fb4rr","ifer",{"beamStrength":2500,"deformGroup":"innerfender_damage_R"}],
          //rigidifier beams
          {"optional":true}
          ["fe1r","fe4r"],
          ["fe3r","fe5r"],
          ["fe4r","fb4rr",{"beamStrength":2500,"deformGroup":"innerfender_damage_R"}],
          {"optional":false}

          //attach
          {"disableTriangleBreaking":true},
          {"beamSpring":201000,"beamDamp":20},
          {"beamDeform":11500,"beamStrength":5000},
          {"deformGroup":"innerfender_damage_R"},
          {"breakGroup":"ifender_a_R"},
          //firewall
          ["fe1r","f1rr"],
          ["fe1r","f5rr"],
          ["fe1r","f6rr"],
          ["fe3r","f1rr"],
          ["fe3r","f5rr"],
          ["fe3r","f6rr"],
          //middle
          {"beamDeform":7500,"beamStrength":5000},
          {"breakGroup":"ifender_b_R"},
          {"deformGroup":"innerfender_damage_R"},
          ["fe4r","fx3r"],
          ["fe4r","fx4r"],
          ["fe4r","f14rr"],
          ["fe4r","f14r"],
          ["fe4r","f15rr"],
          ["fe4r","f6rr"],

          ["ifer","fx3r"],
          ["ifer","fx4r"],
          //front
          {"beamDeform":8500,"beamStrength":3000},
          {"breakGroup":"ifender_c_R"},
          {"deformGroup":"innerfender_damage_R"},
          ["fe5r","f15rr"],
          ["fe5r","f19r"],
          ["fe5r","f19rr"],

          ["fb4rr","f15rr"],
          ["fb4rr","f19r"],
          {"breakGroup":""},
          {"deformGroup":""},
          {"beamDeform":8500,"beamStrength":"FLT_MAX"},
          //rigifiers
          ["ifer","fx3r"],
          ["ifer","fx4r"],
          ["ifer","f10rr"],
          {"disableMeshBreaking":false},
          {"beamPrecompression":1, "beamType":"|NORMAL", "beamLongBound":1.0, "beamShortBound":1.0},
          {"deformLimitExpansion":1.1},
    ],
    "triangles": [
            ["id1:","id2:","id3:"],
            //right inner fender
            {"groundModel":"metal"},
            {"group":"covet_innerfender_R"},
            {"dragCoef":8},

            {"group":""},
    ],
},
"covet_innerfender_FL": {
    "information":{
        "authors":"BeamNG",
        "name":"Front Left Inner Fender",
        "value":235,
    },

    "slotType" : "covet_innerfender_FL",

    "flexbodies": [
        ["mesh", "[group]:", "nonFlexMaterials"],
        {"deformGroup":"innerfender_damage_L", "deformMaterialBase":"covet", "deformMaterialDamaged":"invis"},
        ["covet_innerfender_L", ["covet_innerfender_L","covet_fender_L_alt","covet_fender_L","covet_bumper_F"]],
        {"deformGroup":""},
    ],
    "nodes": [
        ["id", "posX", "posY", "posZ"],
        //--FENDER--
        {"nodeMaterial":"|NM_METAL"},
        {"frictionCoef":0.5},
        {"collision":true},
        {"selfCollision":true},
        {"nodeWeight":0.35},
        {"group":"covet_innerfenders_L"},
        ["fb4ll", 0.79, -1.60, 0.235],
        //rigidifier
        {"selfCollision":false},
        {"group":"covet_innerfender_L"},
        ["ifel", 0.45, -1.25, 0.35],
        {"group":""},
    ],
    "beams": [
          ["id1:", "id2:"],
          {"beamPrecompression":1, "beamType":"|NORMAL", "beamLongBound":1.0, "beamShortBound":1.0},
          //--FENDER--
          {"beamSpring":101000,"beamDamp":25},
          {"beamDeform":3050,"beamStrength":4000},
          //inner fender main shape
          {"deformationTriggerRatio":0.1},
          {"deformLimitExpansion":1.1},
          {"disableMeshBreaking":true},
          {"optional":true}
          {"breakGroup":"ifender_c_L"},
          ["fe1l","fe3l"],
          ["fe3l","fe4l"],
          ["fe4l","fe5l"],
          ["fe5l","fb4ll",{"beamStrength":2500,"deformGroup":"innerfender_damage_L"}],
          {"optional":false}
          //rigidifier
          {"beamSpring":201000,"beamDamp":25},
          {"beamDeform":1200,"beamStrength":4000},

          ["fe1l","ifel"],
          ["fe3l","ifel"],
          ["fe4l","ifel"],
          ["fe5l","ifel"],
          ["fb4ll","ifel",{"beamStrength":2500,"deformGroup":"innerfender_damage_L"}],
          //rigidifier beams
          {"optional":true}
          ["fe1l","fe4l"],
          ["fe3l","fe5l"],
          ["fe4l","fb4ll",{"beamStrength":2500,"deformGroup":"innerfender_damage_L"}],
          {"optional":false}

          //attach
          {"disableTriangleBreaking":true},
          {"beamSpring":201000,"beamDamp":20},
          {"beamDeform":11500,"beamStrength":5000},
          {"breakGroup":"ifender_a_L"},
          {"deformGroup":"innerfender_damage_L"},
          //firewall
          ["fe1l","f1ll"],
          ["fe1l","f5ll"],
          ["fe1l","f6ll"],
          ["fe3l","f1ll"],
          ["fe3l","f5ll"],
          ["fe3l","f6ll"],
          //middle
          {"beamDeform":7500,"beamStrength":5000},
          {"breakGroup":"ifender_b_L"},
          {"deformGroup":"innerfender_damage_L"},
          ["fe4l","fx3l"],
          ["fe4l","fx4l"],
          ["fe4l","f14ll"],
          ["fe4l","f14l"],
          ["fe4l","f15ll"],
          ["fe4l","f6ll"],
          //front
          {"beamDeform":8500,"beamStrength":3000},
          {"breakGroup":"ifender_c_L"},
          {"deformGroup":"innerfender_damage_L"},
          ["fe5l","f15ll"],
          ["fe5l","f19l"],
          ["fe5l","f19ll"],

          ["fb4ll","f15ll"],
          ["fb4ll","f19l"],
          {"breakGroup":""},
          {"deformGroup":""},
          {"beamDeform":8500,"beamStrength":"FLT_MAX"},
          //rigifiers
          ["ifel","fx3l"],
          ["ifel","fx4l"],
          ["ifel","f10ll"],
          {"disableMeshBreaking":false},
          {"beamPrecompression":1, "beamType":"|NORMAL", "beamLongBound":1.0, "beamShortBound":1.0},
          {"deformLimitExpansion":1.1},
    ],
    "triangles": [
            ["id1:","id2:","id3:"],
            //right inner fender
            {"groundModel":"metal"},
            {"group":"covet_innerfender_R"},
            {"dragCoef":8},

            {"group":""},
    ],
},
"covet_innerfender_FR_wide": {
    "information":{
        "authors":"BeamNG",
        "name":"Front Right Inner Fender",
        "value":235,
    },

    "slotType" : "covet_innerfender_FR_wide",

    "flexbodies": [
        ["mesh", "[group]:", "nonFlexMaterials"],
        {"deformGroup":"innerfender_damage_R", "deformMaterialBase":"covet", "deformMaterialDamaged":"invis"},
        ["covet_innerfender_wide_R", ["covet_innerfender_R","covet_fender_R_alt","covet_fender_R","covet_bumper_F"]],
        {"deformGroup":""},
    ],
    "nodes": [
        ["id", "posX", "posY", "posZ"],
        //--FENDER--
        {"nodeMaterial":"|NM_METAL"},
        {"frictionCoef":0.5},
        {"collision":true},
        {"selfCollision":true},
        {"nodeWeight":0.35},
        {"group":"covet_innerfender_R"},
        ["fb4rr", -0.79, -1.60, 0.235],
        //rigidifier
        {"selfCollision":false},
        {"group":"covet_innerfender_R"},
        ["ifer", -0.45, -1.25, 0.35],
        {"group":""},
    ],
    "beams": [
          ["id1:", "id2:"],
          {"beamPrecompression":1, "beamType":"|NORMAL", "beamLongBound":1.0, "beamShortBound":1.0},
          //--FENDER--
          {"beamSpring":101000,"beamDamp":25},
          {"beamDeform":3050,"beamStrength":4000},
          //inner fender main shape
          {"deformationTriggerRatio":0.1},
          {"deformLimitExpansion":1.1},
          {"disableMeshBreaking":true},
          {"optional":true}
          {"breakGroup":"ifender_c_R"},
          ["fe1r","fe3r"],
          ["fe3r","fe4r"],
          ["fe4r","fe5r"],
          ["fe5r","fb4rr",{"beamStrength":2500,"deformGroup":"innerfender_damage_R"}],
          {"optional":false}
          //rigidifier
          {"beamSpring":201000,"beamDamp":25},
          {"beamDeform":1200,"beamStrength":4000},
          //r
          ["fe1r","ifer"],
          ["fe3r","ifer"],
          ["fe4r","ifer"],
          ["fe5r","ifer"],
          ["fb4rr","ifer",{"beamStrength":2500,"deformGroup":"innerfender_damage_R"}],
          //rigidifier beams

          {"optional":true}
          ["fe1r","fe4r"],
          ["fe3r","fe5r"],
          ["fe4r","fb4rr",{"beamStrength":2500,"deformGroup":"innerfender_damage_R"}],
          {"optional":false}

          //attach
          {"disableTriangleBreaking":true},
          {"beamSpring":201000,"beamDamp":20},
          {"beamDeform":11500,"beamStrength":5000},
          {"deformGroup":"innerfender_damage_R"},
          {"breakGroup":"ifender_a_R"},
          //firewall
          ["fe1r","f1rr"],
          ["fe1r","f5rr"],
          ["fe1r","f6rr"],
          ["fe3r","f1rr"],
          ["fe3r","f5rr"],
          ["fe3r","f6rr"],
          //middle
          {"beamDeform":7500,"beamStrength":5000},
          {"breakGroup":"ifender_b_R"},
          {"deformGroup":"innerfender_damage_R"},
          ["fe4r","fx3r"],
          ["fe4r","fx4r"],
          ["fe4r","f14rr"],
          ["fe4r","f14r"],
          ["fe4r","f15rr"],
          ["fe4r","f6rr"],

          ["ifer","fx3r"],
          ["ifer","fx4r"],
          //front
          {"beamDeform":8500,"beamStrength":3000},
          {"breakGroup":"ifender_c_R"},
          {"deformGroup":"innerfender_damage_R"},
          ["fe5r","f15rr"],
          ["fe5r","f19r"],
          ["fe5r","f19rr"],

          ["fb4rr","f15rr"],
          ["fb4rr","f19r"],
          {"breakGroup":""},
          {"deformGroup":""},
          {"beamDeform":8500,"beamStrength":"FLT_MAX"},
          //rigifiers
          ["ifer","fx3r"],
          ["ifer","fx4r"],
          ["ifer","f10rr"],
          {"disableMeshBreaking":false},
          {"beamPrecompression":1, "beamType":"|NORMAL", "beamLongBound":1.0, "beamShortBound":1.0},
          {"deformLimitExpansion":1.1},
    ],
    "triangles": [
            ["id1:","id2:","id3:"],
            //right inner fender
            {"groundModel":"metal"},
            {"group":"covet_innerfender_R"},
            {"dragCoef":8},

            {"group":""},
    ],
},
"covet_innerfender_FL_wide": {
    "information":{
        "authors":"BeamNG",
        "name":"Front Left Inner Fender",
        "value":235,
    },

    "slotType" : "covet_innerfender_FL_wide",

    "flexbodies": [
        ["mesh", "[group]:", "nonFlexMaterials"],
        {"deformGroup":"innerfender_damage_L", "deformMaterialBase":"covet", "deformMaterialDamaged":"invis"},
        ["covet_innerfender_wide_L", ["covet_innerfender_L","covet_fender_L_alt","covet_fender_L","covet_bumper_F"]],
        {"deformGroup":""},
    ],
    "nodes": [
        ["id", "posX", "posY", "posZ"],
        //--FENDER--
        {"nodeMaterial":"|NM_METAL"},
        {"frictionCoef":0.5},
        {"collision":true},
        {"selfCollision":true},
        {"nodeWeight":0.35},
        {"group":"covet_innerfenders_L"},
        ["fb4ll", 0.79, -1.60, 0.235],
        //rigidifier
        {"group":"covet_innerfender_L"},
        ["ifel", 0.45, -1.25, 0.35],
        {"group":""},
    ],
    "beams": [
          ["id1:", "id2:"],
          {"beamPrecompression":1, "beamType":"|NORMAL", "beamLongBound":1.0, "beamShortBound":1.0},
          //--FENDER--
          {"beamSpring":101000,"beamDamp":25},
          {"beamDeform":3050,"beamStrength":4000},
          //inner fender main shape
          {"deformationTriggerRatio":0.1},
          {"deformLimitExpansion":1.1},
          {"disableMeshBreaking":true},
          {"breakGroup":"ifender_c_L"},
          {"optional":true}
          ["fe1l","fe3l"],
          ["fe3l","fe4l"],
          ["fe4l","fe5l"],
          ["fe5l","fb4ll",{"beamStrength":2500,"deformGroup":"innerfender_damage_L"}],
          {"optional":false}
          //rigidifier
          {"beamSpring":201000,"beamDamp":25},
          {"beamDeform":1200,"beamStrength":4000},

          ["fe1l","ifel"],
          ["fe3l","ifel"],
          ["fe4l","ifel"],
          ["fe5l","ifel"],
          ["fb4ll","ifel",{"beamStrength":2500,"deformGroup":"innerfender_damage_L"}],
          //rigidifier beams
          {"optional":true}
          ["fe1l","fe4l"],
          ["fe3l","fe5l"],
          ["fe4l","fb4ll",{"beamStrength":2500,"deformGroup":"innerfender_damage_L"}],
          {"optional":false}

          //attach
          {"disableTriangleBreaking":true},
          {"beamSpring":201000,"beamDamp":20},
          {"beamDeform":11500,"beamStrength":5000},
          {"breakGroup":"ifender_a_L"},
          {"deformGroup":"innerfender_damage_L"},
          //firewall
          ["fe1l","f1ll"],
          ["fe1l","f5ll"],
          ["fe1l","f6ll"],
          ["fe3l","f1ll"],
          ["fe3l","f5ll"],
          ["fe3l","f6ll"],
          //middle
          {"beamDeform":7500,"beamStrength":5000},
          {"breakGroup":"ifender_b_L"},
          {"deformGroup":"innerfender_damage_L"},
          ["fe4l","fx3l"],
          ["fe4l","fx4l"],
          ["fe4l","f14ll"],
          ["fe4l","f14l"],
          ["fe4l","f15ll"],
          ["fe4l","f6ll"],
          //front
          {"beamDeform":8500,"beamStrength":3000},
          {"breakGroup":"ifender_c_L"},
          {"deformGroup":"innerfender_damage_L"},
          ["fe5l","f15ll"],
          ["fe5l","f19l"],
          ["fe5l","f19ll"],

          ["fb4ll","f15ll"],
          ["fb4ll","f19l"],
          {"breakGroup":""},
          {"deformGroup":""},
          {"beamDeform":8500,"beamStrength":"FLT_MAX"},
          //rigifiers
          ["ifel","fx3l"],
          ["ifel","fx4l"],
          ["ifel","f10ll"],
          {"disableMeshBreaking":false},
          {"beamPrecompression":1, "beamType":"|NORMAL", "beamLongBound":1.0, "beamShortBound":1.0},
          {"deformLimitExpansion":1.1},
    ],
    "triangles": [
            ["id1:","id2:","id3:"],
            //right inner fender
            {"groundModel":"metal"},
            {"group":"covet_innerfender_R"},
            {"dragCoef":8},

            {"group":""},
    ],
},
}