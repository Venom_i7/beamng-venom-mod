{
"tire_F_165_70_13_sport": {
    "information":{
        "authors":"BeamNG",
        "name":"165/70R13 Sport Front Tires",
        "value":70,
    },
    "slotType" : "tire_F_13x5",
    "flexbodies": [
        ["mesh", "[group]:", "nonFlexMaterials"],
        ["tire_01a_13x5_23",    ["wheel_FR","tire_FR"], [], {"pos":{"x":-0.48, "y":0.0, "z":0.0}, "rot":{"x":0, "y":0, "z":180}, "scale":{"x":0.975, "y":0.985, "z":0.985}}],
        ["tire_01a_13x5_23",    ["wheel_FL","tire_FL"], [], {"pos":{"x": 0.48, "y":0.0, "z":0.0}, "rot":{"x":0, "y":0, "z":0},   "scale":{"x":0.975, "y":0.985, "z":0.985}}],
    ],
    "variables": [
        ["name", "type", "unit", "category", "default", "min", "max", "title", "description"],
        ["$tirepressure_F", "range", "psi", "Wheels", 27, 0, 50, "Tire Pressure", "Relative to atmospheric pressure", {"subCategory":"Front"}],
    ],
    "pressureWheels": [
        ["name","hubGroup","group","node1:","node2:","nodeS","nodeArm:","wheelDir"],
        {"disableMeshBreaking":false,"disableHubMeshBreaking":false},
        {"hasTire":true},
        {"enableTireReinfBeams":false},
        {"enableTireLbeams":true},
        {"enableTireSideReinfBeams":false},
        {"enableTreadReinfBeams":true},
        {"enableTirePeripheryReinfBeams":true},

        //general settings
        {"radius":0.285},
        {"tireWidth":0.135},

        //tire options
        {"wheelSideBeamSpring":"$=$tirepressure_F*650","wheelSideBeamDamp":25},
        {"wheelSideBeamSpringExpansion":281000,"wheelSideBeamDampExpansion":25},
        {"wheelSideTransitionZone":0.1,"wheelSideBeamPrecompression":0.99},

        {"wheelReinfBeamSpring":24000,"wheelReinfBeamDamp":150},
        {"wheelReinfBeamDampCutoffHz":500,"wheelReinfBeamPrecompression":0.98},

        {"wheelTreadBeamSpring":65000,"wheelTreadBeamDamp":55},
        {"wheelTreadBeamDampCutoffHz":500,"wheelTreadBeamPrecompression":0.98},

        {"wheelTreadReinfBeamSpring":135000,"wheelTreadReinfBeamDamp":45},
        {"wheelTreadReinfBeamDampCutoffHz":500,"wheelTreadReinfBeamPrecompression":0.98},

        {"wheelPeripheryBeamSpring":33000,"wheelPeripheryBeamDamp":30},
        {"wheelPeripheryBeamDampCutoffHz":500,"wheelPeripheryBeamPrecompression":0.98},

        {"wheelPeripheryReinfBeamSpring":96000,"wheelPeripheryReinfBeamDamp":25},
        {"wheelPeripheryReinfBeamDampCutoffHz":500,"wheelPeripheryReinfBeamPrecompression":0.98},

        //general tire values
        {"nodeWeight":0.115},
        {"nodeMaterial":"|NM_RUBBER"},
        {"triangleCollision":false},
        {"pressurePSI":"$tirepressure_F"},
        {"dragCoef":5},

        //groundmodel friction multipliers
        {"frictionCoef":1.0},
        {"slidingFrictionCoef":1.0},
        {"treadCoef":0.5},

        //advanced friction values
        {"noLoadCoef":1.42},
        {"loadSensitivitySlope":0.00021},
        {"fullLoadCoef":0.4},
        {"softnessCoef":0.7},

        //deform values
        {"wheelSideBeamDeform":11000,"wheelSideBeamStrength":15000},
        {"wheelTreadBeamDeform":10000,"wheelTreadBeamStrength":13000},
        {"wheelPeripheryBeamDeform":40000,"wheelPeripheryBeamStrength":40000},
    ],
},
}