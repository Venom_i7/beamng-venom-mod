{
"tire_F_245_45_20_sport": {
    "information":{
        "authors":"BeamNG",
        "name":"245/45R20 Sport Front Tires",
        "value":390,
    },
    "slotType" : "tire_F_20x8",
    "flexbodies": [
        ["mesh", "[group]:", "nonFlexMaterials"],
        ["tire_01a_19x9_26", ["tire_FR","wheel_FR"], [], {"pos":{"x":-0.50, "y":0.0, "z":0.0}, "rot":{"x":0, "y":0, "z":180}, "scale":{"x":0.95, "y":1.1, "z":1.1}}],
        ["tire_01a_19x9_26", ["tire_FL","wheel_FL"], [], {"pos":{"x": 0.50, "y":0.0, "z":0.0}, "rot":{"x":0, "y":0, "z":0},   "scale":{"x":0.95, "y":1.1, "z":1.1}}],
    ],
    "variables": [
        ["name", "type", "unit", "category", "default", "min", "max", "title", "description"],
        ["$tirepressure_F", "range", "psi", "Wheels", 30, 0, 50, "Tire Pressure", "Relative to atmospheric pressure", {"subCategory":"Front"}],
    ],
    "pressureWheels": [
        ["name","hubGroup","group","node1:","node2:","nodeS","nodeArm:","wheelDir"],
        {"disableMeshBreaking":false,"disableHubMeshBreaking":false},
        {"hasTire":true},
        {"enableTireReinfBeams":false},
        {"enableTireLbeams":true},
        {"enableTireSideReinfBeams":false},
        {"enableTreadReinfBeams":true},
        {"enableTirePeripheryReinfBeams":true},

        //general settings
        {"radius":0.365},
        {"tireWidth":0.205},

        //tire options
        {"wheelSideBeamSpring":"$=$tirepressure_F*1167","wheelSideBeamDamp":15},
        {"wheelSideBeamSpringExpansion":501000,"wheelSideBeamDampExpansion":45},
        {"wheelSideTransitionZone":0.12,"wheelSideBeamPrecompression":0.985},

        {"wheelReinfBeamSpring":35000,"wheelReinfBeamDamp":225},
        {"wheelReinfBeamDampCutoffHz":500,"wheelReinfBeamPrecompression":0.98},

        {"wheelTreadBeamSpring":151000,"wheelTreadBeamDamp":100},
        {"wheelTreadBeamDampCutoffHz":500,"wheelTreadBeamPrecompression":0.98},

        {"wheelTreadReinfBeamSpring":151000,"wheelTreadReinfBeamDamp":80},
        {"wheelTreadReinfBeamDampCutoffHz":500,"wheelTreadReinfBeamPrecompression":0.98},

        {"wheelPeripheryBeamSpring":101000,"wheelPeripheryBeamDamp":45},
        {"wheelPeripheryBeamDampCutoffHz":500,"wheelPeripheryBeamPrecompression":0.98},

        {"wheelPeripheryReinfBeamSpring":101000,"wheelPeripheryReinfBeamDamp":45},
        {"wheelPeripheryReinfBeamDampCutoffHz":500,"wheelPeripheryReinfBeamPrecompression":0.98},

        //general tire values
        {"nodeWeight":0.195},
        {"nodeMaterial":"|NM_RUBBER"},
        {"triangleCollision":false},
        {"pressurePSI":"$tirepressure_F"},
        {"dragCoef":5},

        //groundmodel friction multipliers
        {"frictionCoef":1.0},
        {"slidingFrictionCoef":1.0},
        {"stribeckExponent":1.5},
        //{"stribeckVelMult":1.4},
        {"treadCoef":0.5},

        //advanced friction values
        {"noLoadCoef":1.58},
        {"loadSensitivitySlope":0.000155},
        {"fullLoadCoef":0.5},
        {"softnessCoef":0.8},

        //deform values
        {"wheelSideBeamDeform":17000,"wheelSideBeamStrength":22000},
        {"wheelTreadBeamDeform":12000,"wheelTreadBeamStrength":16000},
        {"wheelPeripheryBeamDeform":55000,"wheelPeripheryBeamStrength":55000},

    ],
},
}