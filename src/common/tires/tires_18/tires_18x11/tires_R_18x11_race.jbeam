{
"tire_R_295_30_18_race": {
    "information":{
        "authors":"BeamNG",
        "name":"295/30R18 Race Rear Tires",
        "value":270,
    },
    "slotType" : "tire_R_18x11",
    "flexbodies": [
        ["mesh", "[group]:", "nonFlexMaterials"],
        ["tire_01f_18x11_25", ["wheel_RR","tire_RR"], [], {"pos":{"x":-0.47, "y":0.0, "z":0.0}, "rot":{"x":0, "y":0, "z":180}, "scale":{"x":1.04, "y":1.003, "z":1.003}}],
        ["tire_01f_18x11_25", ["wheel_RL","tire_RL"], [], {"pos":{"x": 0.47, "y":0.0, "z":0.0}, "rot":{"x":0, "y":0, "z":0},   "scale":{"x":1.04, "y":1.003, "z":1.003}}],
    ],
    "variables": [
        ["name", "type", "unit", "category", "default", "min", "max", "title", "description"],
        ["$tirepressure_R", "range", "psi", "Wheels", 27, 0, 50, "Tire Pressure", "Relative to atmospheric pressure", {"subCategory":"Rear"}],
    ],
    "pressureWheels": [
        ["name","hubGroup","group","node1:","node2:","nodeS","nodeArm:","wheelDir"],
        {"disableMeshBreaking":false,"disableHubMeshBreaking":false},
        {"hasTire":true},
        {"enableTireReinfBeams":false},
        {"enableTireLbeams":true},
        {"enableTireSideReinfBeams":false},
        {"enableTreadReinfBeams":true},
        {"enableTirePeripheryReinfBeams":true},

        //general settings
        {"radius":0.32},
        {"tireWidth":0.265},

        //tire options
        {"wheelSideBeamSpring":"$=$tirepressure_R*400","wheelSideBeamDamp":40},
        {"wheelSideBeamSpringExpansion":511000,"wheelSideBeamDampExpansion":40},
        {"wheelSideTransitionZone":0.12,"wheelSideBeamPrecompression":0.97},

        {"wheelReinfBeamSpring":50000,"wheelReinfBeamDamp":205},
        {"wheelReinfBeamDampCutoffHz":500,"wheelReinfBeamPrecompression":0.98},

        {"wheelTreadBeamSpring":81000,"wheelTreadBeamDamp":90},
        {"wheelTreadBeamDampCutoffHz":500,"wheelTreadBeamPrecompression":0.98},

        {"wheelTreadReinfBeamSpring":291000,"wheelTreadReinfBeamDamp":75},
        {"wheelTreadReinfBeamDampCutoffHz":500,"wheelTreadReinfBeamPrecompression":0.98},

        {"wheelPeripheryBeamSpring":101000,"wheelPeripheryBeamDamp":65},
        {"wheelPeripheryBeamDampCutoffHz":500,"wheelPeripheryBeamPrecompression":0.98},

        {"wheelPeripheryReinfBeamSpring":181000,"wheelPeripheryReinfBeamDamp":45},
        {"wheelPeripheryReinfBeamDampCutoffHz":500,"wheelPeripheryReinfBeamPrecompression":0.98},

        //general tire values
        {"nodeWeight":0.19},
        {"nodeMaterial":"|NM_RUBBER"},
        {"triangleCollision":false},
        {"pressurePSI":"$tirepressure_R"},
        {"dragCoef":5},

        //groundmodel friction multipliers
        {"frictionCoef":1.0},
        {"slidingFrictionCoef":1.00},
        {"stribeckExponent":2},
        //{"stribeckVelMult":1.25},
        {"treadCoef":0},

        //advanced friction values
        {"noLoadCoef":2.10},
        {"loadSensitivitySlope":0.000205},
        {"fullLoadCoef":0.75},
        {"softnessCoef":1},

        //deform values
        {"wheelSideBeamDeform":17000,"wheelSideBeamStrength":22000},
        {"wheelTreadBeamDeform":13000,"wheelTreadBeamStrength":17000},
        {"wheelPeripheryBeamDeform":55000,"wheelPeripheryBeamStrength":55000},
    ],
},
"tire_R_295_35_18_race": {
    "information":{
        "authors":"BeamNG",
        "name":"295/35R18 Race Rear Tires",
        "value":300,
    },
    "slotType" : "tire_R_18x11",
    "flexbodies": [
        ["mesh", "[group]:", "nonFlexMaterials"],
        ["tire_01f_17x9_25", ["wheel_RR","tire_RR"], [], {"pos":{"x":-0.47, "y":0.0, "z":0.0}, "rot":{"x":0, "y":0, "z":180}, "scale":{"x":1.205, "y":1.022, "z":1.022}}],
        ["tire_01f_17x9_25", ["wheel_RL","tire_RL"], [], {"pos":{"x": 0.47, "y":0.0, "z":0.0}, "rot":{"x":0, "y":0, "z":0},   "scale":{"x":1.205, "y":1.022, "z":1.022}}],
    ],
    "variables": [
        ["name", "type", "unit", "category", "default", "min", "max", "title", "description"],
        ["$tirepressure_R", "range", "psi", "Wheels", 27, 0, 50, "Tire Pressure", "Relative to atmospheric pressure", {"subCategory":"Rear"}],
    ],
    "pressureWheels": [
        ["name","hubGroup","group","node1:","node2:","nodeS","nodeArm:","wheelDir"],
        {"disableMeshBreaking":false,"disableHubMeshBreaking":false},
        {"hasTire":true},
        {"enableTireReinfBeams":false},
        {"enableTireLbeams":true},
        {"enableTireSideReinfBeams":false},
        {"enableTreadReinfBeams":true},
        {"enableTirePeripheryReinfBeams":true},

        //general settings
        {"radius":0.325},
        {"tireWidth":0.275},

        //tire options
        {"wheelSideBeamSpring":"$=$tirepressure_F*400","wheelSideBeamDamp":40},
        {"wheelSideBeamSpringExpansion":511000,"wheelSideBeamDampExpansion":40},
        {"wheelSideTransitionZone":0.12,"wheelSideBeamPrecompression":0.97},

        {"wheelReinfBeamSpring":50000,"wheelReinfBeamDamp":205},
        {"wheelReinfBeamDampCutoffHz":500,"wheelReinfBeamPrecompression":0.98},

        {"wheelTreadBeamSpring":81000,"wheelTreadBeamDamp":90},
        {"wheelTreadBeamDampCutoffHz":500,"wheelTreadBeamPrecompression":0.98},

        {"wheelTreadReinfBeamSpring":291000,"wheelTreadReinfBeamDamp":75},
        {"wheelTreadReinfBeamDampCutoffHz":500,"wheelTreadReinfBeamPrecompression":0.98},

        {"wheelPeripheryBeamSpring":101000,"wheelPeripheryBeamDamp":65},
        {"wheelPeripheryBeamDampCutoffHz":500,"wheelPeripheryBeamPrecompression":0.98},

        {"wheelPeripheryReinfBeamSpring":181000,"wheelPeripheryReinfBeamDamp":45},
        {"wheelPeripheryReinfBeamDampCutoffHz":500,"wheelPeripheryReinfBeamPrecompression":0.98},

        //general tire values
        {"nodeWeight":0.19},
        {"nodeMaterial":"|NM_RUBBER"},
        {"triangleCollision":false},
        {"pressurePSI":"$tirepressure_F"},
        {"dragCoef":5},

        //groundmodel friction multipliers
        {"frictionCoef":1.0},
        {"slidingFrictionCoef":1.00},
        {"stribeckExponent":2},
        //{"stribeckVelMult":1.25},
        {"treadCoef":0},

        //advanced friction values
        {"noLoadCoef":2.10},
        {"loadSensitivitySlope":0.000205},
        {"fullLoadCoef":0.75},
        {"softnessCoef":1},

        //deform values
        {"wheelSideBeamDeform":17000,"wheelSideBeamStrength":22000},
        {"wheelTreadBeamDeform":13000,"wheelTreadBeamStrength":17000},
        {"wheelPeripheryBeamDeform":55000,"wheelPeripheryBeamStrength":55000},
    ],
},
}