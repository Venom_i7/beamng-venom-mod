{
"etk_engine_i6_3.0_petrol_ecu_na_270": {
    "information":{
        "authors":"BeamNG",
        "name":"270 NA ECU",
        "value":2500,
    },
    "slotType" : "etk_engine_i6_3.0_petrol_ecu",
    "slots": [
        ["type", "default", "description"],
        ["etk_engine_ecu_speedlimit","etk_engine_ecu_speedlimit_250", "Speed Limiter", {"coreSlot":true}],
    ],
    "mainEngine":{
        "revLimiterRPM":6800,
        "revLimiterType": "soft",
    },
},
"etk_engine_i6_3.0_petrol_ecu_300": {
    "information":{
        "authors":"BeamNG",
        "name":"300 ECU",
        "value":2500,
    },
    "slotType" : "etk_engine_i6_3.0_petrol_ecu",
    "slots": [
        ["type", "default", "description"],
        ["etk_engine_ecu_speedlimit","etk_engine_ecu_speedlimit_250", "Speed Limiter", {"coreSlot":true}],
    ],
    "mainEngine":{
        "revLimiterRPM":7000,
        "revLimiterType": "soft",
    },
    "turbocharger": {
        "wastegateStart":6,
        "maxExhaustPower": 13000,
        "engineDef":[
            //engineRPM, efficiency, exhaustFactor
            [0,     0.0,    0.25],
            [650,   0.6,   0.45],
            [1000,   0.75,   0.65],
            [1400,  1.0,   0.8],
            [2000,  0.86,    1.0],
            [2200,  0.77,    1.0],
            [2500,  0.69,    1.0],
            [3000,  0.67,    1.0],
            [4000,  0.70,    1.0],
            [5000,  0.78,    1.0],
            [5800,  0.59,    1.0],
            [6000,  0.50,    1.0],
            [6500,  0.22,    1.0],
            [7000,  0.15,    1.0],
            [8000,  0.1,    1.0],
        ],
    }
},
"etk_engine_i6_3.0_petrol_ecu_340": {
    "information":{
        "authors":"BeamNG",
        "name":"340 ECU",
        "value":2500,
    },
    "slotType" : "etk_engine_i6_3.0_petrol_ecu",
    "slots": [
        ["type", "default", "description"],
        ["etk_engine_ecu_speedlimit","etk_engine_ecu_speedlimit_250", "Speed Limiter", {"coreSlot":true}],
    ],
    "mainEngine":{
        "revLimiterRPM":7000,
        "revLimiterType": "soft",
    },
    "turbocharger": {
        "wastegateStart":10,
        "maxExhaustPower": 14000,
        "engineDef":[
            //engineRPM, efficiency, exhaustFactor
            [0,     0.0,    0.25],
            [650,   0.6,   0.35],
            [1000,   0.65,   0.55],
            [1400,  0.95,   0.8],
            [2000,  0.83,    1.0],
            [2200,  0.75,    1.0],
            [2500,  0.69,    1.0],
            [3000,  0.67,    1.0],
            [4000,  0.70,    1.0],
            [5000,  0.75,    1.0],
            [5200,  0.74,    1.0],
            [5500,  0.70,    1.0],
            [6000,  0.55,    1.0],
            [6500,  0.38,    1.0],
            [7000,  0.3,    1.0],
            [8000,  0.15,    1.0],
        ],
    }
},
"etk_engine_i6_3.0_petrol_ecu_360": {
    "information":{
        "authors":"BeamNG",
        "name":"360 ECU",
        "value":2500,
    },
    "slotType" : "etk_engine_i6_3.0_petrol_ecu",
    "slots": [
        ["type", "default", "description"],
        ["etk_engine_ecu_speedlimit","etk_engine_ecu_speedlimit_250", "Speed Limiter", {"coreSlot":true}],
    ],
    "mainEngine":{
        "revLimiterRPM":7000,
        "revLimiterType": "soft",
    },
    "turbocharger": {
        "wastegateStart":10,
        "maxExhaustPower": 15000,
        "engineDef":[
            //engineRPM, efficiency, exhaustFactor
            [0,     0.0,    0.25],
            [650,   0.6,   0.35],
            [1000,   0.65,   0.55],
            [1400,  0.95,   0.8],
            [2000,  0.83,    1.0],
            [2200,  0.75,    1.0],
            [2500,  0.69,    1.0],
            [3000,  0.67,    1.0],
            [4000,  0.70,    1.0],
            [5000,  0.73,    1.0],
            [5500,  0.70,    1.0],
            [6000,  0.63,    1.0],
            [6500,  0.52,    1.0],
            [7000,  0.37,    1.0],
            [8000,  0.20,    1.0],
        ],
    }
},
"etk_engine_i6_3.0_petrol_ecu_360_stage1": {
    "information":{
        "authors":"BeamNG",
        "name":"360 ECU (Stage 1)",
        "value":2500,
    },
    "slotType" : "etk_engine_i6_3.0_petrol_ecu",
    "slots": [
        ["type", "default", "description"],
        ["etk_engine_ecu_speedlimit","etk_engine_ecu_speedlimit_off", "Speed Limiter", {"coreSlot":true}],
    ],
    "mainEngine":{
        "revLimiterRPM":7000,
        "revLimiterType":"timeBased",
        "revLimiterCutTime":0.1,
    },
    "turbocharger": {
        "wastegateStart":13.5,
        "maxExhaustPower": 15000,
        "engineDef":[
            //engineRPM, efficiency, exhaustFactor
            [0,     0.0,    0.25],
            [650,   0.6,   0.35],
            [1000,   0.65,   0.55],
            [1400,  0.9,   0.8],
            [2000,  0.78,    1.0],
            [2200,  0.73,    1.0],
            [2500,  0.69,    1.0],
            [3000,  0.67,    1.0],
            [4000,  0.70,    1.0],
            [5000,  0.73,    1.0],
            [5500,  0.70,    1.0],
            [6000,  0.63,    1.0],
            [6500,  0.52,    1.0],
            [7000,  0.49,    1.0],
            [8000,  0.40,    1.0],
        ],
    }
},
"etk_engine_i6_3.0_petrol_ecu_360_stage2": {
    "information":{
        "authors":"BeamNG",
        "name":"360 ECU (Stage 2)",
        "value":2500,
    },
    "slotType" : "etk_engine_i6_3.0_petrol_ecu",
    "slots": [
        ["type", "default", "description"],
        ["etk_engine_ecu_speedlimit","etk_engine_ecu_speedlimit_off", "Speed Limiter", {"coreSlot":true}],
    ],
    "mainEngine":{
        "revLimiterRPM":7500,
        "revLimiterType":"timeBased",
        "revLimiterCutTime":0.1,
    },
    "turbocharger": {
        "wastegateStart":16,
        "maxExhaustPower": 15000,
        "engineDef":[
            //engineRPM, efficiency, exhaustFactor
            [0,     0.0,    0.25],
            [650,   0.6,   0.35],
            [1000,   0.65,   0.55],
            [1400,  0.9,   0.8],
            [2000,  0.78,    1.0],
            [2200,  0.73,    1.0],
            [2500,  0.72,    1.0],
            [3000,  0.72,    1.0],
            [4000,  0.75,    1.0],
            [5000,  0.78,    1.0],
            [5500,  0.75,    1.0],
            [5500,  0.75,    1.0],
            [6000,  0.70,    1.0],
            [6500,  0.63,    1.0],
            [7000,  0.68,    1.0],
            [8000,  0.50,    1.0],
        ],
    }
},
"etk_engine_i6_3.0_petrol_ecu_440_ttsport": {
    "information":{
        "authors":"BeamNG",
        "name":"440 ttSport ECU",
        "value":2500,
    },
    "slotType" : "etk_engine_i6_3.0_petrol_ecu",
    "slots": [
        ["type", "default", "description"],
        ["etk_engine_ecu_speedlimit","etk_engine_ecu_speedlimit_270", "Speed Limiter", {"coreSlot":true}],
    ],
    "mainEngine":{
        "revLimiterRPM":7700,
        "revLimiterType":"timeBased",
        "revLimiterCutTime":0.1,
    },
    "turbocharger": {
        "wastegateStart":12.5,
        "maxExhaustPower": 4000,
        "engineDef":[
            //engineRPM, efficiency, exhaustFactor
            [0,     0.0,    0.25],
            [650,   0.6,   0.25],
            [1000,   0.65,   0.35],
            [1300,  0.90,   0.6],
            [1800,  0.95,   0.6],
            [2000,  0.92,    0.9],
            [2200,  0.86,    1.0],
            [2500,  0.83,    1.0],
            [3000,  0.81,    1.0],
            [4000,  0.82,    1.0],
            [5000,  0.86,    1.0],
            [5500,  0.85,    1.0],
            [6000,  0.75,    1.0],
            [6500,  0.63,    1.0],
            [7000,  0.54,    1.0],
            [7500,  0.52,    1.0],
            [8000,  0.35,    1.0],
            [9000,  0.2,    1.0],
        ],
    }
},
"etk_engine_i6_3.0_petrol_ecu_460_ttsport": {
    "information":{
        "authors":"BeamNG",
        "name":"460 ttSport ECU",
        "value":2500,
    },
    "slotType" : "etk_engine_i6_3.0_petrol_ecu",
    "slots": [
        ["type", "default", "description"],
        ["etk_engine_ecu_speedlimit","etk_engine_ecu_speedlimit_270", "Speed Limiter", {"coreSlot":true}],
    ],
    "mainEngine":{
        "revLimiterRPM":7600,
        "revLimiterType":"timeBased",
        "revLimiterCutTime":0.1,
    },
    "turbocharger": {
        "wastegateStart":12.3,
        "maxExhaustPower": 5000,
        "engineDef":[
            //engineRPM, efficiency, exhaustFactor
            [0,     0.0,    0.25],
            [650,   0.6,   0.25],
            [1000,   0.65,   0.35],
            [1300,  0.90,   0.6],
            [1800,  0.95,   0.6],
            [2000,  0.92,    0.9],
            [2200,  0.86,    1.0],
            [2500,  0.83,    1.0],
            [3000,  0.81,    1.0],
            [4000,  0.82,    1.0],
            [5000,  0.86,    1.0],
            [5500,  0.82,    1.0],
            [6000,  0.78,    1.0],
            [6400,  0.73,    1.0],
            [7000,  0.65,    1.0],
            [7500,  0.63,    1.0],
            [8000,  0.47,    1.0],
            [9000,  0.25,    1.0],
        ],
    }
},
"etk_engine_i6_3.0_petrol_ecu_500_ttsport": {
    "information":{
        "authors":"BeamNG",
        "name":"500 ttSport ECU",
        "value":2500,
    },
    "slotType" : "etk_engine_i6_3.0_petrol_ecu",
    "slots": [
        ["type", "default", "description"],
        ["etk_engine_ecu_speedlimit","etk_engine_ecu_speedlimit_290", "Speed Limiter", {"coreSlot":true}],
    ],
    "mainEngine":{
        "revLimiterRPM":7600,
        "revLimiterType":"timeBased",
        "revLimiterCutTime":0.1,
    },
    "turbocharger": {
        "wastegateStart":14.5,
        "maxExhaustPower": 5000,
        "engineDef":[
            //engineRPM, efficiency, exhaustFactor
            [0,     0.0,    0.25],
            [650,   0.6,   0.25],
            [1000,   0.65,   0.35],
            [1300,  0.90,   0.6],
            [1800,  0.85,   0.6],
            [2000,  0.85,    0.9],
            [2200,  0.81,    1.0],
            [2500,  0.79,    1.0],
            [3000,  0.81,    1.0],
            [4000,  0.82,    1.0],
            [5000,  0.86,    1.0],
            [5500,  0.82,    1.0],
            [6000,  0.79,    1.0],
            [6500,  0.75,    1.0],
            [7000,  0.68,    1.0],
            [7500,  0.64,    1.0],
            [8000,  0.47,    1.0],
            [9000,  0.25,    1.0],
        ],
    }
},
"etk_engine_i6_3.0_petrol_ecu_500_ttsport_stage1": {
    "information":{
        "authors":"BeamNG",
        "name":"500 ttSport ECU (Stage 1)",
        "value":2500,
    },
    "slotType" : "etk_engine_i6_3.0_petrol_ecu",
    "slots": [
        ["type", "default", "description"],
        ["etk_engine_ecu_speedlimit","etk_engine_ecu_speedlimit_off", "Speed Limiter", {"coreSlot":true}],
    ],
    "mainEngine":{
        "revLimiterRPM":7600,
        "revLimiterType":"timeBased",
        "revLimiterCutTime":0.1,
    },
    "turbocharger": {
        "wastegateStart":17.5,
        "maxExhaustPower": 5000,
        "engineDef":[
            //engineRPM, efficiency, exhaustFactor
            [0,     0.0,    0.25],
            [650,   0.6,   0.25],
            [1000,   0.65,   0.35],
            [1300,  0.6,   0.6],
            [1800,  0.67,   0.6],
            [2000,  0.71,    0.9],
            [2200,  0.72,    1.0],
            [2500,  0.73,    1.0],
            [3000,  0.75,    1.0],
            [4000,  0.82,    1.0],
            [5000,  0.86,    1.0],
            [5500,  0.82,    1.0],
            [6000,  0.79,    1.0],
            [6500,  0.75,    1.0],
            [7000,  0.68,    1.0],
            [7500,  0.64,    1.0],
            [8000,  0.47,    1.0],
            [9000,  0.25,    1.0],
        ],
    }
},
"etk_engine_i6_3.0_petrol_ecu_500_ttsport_stage2": {
    "information":{
        "authors":"BeamNG",
        "name":"500 ttSport ECU (Stage 2)",
        "value":2500,
    },
    "slotType" : "etk_engine_i6_3.0_petrol_ecu",
    "slots": [
        ["type", "default", "description"],
        ["etk_engine_ecu_speedlimit","etk_engine_ecu_speedlimit_off", "Speed Limiter", {"coreSlot":true}],
    ],
    "mainEngine":{
        "revLimiterRPM":7600,
        "revLimiterType":"timeBased",
        "revLimiterCutTime":0.1,
    },
    "turbocharger": {
        "wastegateStart":20,
        "maxExhaustPower": 5000,
        "engineDef":[
            //engineRPM, efficiency, exhaustFactor
            [0,     0.0,    0.25],
            [650,   0.4,   0.25],
            [1000,   0.45,   0.35],
            [1500,  0.60,   0.6],
            [2000,  0.65,    1.0],
            [3000,  0.70,    1.0],
            [4000,  0.82,    1.0],
            [5000,  0.87,    1.0],
            [5500,  0.86,    1.0],
            [6000,  0.84,    1.0],
            [6500,  0.78,    1.0],
            [7000,  0.70,    1.0],
            [7500,  0.60,    1.0],
            [8000,  0.41,    1.0],
            [9000,  0.25,    1.0],
        ],
    }
},
"etk_engine_i6_3.0_petrol_ecu_500_ttsport_stage3": {
    "information":{
        "authors":"BeamNG",
        "name":"500 ttSport ECU (Stage 3)",
        "value":2500,
    },
    "slotType" : "etk_engine_i6_3.0_petrol_ecu",
    "slots": [
        ["type", "default", "description"],
        ["etk_engine_ecu_speedlimit","etk_engine_ecu_speedlimit_off", "Speed Limiter", {"coreSlot":true}],
    ],
    "mainEngine":{
        "revLimiterRPM":7600,
        "revLimiterType":"timeBased",
        "revLimiterCutTime":0.1,
    },
    "turbocharger": {
        "wastegateStart":25,
        "maxExhaustPower": 6000,
        "engineDef":[
            //engineRPM, efficiency, exhaustFactor
            [0,     0.0,    0.25],
            [650,   0.4,   0.25],
            [1000,   0.45,   0.35],
            [1500,  0.60,   0.6],
            [2000,  0.65,    1.0],
            [3000,  0.70,    1.0],
            [4000,  0.82,    1.0],
            [5000,  0.92,    1.0],
            [5500,  0.88,    1.0],
            [6000,  0.85,    1.0],
            [6500,  0.85,    1.0],
            [7000,  0.72,    1.0],
            [7500,  0.62,    1.0],
            [8000,  0.45,    1.0],
            [9000,  0.25,    1.0],
        ],
    }
},
}