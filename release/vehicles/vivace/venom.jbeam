{
  "vivace_engine_i4_1.5_diesel_venom": {
    "information": {
      "authors": "Venom",
      "name": "1.5L I4 Diesel Engine Venom",
      "value": 4500
    },
    "slotType": "vivace_engine",
    "slots": [
      ["type", "default", "description"],
      ["vivace_enginemounts", "vivace_enginemounts", "Engine Mounts", { "coreSlot": true }],
      ["vivace_oilpan", "vivace_oilpan", "Oil Pan", { "coreSlot": true }],
      ["vivace_turbo_i4_1.5_diesel", "vivace_turbo_i4_1.5_diesel", "Intake", { "coreSlot": true }],
      ["vivace_exhaust_i4_1.5_diesel", "vivace_exhaust_i4_1.5_diesel", "Exhaust"],
      ["vivace_engine_ecu_i4_1.5_diesel", "vivace_engine_ecu_i4_1.5_diesel_110", "Engine Management", { "coreSlot": true }],
      ["n2o_system", "", "Nitrous Oxide System"],
      ["vivace_engine_internals_i4_1.5_diesel", "vivace_engine_internals_i4_1.5_diesel_venom", "Engine Long Block", { "coreSlot": true }],
      ["vivace_transmission", "vivace_transmission_6M_diesel", "Transmission"]
    ],
    "powertrain": [
      ["type", "name", "inputName", "inputIndex"],
      ["combustionEngine", "mainEngine", "dummy", 0]
    ],
    "mainEngine": {
      "torque": [
        ["rpm", "torque"],
        [0, 0],
        [350, 150],
        [600, 175],
        [1000, 200],
        [1750, 225],
        [2000, 250],
        [2500, 275],
        [3000, 300],
        [4000, 300],
        [4500, 300],
        [5000, 300],
        [6000, 300],
        [7000, 300]
      ],

      "idleRPM": 800,
      //max capable
      "maxRPM": 7000,
      "inertia": 0.18,
      "friction": 7,
      "dynamicFriction": 0.013,
      "engineBrakeTorque": 45,
      //"burnEfficiency":0.49,
      "burnEfficiency": [
        [0, 0.77],
        [0.05, 0.85],
        [0.4, 0.93],
        [0.7, 0.99],
        [1, 1.0]
      ],
      //fuel system
      "energyStorage": "mainTank",
      "requiredEnergyType": "diesel",

      //exhaust
      "particulates": 0.07,
      "instantAfterFireCoef": 0.75,
      "sustainedAfterFireCoef": 0.75,

      //cooling and oil system
      "thermalsEnabled": true,
      "engineBlockMaterial": "aluminum",
      "oilVolume": 7,
      "engineBlockAirCoolingEfficiency": 75,

      //engine durability
      "cylinderWallTemperatureDamageThreshold": 79899,
      "headGasketDamageThreshold": 98997797,
      "pistonRingDamageThreshold": 98997797,
      "connectingRodDamageThreshold": 98997797,
      "maxTorqueRating": 5000,
      "maxOverTorqueDamage": 6500,

      //node beam interface
      "torqueReactionNodes:": ["e3r", "e4r", "e2l"],
      "waterDamage": { "[engineGroup]:": ["engine_intake"] },
      "radiator": { "[engineGroup]:": ["radiator"] },
      "engineBlock": { "[engineGroup]:": ["engine_block"] },
      "breakTriggerBeam": "engine",
      "uiName": "Engine",
      "soundConfig": "soundConfig",
      "soundConfigExhaust": "soundConfigExhaust",

      //starter motor
      "starterSample": "event:>Engine>Starter>i4diesel_1990_eng",
      "starterSampleExhaust": "event:>Engine>Starter>i4diesel_1990_exh",
      "shutOffSampleEngine": "event:>Engine>Shutoff>i4diesel_1990_eng",
      "shutOffSampleExhaust": "event:>Engine>Shutoff>i4diesel_1990_exh",
      "starterTorque": 750,
      "starterVolume": 0.76,
      "starterVolumeExhaust": 0.76,
      "shutOffVolumeEngine": 0.76,
      "shutOffVolumeExhaust": 0.76,
      "idleRPMStartRate": 1.5,
      "idleRPMStartCoef": 1.0,

      //damage deformGroups
      "deformGroups": ["mainEngine", "mainEngine_piping", "mainEngine_accessories"],
      "deformGroups_oilPan": ["oilpan_damage"]
    },
    "soundConfig": {
      "sampleName": "I4D_engine",
      "intakeMuffling": 1,

      "mainGain": -5,
      "onLoadGain": 1.0,
      "offLoadGain": 0.6,

      "maxLoadMix": 1,
      "minLoadMix": 0,

      "lowShelfGain": 4,
      "lowShelfFreq": 320,

      "highShelfGain": 8,
      "highShelfFreq": 3500,

      "eqLowGain": -8,
      "eqLowFreq": 1000,
      "eqLowWidth": 0.1,

      "eqHighGain": 0,
      "eqHighFreq": 6000,
      "eqHighWidth": 0.1,

      "fundamentalFrequencyCylinderCount": 4,
      "eqFundamentalGain": 1
    },
    "soundConfigExhaust": {
      "sampleName": "I4D_exhaust",

      "mainGain": -5.5,
      "onLoadGain": 1.0,
      "offLoadGain": 0.35,

      "maxLoadMix": 0.9,
      "minLoadMix": 0,

      "lowShelfGain": -8,
      "lowShelfFreq": 180,

      "highShelfGain": -4,
      "highShelfFreq": 1500,

      "eqLowGain": -5,
      "eqLowFreq": 400,
      "eqLowWidth": 0.15,

      "eqHighGain": 0,
      "eqHighFreq": 1800,
      "eqHighWidth": 0.1,

      "fundamentalFrequencyCylinderCount": 4,
      "eqFundamentalGain": -1
    },
    "vehicleController": {
      "clutchLaunchStartRPM": 1500,
      "clutchLaunchTargetRPM": 1800,
      //**highShiftDown can be overwritten by automatic transmissions**
      "highShiftDownRPM": [0, 0, 0, 2400, 2900, 3200, 3600, 3600],
      //**highShiftUp can be overwritten by intake modifications**
      "highShiftUpRPM": 4400
    },
    "flexbodies": [
      ["mesh", "[group]:", "nonFlexMaterials"],
      ["vivace_engine_i4", ["vivace_engine"]],
      ["vivace_intake_i4", ["vivace_engine"]]
    ],
    "nodes": [
      ["id", "posX", "posY", "posZ"],
      //--2.0L I4 Engine--
      { "selfCollision": false },
      { "collision": true },
      { "nodeMaterial": "|NM_METAL" },
      { "frictionCoef": 0.5 },
      { "group": "vivace_engine" },
      { "engineGroup": "engine_block" },
      { "nodeWeight": 17 },
      [
        "e1r",
        0.14,
        -1.49,
        0.21,
        {
          "chemEnergy": 1000,
          "burnRate": 0.39,
          "flashPoint": 800,
          "specHeat": 0.2,
          "selfIgnitionCoef": false,
          "smokePoint": 650,
          "baseTemp": "thermals",
          "conductionRadius": 0.12
        }
      ],
      ["e1l", 0.14, -1.72, 0.26],
      [
        "e2r",
        -0.37,
        -1.49,
        0.21,
        {
          "chemEnergy": 1000,
          "burnRate": 0.39,
          "flashPoint": 800,
          "specHeat": 0.2,
          "selfIgnitionCoef": false,
          "smokePoint": 650,
          "baseTemp": "thermals",
          "conductionRadius": 0.12
        }
      ],
      ["e2l", -0.37, -1.72, 0.26],
      { "engineGroup": ["engine_block", "engine_intake"] },
      [
        "e3r",
        0.14,
        -1.63,
        0.73,
        {
          "chemEnergy": 1000,
          "burnRate": 0.39,
          "flashPoint": 800,
          "specHeat": 0.2,
          "selfIgnitionCoef": false,
          "smokePoint": 650,
          "baseTemp": "thermals",
          "conductionRadius": 0.12
        }
      ],
      ["e3l", 0.14, -1.39, 0.68, { "isExhaust": "mainEngine" }],
      [
        "e4r",
        -0.37,
        -1.63,
        0.73,
        {
          "chemEnergy": 1000,
          "burnRate": 0.39,
          "flashPoint": 800,
          "specHeat": 0.2,
          "selfIgnitionCoef": false,
          "smokePoint": 650,
          "baseTemp": "thermals",
          "conductionRadius": 0.12
        }
      ],
      ["e4l", -0.37, -1.39, 0.68],
      ["tc1", 0, -1.65, 0.42],
      { "engineGroup": "" },
      { "group": "" },
      //engine mount node
      ["em1r", -0.42, -1.5, 0.62, { "nodeWeight": 3 }]
    ],
    "beams": [
      ["id1:", "id2:"],
      { "beamType": "|NORMAL", "beamLongBound": 1.0, "beamShortBound": 1.0 },
      { "beamSpring": 15001000, "beamDamp": 400 },
      { "beamDeform": 175000, "beamStrength": "FLT_MAX" },
      //engine
      { "deformGroup": "mainEngine", "deformationTriggerRatio": 0.001 },
      ["e1r", "e1l"],
      ["e2r", "e2l"],
      ["e3r", "e3l"],
      ["e4r", "e4l"],

      ["e1r", "e2r"],
      ["e1l", "e2l"],
      ["e3r", "e4r"],
      ["e3l", "e4l"],

      ["e1r", "e3r"],
      ["e1l", "e3l"],
      ["e2r", "e4r"],
      ["e2l", "e4l"],

      ["e2r", "e3r"],
      ["e2l", "e3l"],
      ["e2r", "e3l"],
      ["e2l", "e3r"],

      ["e1r", "e4r"],
      ["e1l", "e4l"],
      ["e1r", "e4l"],
      ["e1l", "e4r"],

      ["e1r", "e2l"],
      ["e1l", "e2r"],
      ["e3r", "e4l"],
      ["e3l", "e4r"],

      ["e1r", "e3l"],
      ["e1l", "e3r"],
      ["e2r", "e4l"],
      ["e2l", "e4r"],

      ["tc1", "e3l"],
      ["tc1", "e3r"],
      ["tc1", "e4l"],
      ["tc1", "e4r"],
      ["tc1", "e1l"],
      ["tc1", "e1r"],
      ["tc1", "e2l"],
      ["tc1", "e2r"],
      { "deformGroup": "" },
      { "breakGroup": "" },

      //engine mount node
      { "beamSpring": 4400000, "beamDamp": 150 },
      { "beamDeform": 90000, "beamStrength": "FLT_MAX" },
      ["em1r", "e3l"],
      ["em1r", "e3r"],
      ["em1r", "e4l"],
      ["em1r", "e4r"],
      ["em1r", "e1r"],
      ["em1r", "e1l"],
      ["em1r", "e2l"],
      ["em1r", "e2r"],
      { "beamPrecompression": 1, "beamType": "|NORMAL", "beamLongBound": 1.0, "beamShortBound": 1.0 }
    ]
  },
  "vivace_turbo_i4_1.5_diesel_venom": {
    "information": {
      "authors": "Venom",
      "name": "Venom Turbocharger",
      "value": 3000
    },
    "slotType": "vivace_turbo_i4_1.5_diesel",
    "variables": [
      ["name", "type", "unit", "category", "default", "min", "max", "title", "description"],
      ["$wastegateStart", "range", "psi", "Engine", 15, 1, 90, "Wastegate Pressure", "Pressure at which the wastegate begins to open", { "stepDis": 0.5 }]
    ],
    "turbocharger": {
      "bovEnabled": false,
      "hissLoopEvent": "event:>Vehicle>Forced_Induction>Turbo_01>turbo_hiss",
      "whineLoopEvent": "event:>Vehicle>Forced_Induction>Turbo_01>turbo_spin",
      "turboSizeCoef": 0.0,
      "bovSoundVolumeCoef": 0.0,
      "hissVolumePerPSI": 0.025,
      "whineVolumePer10kRPM": 0.016,
      "whinePitchPer10kRPM": 0.075,
      "wastegateStart": "$wastegateStart",
      "wastegateLimit": "$=$wastegateStart+10",
      "maxExhaustPower": 175000,
      "backPressureCoef": 0.000005,
      "pressureRatePSI": 135,
      "frictionCoef": 7,
      "inertia": 1,
      "damageThresholdTemperature": 7797,
      "pressurePSI": [
        //turbineRPM, pressure(PSI)
        [0, 0],
        [24000, 5],
        [60000, 15],
        [90000, 25],
        [150000, 40],
        [200000, 60],
        [250000, 85],
        [300000, 120]
      ],
      "engineDef": [
        //engineRPM, efficiency, exhaustFactor
        [0, 0.0, 0.1],
        [650, 0.38, 0.3],
        [1000, 0.95, 0.5],
        [1400, 1.0, 0.7],
        [2000, 1.0, 0.8],
        [2500, 1.0, 0.9],
        [3000, 1.0, 1.0],
        [3500, 1.0, 1.0],
        [4000, 1.0, 1.0],
        [4500, 1.0, 1.0],
        [5000, 1.0, 1.0],
        [6000, 1.0, 1.0],
        [7000, 1.0, 1.0],
        [8000, 1.0, 1.0]
      ]
    },
    "soundConfig": {
      //"$+maxLoadMix": 0.2,
      //"$+intakeMuffling":0.2,
      //"$+mainGain":2,
      //"$+eqHighGain": 5,
    },
    "mainEngine": {
      //turbocharger name
      "turbocharger": "turbocharger",
      //"instantAfterFireCoef": 1,
      //"sustainedAfterFireCoef": 0.75,
      "$*instantAfterFireCoef": 2,
      "$*sustainedAfterFireCoef": 2,

      //damage deformGroups
      "deformGroups_turbo": ["mainEngine_turbo", "mainEngine_intercooler"]
    },
    "flexbodies": [
      ["mesh", "[group]:", "nonFlexMaterials"],
      ["vivace_enginecover_i4", ["vivace_engine"]],
      ["vivace_turbo_i4", ["vivace_engine", "vivace_downpipe"]],
      ["vivace_intercooler", ["vivace_body"]],
      { "deformGroup": "airbox", "deformMaterialBase": "vivace_mechanical", "deformMaterialDamaged": "invis" },
      ["vivace_airbox", ["vivace_engine", "vivace_body"]],
      { "deformGroup": "" }
    ],
    "nodes": [
      ["id", "posX", "posY", "posZ"],
      //--turbo and intercooler weight--
      { "selfCollision": false },
      { "collision": false },
      { "nodeMaterial": "|NM_METAL" },
      { "frictionCoef": 0.5 },
      { "group": "" },
      { "nodeWeight": 9 },
      ["turbo", -0.15, -1.35, 0.55],
      { "collision": true },
      { "group": "vivace_intercooler" },
      { "nodeWeight": 5 },
      ["interc", 0.0, -1.95, 0.35],
      { "nodeWeight": 1 },
      { "group": "vivace_intercooler" },
      ["airb1", 0.4, -1.55, 0.74],
      ["airb2", 0.1, -1.75, 0.65],
      { "group": "vivace_downpipe" },
      { "nodeWeight": 2.0 },
      ["exd1", 0.08, -1.17, 0.43],
      { "group": "" }
    ],
    "beams": [
      ["id1:", "id2:"],
      { "beamType": "|NORMAL", "beamLongBound": 1.0, "beamShortBound": 1.0 },

      //turbo
      { "beamSpring": 2750550, "beamDamp": 125 },
      { "beamDeform": 30000, "beamStrength": "FLT_MAX" },
      { "deformLimitExpansion": "" },
      { "deformGroup": "mainEngine_turbo", "deformationTriggerRatio": 0.001 },
      ["turbo", "e1r"],
      ["turbo", "e1l"],
      ["turbo", "e2r"],
      ["turbo", "e2l"],
      ["turbo", "e3r"],
      ["turbo", "e3l"],
      ["turbo", "e4r"],
      ["turbo", "e4l"],

      //downpipe
      { "beamSpring": 2750550, "beamDamp": 125 },
      { "beamDeform": 30000, "beamStrength": "FLT_MAX" },
      ["exd1", "e1r"],
      ["exd1", "e1l"],
      ["exd1", "e2r"],
      ["exd1", "e2l"],
      ["exd1", "e3r"],
      ["exd1", "e3l", { "isExhaust": "mainEngine" }],
      ["exd1", "e4r"],
      ["exd1", "e4l"],

      //intercooler
      { "beamSpring": 2001000, "beamDamp": 100 },
      { "beamDeform": 3000, "beamStrength": "FLT_MAX" },
      { "deformGroup": "mainEngine_intercooler", "deformationTriggerRatio": 0.02 },
      ["interc", "f13rr"],
      ["interc", "f13ll"],
      ["interc", "f11rr"],
      ["interc", "f11ll"],
      { "beamDeform": 7000, "beamStrength": "FLT_MAX" },
      //["interc","f18"],
      ["interc", "f18r"],
      ["interc", "f18l"],

      //piping
      { "beamSpring": 501000, "beamDamp": 60 },
      { "beamDeform": 4000, "beamStrength": 12000 },
      { "deformGroup": "mainEngine_piping", "deformationTriggerRatio": 0.001 },
      ["airb1", "e1r"],
      ["airb1", "e1l"],
      ["airb1", "e3r"],
      ["airb1", "e3l"],
      ["airb1", "e4r"],
      ["airb1", "e4l"],

      ["airb2", "e2r"],
      ["airb2", "e2l"],
      ["airb2", "e4r"],
      ["airb2", "e4l"],
      ["airb2", "e3r"],
      ["airb2", "e3l"],
      { "breakGroup": "" },

      //airbox pipe
      { "beamSpring": 10550, "beamDamp": 125 },
      { "beamDeform": 5000, "beamStrength": 3000 },
      { "breakGroup": "airbox" },
      { "deformGroup": "airbox", "deformationTriggerRatio": 0.01 },
      ["e3r", "f12ll"],
      ["e3l", "f12ll"],
      ["e1l", "f12ll"],
      ["e1r", "f12ll"],
      ["e3l", "fs1l"],
      ["e3r", "fs1l"],
      ["e3l", "f14l"],
      ["e3r", "f14l"],
      { "breakGroup": "" },

      { "beamType": "|BOUNDED", "beamLongBound": 0.025, "beamShortBound": 0.025 },
      { "beamSpring": 20000, "beamDamp": 50 },
      { "beamLimitSpring": 1001000, "beamLimitDamp": 200 },
      { "beamDeform": 4000, "beamStrength": 50000 },
      { "deformGroup": "mainEngine_piping", "deformationTriggerRatio": 0.001 },
      ["airb1", "f12ll"],
      ["airb1", "f5l"],
      ["airb1", "f5"],
      ["airb1", "f12ll"],
      ["airb1", "f14ll"],
      { "deformGroup": "" },
      { "deformLimitExpansion": 1.2 },
      { "beamPrecompression": 1, "beamType": "|NORMAL", "beamLongBound": 1.0, "beamShortBound": 1.0 }
    ]
  },
  "vivace_engine_ecu_i4_1.5_diesel_venom": {
    "information": {
      "authors": "Venom",
      "name": "Venom Diesel ECU",
      "value": 2300
    },
    "slotType": "vivace_engine_ecu_i4_1.5_diesel",
    "slots": [
      ["type", "default", "description"],
      ["vivace_engine_ecu_i4_1.5_diesel_speedlimit", "vivace_engine_ecu_i4_1.5_diesel_speedlimit_off", "Speed Limiter", { "coreSlot": true }]
    ],
    "variables": [
      ["name", "type", "unit", "category", "default", "min", "max", "title", "description"],
      ["$idleRPMConfig", "range", "rpm", "Engine", 700, 100, 3000, "RPM Idle", "Engine idle RPM", { "stepDis": 50 }],
      ["$revLimiterRPM", "range", "rpm", "Engine", 6000, 1500, 7000, "RPM Limit", "RPM where the rev limiter prevents further revving", { "stepDis": 50 }],
      ["$revLimiterCutTime", "range", "s", "Engine", 0.15, 0.01, 0.5, "RPM Limit Cut Time", "How fast the rev limiter cycles", { "stepDis": 0.01 }],
      ["$venomFineTuneLowRPM", "range", "nm", "Engine", 0, -300, 2000, "KuyPeD tuning low RPM", "KuyPeD fine ecu torque low RPM (500-4000) gain tuning", { "stepDis": 1 }],
      ["$venomFineTuneHighRPM", "range", "nm", "Engine", 0, -300, 2500, "KuyPeD tuning high RPM", "KuyPeD fine ecu torque high RPM (4000-8000) gain tuning", { "stepDis": 1 }]
    ],
    "controller": [["fileName"], ["twoStepLaunch", { "rpmLimit": 2500 }]],
    "mainEngine": {
      "torqueModIntake": [
        ["rpm", "torque"],
        [500, "$venomFineTuneLowRPM"],
        [1000, "$venomFineTuneLowRPM"],
        [2000, "$venomFineTuneLowRPM"],
        [2500, "$venomFineTuneLowRPM"],
        [3000, "$venomFineTuneLowRPM"],
        [4000, "$venomFineTuneHighRPM"],
        [5000, "$venomFineTuneHighRPM"],
        [6000, "$venomFineTuneHighRPM"],
        [7000, "$venomFineTuneHighRPM"],
        [8000, "$venomFineTuneHighRPM"]
      ],
      "idleRPM": "$idleRPMConfig",
      "revLimiterRPM": "$revLimiterRPM",
      "revLimiterType": "timeBased",
      "revLimiterCutTime": "$revLimiterCutTime"
    }
  },
  "vivace_engine_internals_i4_1.5_diesel_venom": {
    "information": {
      "authors": "Venom",
      "name": "Venom Billet Block",
      "value": 7900
    },
    "slotType": "vivace_engine_internals_i4_1.5_diesel",
    "mainEngine": {
      "torqueModMult": [
        ["rpm", "torque"],
        [0, 1],
        [1000, 1.1],
        [2000, 1.2],
        [3000, 1.3],
        [4000, 1.4],
        [5000, 1.5],
        [6000, 1.5],
        [7000, 1.5],
        [8000, 1.5]
      ],
      //max rpm physically capable of
      "$+maxRPM": 1000,
      "$*friction": 0.75,
      "$*dynamicFriction": 0.975,
      "$*inertia": 1.2,
      "$*engineBrakeTorque": 1.75,
      //engine durability
      "$+cylinderWallTemperatureDamageThreshold": 9899,
      "$+headGasketDamageThreshold": 98997797,
      "$+pistonRingDamageThreshold": 98997797,
      "$+connectingRodDamageThreshold": 98997797,
      "$*maxTorqueRating": 7.75,
      "$*maxOverTorqueDamage": 8.75
    }
  }
}
