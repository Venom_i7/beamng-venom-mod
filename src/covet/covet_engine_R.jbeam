 {
"covet_engine_1.5_R": {
    "information":{
        "authors":"BeamNG",
        "name":"1.5L DOHC I4 Engine",
        "value":24000,
    },
    "slotType" : "covet_engine_midengine",
    "slots": [
        ["type", "default", "description"],
        ["covet_enginemounts_midengine","covet_enginemounts_midengine", "Engine Mounts"],
        ["covet_intake_midengine","covet_turbo_midengine_stage1", "Intake", {"coreSlot":true}],
        ["covet_engine_ecu","covet_engine_ecu", "Engine Management", {"coreSlot":true}],
        ["covet_oilpan_midengine","covet_oilpan_midengine", "Oil Pan", {"coreSlot":true}],
        ["n2o_system_midengine","", "Nitrous Oxide System"],
        ["covet_engine_internals","covet_engine_internals_heavy", "Engine Long Block", {"coreSlot":true}],
        ["covet_exhaust_midengine","covet_exhaust_midengine", "Exhaust"],
        ["covet_transaxle_R","covet_transaxle_R_5M", "Transaxle"],
    ],
    "powertrain": [
        ["type", "name", "inputName", "inputIndex"],
        ["combustionEngine", "mainEngine", "dummy", 0],
    ],
    "mainEngine": {
        "torque":[
            ["rpm", "torque"],
            [0, 0],
            [500,  45],
            [1000,  78],
            [1500,  105],
            [2000,  120],
            [3000,  140],
            [4000,  154],
            [5000,  161],
            [5500,  161],
            [6000,  160],
            [6500,  158],
            [7000,  150],
            [7500,  138],
            [8000,  122],
            [9000,   75],
            [10000,  40],
            [11000,  10],
        ],

        "idleRPM":800,
        "maxRPM":7200,
        "revLimiterType":"timeBased",
        "revLimiterCutTime":0.13,
        "inertia":0.056,
        "friction":12,
        "dynamicFriction":0.024,
        "engineBrakeTorque":30,
        //"burnEfficiency":0.31
        "burnEfficiency":[
            [0, 0.12],
            [0.05, 0.32],
            [0.4, 0.44],
            [0.7, 0.51],
            [1, 0.37],
        ],
        //fuel system
        "energyStorage": ["fueltank_R", "fueltank_L"],
        "requiredEnergyType":"gasoline",

        //exhaust
        "instantAfterFireSound": "event:>Vehicle>Afterfire>i4_01>single",
        "sustainedAfterFireSound": "event:>Vehicle>Afterfire>i4_01>multi",
        "shiftAfterFireSound": "event:>Vehicle>Afterfire>i4_01>shift",
        "particulates":0.03,
        "instantAfterFireCoef": 0.75,
        "sustainedAfterFireCoef": 0.5,
        "instantAfterFireVolumeCoef": 0.8,
        "sustainedAfterFireVolumeCoef": 0.5,
        "shiftAfterFireVolumeCoef": 0.7,

        //cooling and oil system
        "thermalsEnabled":true,
        "engineBlockMaterial":"aluminum",
        "oilVolume":4,

        //engine durability
        "cylinderWallTemperatureDamageThreshold":130,
        "headGasketDamageThreshold":1500000,
        "pistonRingDamageThreshold":1500000,
        "connectingRodDamageThreshold":2000000,
        "maxTorqueRating": 220,
        "maxOverTorqueDamage": 300,

        //node beam interface
        "torqueReactionNodes:":["e1l","e2l","e4r"],
        "waterDamage": {"[engineGroup]:":["engine_intake"]},
        "radiator": {"[engineGroup]:":["radiator"]},
        "engineBlock": {"[engineGroup]:":["engine_block"]},
        "breakTriggerBeam":"engine",
        "uiName":"Engine",
        "soundConfig": "soundConfig",
        "soundConfigExhaust": "soundConfigExhaust",

        //starter motor
        "starterSample":"event:>Engine>Starter>i4_2001_eng",
        "starterSampleExhaust":"event:>Engine>Starter>i4_2001_exh",
        "shutOffSampleEngine":"event:>Engine>Shutoff>i4_2001_eng",
        "shutOffSampleExhaust":"event:>Engine>Shutoff>i4_2001_exh",
        "starterVolume":0.76,
        "starterVolumeExhaust":0.76,
        "shutOffVolumeEngine":0.76,
        "shutOffVolumeExhaust":0.76,
        "starterThrottleKillTime":0.5,
        "idleRPMStartRate":1,
        "idleRPMStartCoef":1,

        //damage deformGroups
        "deformGroups":["mainEngine", "mainEngine_piping", "mainEngine_accessories"]
        "deformGroups_oilPan":["oilpan_damage"]
    },
    "soundConfig": {
        "sampleName": "I4_2_engine_cabin",
        "intakeMuffling": 0.7,

        "mainGain": -7,
        "onLoadGain":1,
        "offLoadGain":0.45,

        "maxLoadMix": 0.65,
        "minLoadMix": 0,

        "lowShelfGain":-5,
        "lowShelfFreq":80,

        "highShelfGain":-4,
        "highShelfFreq":2500,

        "eqLowGain": -6,
        "eqLowFreq": 500,
        "eqLowWidth": 0.1,

        "eqHighGain": 0,
        "eqHighFreq": 2500,
        "eqHighWidth": 0.2,

        "fundamentalFrequencyCylinderCount":4,
        "eqFundamentalGain": -4,
    },
    "soundConfigExhaust": {
        "sampleName": "I4_2_exhaust",

        "mainGain": 1,
        "onLoadGain":1,
        "offLoadGain":0.10,

        "maxLoadMix": 0.6,
        "minLoadMix": 0,

        "lowShelfGain":-8,
        "lowShelfFreq":70,

        "highShelfGain":0,
        "highShelfFreq":2000,

        "eqLowGain": -1,
        "eqLowFreq": 90,
        "eqLowWidth": 0.5,

        "eqHighGain": 0,
        "eqHighFreq": 4000,
        "eqHighWidth": 0.1,

        "fundamentalFrequencyCylinderCount":4,
        "eqFundamentalGain": -7,
    },
    "vehicleController": {
        "clutchLaunchStartRPM":1700,
        //overwritten by stage 3 turbo
        "clutchLaunchTargetRPM":3900,
        //**highShiftDown can be overwritten by automatic transmissions**
        "highShiftDownRPM":[0,0,0,2700,3350,3600,3800,3800],
        //**highShiftUp can be overwritten by intake modifications**
        "highShiftUpRPM":6800,
    },
    "flexbodies": [
         ["mesh", "[group]:", "nonFlexMaterials"],
         ["covet_engine_midengine", ["covet_engine_mid"]],
         ["covet_head_dohc_midengine", ["covet_engine_mid"]],
         {"deformGroup":"radhose_damage_R", "deformMaterialBase":"covet_mechanical", "deformMaterialDamaged":"invis"},
         ["covet_radhose_midengine_R", ["covet_engine_mid","covet_body"]],
         {"deformGroup":""},
    ],
    "nodes": [
         ["id", "posX", "posY", "posZ"],
         {"selfCollision":false},
         {"collision":true},
         //engine box
         {"frictionCoef":0.5},
         {"nodeMaterial":"|NM_METAL"},
         {"group":"covet_engine_mid"},
         {"engineGroup":"engine_block"},
         {"nodeWeight":13},
         {"chemEnergy":500,"burnRate":0.39,"flashPoint":800,"specHeat": 0.2,"selfIgnitionCoef":false,"smokePoint":650,"baseTemp":"thermals","conductionRadius":0.12},
         ["e1r",-0.12,0.556,0.24,{"chemEnergy":2000,"burnRate":0.39,"flashPoint":800,"specHeat": 0.1,"selfIgnitionCoef":false,"smokePoint":650,"baseTemp":"thermals","conductionRadius":0.07}],
         ["e1l",0.12,0.556,0.24,{"chemEnergy":2000,"burnRate":0.39,"flashPoint":800,"specHeat": 0.1,"selfIgnitionCoef":false,"smokePoint":650,"baseTemp":"thermals","conductionRadius":0.07}],
         ["e2r",-0.12,1.033,0.24,{"chemEnergy":2000,"burnRate":0.39,"flashPoint":800,"specHeat": 0.1,"selfIgnitionCoef":false,"smokePoint":650,"baseTemp":"thermals","conductionRadius":0.07}],
         ["e2l",0.12,1.033,0.24,{"chemEnergy":2000,"burnRate":0.39,"flashPoint":800,"specHeat": 0.1,"selfIgnitionCoef":false,"smokePoint":650,"baseTemp":"thermals","conductionRadius":0.07}],
         {"engineGroup":["engine_block","engine_intake"]},
         {"selfCollision":true},
         ["e3r",-0.18,0.556,0.709, {"isExhaust":"mainEngine"}],
         ["e3l",0.18,0.556,0.709],
         ["e4r",-0.18,1.033,0.709],
         ["e4l",0.18,1.033,0.709],
         {"chemEnergy":false,"burnRate":false,"flashPoint":false, "specHeat": false,"selfIgnitionCoef":false,"smokePoint":false,"baseTemp":false,"conductionRadius":false},
         {"engineGroup":""},
         {"group":""},

         //engine mount nodes
         ["em1r",-0.167,0.761,0.364, {"nodeWeight":3}],
         ["em1l",0.167,0.761,0.364, {"nodeWeight":3}],
    ],
    "beams": [
          ["id1:", "id2:"],
          {"beamType":"|NORMAL", "beamLongBound":1.0, "beamShortBound":1.0},
          {"beamSpring":11200800,"beamDamp":258},
          {"beamDeform":1120000,"beamStrength":"FLT_MAX"},
          //engine
          {"deformLimitExpansion":1.2},
          {"deformGroup":"mainEngine", "deformationTriggerRatio":0.001}
          ["e1r","e1l"],
          ["e2r","e2l"],
          ["e3r","e3l"],
          ["e4r","e4l"],
          ["e1r","e2r"],
          ["e1l","e2l"],
          ["e3r","e4r"],
          ["e3l","e4l"],
          ["e1r","e3r"],
          ["e1l","e3l"],
          ["e2r","e4r"],
          ["e2l","e4l"],
          ["e2r","e3r"],
          ["e2l","e3l"],
          ["e2r","e3l"],
          ["e2l","e3r"],
          ["e1r","e4r"],
          ["e1l","e4l"],
          ["e1r","e4l"],
          ["e1l","e4r"],
          ["e1r","e2l"],
          ["e1l","e2r"],
          ["e3r","e4l"],
          ["e3l","e4r"],
          ["e1r","e3l"],
          ["e1l","e3r"],
          ["e2r","e4l"],
          ["e2l","e4r"],
          {"deformGroup":""}

          //engine mount nodes
          {"beamSpring":2956300,"beamDamp":130.43},
          {"beamDeform":63000,"beamStrength":"FLT_MAX"},
          ["em1r","e3l"],
          ["em1r","e3r"],
          ["em1r","e4l"],
          ["em1r","e4r"],
          ["em1r", "e1r"],
          ["em1r", "e1l"],
          ["em1r", "e2l"],
          ["em1r", "e2r"],

          ["em1l","e3l"],
          ["em1l","e3r"],
          ["em1l","e4l"],
          ["em1l","e4r"],
          ["em1l", "e1r"],
          ["em1l", "e1l"],
          ["em1l", "e2l"],
          ["em1l", "e2r"],

          //radhose
          {"beamSpring":15000,"beamDamp":300},
          {"beamDeform":1450,"beamStrength":3600},
          {"deformGroup":"radhose_damage_R","deformationTriggerRatio":0.1},
          {"optional":true}
          ["em1l","e3l"],
          ["em1l","e3r"],
          ["em1l","e4l"],
          ["em1l","e4r"],
          ["em1l", "e1r"],
          ["em1l", "e1l"],
          ["em1l", "e2l"],
          ["em1l", "e2r"],

          ["e1r", "f3r"],
          ["e3r", "f3r"],
          ["e1r", "f3l"],
          ["e1l", "f3l"],
          ["e1l", "f3r"],
          ["e1l", "f9l"],
          ["e1r", "f9r"],
          ["e3l", "f3l"],
          {"optional":false}
          {"deformGroup":""},

          {"beamPrecompression":1, "beamType":"|NORMAL", "beamLongBound":1.0, "beamShortBound":1.0},
    ],
    "triangles": [
            ["id1:","id2:","id3:"],
            {"groundModel":"plastic"},
            {"group":"covet_engine_mid"},
            {"dragCoef":0.5},
            ["e2r", "e1r", "e4r"],
            ["e1r", "e3r", "e4r"],

            ["e2l", "e4l", "e1l"],
            ["e1l", "e4l", "e3l"],

            ["e4l", "e2l", "e2r"],
            ["e2r", "e4r", "e4l"],
            ["e3l", "e1r", "e1l"],
            ["e1r", "e3l", "e3r"],

            ["e4r", "e3r", "e3l"],
            ["e3l", "e4l", "e4r"],
            {"optional":true},
            ["e2l", "oilpan", "e2r"],
            ["e1r", "oilpan", "e1l"],
            ["e1l", "oilpan", "e2l"],
            ["e2r", "oilpan", "e1r"],
            {"group":""},
    ],
},
"covet_oilpan_midengine": {
    "information":{
        "authors":"BeamNG",
        "name":"Stock Oil Pan",
        "value":90,
    },
    "slotType" : "covet_oilpan_midengine",
    "mainEngine": {
        //engine durability
        "oilpanMaximumSafeG": 1.3

        //node beam interface
        "oilpanNodes:":["oilpan","oilref"],

        //engine deform groups
        "deformGroups_oilPan":["oilpan_damage"]
    },
    "nodes": [
         ["id", "posX", "posY", "posZ"],
         {"selfCollision":false},
         {"collision":true},
         {"frictionCoef":0.5},
         {"nodeMaterial":"|NM_METAL"},

         //oil pan node
         {"group":""},
         {"nodeWeight":2},
         ["oilpan", 0.0, 0.8, 0.19],
         ["oilref", 0.0, 0.8, 0.59, {"nodeWeight":1, "collision":false}],
    ],
    "beams": [
          ["id1:", "id2:"],
          {"beamPrecompression":1, "beamType":"|NORMAL", "beamLongBound":1.0, "beamShortBound":1.0},

          //oilpan node
          {"beamSpring":1501000,"beamDamp":250},
          {"beamDeform":8000,"beamStrength":"FLT_MAX"},
          {"deformGroup":"oilpan_damage","deformationTriggerRatio":0.005},
          ["oilpan", "e1r"],
          ["oilpan", "e1l"],
          ["oilpan", "e2r"],
          ["oilpan", "e2l"],
          ["oilpan", "e3r"],
          ["oilpan", "e3l"],
          ["oilpan", "e4r"],
          ["oilpan", "e4l"],
          {"deformGroup":""},

          //oil ref
          {"beamSpring":1001000,"beamDamp":150},
          {"beamDeform":25000,"beamStrength":"FLT_MAX"},
          ["oilref", "e1r"],
          ["oilref", "e1l"],
          ["oilref", "e2r"],
          ["oilref", "e2l"],
          ["oilref", "e3r"],
          ["oilref", "e3l"],
          ["oilref", "e4r"],
          ["oilref", "e4l"],
          {"beamPrecompression":1, "beamType":"|NORMAL", "beamLongBound":1.0, "beamShortBound":1.0},
    ],
},
"covet_oilpan_midengine_race": {
    "information":{
        "authors":"BeamNG",
        "name":"Race Oil Pan",
        "value":250,
    },
    "slotType" : "covet_oilpan_midengine",
    "mainEngine": {

        //engine durability
        "oilpanMaximumSafeG": 1.9

        //node beam interface
        "oilpanNodes:":["oilpan","oilref"],

        //engine deform groups
        "deformGroups_oilPan":["oilpan_damage"]
    },
    "nodes": [
         ["id", "posX", "posY", "posZ"],
         {"selfCollision":false},
         {"collision":true},
         {"frictionCoef":0.5},
         {"nodeMaterial":"|NM_METAL"},

         //oil pan node
         {"group":""},
         {"nodeWeight":2},
         ["oilpan", 0.0, 0.8, 0.19],
         ["oilref", 0.0, 0.8, 0.59, {"nodeWeight":1, "collision":false}],
    ],
    "beams": [
          ["id1:", "id2:"],
          {"beamPrecompression":1, "beamType":"|NORMAL", "beamLongBound":1.0, "beamShortBound":1.0},

          //oilpan node
          {"beamSpring":1501000,"beamDamp":250},
          {"beamDeform":8000,"beamStrength":"FLT_MAX"},
          {"deformGroup":"oilpan_damage","deformationTriggerRatio":0.005},
          ["oilpan", "e1r"],
          ["oilpan", "e1l"],
          ["oilpan", "e2r"],
          ["oilpan", "e2l"],
          ["oilpan", "e3r"],
          ["oilpan", "e3l"],
          ["oilpan", "e4r"],
          ["oilpan", "e4l"],
          {"deformGroup":""},

          //oil ref
          {"beamSpring":1001000,"beamDamp":150},
          {"beamDeform":25000,"beamStrength":"FLT_MAX"},
          ["oilref", "e1r"],
          ["oilref", "e1l"],
          ["oilref", "e2r"],
          ["oilref", "e2l"],
          ["oilref", "e3r"],
          ["oilref", "e3l"],
          ["oilref", "e4r"],
          ["oilref", "e4l"],
          {"beamPrecompression":1, "beamType":"|NORMAL", "beamLongBound":1.0, "beamShortBound":1.0},
    ],
},
"covet_turbo_midengine_stage1": {
    "information":{
        "authors":"BeamNG",
        "name":"Stock Turbocharger",
        "value":1950,
    },
    "slotType" : "covet_intake_midengine",
    "turbocharger": {
        "bovSoundFileName":"event:>Vehicle>Forced_Induction>Turbo_02_cabin>turbo_bov_tuned",
        "hissLoopEvent":"event:>Vehicle>Forced_Induction>Turbo_07_cabin>turbo_hiss_tuned",
        "whineLoopEvent":"event:>Vehicle>Forced_Induction>Turbo_05_cabin>turbo_spin_tuned",
        "turboSizeCoef": 0.65,
        "bovSoundVolumeCoef": 0.45,
        "hissVolumePerPSI": 0.030,
        "whineVolumePer10kRPM": 0.0070,
        "whinePitchPer10kRPM": 0.03,
        "wastegateStart":12.5,
        "wastegateLimit":13.5,
        "maxExhaustPower": 13000,
        "backPressureCoef": 0.000035,
        "frictionCoef": 10.4,
        "inertia":1.4,
        "damageThresholdTemperature": 610,
        "pressurePSI":[
            //turbineRPM, pressure(PSI)
            [0,         -3.5],
            [30000,     0],
            [60000,     7],
            [90000,     10],
            [150000,    14],
            [200000,    18],
            [250000,    20],
        ],
        "engineDef":[
            //engineRPM, efficiency, exhaustFactor
            [0,     0.0,    0.0],
            [650,   0.10,   0.08],
            [1400,  0.12,   0.10],
            [2000,  0.20,    0.20],
            [2500,  0.40,    0.30],
            [3000,  0.64,    0.55],
            [3500,  0.84,    0.65],
            [4000,  0.88,    0.90],
            [5000,  0.94,    0.95],
            [6000,  0.92,    0.94],
            [7000,  0.90,    0.90],
            [8000,  0.83,    1.0],
            [9000,  0.75,    1.0],
            [10000, 0.65,    1.0],
            [11000, 0.55,    1.0],
        ],
    },
    "mainEngine": {
        //turbocharger name
        "turbocharger":"turbocharger",
        "$*instantAfterFireCoef": 1.8,
        "$*sustainedAfterFireCoef": 1.25,

        //damage deformGroups
        "deformGroups_turbo":["mainEngine_turbo_R","mainEngine_intercooler_R"]
    },
    "soundConfig": {
        "$+maxLoadMix": 0.1,
        "$+intakeMuffling":-0.2,
        "$+mainGain":1.5,
        "$+eqLowGain": 1.5,
    },
    "soundConfigExhaust": {
        "$+maxLoadMix": 0.1,
        "$+minLoadMix": 0.01,
        "$+mainGain": 0.5,
        "$+offLoadGain": 0.00,
        "$+eqLowGain": 0.25,
        "$+eqFundamentalGain": 0.5,
    },
    "flexbodies": [
        ["mesh", "[group]:", "nonFlexMaterials"],
        ["covet_intercooler_midengine", ["covet_intercooler_midengine"]],
        ["covet_podfilter_midengine", ["covet_intercooler_midengine"]],
        ["covet_header_turbo_midengine", ["covet_engine_mid","covet_turbo"]],
        ["covet_intake_midengine", ["covet_engine_mid","covet_engine_intake"]],
        {"deformGroup":"mainEngine_chargepipe_R", "deformMaterialBase":"covet_mechanical", "deformMaterialDamaged":"invis"},
        ["covet_icpipe_a_midengine", ["covet_intercooler_midengine","covet_engine_intake"]],
        ["covet_icpipe_b_midengine", ["covet_intercooler_midengine","covet_turbo"]],
        ["covet_intakepipe_midengine", ["covet_intercooler_midengine","covet_engine_intake"]],
        {"deformGroup":""},
    ],
    "nodes": [
         ["id", "posX", "posY", "posZ"],
         {"selfCollision":false},
         {"collision":false},
         //intake
         {"frictionCoef":0.5},
         {"nodeMaterial":"|NM_METAL"},
         {"nodeWeight":3.5},
         {"group":["covet_engine_mid","covet_engine_intake"]},
         ["intck1l",0.25,0.556,0.701],
         ["intck2l",0.25,1.033,0.701],
         {"engineGroup":""},

         //turbos
         {"nodeWeight":2.0},
         ["trb1r",0.18,1.20,0.505, {"group":"covet_turbo"}],
         ["trb2r",0.18,1.35,0.505, {"group":"covet_turbo", "afterFireAudioCoef":1.0, "afterFireVisualCoef":1.0, "afterFireVolumeCoef":1.0, "afterFireMufflingCoef":1.0, "exhaustAudioMufflingCoef":1.0, "exhaustAudioGainChange":0}],

         //intercoolers
         {"nodeWeight":3.0},
         {"selfCollision":true},
         {"collision":true},
         ["ic1l",0.809,0.762,0.461, {"group":"covet_intercooler_midengine"}],
         {"group":""},
    ],
    "beams": [
          ["id1:", "id2:"],
          {"beamPrecompression":1, "beamType":"|NORMAL", "beamLongBound":1.0, "beamShortBound":1.0},
          //intake
          {"deformLimitExpansion":1.2},
          {"beamSpring":3500940,"beamDamp":235},
          {"beamDeform":105000,"beamStrength":"FLT_MAX"},

          ["intck2l", "intck1l"],
          ["intck2l", "e4r"],
          ["e4r", "intck1l"],
          ["intck1l", "e3r"],
          ["intck2l", "e3r"],
          ["intck2l", "e4l"],
          ["e4l", "intck1l"],
          ["intck1l", "e3l"],
          ["e3l", "intck2l"],
          ["intck2l", "e2l"],
          ["e2l", "intck1l"],
          ["intck1l", "e1l"],
          ["e1l", "intck2l"],
          ["intck2l", "e2r"],
          ["e2r", "intck1l"],
          ["intck1l", "e1r"],
          ["e1r", "intck2l"],

          //turbo
          {"beamSpring":3000000,"beamDamp":130},
          {"beamDeform":90000,"beamStrength":"FLT_MAX"},
          {"deformGroup":"mainEngine_turbo_R", "deformationTriggerRatio":0.001}
          ["trb1r", "e3l"],
          ["trb1r", "e4l"],
          ["trb1r", "e3r", {"isExhaust":"mainEngine"}],
          ["trb1r", "e4r"],
          ["trb1r", "e1l"],
          ["trb1r", "e2l"],
          ["trb1r", "e1r"],
          ["trb1r", "e2r"],
          ["trb2r", "e3l"],
          ["trb2r", "e4l"],
          ["trb2r", "e3r"],
          ["trb2r", "e4r"],
          ["trb2r", "e1l"],
          ["trb2r", "e2l"],
          ["trb2r", "e1r"],
          ["trb2r", "e2r"],
          ["trb2r", "trb1r", {"isExhaust":"mainEngine"}]
          {"deformGroup":""}

          //intercooler
          {"beamSpring":2000000,"beamDamp":130},
          {"beamDeform":15000,"beamStrength":"FLT_MAX"},
          {"deformGroup":"mainEngine_intercooler_R", "deformationTriggerRatio":0.02}
          ["ic1l", "f3ll"],
          ["ic1l", "f4ll"],
          ["ic1l", "q5l"],
          ["ic1l", "q6l"],
          ["ic1l", "q1l"],
          ["ic1l", "q2ll"],

          ["ic1l", "f8l"],
          ["ic1l", "rx3l"],

          //charge pipes
          {"beamSpring":50000,"beamDamp":130},
          {"beamDeform":10000,"beamStrength":20000},
          {"deformGroup":"mainEngine_chargepipe_R", "deformationTriggerRatio":0.02}
          ["ic1l", "trb1r"],
          ["ic1l", "intck1l"],
          ["ic1l", "intck2l"],
          {"deformGroup":""}
          {"beamPrecompression":1, "beamType":"|NORMAL", "beamLongBound":1.0, "beamShortBound":1.0},
    ],
},
"covet_turbo_midengine_stage2": {
    "information":{
        "authors":"BeamNG",
        "name":"Evolution Turbocharger",
        "value":4400,
    },
    "slotType" : "covet_intake_midengine",
    "turbocharger": {
        "bovSoundFileName":"event:>Vehicle>Forced_Induction>Turbo_05_cabin>turbo_bov_race",
        "hissLoopEvent":"event:>Vehicle>Forced_Induction>Turbo_05_cabin>turbo_hiss_race",
        "whineLoopEvent":"event:>Vehicle>Forced_Induction>Turbo_05_cabin>turbo_spin_tuned",
        "turboSizeCoef": 0.75,
        "bovSoundVolumeCoef": 0.45,
        "hissVolumePerPSI": 0.030,
        "whineVolumePer10kRPM": 0.0070,
        "whinePitchPer10kRPM": 0.03,
        "wastegateStart":17.3,
        "wastegateLimit":18.3,
        "maxExhaustPower": 25000,
        "backPressureCoef": 0.000025,
        "frictionCoef": 16,
        "inertia":1.4,
        "damageThresholdTemperature": 650,
        "pressurePSI":[
            //turbineRPM, pressure(PSI)
            [0,         -3.5],
            [30000,     -0.5],
            [60000,     5],
            [90000,     8.5],
            [150000,    12],
            [200000,    15.5],
            [250000,    19],
            [300000,    23],
        ],
        "engineDef":[
            //engineRPM, efficiency, exhaustFactor
            [0,     0.0,    0.0],
            [650,   0.10,   0.08],
            [1400,  0.12,   0.10],
            [2000,  0.20,    0.20],
            [2500,  0.40,    0.30],
            [3000,  0.64,    0.55],
            [3500,  0.84,    0.68],
            [4000,  0.88,    0.93],
            [5000,  0.92,    0.94],
            [6000,  0.94,    0.95],
            [7000,  0.92,    0.96],
            [8000,  0.88,    1.0],
            [9000,  0.75,    1.0],
            [10000, 0.65,    1.0],
            [11000, 0.55,    1.0],
        ],
    },
    "mainEngine": {
        //turbocharger name
        "turbocharger":"turbocharger",
        "$*instantAfterFireCoef": 2.0,
        "$*sustainedAfterFireCoef": 1.5,

        //damage deformGroups
        "deformGroups_turbo":["mainEngine_turbo_R","mainEngine_intercooler_R"]
    },
    "soundConfig": {
        "$+maxLoadMix": 0.1,
        "$+intakeMuffling":-0.2,
        "$+mainGain":1.5,
        "$+eqLowGain": 1.5,
    },
    "soundConfigExhaust": {
        "$+maxLoadMix": 0.1,
        "$+minLoadMix": 0.01,
        "$+mainGain": 0.5,
        "$+offLoadGain": 0.00,
        "$+eqLowGain": 0.25,
        "$+eqFundamentalGain": 0.5,
    },
    "vehicleController": {
        "revMatchThrottle":0.35,
    },
    "flexbodies": [
        ["mesh", "[group]:", "nonFlexMaterials"],
        ["covet_intercooler_midengine", ["covet_intercooler_midengine"]],
        ["covet_podfilter_midengine", ["covet_intercooler_midengine"]],
        ["covet_header_turbo_midengine", ["covet_engine_mid","covet_turbo"]],
        ["covet_intake_midengine", ["covet_engine_mid","covet_engine_intake"]],
        {"deformGroup":"mainEngine_chargepipe_R", "deformMaterialBase":"covet_mechanical", "deformMaterialDamaged":"invis"},
        ["covet_icpipe_a_midengine", ["covet_intercooler_midengine","covet_engine_intake"]],
        ["covet_icpipe_b_midengine", ["covet_intercooler_midengine","covet_turbo"]],
        ["covet_intakepipe_midengine", ["covet_intercooler_midengine","covet_engine_intake"]],
        {"deformGroup":""},
        ],
    "nodes": [
         ["id", "posX", "posY", "posZ"],
         {"selfCollision":false},
         {"collision":false},
         //intake
         {"frictionCoef":0.5},
         {"nodeMaterial":"|NM_METAL"},
         {"nodeWeight":3.5},
         {"group":["covet_engine_mid","covet_engine_intake"]},
         ["intck1l",0.25,0.556,0.701],
         ["intck2l",0.25,1.033,0.701],
         {"engineGroup":""},

         //turbos
         {"nodeWeight":2.0},
         ["trb1r",0.18,1.20,0.505, {"group":"covet_turbo"}],
         ["trb2r",0.18,1.35,0.505, {"group":"covet_turbo", "afterFireAudioCoef":1.0, "afterFireVisualCoef":1.0, "afterFireVolumeCoef":1.0, "afterFireMufflingCoef":1.0, "exhaustAudioMufflingCoef":1.0, "exhaustAudioGainChange":0}],

         //intercoolers
         {"nodeWeight":3.0},
         {"selfCollision":true},
         {"collision":true},
         ["ic1l",0.809,0.762,0.461, {"group":"covet_intercooler_midengine"}],
         {"group":""},
    ],
    "beams": [
          ["id1:", "id2:"],
          {"beamPrecompression":1, "beamType":"|NORMAL", "beamLongBound":1.0, "beamShortBound":1.0},
          //intake
          {"deformLimitExpansion":1.2},
          {"beamSpring":3500940,"beamDamp":235},
          {"beamDeform":105000,"beamStrength":"FLT_MAX"},

          ["intck2l", "intck1l"],
          ["intck2l", "e4r"],
          ["e4r", "intck1l"],
          ["intck1l", "e3r"],
          ["intck2l", "e3r"],
          ["intck2l", "e4l"],
          ["e4l", "intck1l"],
          ["intck1l", "e3l"],
          ["e3l", "intck2l"],
          ["intck2l", "e2l"],
          ["e2l", "intck1l"],
          ["intck1l", "e1l"],
          ["e1l", "intck2l"],
          ["intck2l", "e2r"],
          ["e2r", "intck1l"],
          ["intck1l", "e1r"],
          ["e1r", "intck2l"],

          //turbo
          {"beamSpring":3000000,"beamDamp":130},
          {"beamDeform":90000,"beamStrength":"FLT_MAX"},
          {"deformGroup":"mainEngine_turbo_R", "deformationTriggerRatio":0.001}
          ["trb1r", "e3l"],
          ["trb1r", "e4l"],
          ["trb1r", "e3r", {"isExhaust":"mainEngine"}],
          ["trb1r", "e4r"],
          ["trb1r", "e1l"],
          ["trb1r", "e2l"],
          ["trb1r", "e1r"],
          ["trb1r", "e2r"],
          ["trb2r", "e3l"],
          ["trb2r", "e4l"],
          ["trb2r", "e3r"],
          ["trb2r", "e4r"],
          ["trb2r", "e1l"],
          ["trb2r", "e2l"],
          ["trb2r", "e1r"],
          ["trb2r", "e2r"],
          ["trb2r", "trb1r", {"isExhaust":"mainEngine"}]
          {"deformGroup":""}

          //intercooler
          {"beamSpring":2000000,"beamDamp":130},
          {"beamDeform":15000,"beamStrength":"FLT_MAX"},
          {"deformGroup":"mainEngine_intercooler_R", "deformationTriggerRatio":0.02}
          ["ic1l", "f3ll"],
          ["ic1l", "f4ll"],
          ["ic1l", "q5l"],
          ["ic1l", "q6l"],
          ["ic1l", "q1l"],
          ["ic1l", "q2ll"],

          ["ic1l", "f8l"],
          ["ic1l", "rx3l"],

          //charge pipes
          {"beamSpring":50000,"beamDamp":130},
          {"beamDeform":10000,"beamStrength":20000},
          {"deformGroup":"mainEngine_chargepipe_R", "deformationTriggerRatio":0.02}
          ["ic1l", "trb1r"],
          ["ic1l", "intck1l"],
          ["ic1l", "intck2l"],
          {"deformGroup":""}
          {"beamPrecompression":1, "beamType":"|NORMAL", "beamLongBound":1.0, "beamShortBound":1.0},
    ],
},
"covet_turbo_midengine_stage3": {
    "information":{
        "authors":"BeamNG",
        "name":"Stage 3 Variable Boost Turbocharger",
        "value":7700,
    },
    "slotType" : "covet_intake_midengine",
    "variables": [
        ["name", "type", "unit", "category", "default", "min", "max", "title", "description"],
        ["$wastegateStart", "range", "psi", "Engine", 12, 12, 35, "Wastegate Pressure", "Pressure at which the wastegate begins to open", {"stepDis":0.5}],
    ],
    "turbocharger": {
        "bovSoundFileName":"event:>Vehicle>Forced_Induction>Turbo_07_cabin>turbo_bov_race",
        "hissLoopEvent":"event:>Vehicle>Forced_Induction>Turbo_05_cabin>turbo_hiss_race",
        "whineLoopEvent":"event:>Vehicle>Forced_Induction>Turbo_07_cabin>turbo_spin_race",
        "turboSizeCoef": 0.90,
        "bovSoundVolumeCoef": 0.45,
        "hissVolumePerPSI": 0.030,
        "whineVolumePer10kRPM": 0.0070,
        "whinePitchPer10kRPM": 0.05,
        "wastegateStart":"$wastegateStart",
        "wastegateLimit":"$=$wastegateStart+1",
        "maxExhaustPower": 18000,
        "backPressureCoef": 0.00003,
        "frictionCoef": 25,
        "inertia":2.6,
        "damageThresholdTemperature": 730,
        "pressurePSI":[
            //turbineRPM, pressure(PSI)
            [0,         -4.5],
            [30000,     -1.5],
            [60000,     8],
            [90000,     16],
            [150000,    24],
            [200000,    32],
            [250000,    38],
        ],
        "engineDef":[
            //engineRPM, efficiency, exhaustFactor
            [0,     0.0,    0.0],
            [650,   0.10,   0.08],
            [1400,  0.12,   0.10],
            [2000,  0.20,    0.20],
            [2500,  0.40,    0.30],
            [3000,  0.64,    0.55],
            [3500,  0.84,    0.68],
            [4000,  0.88,    0.93],
            [5000,  0.92,    0.95],
            [6000,  0.95,    0.96],
            [7000,  0.96,    0.98],
            [8000,  0.98,    1.0],
            [9000,  0.87,    1.0],
            [10000, 0.65,    1.0],
            [11000, 0.55,    1.0],
        ],
    },
    "mainEngine": {
        //turbocharger name
        "turbocharger":"turbocharger",
        "$*instantAfterFireCoef": 2.5,
        "$*sustainedAfterFireCoef": 2.00,

        //damage deformGroups
        "deformGroups_turbo":["mainEngine_turbo_R","mainEngine_intercooler_R"]
    },
    "soundConfig": {
        "$+maxLoadMix": 0.1,
        "$+intakeMuffling":-0.2,
        "$+mainGain":1.5,
        "$+eqLowGain": 1.5,
    },
    "soundConfigExhaust": {
        "$+maxLoadMix": 0.1,
        "$+minLoadMix": 0.01,
        "$+mainGain": 0.5,
        "$+offLoadGain": 0.00,
        "$+eqLowGain": 0.25,
        "$+eqFundamentalGain": 0.5,
    },
    "vehicleController": {
        "clutchLaunchStartRPM":2100,
        "clutchLaunchTargetRPM":4200,
        //**highShiftDown can be overwritten by automatic transmissions**
        "highShiftDownRPM":[0,0,0,2700,3350,3600,3800,3800],
        //**highShiftUp can be overwritten by intake modifications**
        "highShiftUpRPM":6800,
        "revMatchThrottle":0.35,
    },
    "flexbodies": [
        ["mesh", "[group]:", "nonFlexMaterials"],
        ["covet_intercooler_midengine", ["covet_intercooler_midengine"]],
        ["covet_podfilter_midengine", ["covet_intercooler_midengine"]],
        ["covet_header_turbo_midengine", ["covet_engine_mid","covet_turbo"]],
        ["covet_intake_midengine", ["covet_engine_mid","covet_engine_intake"]],
        {"deformGroup":"mainEngine_chargepipe_R", "deformMaterialBase":"covet_mechanical", "deformMaterialDamaged":"invis"},
        ["covet_icpipe_a_midengine", ["covet_intercooler_midengine","covet_engine_intake"]],
        ["covet_icpipe_b_midengine", ["covet_intercooler_midengine","covet_turbo"]],
        ["covet_intakepipe_midengine", ["covet_intercooler_midengine","covet_engine_intake"]],
        {"deformGroup":""},
        ],
    "nodes": [
         ["id", "posX", "posY", "posZ"],
         {"selfCollision":false},
         {"collision":false},
         //intake
         {"frictionCoef":0.5},
         {"nodeMaterial":"|NM_METAL"},
         {"nodeWeight":3.5},
         {"group":["covet_engine_mid","covet_engine_intake"]},
         ["intck1l",0.25,0.556,0.701],
         ["intck2l",0.25,1.033,0.701],
         {"engineGroup":""},

         //turbos
         {"nodeWeight":2.0},
         ["trb1r",0.18,1.20,0.505, {"group":"covet_turbo"}],
         ["trb2r",0.18,1.35,0.505, {"group":"covet_turbo", "afterFireAudioCoef":1.0, "afterFireVisualCoef":1.0, "afterFireVolumeCoef":1.0, "afterFireMufflingCoef":1.0, "exhaustAudioMufflingCoef":1.0, "exhaustAudioGainChange":0}],

         //intercoolers
         {"nodeWeight":3.0},
         {"selfCollision":true},
         {"collision":true},
         ["ic1l",0.809,0.762,0.461, {"group":"covet_intercooler_midengine"}],
         {"group":""},
    ],
    "beams": [
          ["id1:", "id2:"],
          {"beamPrecompression":1, "beamType":"|NORMAL", "beamLongBound":1.0, "beamShortBound":1.0},
          //intake
          {"deformLimitExpansion":1.2},
          {"beamSpring":3500940,"beamDamp":235},
          {"beamDeform":105000,"beamStrength":"FLT_MAX"},

          ["intck2l", "intck1l"],
          ["intck2l", "e4r"],
          ["e4r", "intck1l"],
          ["intck1l", "e3r"],
          ["intck2l", "e3r"],
          ["intck2l", "e4l"],
          ["e4l", "intck1l"],
          ["intck1l", "e3l"],
          ["e3l", "intck2l"],
          ["intck2l", "e2l"],
          ["e2l", "intck1l"],
          ["intck1l", "e1l"],
          ["e1l", "intck2l"],
          ["intck2l", "e2r"],
          ["e2r", "intck1l"],
          ["intck1l", "e1r"],
          ["e1r", "intck2l"],

          //turbo
          {"beamSpring":3000000,"beamDamp":130},
          {"beamDeform":90000,"beamStrength":"FLT_MAX"},
          {"deformGroup":"mainEngine_turbo_R", "deformationTriggerRatio":0.001}
          ["trb1r", "e3l"],
          ["trb1r", "e4l"],
          ["trb1r", "e3r", {"isExhaust":"mainEngine"}],
          ["trb1r", "e4r"],
          ["trb1r", "e1l"],
          ["trb1r", "e2l"],
          ["trb1r", "e1r"],
          ["trb1r", "e2r"],
          ["trb2r", "e3l"],
          ["trb2r", "e4l"],
          ["trb2r", "e3r"],
          ["trb2r", "e4r"],
          ["trb2r", "e1l"],
          ["trb2r", "e2l"],
          ["trb2r", "e1r"],
          ["trb2r", "e2r"],
          ["trb2r", "trb1r", {"isExhaust":"mainEngine"}]
          {"deformGroup":""}

          //intercooler
          {"beamSpring":2000000,"beamDamp":130},
          {"beamDeform":15000,"beamStrength":"FLT_MAX"},
          {"deformGroup":"mainEngine_intercooler_R", "deformationTriggerRatio":0.02}
          ["ic1l", "f3ll"],
          ["ic1l", "f4ll"],
          ["ic1l", "q5l"],
          ["ic1l", "q6l"],
          ["ic1l", "q1l"],
          ["ic1l", "q2ll"],

          ["ic1l", "f8l"],
          ["ic1l", "rx3l"],

          //charge pipes
          {"beamSpring":50000,"beamDamp":130},
          {"beamDeform":10000,"beamStrength":20000},
          {"deformGroup":"mainEngine_chargepipe_R", "deformationTriggerRatio":0.02}
          ["ic1l", "trb1r"],
          ["ic1l", "intck1l"],
          ["ic1l", "intck2l"],
          {"deformGroup":""}
          {"beamPrecompression":1, "beamType":"|NORMAL", "beamLongBound":1.0, "beamShortBound":1.0},
    ],
},
}
