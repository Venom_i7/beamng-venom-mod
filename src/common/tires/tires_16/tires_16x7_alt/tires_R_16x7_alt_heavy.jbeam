{
"tire_R_225_75_16_alt_heavy": {
    "information":{
        "authors":"BeamNG",
        "name":"225/75R16 Heavy Duty Rear Tires",
        "value":240,
    },
    "slotType" : "tire_R_16x7_alt",
    "flexbodies": [
        ["mesh", "[group]:", "nonFlexMaterials"],
        ["tire_01a_16x8_29", ["tire_RR","wheel_RR"], [], {"pos":{"x":-0.54, "y":0.0, "z":0.0}, "rot":{"x":0, "y":0, "z":180}, "scale":{"x":0.95, "y":1, "z":1}}],
        ["tire_01a_16x8_29", ["tire_RL","wheel_RL"], [], {"pos":{"x": 0.54, "y":0.0, "z":0.0}, "rot":{"x":0, "y":0, "z":0},   "scale":{"x":0.95, "y":1, "z":1}}],
    ],
    "variables": [
        ["name", "type", "unit", "category", "default", "min", "max", "title", "description"],
        ["$tirepressure_R", "range", "psi", "Wheels", 35, 0, 80, "Tire Pressure", "Relative to atmospheric pressure", {"subCategory":"Rear"}],
    ],
    "pressureWheels": [
        ["name","hubGroup","group","node1:","node2:","nodeS","nodeArm:","wheelDir"],
        {"disableMeshBreaking":false,"disableHubMeshBreaking":false},
        {"hasTire":true},
        {"enableTireReinfBeams":false},
        {"enableTireLbeams":true},
        {"enableTireSideReinfBeams":false},
        {"enableTreadReinfBeams":true},
        {"enableTirePeripheryReinfBeams":true},

        //general settings
        {"radius":0.370},
        {"tireWidth":0.205},

        //tire options
        {"wheelSideBeamSpring":"$=$tirepressure_R*900","wheelSideBeamDamp":40},
        {"wheelSideBeamSpringExpansion":461000,"wheelSideBeamDampExpansion":40},
        {"wheelSideTransitionZone":0.08,"wheelSideBeamPrecompression":0.99},

        {"wheelReinfBeamSpring":25000,"wheelReinfBeamDamp":190},
        {"wheelReinfBeamDampCutoffHz":500,"wheelReinfBeamPrecompression":0.99},

        {"wheelTreadBeamSpring":120000,"wheelTreadBeamDamp":85},
        {"wheelTreadBeamDampCutoffHz":500,"wheelTreadBeamPrecompression":0.99},

        {"wheelTreadReinfBeamSpring":200000,"wheelTreadReinfBeamDamp":65},
        {"wheelTreadReinfBeamDampCutoffHz":500,"wheelTreadReinfBeamPrecompression":0.99},

        {"wheelPeripheryBeamSpring":80000,"wheelPeripheryBeamDamp":50},
        {"wheelPeripheryBeamDampCutoffHz":500,"wheelPeripheryBeamPrecompression":0.99},

        {"wheelPeripheryReinfBeamSpring":120000,"wheelPeripheryReinfBeamDamp":40},
        {"wheelPeripheryReinfBeamDampCutoffHz":500,"wheelPeripheryReinfBeamPrecompression":0.99},

        //general tire values
        {"nodeWeight":0.19},
        {"nodeMaterial":"|NM_RUBBER"},
        {"triangleCollision":false},
        {"pressurePSI":"$tirepressure_R"},
        {"dragCoef":5},

        //groundmodel friction multipliers
        {"frictionCoef":1.0},
        {"slidingFrictionCoef":1.0},
        {"treadCoef":0.7},

        //advanced friction values
        {"noLoadCoef":1.35},
        {"loadSensitivitySlope":0.00010},
        {"fullLoadCoef":0.5},
        {"softnessCoef":0.7},

        //deform values
        {"wheelSideBeamDeform":28000,"wheelSideBeamStrength":36000},
        {"wheelTreadBeamDeform":24000,"wheelTreadBeamStrength":32000},
        {"wheelPeripheryBeamDeform":70000,"wheelPeripheryBeamStrength":70000},
    ],
},
"tire_R_245_75_16_alt_heavy": {
    "information":{
        "authors":"BeamNG",
        "name":"245/75R16 Heavy Duty Rear Tires",
        "value":260,
    },
    "slotType" : "tire_R_16x7_alt",
    "flexbodies": [
        ["mesh", "[group]:", "nonFlexMaterials"],
        ["tire_01a_16x8_31", ["wheel_RR","tire_RR"], [], {"pos":{"x":-0.54, "y":0.0, "z":0.0}, "rot":{"x":0, "y":0, "z":180}, "scale":{"x":0.975, "y":0.99, "z":0.99}}],
        ["tire_01a_16x8_31", ["wheel_RL","tire_RL"], [], {"pos":{"x": 0.54, "y":0.0, "z":0.0}, "rot":{"x":0, "y":0, "z":0},   "scale":{"x":0.975, "y":0.99, "z":0.99}}],
    ],
    "variables": [
        ["name", "type", "unit", "category", "default", "min", "max", "title", "description"],
        ["$tirepressure_R", "range", "psi", "Wheels", 35, 0, 80, "Tire Pressure", "Relative to atmospheric pressure", {"subCategory":"Rear"}],
    ],
    "pressureWheels": [
        ["name","hubGroup","group","node1:","node2:","nodeS","nodeArm:","wheelDir"],
        {"disableMeshBreaking":false,"disableHubMeshBreaking":false},
        {"hasTire":true},
        {"enableTireReinfBeams":false},
        {"enableTireLbeams":true},
        {"enableTireSideReinfBeams":false},
        {"enableTreadReinfBeams":true},
        {"enableTirePeripheryReinfBeams":true},

        //general settings
        {"radius":0.387},
        {"tireWidth":0.215},

        //tire options
        {"wheelSideBeamSpring":"$=$tirepressure_R*900","wheelSideBeamDamp":45},
        {"wheelSideBeamSpringExpansion":471000,"wheelSideBeamDampExpansion":45},
        {"wheelSideTransitionZone":0.08,"wheelSideBeamPrecompression":0.99},

        {"wheelReinfBeamSpring":26000,"wheelReinfBeamDamp":180},
        {"wheelReinfBeamDampCutoffHz":500,"wheelReinfBeamPrecompression":0.99},

        {"wheelTreadBeamSpring":120000,"wheelTreadBeamDamp":80},
        {"wheelTreadBeamDampCutoffHz":500,"wheelTreadBeamPrecompression":0.99},

        {"wheelTreadReinfBeamSpring":200000,"wheelTreadReinfBeamDamp":60},
        {"wheelTreadReinfBeamDampCutoffHz":500,"wheelTreadReinfBeamPrecompression":0.99},

        {"wheelPeripheryBeamSpring":80000,"wheelPeripheryBeamDamp":50},
        {"wheelPeripheryBeamDampCutoffHz":500,"wheelPeripheryBeamPrecompression":0.99},

        {"wheelPeripheryReinfBeamSpring":120000,"wheelPeripheryReinfBeamDamp":40},
        {"wheelPeripheryReinfBeamDampCutoffHz":500,"wheelPeripheryReinfBeamPrecompression":0.99},

        //general tire values
        {"nodeWeight":0.2},
        {"nodeMaterial":"|NM_RUBBER"},
        {"triangleCollision":false},
        {"pressurePSI":"$tirepressure_R"},
        {"dragCoef":5},

        //groundmodel friction multipliers
        {"frictionCoef":1.0},
        {"slidingFrictionCoef":1.0},
        {"treadCoef":0.7},

        //advanced friction values
        {"noLoadCoef":1.33},
        {"loadSensitivitySlope":0.00009},
        {"fullLoadCoef":0.5},
        {"softnessCoef":0.7},

        //deform values
        {"wheelSideBeamDeform":28000,"wheelSideBeamStrength":36000},
        {"wheelTreadBeamDeform":24000,"wheelTreadBeamStrength":32000},
        {"wheelPeripheryBeamDeform":70000,"wheelPeripheryBeamStrength":70000},
    ],
},
"tire_R_265_75_16_alt_heavy": {
    "information":{
        "authors":"BeamNG",
        "name":"265/75R16 Heavy Duty Rear Tires",
        "value":280,
    },
    "slotType" : "tire_R_16x7_alt",
    "flexbodies": [
        ["mesh", "[group]:", "nonFlexMaterials"],
        ["tire_01a_16x8_31", ["wheel_RR","tire_RR"], [], {"pos":{"x":-0.54, "y":0.0, "z":0.0}, "rot":{"x":0, "y":0, "z":180}, "scale":{"x":0.99, "y":1.025, "z":1.025}}],
        ["tire_01a_16x8_31", ["wheel_RL","tire_RL"], [], {"pos":{"x": 0.54, "y":0.0, "z":0.0}, "rot":{"x":0, "y":0, "z":0},   "scale":{"x":0.99, "y":1.025, "z":1.025}}],
    ],
    "variables": [
        ["name", "type", "unit", "category", "default", "min", "max", "title", "description"],
        ["$tirepressure_R", "range", "psi", "Wheels", 35, 0, 80, "Tire Pressure", "Relative to atmospheric pressure", {"subCategory":"Rear"}],
    ],
    "pressureWheels": [
        ["name","hubGroup","group","node1:","node2:","nodeS","nodeArm:","wheelDir"],
        {"disableMeshBreaking":false,"disableHubMeshBreaking":false},
        {"hasTire":true},
        {"enableTireReinfBeams":false},
        {"enableTireLbeams":true},
        {"enableTireSideReinfBeams":false},
        {"enableTreadReinfBeams":true},
        {"enableTirePeripheryReinfBeams":true},

        //general settings
        {"radius":0.4},
        {"tireWidth":0.235},

        //tire options
        {"wheelSideBeamSpring":"$=$tirepressure_R*940","wheelSideBeamDamp":45},
        {"wheelSideBeamSpringExpansion":471000,"wheelSideBeamDampExpansion":45},
        {"wheelSideTransitionZone":0.08,"wheelSideBeamPrecompression":0.99},

        {"wheelReinfBeamSpring":28000,"wheelReinfBeamDamp":180},
        {"wheelReinfBeamDampCutoffHz":500,"wheelReinfBeamPrecompression":0.99},

        {"wheelTreadBeamSpring":125000,"wheelTreadBeamDamp":80},
        {"wheelTreadBeamDampCutoffHz":500,"wheelTreadBeamPrecompression":0.99},

        {"wheelTreadReinfBeamSpring":210000,"wheelTreadReinfBeamDamp":60},
        {"wheelTreadReinfBeamDampCutoffHz":500,"wheelTreadReinfBeamPrecompression":0.99},

        {"wheelPeripheryBeamSpring":85000,"wheelPeripheryBeamDamp":50},
        {"wheelPeripheryBeamDampCutoffHz":500,"wheelPeripheryBeamPrecompression":0.99},

        {"wheelPeripheryReinfBeamSpring":125000,"wheelPeripheryReinfBeamDamp":40},
        {"wheelPeripheryReinfBeamDampCutoffHz":500,"wheelPeripheryReinfBeamPrecompression":0.99},

        //general tire values
        {"nodeWeight":0.215},
        {"nodeMaterial":"|NM_RUBBER"},
        {"triangleCollision":false},
        {"pressurePSI":"$tirepressure_R"},
        {"dragCoef":5},

        //groundmodel friction multipliers
        {"frictionCoef":1.0},
        {"slidingFrictionCoef":1.0},
        {"treadCoef":0.7},

        //advanced friction values
        {"noLoadCoef":1.33},
        {"loadSensitivitySlope":0.000087},
        {"fullLoadCoef":0.5},
        {"softnessCoef":0.7},

        //deform values
        {"wheelSideBeamDeform":28000,"wheelSideBeamStrength":36000},
        {"wheelTreadBeamDeform":24000,"wheelTreadBeamStrength":32000},
        {"wheelPeripheryBeamDeform":70000,"wheelPeripheryBeamStrength":70000},
    ],
},
}