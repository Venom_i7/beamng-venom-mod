# Venom Performance Mod (WIP)

Venom performance part mod for BeamNG cars

## Car list

- Bastion
  - New engine
  - New internal part
  - New twin-turbo
- Bolide
  - WIP
- Citybus
  - WIP
- More car is coming soon!

## Engine torque calculation

Each car's engine is calculate by this link

> https://docs.google.com/spreadsheets/d/1NhK2gHCZFCZe0hdJeyLhQOOY8Q0nzgnmZd_pp5HFihc/edit?usp=sharing

## Release Script

(Support only Windows 7 and later) Please download and install 7zip before run "release.bat" file.
You can download 7zip from here

> https://www.7-zip.org/download.html
