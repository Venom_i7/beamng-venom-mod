{"bolide_innerfender_a_FR":
{
    "information":{
        "authors":"BeamNG",
        "name":"Front Right Inner Fender",
        "value":150,
    },

    "slotType" : "bolide_innerfender_a_FR",
    "slots": [
        ["type", "default", "description"],
        ["bolide_mudflap_a_FR","", "Front Right Mudflap"],
    ],
    "flexbodies":[
        ["mesh", "[group]:","nonFlexMaterials"],
        ["bolide_innerfender_a_FR", ["bolide_innerfender_FR", "bolide_fender_R"]],
    ],

    "nodes":[
         ["id", "posX", "posY", "posZ"],
         {"frictionCoef":0.5},
         {"nodeMaterial":"|NM_PLASTIC"},

         //--INNER FENDER--
         {"selfCollision":true},
         {"collision":true},
         {"nodeWeight":0.9},
         {"group":"bolide_innerfender_FR"}
         ["fif1r",-0.6,-1.687,0.233],
         ["fif2r",-0.619,-1.323,0.65],
         ["fif3r",-0.502,-0.99,0.207],
         {"group":"none"},
        ],
    "beams":[
          ["id1:", "id2:"],
          //--INNER FENDER--

          {"beamPrecompression":1, "beamType":"|NORMAL", "beamLongBound":1, "beamShortBound":1},
          //inner fender main shape
          {"deformLimitExpansion":1.2},
          {"beamSpring":100100,"beamDamp":100},
          {"beamDeform":14000,"beamStrength":"FLT_MAX"},
          ["fif3r", "fif2r"],
          ["fif2r", "fif1r"],
          ["fif3r", "fe16r"],
          ["fif2r", "fe11r"],
          ["fe15r", "fif1r"],

          //surficial crossing
          {"deformLimitExpansion":""},
          {"beamDeform":8000,"beamStrength":"FLT_MAX"},
          ["fif1r", "fe11r"],
          ["fif2r", "fe15r"],
          ["fif2r", "fe16r"],
          ["fif3r", "fe11r"],
          ["fif3r", "fe12r"],
          ["fe12r", "fif2r"],
          ["fif2r", "fe10r"],
          ["fe10r", "fif1r"],

          //rigids
          {"beamDeform":6000,"beamStrength":"FLT_MAX"},
          ["fif3r", "fif1r"],
          ["fif1r", "fe14r"],
          ["fif3r", "fe17r"],
          ["fif2r", "fe7r"],

          //attach to subframe
          {"deformLimitExpansion":""},
          {"beamSpring":245000,"beamDamp":50},
          {"beamDeform":8500,"beamStrength":20000},
          {"breakGroup":"innerfender_FR_a"},
          ["fif3r", "fx2r"],
          ["fif3r", "fx6r"],
          ["fif3r", "fx4r"],
          ["fif2r", "fx4r"],
          ["fif2r", "fx7r"],
          {"breakGroup":"innerfender_FR_b"},
          ["fx1r", "fif1r"],
          ["fif1r", "fx8r"],
          ["fif1r", "fx3r"],
          ["fif2r", "fx3r"],
          ["fif2r", "fx1r"],
          {"breakGroup":""},

    ],
    "triangles": [
         ["id1:","id2:","id3:"],
         //inner fender
         {"groundModel":"metal"},
         {"group":"bolide_innerfender_FR"},
         {"dragCoef":6},
         ["fe12r", "fe16r", "fif3r"],
         ["fe12r", "fif2r",  "fe11r"],
         ["fe12r", "fif3r",  "fif2r"],
         ["fe11r", "fif2r",  "fe10r"],
         ["fif2r",  "fif1r",  "fe10r"],
         ["fe10r", "fif1r",  "fe15r"],
         {"group":""},
    ],
},
"bolide_innerfender_a_FL":
{
    "information":{
        "authors":"BeamNG",
        "name":"Front Left Inner Fender",
        "value":150,
    },

    "slotType" : "bolide_innerfender_a_FL",
    "slots": [
        ["type", "default", "description"],
        ["bolide_mudflap_a_FL","", "Front Left Mudflap"],
    ],
    "flexbodies":[
        ["mesh", "[group]:","nonFlexMaterials"],
        ["bolide_innerfender_a_FL", ["bolide_innerfender_FL", "bolide_fender_L"]],
    ],

    "nodes":[
         ["id", "posX", "posY", "posZ"],
         {"frictionCoef":0.5},
         {"nodeMaterial":"|NM_PLASTIC"},

         //--INNER FENDER--
         {"selfCollision":true},
         {"collision":true},
         {"nodeWeight":0.9},
         {"group":"bolide_innerfender_FL"}
         ["fif1l",0.6,-1.687,0.233],
         ["fif2l",0.619,-1.323,0.65],
         ["fif3l",0.502,-0.99,0.207],
         {"group":"none"},
        ],
    "beams":[
          ["id1:", "id2:"],
          //--INNER FENDER--

          {"beamPrecompression":1, "beamType":"|NORMAL", "beamLongBound":1, "beamShortBound":1},
          //inner fender main shape
          {"deformLimitExpansion":1.2},
          {"beamSpring":100100,"beamDamp":100},
          {"beamDeform":14000,"beamStrength":"FLT_MAX"},
          ["fif3l", "fif2l"],
          ["fif2l", "fif1l"],
          ["fif3l", "fe16l"],
          ["fif2l", "fe11l"],
          ["fe15l", "fif1l"],

          //surficial crossing
          {"deformLimitExpansion":""},
          {"beamDeform":8000,"beamStrength":"FLT_MAX"},
          ["fif1l", "fe11l"],
          ["fif2l", "fe15l"],
          ["fif2l", "fe16l"],
          ["fif3l", "fe11l"],
          ["fif3l", "fe12l"],
          ["fe12l", "fif2l"],
          ["fif2l", "fe10l"],
          ["fe10l", "fif1l"],

          //rigids
          {"beamDeform":6000,"beamStrength":"FLT_MAX"},
          ["fif3l", "fif1l"],
          ["fif1l", "fe14l"],
          ["fif3l", "fe17l"],
          ["fif2l", "fe7l"],

          //attach to subframe
          {"deformLimitExpansion":""},
          {"beamSpring":245000,"beamDamp":50},
          {"beamDeform":8500,"beamStrength":20000},
          {"breakGroup":"innerfender_FL_a"},
          ["fif3l", "fx2l"],
          ["fif3l", "fx6l"],
          ["fif3l", "fx4l"],
          ["fif2l", "fx4l"],
          ["fif2l", "fx7l"],
          {"breakGroup":"innerfender_FL_b"},
          ["fx1l", "fif1l"],
          ["fif1l", "fx8l"],
          ["fif1l", "fx3l"],
          ["fif2l", "fx3l"],
          ["fif2l", "fx1l"],
          {"breakGroup":""},

    ],
    "triangles": [
         ["id1:","id2:","id3:"],
         //inner fender
         {"groundModel":"metal"},
         {"group":"bolide_innerfender_FL"},
         {"dragCoef":6},
         ["fe16l", "fe12l", "fif3l"],
         ["fif2l",  "fe12l", "fe11l"],
         ["fif3l",  "fe12l", "fif2l"],
         ["fif2l",  "fe11l", "fe10l"],
         ["fif1l",  "fif2l",  "fe10l"],
         ["fif1l",  "fe10l", "fe15l"],
         {"group":""},
    ],
},

"bolide_innerfender_b_FR":
{
    "information":{
        "authors":"BeamNG",
        "name":"GTT Front Right Inner Fender",
        "value":180,
    },

    "slotType" : "bolide_innerfender_b_FR",
    "slots": [
        ["type", "default", "description"],
        ["bolide_mudflap_a_FR","", "Front Right Mudflap"],
    ],
    "flexbodies":[
        ["mesh", "[group]:","nonFlexMaterials"],
        ["bolide_innerfender_b_FR", ["bolide_innerfender_FR", "bolide_fender_R"]],
    ],

    "nodes":[
         ["id", "posX", "posY", "posZ"],
         {"frictionCoef":0.5},
         {"nodeMaterial":"|NM_PLASTIC"},

         //--INNER FENDER--
         {"selfCollision":true},
         {"collision":true},
         {"nodeWeight":0.7},
         {"group":"bolide_innerfender_FR"}
         ["fif1r",-0.6,-1.687,0.233],
         ["fif2r",-0.619,-1.323,0.65],
         ["fif3r",-0.502,-0.99,0.207],
         {"group":"none"},
        ],
    "beams":[
          ["id1:", "id2:"],
          //--INNER FENDER--

          {"beamPrecompression":1, "beamType":"|NORMAL", "beamLongBound":1, "beamShortBound":1},
          //inner fender main shape
          {"deformLimitExpansion":1.2},
          {"beamSpring":100100,"beamDamp":100},
          {"beamDeform":14000,"beamStrength":"FLT_MAX"},
          ["fif3r", "fif2r"],
          ["fif2r", "fif1r"],
          ["fif3r", "fe16r"],
          ["fif2r", "fe11r"],
          ["fe15r", "fif1r"],

          //surficial crossing
          {"deformLimitExpansion":""},
          {"beamDeform":8000,"beamStrength":"FLT_MAX"},
          ["fif1r", "fe11r"],
          ["fif2r", "fe15r"],
          ["fif2r", "fe16r"],
          ["fif3r", "fe11r"],
          ["fif3r", "fe12r"],
          ["fe12r", "fif2r"],
          ["fif2r", "fe10r"],
          ["fe10r", "fif1r"],

          //rigids
          {"beamDeform":6000,"beamStrength":"FLT_MAX"},
          ["fif3r", "fif1r"],
          ["fif1r", "fe14r"],
          ["fif3r", "fe17r"],
          ["fif2r", "fe7r"],

          //attach to subframe
          {"deformLimitExpansion":""},
          {"beamSpring":245000,"beamDamp":50},
          {"beamDeform":8500,"beamStrength":20000},
          {"breakGroup":"innerfender_FR_a"},
          ["fif3r", "fx2r"],
          ["fif3r", "fx6r"],
          ["fif3r", "fx4r"],
          ["fif2r", "fx4r"],
          ["fif2r", "fx7r"],
          {"breakGroup":"innerfender_FR_b"},
          ["fx1r", "fif1r"],
          ["fif1r", "fx8r"],
          ["fif1r", "fx3r"],
          ["fif2r", "fx3r"],
          ["fif2r", "fx1r"],
          {"breakGroup":""},

    ],
    "triangles": [
         ["id1:","id2:","id3:"],
         //inner fender
         {"groundModel":"metal"},
         {"group":"bolide_innerfender_FR"},
         {"dragCoef":1},
         ["fe12r", "fe16r", "fif3r"],
         ["fe12r", "fif2r",  "fe11r"],
         ["fe12r", "fif3r",  "fif2r"],
         ["fe11r", "fif2r",  "fe10r"],
         ["fif2r",  "fif1r",  "fe10r"],
         ["fe10r", "fif1r",  "fe15r"],
         {"group":""},
    ],
},
"bolide_innerfender_b_FL":
{
    "information":{
        "authors":"BeamNG",
        "name":"GTT Front Left Inner Fender",
        "value":180,
    },

    "slotType" : "bolide_innerfender_b_FL",
    "slots": [
        ["type", "default", "description"],
        ["bolide_mudflap_a_FL","", "Front Left Mudflap"],
    ],
    "flexbodies":[
        ["mesh", "[group]:","nonFlexMaterials"],
        ["bolide_innerfender_b_FL", ["bolide_innerfender_FL", "bolide_fender_L"]],
    ],

    "nodes":[
         ["id", "posX", "posY", "posZ"],
         {"frictionCoef":0.5},
         {"nodeMaterial":"|NM_PLASTIC"},

         //--INNER FENDER--
         {"selfCollision":true},
         {"collision":true},
         {"nodeWeight":0.7},
         {"group":"bolide_innerfender_FL"}
         ["fif1l",0.6,-1.687,0.233],
         ["fif2l",0.619,-1.323,0.65],
         ["fif3l",0.502,-0.99,0.207],
         {"group":"none"},
        ],
    "beams":[
          ["id1:", "id2:"],
          //--INNER FENDER--

          {"beamPrecompression":1, "beamType":"|NORMAL", "beamLongBound":1, "beamShortBound":1},
          //inner fender main shape
          {"deformLimitExpansion":1.2},
          {"beamSpring":100100,"beamDamp":100},
          {"beamDeform":14000,"beamStrength":"FLT_MAX"},
          ["fif3l", "fif2l"],
          ["fif2l", "fif1l"],
          ["fif3l", "fe16l"],
          ["fif2l", "fe11l"],
          ["fe15l", "fif1l"],

          //surficial crossing
          {"deformLimitExpansion":""},
          {"beamDeform":8000,"beamStrength":"FLT_MAX"},
          ["fif1l", "fe11l"],
          ["fif2l", "fe15l"],
          ["fif2l", "fe16l"],
          ["fif3l", "fe11l"],
          ["fif3l", "fe12l"],
          ["fe12l", "fif2l"],
          ["fif2l", "fe10l"],
          ["fe10l", "fif1l"],

          //rigids
          {"beamDeform":6000,"beamStrength":"FLT_MAX"},
          ["fif3l", "fif1l"],
          ["fif1l", "fe14l"],
          ["fif3l", "fe17l"],
          ["fif2l", "fe7l"],

          //attach to subframe
          {"deformLimitExpansion":""},
          {"beamSpring":245000,"beamDamp":50},
          {"beamDeform":8500,"beamStrength":20000},
          {"breakGroup":"innerfender_FL_a"},
          ["fif3l", "fx2l"],
          ["fif3l", "fx6l"],
          ["fif3l", "fx4l"],
          ["fif2l", "fx4l"],
          ["fif2l", "fx7l"],
          {"breakGroup":"innerfender_FL_b"},
          ["fx1l", "fif1l"],
          ["fif1l", "fx8l"],
          ["fif1l", "fx3l"],
          ["fif2l", "fx3l"],
          ["fif2l", "fx1l"],
          {"breakGroup":""},

    ],
    "triangles": [
         ["id1:","id2:","id3:"],
         //inner fender
         {"groundModel":"metal"},
         {"group":"bolide_innerfender_FL"},
         {"dragCoef":1},
         ["fe16l", "fe12l", "fif3l"],
         ["fif2l",  "fe12l", "fe11l"],
         ["fif3l",  "fe12l", "fif2l"],
         ["fif2l",  "fe11l", "fe10l"],
         ["fif1l",  "fif2l",  "fe10l"],
         ["fif1l",  "fe10l", "fe15l"],
         {"group":""},
    ],
},
"bolide_innerfender_c_FR":
{
    "information":{
        "authors":"BeamNG",
        "name":"GTR Front Right Inner Fender",
        "value":280,
    },

    "slotType" : "bolide_innerfender_c_FR",
    "slots": [
        ["type", "default", "description"],
        ["bolide_mudflap_a_FR","", "Front Right Mudflap", {"nodeMove":{"x":-0.03, "y":0.0, "z":-0.05}}],
    ],
    "flexbodies":[
        ["mesh", "[group]:","nonFlexMaterials"],
        ["bolide_innerfender_c_FR", ["bolide_innerfender_FR", "bolide_fender_R"]],
    ],

    "nodes":[
         ["id", "posX", "posY", "posZ"],
         {"frictionCoef":0.5},
         {"nodeMaterial":"|NM_PLASTIC"},

         //--INNER FENDER--
         {"selfCollision":true},
         {"collision":true},
         {"nodeWeight":0.3},
         {"group":"bolide_innerfender_FR"}
         ["fif1r",-0.6,-1.687,0.233],
         ["fif2r",-0.619,-1.323,0.65],
         ["fif3r",-0.502,-0.99,0.207],
         {"group":"none"},
        ],
    "beams":[
          ["id1:", "id2:"],
          //--INNER FENDER--

          {"beamPrecompression":1, "beamType":"|NORMAL", "beamLongBound":1, "beamShortBound":1},
          //inner fender main shape
          {"deformLimitExpansion":1.2},
          {"beamSpring":100100,"beamDamp":100},
          {"beamDeform":14000,"beamStrength":"FLT_MAX"},
          ["fif3r", "fif2r"],
          ["fif2r", "fif1r"],
          ["fif3r", "fe16r"],
          ["fif2r", "fe11r"],
          ["fe15r", "fif1r"],

          //surficial crossing
          {"deformLimitExpansion":""},
          {"beamDeform":8000,"beamStrength":"FLT_MAX"},
          ["fif1r", "fe11r"],
          ["fif2r", "fe15r"],
          ["fif2r", "fe16r"],
          ["fif3r", "fe11r"],
          ["fif3r", "fe12r"],
          ["fe12r", "fif2r"],
          ["fif2r", "fe10r"],
          ["fe10r", "fif1r"],

          //rigids
          {"beamDeform":6000,"beamStrength":"FLT_MAX"},
          ["fif3r", "fif1r"],
          ["fif1r", "fe14r"],
          ["fif3r", "fe17r"],
          ["fif2r", "fe7r"],

          //attach to subframe
          {"deformLimitExpansion":""},
          {"beamSpring":245000,"beamDamp":50},
          {"beamDeform":8500,"beamStrength":20000},
          {"breakGroup":"innerfender_FR_a"},
          ["fif3r", "fx2r"],
          ["fif3r", "fx6r"],
          ["fif3r", "fx4r"],
          ["fif2r", "fx4r"],
          ["fif2r", "fx7r"],
          {"breakGroup":"innerfender_FR_b"},
          ["fx1r", "fif1r"],
          ["fif1r", "fx8r"],
          ["fif1r", "fx3r"],
          ["fif2r", "fx3r"],
          ["fif2r", "fx1r"],
          {"breakGroup":""},

    ],
    "triangles": [
         ["id1:","id2:","id3:"],
         //inner fender
         {"groundModel":"metal"},
         {"group":"bolide_innerfender_FR"},
         {"dragCoef":1},
         ["fe12r", "fe16r", "fif3r"],
         ["fe12r", "fif2r",  "fe11r"],
         ["fe12r", "fif3r",  "fif2r"],
         ["fe11r", "fif2r",  "fe10r"],
         ["fif2r",  "fif1r",  "fe10r"],
         ["fe10r", "fif1r",  "fe15r"],
         {"group":""},
    ],
},
"bolide_innerfender_c_FL":
{
    "information":{
        "authors":"BeamNG",
        "name":"GTR Front Left Inner Fender",
        "value":250,
    },

    "slotType" : "bolide_innerfender_c_FL",
    "slots": [
        ["type", "default", "description"],
        ["bolide_mudflap_a_FL","", "Front Left Mudflap", {"nodeMove":{"x":0.03, "y":0.0, "z":-0.05}}],
    ],
    "flexbodies":[
        ["mesh", "[group]:","nonFlexMaterials"],
        ["bolide_innerfender_c_FL", ["bolide_innerfender_FL", "bolide_fender_L"]],
    ],

    "nodes":[
         ["id", "posX", "posY", "posZ"],
         {"frictionCoef":0.5},
         {"nodeMaterial":"|NM_PLASTIC"},

         //--INNER FENDER--
         {"selfCollision":true},
         {"collision":true},
         {"nodeWeight":0.3},
         {"group":"bolide_innerfender_FL"}
         ["fif1l",0.6,-1.687,0.233],
         ["fif2l",0.619,-1.323,0.65],
         ["fif3l",0.502,-0.99,0.207],
         {"group":"none"},
        ],
    "beams":[
          ["id1:", "id2:"],
          //--INNER FENDER--

          {"beamPrecompression":1, "beamType":"|NORMAL", "beamLongBound":1, "beamShortBound":1},
          //inner fender main shape
          {"deformLimitExpansion":1.2},
          {"beamSpring":100100,"beamDamp":100},
          {"beamDeform":14000,"beamStrength":"FLT_MAX"},
          ["fif3l", "fif2l"],
          ["fif2l", "fif1l"],
          ["fif3l", "fe16l"],
          ["fif2l", "fe11l"],
          ["fe15l", "fif1l"],

          //surficial crossing
          {"deformLimitExpansion":""},
          {"beamDeform":8000,"beamStrength":"FLT_MAX"},
          ["fif1l", "fe11l"],
          ["fif2l", "fe15l"],
          ["fif2l", "fe16l"],
          ["fif3l", "fe11l"],
          ["fif3l", "fe12l"],
          ["fe12l", "fif2l"],
          ["fif2l", "fe10l"],
          ["fe10l", "fif1l"],

          //rigids
          {"beamDeform":6000,"beamStrength":"FLT_MAX"},
          ["fif3l", "fif1l"],
          ["fif1l", "fe14l"],
          ["fif3l", "fe17l"],
          ["fif2l", "fe7l"],

          //attach to subframe
          {"deformLimitExpansion":""},
          {"beamSpring":245000,"beamDamp":50},
          {"beamDeform":8500,"beamStrength":20000},
          {"breakGroup":"innerfender_FL_a"},
          ["fif3l", "fx2l"],
          ["fif3l", "fx6l"],
          ["fif3l", "fx4l"],
          ["fif2l", "fx4l"],
          ["fif2l", "fx7l"],
          {"breakGroup":"innerfender_FL_b"},
          ["fx1l", "fif1l"],
          ["fif1l", "fx8l"],
          ["fif1l", "fx3l"],
          ["fif2l", "fx3l"],
          ["fif2l", "fx1l"],
          {"breakGroup":""},

    ],
    "triangles": [
         ["id1:","id2:","id3:"],
         //inner fender
         {"groundModel":"metal"},
         {"group":"bolide_innerfender_FL"},
         {"dragCoef":1},
         ["fe16l", "fe12l", "fif3l"],
         ["fif2l",  "fe12l", "fe11l"],
         ["fif3l",  "fe12l", "fif2l"],
         ["fif2l",  "fe11l", "fe10l"],
         ["fif1l",  "fif2l",  "fe10l"],
         ["fif1l",  "fe10l", "fe15l"],
         {"group":""},
    ],
},
}