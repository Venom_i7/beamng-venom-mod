{
  "scintilla_engine_7.5_v10_venom": {
    "information": {
      "authors": "Venom",
      "name": "Venom 7.5L V10 Engine",
      "value": 375000
    },
    "slotType": "scintilla_engine",
    "slots": [
      ["type", "default", "description"],
      [
        "scintilla_intake_5.0",
        "scintilla_intake_5.0_stock",
        "Intake",
        {
          "coreSlot": true
        }
      ],
      ["scintilla_exhaust", "scintilla_exhaust", "Exhaust"],
      [
        "scintilla_engine_5.0_ecu",
        "scintilla_engine_5.0_ecu",
        "Engine Management",
        {
          "coreSlot": true
        }
      ],
      [
        "scintilla_engine_5.0_internals",
        "scintilla_engine_5.0_internals",
        "Engine Long Block",
        {
          "coreSlot": true
        }
      ],
      [
        "scintilla_oilpan",
        "scintilla_oilpan",
        "Oil Pan",
        {
          "coreSlot": true
        }
      ],
      ["scintilla_transaxle", "scintilla_transaxle_7DCT", "Transaxle"],
      ["scintilla_enginemounts", "scintilla_enginemounts", "Engine Mounts"],
      ["n2o_system", "", "Nitrous Oxide System"]
    ],
    "powertrain": [
      ["type", "name", "inputName", "inputIndex"],
      ["combustionEngine", "mainEngine", "dummy", 0]
    ],
    "mainEngine": {
      "torque": [
        ["rpm", "torque"],
        [0, 0],
        [500, 250],
        [1000, 350],
        [2000, 450],
        [3000, 550],
        [4000, 650],
        [5000, 750],
        [6500, 850],
        [8000, 900],
        [9000, 900],
        [10000, 900],
        [11000, 900]
      ],
      "idleRPM": 800,
      //max physically capable of
      "maxRPM": 11000,
      "inertia": 0.1,
      "friction": 8,
      "dynamicFriction": 0.015,
      "engineBrakeTorque": 95,
      "burnEfficiency": [
        [0, 0.1],
        [0.05, 0.5],
        [0.4, 0.77],
        [0.7, 0.95],
        [1, 1]
      ],
      //fuel system
      "energyStorage": ["fueltank_R", "fueltank_L"],
      "requiredEnergyType": "gasoline",
      //exhaust
      "instantAfterFireSound": "event:>Vehicle>Afterfire>i5_01>single",
      "sustainedAfterFireSound": "event:>Vehicle>Afterfire>i5_01>multi",
      "shiftAfterFireSound": "event:>Vehicle>Afterfire>i5_01>shift",
      "particulates": 0.02,
      "afterFireAudibleThresholdInstant": 550000,
      "afterFireVisualThresholdSustained": 15000000,
      "instantAfterFireCoef": 1.55,
      "instantAfterFireVolumeCoef": 1.0,
      "afterFireAudibleThresholdShift": 187000,
      "shiftAfterFireVolumeCoef": 1.3,
      "afterFireAudibleThresholdSustained": 44000,
      "sustainedAfterFireCoef": 0.17,
      "sustainedAfterFireTime": 0.27,
      "sustainedAfterFireVolumeCoef": 0.87,
      //cooling and oil system
      "thermalsEnabled": true,
      "engineBlockMaterial": "aluminum",
      "oilVolume": 15,
      "engineBlockAirCoolingEfficiency": 790,
      //engine durability
      "cylinderWallTemperatureDamageThreshold": 77970,
      "headGasketDamageThreshold": 98990000,
      "pistonRingDamageThreshold": 98990000,
      "connectingRodDamageThreshold": 98990000,
      "maxTorqueRating": 2500,
      "maxOverTorqueDamage": 3500,
      //node beam interface
      "torqueReactionNodes:": ["e1l", "e2l", "e4r"],
      "waterDamage": {
        "[engineGroup]:": ["engine_intake"]
      },
      "radiator": {
        "[engineGroup]:": ["radiator"]
      },
      "engineBlock": {
        "[engineGroup]:": ["engine_block"]
      },
      "breakTriggerBeam": "engine",
      "uiName": "Engine",
      "soundConfig": "soundConfig",
      "soundConfigExhaust": "soundConfigExhaust",
      //engine deform groups
      "deformGroups": ["mainEngine", "mainEngine_intake", "mainEngine_accessories"],
      "deformGroups_oilPan": ["oilpan_damage"],
      //starter motor
      "starterSample": "event:>Engine>Starter>v8flat_1986_eng",
      "starterSampleExhaust": "event:>Engine>Starter>v8flat_1986_exh",
      "shutOffSampleEngine": "event:>Engine>Shutoff>v8flat_1986_eng",
      "shutOffSampleExhaust": "event:>Engine>Shutoff>v8flat_1986_exh",
      "starterVolume": 0.76,
      "starterVolumeExhaust": 0.76,
      "shutOffVolumeEngine": 0.76,
      "shutOffVolumeExhaust": 0.76,
      "starterThrottleKillTime": 0.96,
      "idleRPMStartRate": 2,
      "idleRPMStartCoef": 2
    },
    "soundConfig": {
      "sampleName": "V10_2_engine",
      "intakeMuffling": 0.55,
      "mainGain": -11,
      "onLoadGain": 1,
      "offLoadGain": 0.65,
      "maxLoadMix": 0.8,
      "minLoadMix": 0.0,
      "lowShelfGain": 5,
      "lowShelfFreq": 350,
      "highShelfGain": -1,
      "highShelfFreq": 2700,
      "eqLowGain": 2,
      "eqLowFreq": 700,
      "eqLowWidth": 0.15,
      "eqHighGain": -8,
      "eqHighFreq": 1800,
      "eqHighWidth": 0.25,
      "fundamentalFrequencyCylinderCount": 10,
      "eqFundamentalGain": 6
    },
    "soundConfigExhaust": {
      "sampleName": "V10_2_exhaust",
      "mainGain": -1,
      "onLoadGain": 1,
      "offLoadGain": 0.52,
      "maxLoadMix": 0.7,
      "minLoadMix": 0,
      "lowShelfGain": -5,
      "lowShelfFreq": 220,
      "highShelfGain": 2,
      "highShelfFreq": 4000,
      "eqLowGain": 10,
      "eqLowFreq": 600,
      "eqLowWidth": 0.15,
      "eqHighGain": 10,
      "eqHighFreq": 1500,
      "eqHighWidth": 0.1,
      "fundamentalFrequencyCylinderCount": 10,
      "eqFundamentalGain": -6
    },
    "vehicleController": {
      "clutchLaunchStartRPM": 1800,
      "clutchLaunchTargetRPM": 2500,
      "highShiftDownRPM": [0, 0, 0, 3500, 4200, 4400, 4500, 4600],
      "highShiftUpRPM": 7900,
      "revMatchThrottle": 0.7
    },
    "flexbodies": [
      ["mesh", "[group]:", "nonFlexMaterials"],
      ["scintilla_engine", ["scintilla_engine"]],
      {
        "deformGroup": "radhose_R_damage",
        "deformMaterialBase": "scintilla_engine",
        "deformMaterialDamaged": "invis"
      },
      ["scintilla_radhose_R", ["scintilla_chassis", "scintilla_engine"]],
      {
        "deformGroup": ""
      }
    ],
    "nodes": [
      ["id", "posX", "posY", "posZ"],
      {
        "selfCollision": false
      },
      {
        "collision": true
      },
      //engine box
      {
        "frictionCoef": 0.5
      },
      {
        "nodeMaterial": "|NM_METAL"
      },
      {
        "group": "scintilla_engine"
      },
      {
        "engineGroup": "engine_block"
      },
      {
        "nodeWeight": 21
      },
      {
        "chemEnergy": 500,
        "burnRate": 0.39,
        "flashPoint": 800,
        "specHeat": 0.2,
        "selfIgnitionCoef": false,
        "smokePoint": 650,
        "baseTemp": "thermals",
        "conductionRadius": 0.12
      },
      [
        "e1r",
        -0.143,
        0.605,
        0.154,
        {
          "isExhaust": "mainEngine"
        }
      ],
      [
        "e1l",
        0.143,
        0.605,
        0.154,
        {
          "isExhaust": "mainEngine"
        }
      ],
      ["e2r", -0.143, 1.198, 0.154],
      ["e2l", 0.143, 1.198, 0.154],
      {
        "engineGroup": ["engine_block", "engine_intake"]
      },
      {
        "selfCollision": true
      },
      ["e3r", -0.31, 0.605, 0.535],
      ["e3l", 0.31, 0.605, 0.535],
      ["e4r", -0.31, 1.198, 0.535],
      ["e4l", 0.31, 1.198, 0.535],
      {
        "chemEnergy": false,
        "burnRate": false,
        "flashPoint": false,
        "specHeat": false,
        "selfIgnitionCoef": false,
        "smokePoint": false,
        "baseTemp": false,
        "conductionRadius": false
      },
      {
        "engineGroup": ""
      },
      //engine mount nodes
      {
        "group": "scintilla_enignemounts"
      },
      [
        "em1r",
        -0.29,
        0.768,
        0.199,
        {
          "nodeWeight": 3
        }
      ],
      [
        "em1l",
        0.29,
        0.768,
        0.199,
        {
          "nodeWeight": 3
        }
      ],
      {
        "group": ""
      }
    ],
    "beams": [
      ["id1:", "id2:"],
      {
        "beamType": "|NORMAL",
        "beamLongBound": 1.0,
        "beamShortBound": 1.0
      },
      {
        "beamSpring": 11200800,
        "beamDamp": 258
      },
      {
        "beamDeform": 1120000,
        "beamStrength": "FLT_MAX"
      },
      //engine
      {
        "deformLimitExpansion": 1.2
      },
      {
        "deformGroup": "mainEngine",
        "deformationTriggerRatio": 0.001
      },
      ["e1r", "e1l"],
      ["e2r", "e2l"],
      ["e3r", "e3l"],
      ["e4r", "e4l"],
      ["e1r", "e2r"],
      ["e1l", "e2l"],
      ["e3r", "e4r"],
      ["e3l", "e4l"],
      ["e1r", "e3r"],
      ["e1l", "e3l"],
      ["e2r", "e4r"],
      ["e2l", "e4l"],
      ["e2r", "e3r"],
      ["e2l", "e3l"],
      ["e2r", "e3l"],
      ["e2l", "e3r"],
      ["e1r", "e4r"],
      ["e1l", "e4l"],
      ["e1r", "e4l"],
      ["e1l", "e4r"],
      ["e1r", "e2l"],
      ["e1l", "e2r"],
      ["e3r", "e4l"],
      ["e3l", "e4r"],
      ["e1r", "e3l"],
      ["e1l", "e3r"],
      ["e2r", "e4l"],
      ["e2l", "e4r"],
      {
        "deformGroup": ""
      },
      //engine mount nodes
      {
        "beamSpring": 2956300,
        "beamDamp": 130.43
      },
      {
        "beamDeform": 63000,
        "beamStrength": "FLT_MAX"
      },
      ["em1r", "e3l"],
      ["em1r", "e3r"],
      ["em1r", "e4l"],
      ["em1r", "e4r"],
      ["em1r", "e1r"],
      ["em1r", "e1l"],
      ["em1r", "e2l"],
      ["em1r", "e2r"],
      ["em1l", "e3l"],
      ["em1l", "e3r"],
      ["em1l", "e4l"],
      ["em1l", "e4r"],
      ["em1l", "e1r"],
      ["em1l", "e1l"],
      ["em1l", "e2l"],
      ["em1l", "e2r"],
      //radhose
      {
        "beamSpring": 15000,
        "beamDamp": 300
      },
      {
        "beamDeform": 1450,
        "beamStrength": 3600
      },
      {
        "deformGroup": "radhose_damage",
        "deformationTriggerRatio": 0.1
      },
      {
        "optional": true
      },
      ["em1l", "e3l"],
      ["em1l", "e3r"],
      ["em1l", "e4l"],
      ["em1l", "e4r"],
      ["em1l", "e1r"],
      ["em1l", "e1l"],
      ["em1l", "e2l"],
      ["em1l", "e2r"],
      ["e1r", "f3r"],
      ["e3r", "f3r"],
      ["e1r", "f3l"],
      ["e1l", "f3l"],
      ["e1l", "f3r"],
      ["e1l", "f9l"],
      ["e1r", "f9r"],
      ["e3l", "f3l"],
      {
        "optional": false
      },
      {
        "deformGroup": ""
      },
      {
        "beamPrecompression": 1,
        "beamType": "|NORMAL",
        "beamLongBound": 1.0,
        "beamShortBound": 1.0
      }
    ]
  },
  "scintilla_intake_7.0_turbo_stage4_venom": {
    "information": {
      "authors": "Venom",
      "name": "Stage 4 Venom Twin Turbocharger System",
      "value": 28900
    },
    "slotType": "scintilla_intake_5.0",
    "flexbodies": [
      ["mesh", "[group]:", "nonFlexMaterials"],
      ["scintilla_intake", ["scintilla_engine", "scintilla_engine_intake"]],
      ["scintilla_downpipe_turbo", ["scintilla_engine", "scintilla_turbo_R", "scintilla_turbo_L"]],
      ["scintilla_header_turbo", ["scintilla_headers", "scintilla_turbo_R", "scintilla_turbo_L"]],
      ["scintilla_turbo", ["scintilla_engine", "scintilla_turbo_R", "scintilla_turbo_L"]],
      ["scintilla_intercooler_R", ["scintilla_intercooler_R"]],
      ["scintilla_chargepipe_R", ["scintilla_intercooler_R", "scintilla_engine"]],
      ["scintilla_intercooler_L", ["scintilla_intercooler_L"]],
      ["scintilla_chargepipe_L", ["scintilla_intercooler_L", "scintilla_engine"]],
      ["scintilla_airbox", ["scintilla_airbox"]],
      ["scintilla_intake_pipe_turbo", ["scintilla_airbox", "scintilla_engine_intake"]]
    ],
    "variables": [
      ["name", "type", "unit", "category", "default", "min", "max", "title", "description"],
      ["$wastegateStart", "range", "psi", "Engine", 19, 10, 75, "Wastegate Pressure", "Pressure at which the wastegate begins to open", { "stepDis": 0.5 }]
    ],
    "turbocharger": {
      "bovSoundFileName": "event:>Vehicle>Forced_Induction>Turbo_06>turbo_bov_race",
      "hissLoopEvent": "event:>Vehicle>Forced_Induction>Turbo_06>turbo_hiss_race",
      "whineLoopEvent": "event:>Vehicle>Forced_Induction>Turbo_06>turbo_spin_race",
      "bovEnabled": true,
      "turboSizeCoef": 1.0,
      "bovSoundVolumeCoef": 0.95,
      "hissVolumePerPSI": 0.052,
      "whineVolumePer10kRPM": 0.012,
      "whinePitchPer10kRPM": 0.055,
      "wastegateStart": "$wastegateStart",
      "pressureRatePSI": 75,
      "maxExhaustPower": 398500,
      "backPressureCoef": 0.00051,
      "frictionCoef": 7.5,
      "inertia": 3.1,
      "damageThresholdTemperature": 989970,
      "pressurePSI": [
        //turbineRPM, pressure(PSI)
        [0, -2.5],
        [30000, -0.5],
        [60000, 10],
        [90000, 20],
        [150000, 32],
        [200000, 47],
        [250000, 67],
        [300000, 95]
      ],
      "engineDef": [
        //engineRPM, efficiency, exhaustFactor
        [0, 0.0, 0.0],
        [650, 0.03, 0.1],
        [1400, 0.05, 0.2],
        [2000, 0.18, 0.3],
        [3000, 0.44, 0.45],
        [4000, 0.6, 0.6],
        [5000, 0.67, 0.75],
        [6000, 0.89, 0.95],
        [7000, 0.98, 0.99],
        [8000, 0.99, 0.99],
        [9000, 0.8, 0.99],
        [10000, 0.58, 0.99],
        [11000, 0.3, 0.99]
      ]
    },
    "mainEngine": {
      //turbocharger name
      "turbocharger": "turbocharger",
      "$*instantAfterFireCoef": 5.0,
      "$*sustainedAfterFireCoef": 2.5,
      "$*sustainedAfterFireVolumeCoef": 1.1,

      //damage deformGroups
      "deformGroups_turbo": ["mainEngine_turbo_R", "mainEngine_turbo_L", "mainEngine_intercooler_R", "mainEngine_intercooler_L"]
    },
    "soundConfig": {
      "$+intakeMuffling": -0.3,
      "$+mainGain": 1.2,
      "$+offLoadGain": 0.09,
      "$+maxLoadMix": 0.2,
      "$+minLoadMix": 0.2,
      "$+lowShelfGain": -0.5,
      "$+highShelfGain": 2,
      "$+eqLowGain": 3,
      "$+eqHighGain": 1,
      "$+eqFundamentalGain": -0.5
    },
    "soundConfigExhaust": {
      "$+mainGain": -0.3,
      "$+maxLoadMix": 0.25,
      "$+minLoadMix": 0.125,
      "$+lowShelfGain": -0.3,
      "$+highShelfGain": 1.5,
      "$+eqLowGain": 0.0,
      "$+eqHighGain": 0.0,
      "$+eqFundamentalGain": -0.5
    },
    "vehicleController": {
      "transmissionGearChangeDelay": 0.5
    },
    "nodes": [
      ["id", "posX", "posY", "posZ"],
      { "selfCollision": false },
      { "collision": true },
      //intake
      { "frictionCoef": 0.5 },
      { "nodeMaterial": "|NM_METAL" },
      { "nodeWeight": 3.5 },
      { "group": ["scintilla_engine", "scintilla_engine_intake"] },
      ["intck1r", -0.207, 0.605, 0.737],
      ["intck1l", 0.207, 0.605, 0.737],
      ["intck2r", -0.207, 1.198, 0.737],
      ["intck2l", 0.207, 1.198, 0.737],
      { "nodeWeight": 1.5 },
      { "engineGroup": "engine_intake" },
      { "group": "scintilla_airbox" },
      ["intck3r", -0.508, 1.176, 0.748],
      ["intck3l", 0.508, 1.176, 0.748],
      ["intck4r", -0.355, 1.55, 0.682],
      ["intck4l", 0.355, 1.55, 0.682],
      { "engineGroup": "" },

      //turbos
      { "nodeWeight": 2.0 },
      ["trb1r", -0.409, 0.742, 0.442, { "group": "scintilla_turbo_R" }],
      ["trb1l", 0.409, 0.742, 0.442, { "group": "scintilla_turbo_L" }],
      ["trb2r", -0.409, 0.979, 0.442, { "group": "scintilla_turbo_R" }],
      ["trb2l", 0.409, 0.979, 0.442, { "group": "scintilla_turbo_L" }],

      //exhaust manifolds
      { "nodeWeight": 2.0 },
      { "group": "scintilla_headers" },
      [
        "exm1r",
        -0.292,
        1.401,
        0.446,
        {
          "afterFireAudioCoef": 1.0,
          "afterFireVisualCoef": 1.0,
          "afterFireVolumeCoef": 1.0,
          "afterFireMufflingCoef": 1.0,
          "exhaustAudioMufflingCoef": 1.0,
          "exhaustAudioGainChange": 0
        }
      ],
      [
        "exm1l",
        0.292,
        1.401,
        0.446,
        {
          "afterFireAudioCoef": 1.0,
          "afterFireVisualCoef": 1.0,
          "afterFireVolumeCoef": 1.0,
          "afterFireMufflingCoef": 1.0,
          "exhaustAudioMufflingCoef": 1.0,
          "exhaustAudioGainChange": 0
        }
      ],

      //intercoolers
      { "nodeWeight": 1.0 },
      { "group": "scintilla_intercooler_R" },
      ["ic1r", -0.703, 0.746, 0.399],
      ["ic2r", -0.601, 0.854, 0.849],
      ["ic3r", -0.84, 0.802, 0.45],
      ["ic4r", -0.759, 0.912, 0.836],
      { "group": "scintilla_intercooler_L" },
      ["ic1l", 0.703, 0.746, 0.399],
      ["ic2l", 0.601, 0.854, 0.849],
      ["ic3l", 0.84, 0.802, 0.45],
      ["ic4l", 0.759, 0.912, 0.836],
      { "deformGroup": "" },
      { "group": "" }
    ],
    "beams": [
      ["id1:", "id2:"],
      { "beamPrecompression": 1, "beamType": "|NORMAL", "beamLongBound": 1.0, "beamShortBound": 1.0 },
      //intake
      { "deformLimitExpansion": 1.2 },
      { "beamSpring": 3500940, "beamDamp": 235 },
      { "beamDeform": 105000, "beamStrength": "FLT_MAX" },
      ["intck2l", "intck2r"],
      ["intck2r", "intck1r"],
      ["intck1r", "intck1l"],
      ["intck1l", "intck2l"],
      ["intck2l", "intck1r"],
      ["intck2r", "intck1l"],
      ["intck2r", "e4r"],
      ["e4r", "intck1r"],
      ["intck1r", "e3r"],
      ["intck2r", "e3r"],
      ["intck2l", "e4l"],
      ["e4l", "intck1l"],
      ["intck1l", "e3l"],
      ["e3l", "intck2l"],
      ["intck2l", "e2l"],
      ["e2l", "intck1l"],
      ["intck1l", "e1l"],
      ["e1l", "intck2l"],
      ["intck2r", "e2r"],
      ["e2r", "intck1r"],
      ["intck1r", "e1r"],
      ["e1r", "intck2r"],

      //air boxes
      { "beamSpring": 2000000, "beamDamp": 130 },
      { "beamDeform": 15000, "beamStrength": "FLT_MAX" },
      ["intck3l", "intck4l"],
      ["intck4l", "intck4r"],
      ["intck4r", "intck3r"],
      ["intck3r", "intck3l"],
      ["intck3l", "intck4r"],
      ["intck4l", "intck3r"],
      ["intck3l", "intck2l"],
      ["intck3l", "intck1l"],
      ["intck3r", "intck2r"],
      ["intck3r", "intck1r"],
      ["intck4l", "intck2l"],
      ["intck4r", "intck2l"],
      ["intck4r", "intck2r"],
      ["intck4l", "intck2r"],
      ["intck4l", "e2l"],
      ["intck4l", "e2r"],
      ["intck3l", "e2l"],
      ["intck3r", "e2r"],
      ["intck4r", "e2r"],
      ["intck4r", "e2l"],
      ["intck3r", "e2l"],

      //turbos
      { "beamSpring": 3000000, "beamDamp": 130 },
      { "beamDeform": 90000, "beamStrength": "FLT_MAX" },
      { "deformGroup": "mainEngine_turbo_R", "deformationTriggerRatio": 0.001 },
      ["trb1r", "e3l"],
      ["trb1r", "e4l"],
      ["trb1r", "e3r"],
      ["trb1r", "e4r"],
      ["trb1r", "e1l"],
      ["trb1r", "e2l"],
      ["trb1r", "e1r", { "isExhaust": "mainEngine" }],
      ["trb1r", "e2r"],
      ["trb2r", "e3l"],
      ["trb2r", "e4l"],
      ["trb2r", "e3r"],
      ["trb2r", "e4r"],
      ["trb2r", "e1l"],
      ["trb2r", "e2l"],
      ["trb2r", "e1r"],
      ["trb2r", "e2r"],
      ["trb2r", "trb1r", { "isExhaust": "mainEngine" }],
      { "deformGroup": "mainEngine_turbo_L", "deformationTriggerRatio": 0.001 },
      ["trb1l", "e3l"],
      ["trb1l", "e4l"],
      ["trb1l", "e3r"],
      ["trb1l", "e4r"],
      ["trb1l", "e1l", { "isExhaust": "mainEngine" }],
      ["trb1l", "e2l"],
      ["trb1l", "e1r"],
      ["trb1l", "e2r"],
      ["trb2l", "e3l"],
      ["trb2l", "e4l"],
      ["trb2l", "e3r"],
      ["trb2l", "e4r"],
      ["trb2l", "e1l"],
      ["trb2l", "e2l"],
      ["trb2l", "e1r"],
      ["trb2l", "e2r"],
      ["trb2l", "trb1l", { "isExhaust": "mainEngine" }],
      { "deformGroup": "" },

      //intercoolers
      { "beamSpring": 1201000, "beamDamp": 130 },
      { "beamDeform": 15000, "beamStrength": "FLT_MAX" },
      { "deformGroup": "mainEngine_intercooler_R", "deformationTriggerRatio": 0.02 },
      ["ic3r", "ic1r"],
      ["ic1r", "ic2r"],
      ["ic2r", "ic4r"],
      ["ic4r", "ic1r"],
      ["ic3r", "ic2r"],
      ["ic4r", "ic3r"],
      { "deformGroup": "mainEngine_intercooler_L", "deformationTriggerRatio": 0.02 },
      ["ic3l", "ic1l"],
      ["ic1l", "ic2l"],
      ["ic2l", "ic4l"],
      ["ic4l", "ic1l"],
      ["ic3l", "ic2l"],
      ["ic4l", "ic3l"],

      //attach to engine
      { "beamSpring": 601000, "beamDamp": 50 },
      { "beamDeform": 7000, "beamStrength": 130000 },
      { "breakGroup": "mainEngine_intercooler_L" },
      ["ic1l", "e1l"],
      ["ic1l", "e2l"],
      ["ic1l", "e3l"],
      ["ic1l", "e4l"],
      ["ic2l", "e1l"],
      ["ic2l", "e2l"],
      ["ic2l", "e3l"],
      ["ic2l", "e4l"],
      ["ic3l", "e1l"],
      ["ic3l", "e2l"],
      ["ic3l", "e3l"],
      ["ic3l", "e4l"],
      ["ic4l", "e1l"],
      ["ic4l", "e2l"],
      ["ic4l", "e3l"],
      ["ic4l", "e4l"],
      { "breakGroup": "mainEngine_intercooler_R" },
      ["ic1r", "e1r"],
      ["ic1r", "e2r"],
      ["ic1r", "e3r"],
      ["ic1r", "e4r"],
      ["ic2r", "e1r"],
      ["ic2r", "e2r"],
      ["ic2r", "e3r"],
      ["ic2r", "e4r"],
      ["ic3r", "e1r"],
      ["ic3r", "e2r"],
      ["ic3r", "e3r"],
      ["ic3r", "e4r"],
      ["ic4r", "e1r"],
      ["ic4r", "e2r"],
      ["ic4r", "e3r"],
      ["ic4r", "e4r"],

      //attach to subframe
      { "breakGroup": "mainEngine_intercooler_R" },
      ["ic1r", "rx11rr"],
      ["ic1r", "rx2rr"],
      ["ic1r", "rx5rr"],
      ["ic3r", "rx11rr"],
      ["ic3r", "rx2rr"],
      ["ic3r", "rx5rr"],
      ["ic4r", "rx5rr"],
      ["ic4r", "rx6r"],
      ["ic2r", "rx5rr"],
      ["ic2r", "rx6r"],
      ["ic2r", "rx5r"],

      { "breakGroup": "mainEngine_intercooler_L" },
      ["ic1l", "rx11ll"],
      ["ic1l", "rx2ll"],
      ["ic1l", "rx5ll"],
      ["ic3l", "rx11ll"],
      ["ic3l", "rx2ll"],
      ["ic3l", "rx5ll"],
      ["ic4l", "rx5ll"],
      ["ic4l", "rx6l"],
      ["ic2l", "rx5ll"],
      ["ic2l", "rx6l"],
      ["ic2l", "rx5l"],

      //charge pipes
      { "beamSpring": 50000, "beamDamp": 130 },
      { "beamDeform": 10000, "beamStrength": 20000 },
      { "deformGroup": "mainEngine_chargepipe_R", "deformationTriggerRatio": 0.02 },
      ["trb1r", "intck4r"],
      ["trb1r", "intck3r"],
      ["ic2r", "intck1r", { "breakGroupType": 1 }],
      ["ic2r", "intck2r", { "breakGroupType": 1 }],
      { "deformGroup": "mainEngine_chargepipe_L", "deformationTriggerRatio": 0.02 },
      ["trb1l", "intck3l"],
      ["trb1l", "intck4l"],
      ["ic2l", "intck1l", { "breakGroupType": 1 }],
      ["ic2l", "intck2l", { "breakGroupType": 1 }],
      { "deformGroup": "" },
      { "breakGroup": "" },

      //exhaust manifolds
      { "beamSpring": 3000000, "beamDamp": 130 },
      { "beamDeform": 90000, "beamStrength": "FLT_MAX" },
      { "deformGroup": "mainEngine_turbo_R", "deformationTriggerRatio": 0.001 },
      ["exm1r", "e3l"],
      ["exm1r", "e4l"],
      ["exm1r", "e3r"],
      ["exm1r", "e4r"],
      ["exm1r", "e1l"],
      ["exm1r", "e2l"],
      ["exm1r", "e2r"],
      ["exm1r", "e1r"],
      ["exm1r", "trb2r", { "isExhaust": "mainEngine" }],
      { "deformGroup": "mainEngine_turbo_L", "deformationTriggerRatio": 0.001 },
      ["exm1l", "e3l"],
      ["exm1l", "e4l"],
      ["exm1l", "e3r"],
      ["exm1l", "e4r"],
      ["exm1l", "e1l"],
      ["exm1l", "e2l"],
      ["exm1l", "e1r"],
      ["exm1l", "e2r"],
      ["exm1l", "trb2l", { "isExhaust": "mainEngine" }],

      { "beamPrecompression": 1, "beamType": "|NORMAL", "beamLongBound": 1.0, "beamShortBound": 1.0 }
    ]
  },
  "scintilla_engine_5.0_ecu_venom": {
    "information": {
      "authors": "Venom",
      "name": "Venom ECU",
      "value": 7900
    },
    "slotType": "scintilla_engine_5.0_ecu",
    "variables": [
      ["name", "type", "unit", "category", "default", "min", "max", "title", "description"],
      ["$revLimiterRPM", "range", "rpm", "Engine", 9000, 4500, 11000, "RPM Limit", "RPM where the rev limiter prevents further revving", { "stepDis": 50 }],
      ["$revLimiterCutTime", "range", "s", "Engine", 0.1, 0.01, 0.5, "RPM Limit Cut Time", "How fast the rev limiter cycles", { "stepDis": 0.01 }],
      ["$venomFineTuneLowRPM", "range", "nm", "Engine", 0, -500, 1800, "KuyPeD tuning low RPM", "KuyPeD fine ecu torque low RPM (1000-6000) gain tuning", { "stepDis": 1 }],
      ["$venomFineTuneHighRPM", "range", "nm", "Engine", 0, -500, 3000, "KuyPeD tuning high RPM", "KuyPeD fine ecu torque high RPM (6000-11000) gain tuning", { "stepDis": 1 }]
    ],
    "controller": [["fileName"]],
    "mainEngine": {
      "torqueModIntake": [
        ["rpm", "torque"],
        [500, "$venomFineTuneLowRPM"],
        [1000, "$venomFineTuneLowRPM"],
        [2000, "$venomFineTuneLowRPM"],
        [2500, "$venomFineTuneLowRPM"],
        [3000, "$venomFineTuneLowRPM"],
        [4000, "$venomFineTuneLowRPM"],
        [5000, "$venomFineTuneLowRPM"],
        [6000, "$venomFineTuneHighRPM"],
        [7000, "$venomFineTuneHighRPM"],
        [8000, "$venomFineTuneHighRPM"],
        [9000, "$venomFineTuneHighRPM"],
        [10000, "$venomFineTuneHighRPM"],
        [11000, "$venomFineTuneHighRPM"],
        [12000, "$venomFineTuneHighRPM"]
      ],
      "$+idleRPM": 350,
      "revLimiterRPM": "$revLimiterRPM",
      "revLimiterType": "timeBased",
      "revLimiterCutTime": "$revLimiterCutTime",
      "$*instantAfterFireCoef": 1.4,
      "$*shiftAfterFireVolumeCoef": 1.4,
      "$*sustainedAfterFireCoef": 2.0,
      "$*sustainedAfterFireTime": 2.0,
      "$*sustainedAfterFireVolumeCoef": 1.2
    },
    "vehicleController": {
      "highShiftUpRPM": "$=$revLimiterRPM-800"
    }
  },
  "scintilla_radiator_venom": {
    "information": {
      "authors": "Venom",
      "name": "Venom Radiators",
      "value": 7500
    },
    "slotType": "scintilla_radiator",
    "flexbodies": [
      ["mesh", "[group]:", "nonFlexMaterials"],
      ["scintilla_radiator_R", ["scintilla_radiator_R"]],
      ["scintilla_radiator_L", ["scintilla_radiator_L"]],
      ["scintilla_radhose_M", ["scintilla_chassis"]],
      ["scintilla_coolantlines_brackets", ["scintilla_chassis"]],
      {
        "deformGroup": "radhose_F_damage",
        "deformMaterialBase": "scintilla_engine",
        "deformMaterialDamaged": "invis"
      },
      ["scintilla_radhose_F", ["scintilla_radiator_R", "scintilla_radiator_L", "scintilla_chassis"]],
      {
        "deformGroup": "radhose_FR_damage",
        "deformMaterialBase": "scintilla_engine",
        "deformMaterialDamaged": "invis"
      },
      ["scintilla_radhose_FR", ["scintilla_radiator_R", "scintilla_chassis"]],
      {
        "deformGroup": "radhose_FL_damage",
        "deformMaterialBase": "scintilla_engine",
        "deformMaterialDamaged": "invis"
      },
      ["scintilla_radhose_FL", ["scintilla_radiator_L", "scintilla_chassis"]],
      {
        "deformGroup": ""
      }
    ],
    "props": [
      ["func", "mesh", "idRef:", "idX:", "idY:", "baseRotation", "rotation", "translation", "min", "max", "offset", "multiplier"]
      //["radiatorFanSpin"  , "scintilla_radiator_fan_R", "rad1r","rad1l","fx1r",  {"x":9, "y":0, "z":180}, {"x":0, "y":0.25, "z":0}    , {"x":0, "y":0, "z":0}, -360, 360, 0, 1],
      //["radiatorFanSpin"  , "scintilla_radiator_fan_L", "rad1l","rad1r","fx1l",  {"x":9, "y":0, "z":0},   {"x":0, "y":0.25, "z":0}    , {"x":0, "y":0, "z":0}, -360, 360, 0, 1]
    ],
    "mainEngine": {
      "radiatorArea": 0.75,
      "radiatorEffectiveness": 779700,
      "coolantVolume": 25.5,
      "radiatorFanType": "electric",
      "radiatorFanTemperature": 15,
      "radiatorFanMaxAirSpeed": 7,
      "thermostatTemperature": 72,
      "radiatorFanLoopEvent": "event:>Vehicle>Cooling Fan>Electric_05",
      "radiatorFanVolume": 0.95,
      //damage deformGroups
      "deformGroups_radiator": ["radiator_damage", "radhose_R_damage", "radhose_F_damage", "radhose_FR_damage", "radhose_FL_damage"]
    },
    "nodes": [
      ["id", "posX", "posY", "posZ"],
      //radiator
      {
        "frictionCoef": 0.5
      },
      {
        "nodeMaterial": "|NM_METAL"
      },
      {
        "selfCollision": false
      },
      {
        "collision": true
      },
      {
        "engineGroup": "radiator"
      },
      {
        "nodeWeight": 0.3
      },
      {
        "group": "scintilla_radiator_R"
      },
      {
        "chemEnergy": 200,
        "burnRate": 0.4,
        "flashPoint": 250,
        "specHeat": 0.8,
        "smokePoint": 150,
        "selfIgnitionCoef": false
      },
      ["rad1r", -0.491, -2.077, 0.159],
      ["rad1rr", -0.825, -1.815, 0.159],
      ["rad2r", -0.563, -1.97, 0.512],
      ["rad2rr", -0.807, -1.776, 0.512],
      ["rad3r", -0.616, -1.844, 0.159],
      ["rad4r", -0.642, -1.819, 0.512],
      {
        "group": "scintilla_radiator_L"
      },
      ["rad1l", 0.491, -2.077, 0.159],
      ["rad1ll", 0.825, -1.815, 0.159],
      ["rad2l", 0.563, -1.97, 0.512],
      ["rad2ll", 0.807, -1.776, 0.512],
      ["rad3l", 0.616, -1.844, 0.159],
      ["rad4l", 0.642, -1.819, 0.512],
      {
        "chemEnergy": false,
        "burnRate": false,
        "flashPoint": false,
        "specHeat": false,
        "smokePoint": false,
        "selfIgnitionCoef": false
      },
      {
        "engineGroup": ""
      },
      {
        "group": ""
      }
    ],
    "beams": [
      ["id1:", "id2:"],
      {
        "beamType": "|NORMAL",
        "beamLongBound": 1.0,
        "beamShortBound": 1.0
      },
      //radiator main shape
      {
        "beamSpring": 401000,
        "beamDamp": 30
      },
      {
        "beamDeform": 16500,
        "beamStrength": "FLT_MAX"
      },
      {
        "deformGroup": "radiator_damage",
        "deformationTriggerRatio": 0.03
      },
      {
        "deformLimitExpansion": 1.1
      },
      ["rad1r", "rad1rr"],
      ["rad1rr", "rad2rr"],
      ["rad2rr", "rad2r"],
      ["rad2r", "rad1r"],
      ["rad1rr", "rad3r"],
      ["rad3r", "rad1r"],
      ["rad3r", "rad4r"],
      ["rad4r", "rad2rr"],
      ["rad4r", "rad2r"],
      ["rad1l", "rad1ll"],
      ["rad1ll", "rad2ll"],
      ["rad2ll", "rad2l"],
      ["rad2l", "rad1l"],
      ["rad1ll", "rad3l"],
      ["rad3l", "rad1l"],
      ["rad3l", "rad4l"],
      ["rad4l", "rad2ll"],
      ["rad4l", "rad2l"],
      //crossing
      {
        "beamDeform": 5500,
        "beamStrength": "FLT_MAX"
      },
      {
        "deformLimitExpansion": ""
      },
      ["rad1r", "rad2rr"],
      ["rad2r", "rad1rr"],
      ["rad1rr", "rad4r"],
      ["rad2rr", "rad3r"],
      ["rad3r", "rad2r"],
      ["rad1r", "rad4r"],
      ["rad1l", "rad2ll"],
      ["rad2l", "rad1ll"],
      ["rad1ll", "rad4l"],
      ["rad2ll", "rad3l"],
      ["rad3l", "rad2l"],
      ["rad1l", "rad4l"],
      //attach
      {
        "beamSpring": 301000,
        "beamDamp": 10
      },
      {
        "beamDeform": 4500,
        "beamStrength": 118000
      },
      {
        "deformGroup": "radiator_damage",
        "deformationTriggerRatio": 0.2
      },
      {
        "breakGroup": "radiator_R1"
      },
      ["rad2rr", "f13rr"],
      ["rad2rr", "fx3r"],
      ["rad2r", "bbf2r"],
      ["rad2r", "fx3r"],
      ["rad2r", "bbf1r"],
      ["rad2r", "fx1r"],
      ["rad2rr", "fx1r"],
      ["rad2rr", "f6rrr"],
      ["rad4r", "f13rr"],
      ["rad4r", "fx1r"],
      ["rad4r", "fx3r"],
      ["rad2r", "f13rr"],
      {
        "breakGroup": "radiator_L1"
      },
      ["rad2ll", "f13ll"],
      ["rad2ll", "fx3l"],
      ["rad2l", "bbf2l"],
      ["rad2l", "fx3l"],
      ["rad2l", "bbf1l"],
      ["rad2l", "fx1l"],
      ["rad2ll", "fx1l"],
      ["rad2ll", "f6lll"],
      ["rad4l", "f13ll"],
      ["rad4l", "fx1l"],
      ["rad4l", "fx3l"],
      ["rad2l", "f13ll"],
      //to front bupmer support
      {
        "optional": true
      },
      {
        "breakGroup": "radiator_R2"
      },
      ["rad1rr", "fbs2rrr"],
      ["rad1rr", "fbs1rr"],
      ["rad1rr", "fbs4rrr"],
      ["rad1rr", "fbs2rr"],
      ["rad1r", "fbs1r"],
      ["rad1r", "fbs1rr"],
      ["rad1r", "fbs3r"],
      ["rad1r", "fbs2rrr"],
      {
        "breakGroup": "radiator_L2"
      },
      ["rad1ll", "fbs2lll"],
      ["rad1ll", "fbs1ll"],
      ["rad1ll", "fbs4lll"],
      ["rad1ll", "fbs2ll"],
      ["rad1l", "fbs1l"],
      ["rad1l", "fbs1ll"],
      ["rad1l", "fbs3l"],
      ["rad1l", "fbs2lll"],
      {
        "optional": false
      },
      {
        "breakGroup": ""
      },
      //radhose
      {
        "beamSpring": 50000,
        "beamDamp": 20
      },
      {
        "beamDeform": 1450,
        "beamStrength": 12600
      },
      {
        "deformGroup": "radhose_R_damage",
        "deformationTriggerRatio": 0.02
      },
      {
        "breakGroup": "radhose_R"
      },
      ["e3l", "f4l"],
      ["f4l", "e1l"],
      ["f4r", "e3l"],
      ["e1l", "f4r"],
      ["f4r", "e3r"],
      ["f4r", "e1r"],
      ["f4r", "e1l"],
      ["f4r", "e3l"],
      {
        "breakGroup": ""
      },
      {
        "deformGroup": "radhose_F_damage",
        "deformationTriggerRatio": 0.02
      },
      {
        "breakGroup": "radhose_F"
      },
      ["rad1l", "rad1r"],
      ["rad1r", "rad2l"],
      ["rad2r", "rad1l"],
      ["rad1l", "bbf1r"],
      ["rad2l", "bbf1r"],
      ["rad1r", "bbf1l"],
      ["rad2r", "bbf1l"],
      {
        "deformGroup": "radhose_FR_damage",
        "deformationTriggerRatio": 0.02
      },
      {
        "breakGroup": "radhose_FR"
      },
      ["rad2r", "fx2r"],
      ["rad2rr", "fx2r"],
      ["rad1rr", "fx2r"],
      ["rad1r", "fx2r"],
      {
        "deformGroup": "radhose_FL_damage",
        "deformationTriggerRatio": 0.02
      },
      {
        "breakGroup": "radhose_FL"
      },
      ["rad2l", "fx2l"],
      ["rad2ll", "fx2l"],
      ["rad1ll", "fx2l"],
      ["rad1l", "fx2l"],
      {
        "deformGroup": ""
      },
      {
        "breakGroup": ""
      },
      //support beams
      {
        "beamType": "|SUPPORT",
        "beamLongBound": 10
      },
      {
        "beamSpring": 101000,
        "beamDamp": 5
      },
      {
        "beamDeform": 8080,
        "beamStrength": 178000
      },
      ["rad1rr", "f13rr"],
      ["rad1r", "bbf2r"],
      ["rad2r", "fx3r"],
      ["rad2rr", "fx3r"],
      ["rad2rr", "bbf2r"],
      ["rad1rr", "fx1r"],
      ["rad1ll", "f13ll"],
      ["rad1l", "bbf2l"],
      ["rad2l", "fx3l"],
      ["rad2ll", "fx3l"],
      ["rad2ll", "bbf2l"],
      ["rad1ll", "fx1l"],
      //to front bumper support
      {
        "optional": true
      },
      ["fbs2rrr", "rad2rr"],
      ["fbs1r", "rad2r"],
      ["fbs1rr", "rad2r"],
      ["fbs1rr", "rad2rr"],
      ["fbs2rr", "rad2r"],
      ["fbs2rr", "rad2rr"],
      ["rad1rr", "fbs2rrr"],
      ["fbs1rr", "rad1rr"],
      ["rad1rr", "fbs2rr"],
      ["rad1r", "fbs1r"],
      ["rad1r", "fbs3r"],
      ["fbs2lll", "rad2ll"],
      ["fbs1l", "rad2l"],
      ["fbs1ll", "rad2l"],
      ["fbs1ll", "rad2ll"],
      ["fbs2ll", "rad2l"],
      ["fbs2ll", "rad2ll"],
      ["rad1ll", "fbs2lll"],
      ["fbs1ll", "rad1ll"],
      ["rad1ll", "fbs2ll"],
      ["rad1l", "fbs1l"],
      ["rad1l", "fbs3l"],
      {
        "optional": false
      },
      {
        "beamPrecompression": 1,
        "beamType": "|NORMAL",
        "beamLongBound": 1.0,
        "beamShortBound": 1.0
      },
      {
        "beamPrecompression": 1,
        "beamType": "|NORMAL",
        "beamLongBound": 1.0,
        "beamShortBound": 1.0
      }
    ],
    "triangles": [
      ["id1:", "id2:", "id3:"],
      //radiators
      {
        "groundModel": "metal"
      },
      {
        "dragCoef": 2
      },
      {
        "triangleType": "NONCOLLIDABLE"
      },
      {
        "group": "scintilla_radiator_R"
      },
      ["rad1r", "rad2r", "rad1rr"],
      ["rad2rr", "rad1rr", "rad2r"],
      ["rad1r", "rad1rr", "rad3r"],
      ["rad1rr", "rad4r", "rad3r"],
      ["rad2rr", "rad4r", "rad1rr"],
      ["rad3r", "rad4r", "rad2r"],
      ["rad3r", "rad2r", "rad1r"],
      ["rad2r", "rad4r", "rad2rr"],
      {
        "group": "scintilla_radiator_L"
      },
      ["rad2l", "rad1l", "rad1ll"],
      ["rad1ll", "rad2ll", "rad2l"],
      ["rad1ll", "rad1l", "rad3l"],
      ["rad4l", "rad1ll", "rad3l"],
      ["rad4l", "rad2ll", "rad1ll"],
      ["rad4l", "rad3l", "rad2l"],
      ["rad2l", "rad3l", "rad1l"],
      ["rad4l", "rad2l", "rad2ll"],
      {
        "triangleType": "NORMALTYPE"
      },
      {
        "group": ""
      }
    ]
  }
}
