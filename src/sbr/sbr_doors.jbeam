{
"sbr_door_R": {
    "information":{
        "authors":"BeamNG",
        "name":"Right Door",
        "value":800,
    },
    "slotType" : "sbr_door_R",
    "slots": [
        ["type", "default", "description"],
        ["sbr_mirror_R","sbr_mirror_R", "Right Mirror"],
        ["sbr_doorpanel_R","sbr_doorpanel_R", "Right Door Panel"],
        ["sbr_doorglass_R","sbr_doorglass_R", "Right Door Glass"],
        ["sbr_doordetent_R","sbr_doordetent_R", "Right Door Detent"],
    ],
    "flexbodies": [
         ["mesh", "[group]:", "nonFlexMaterials"],
         ["sbr_door_R", ["sbr_door_R"]],
         {"deformGroup":""},
    ],
    "controller": [
        ["fileName"],
        ["advancedCouplerControl", {"name":"doorRCoupler"}],
    ]
    "doorRCoupler":{
        "couplerNodes":[
            ["cid1",    "cid2",     "autoCouplingStrength",    "autoCouplingRadius",   "autoCouplingLockRadius",   "autoCouplingSpeed", "couplingStartRadius",  "breakGroup"]
            ["q7r",     "d14rr",     35000,                     0.01,                   0.005,                      0.2,                 0.1,                   "doorR_latch"]
        ]
        "groupType": "autoCoupling",
        "attachSoundVolume":1,
        "detachSoundVolume":1,
        "soundNode:":["d14rr"]
        "attachSoundEvent": "event:>Vehicle>Latches>Door>modern_09_close",
        "detachSoundEvent": "event:>Vehicle>Latches>Door>modern_09_open",
        "breakSoundEvent":""
        "openForceMagnitude": 70,
        "openForceDuration": 0.6,
        "closeForceMagnitude": 70,
        "closeForceDuration": 0.6
    }
    "triggers":[
        ["id", "label", "idRef:", "idX:", "idY:", "type", "size", "baseRotation", "rotation", "translation", "baseTranslation"],
        ["door_R", "", "d9r","d8r","d6r", "box", {"x":0.145, "y":0.02, "z":0.030}, {"x":0, "y":-1, "z":0}, {"x":0, "y":0, "z":0}, {"x":0, "y":0, "z":0}, {"x":0.08, "y":0.065, "z":0.01}],
        ["door_R_int", "", "d7r","d8r","d4r", "box", {"x":0.17, "y":0.03, "z":0.06}, {"x":-13, "y":0, "z":1}, {"x":0, "y":0, "z":0}, {"x":0, "y":0, "z":0}, {"x":0.25, "y":0.042, "z":0.088}],
    ],
    "triggerEventLinks":[
        ["triggerId:triggers", "action", "targetEventId:events"],
        ["door_R", "action0", "doorLatch_R"],
        ["door_R_int", "action0", "doorLatch_R"],
    ],
    "events":[
        ["id", "title", "desc"],
        ["doorLatch_R", "ui.inputActions.sbr.doorLatch_R.title", "ui.inputActions.sbr.doorLatch_R.description", {"onUp":"controller.getControllerSafe('doorRCoupler').toggleGroup()"}],
    ],
    "sounds": {
        "$+cabinFilterCoef": 0.04
    },
    "nodes": [
         ["id", "posX", "posY", "posZ"],
         {"nodeMaterial":"|NM_METAL"},
         {"frictionCoef":0.5},
         //--DOOR--
         {"collision":true},
         {"selfCollision":true},
         {"group":"sbr_door_R"},
         {"nodeWeight":1.6},
         ["d1r",-0.85,-0.68, 0.21],
         ["d2r",-0.855,-0.11, 0.225],
         ["d3r",-0.86,0.44, 0.24],

         ["d4r",-0.885,-0.68, 0.49],
         ["d5r",-0.89,-0.11,0.49, {"selfCollision":false}],
         ["d6r",-0.895,0.46,  0.49],

         ["d7r",-0.815,-0.65, 0.78],
         ["d8r",-0.815,-0.12, 0.79, {"selfCollision":false}],
         ["d9r",-0.815,0.45, 0.795],

         //rigidifier
         {"selfCollision":false},
         {"collision":false},
         {"nodeWeight":1.9},
         ["d14r",-0.46, 0.17, 0.52],
         {"group":""},

         //latch node
         {"nodeWeight":0.45},
         ["d14rr", -0.88, 0.50, 0.49],
    ],
    "beams": [
          ["id1:", "id2:"],
          //--DOOR--
          {"beamType":"|NORMAL", "beamLongBound":1.0, "beamShortBound":1.0},
          {"beamSpring":1450550,"beamDamp":75},
          {"beamDeform":32400,"beamStrength":"FLT_MAX"},
          //door main shape
          {"deformLimitExpansion":1.1},
          ["d1r","d2r"],
          ["d2r","d3r"],
          ["d4r","d5r"],
          ["d5r","d6r"],
          ["d7r","d8r"],
          ["d8r","d9r"],

          ["d1r","d4r"],
          ["d2r","d5r"],
          ["d3r","d6r"],
          ["d4r","d7r"],
          ["d5r","d8r"],
          ["d6r","d9r"],

          //corner rigids
          {"deformLimitExpansion":""},
          {"beamDeform":5000,"beamStrength":"FLT_MAX"},
          ["d1r","d7r"],
          ["d2r","d8r"],
          ["d3r","d9r"],

          //lengthwise
          ["d1r","d3r"],
          ["d4r","d6r"],
          ["d7r","d9r"],
          ["d4r","d3r"],

          {"beamDeform":7000,"beamStrength":"FLT_MAX"},
          //surficial crossing
          ["d1r","d5r"],
          ["d2r","d4r"],
          ["d2r","d6r"],
          ["d3r","d5r"],
          ["d4r","d8r"],
          ["d5r","d7r"],
          ["d5r","d9r"],
          ["d6r","d8r"],

          //rigidifier
          {"beamSpring":1450550,"beamDamp":75},
          {"beamDeform":4500,"beamStrength":"FLT_MAX"},
          {"deformGroup":"doorglass_R_break","deformationTriggerRatio":0.05},
          ["d1r","d14r"],
          ["d2r","d14r"],
          ["d3r","d14r"],
          ["d4r","d14r"],
          ["d5r","d14r"],
          ["d6r","d14r"],
          ["d7r","d14r"],
          ["d8r","d14r"],
          ["d9r","d14r"],
          {"deformGroup":""},

          //latch node
          {"beamDeform":2500,"beamStrength":"FLT_MAX"},
          ["d9r","d14rr",{"deformLimitExpansion":0.01}],
          ["d6r","d14rr",{"deformLimitExpansion":0.01}],
          ["d8r","d14rr"],
          ["d5r","d14rr"],
          ["d2r","d14rr"],
          ["d3r","d14rr"],

          //pop open
          {"breakGroupType":1},
          {"beamType":"|SUPPORT", "beamLongBound":8},
          {"beamSpring":55000,"beamDamp":1320},
          {"beamDeform":40000,"beamStrength":10000},
          {"beamPrecompression":1.017},
          ["f8r", "d6r", {"breakGroup":"doorhinge_a_R"}],
          {"beamType":"|NORMAL", "beamLongBound":1, "beamShortBound":1},
          {"beamPrecompression":1},
          {"breakGroupType":0},
          {"breakGroup":""},

          //door support beams
          {"disableTriangleBreaking":true},
          {"beamType":"|SUPPORT","beamLongBound":6},
          {"beamSpring":1450550,"beamDamp":0},
          {"beamDeform":9750,"beamStrength":200000},
          ["d3r","q1r"],
          ["d8r","q1r"],
          ["d5r","q7r"],
          ["d9r","rf3r"],
          ["d4r","f1rr", {"beamPrecompression":0.93}],
          ["d4r","f6rr", {"beamPrecompression":0.93}],
          ["d9r","f3rr"],
          ["d9r","f8r"],
          ["d1r","f1r"],
          ["d2r","f2r"],
          ["d3r","f3r"],
          ["d3r","f4rr"],
          ["d4r","f5r", {"beamPrecompression":0.92}],
          ["d7r","f6r"],
          ["d8r","f6rr", {"beamPrecompression":0.92}],
          ["d10r","rf1", {"optional":true}],
          ["d11r","rf3", {"optional":true}],

          //door hinge
          {"beamType":"|NORMAL", "beamLongBound":1, "beamShortBound":1},
          {"beamSpring":3301000,"beamDamp":100},
          {"beamDeform":25800,"beamStrength":98000},
          {"breakGroup":"doorhinge_a_R"},
          ["d7r", "f6rr", {"deformGroup":"mirrorsignal_R_break","deformationTriggerRatio":0.1}],
          ["d7r", "f5rr"],
          ["d7r", "f6r"],
          ["d7r", "f5r"],
          ["d1r", "f2rr"],

          {"breakGroup":"doorhinge_b_R"},
          ["d1r", "f1rr"],
          ["d1r", "f5rr"],
          ["d1r", "f1r"],
          ["d1r", "f5r"],
          ["d7r", "p1r"],
          {"breakGroup":""},

          //damping
          {"beamDeform":27600,"beamStrength":225000},
          {"beamSpring":0,"beamDamp":17.5},
          {"breakGroup":"doorhinge_a_R"},
          ["d3r","f3r"],
          {"breakGroup":"doorhinge_b_R"},
          ["d9r","f3r"],
          {"breakGroup":""},
/*
          //--LATCH--
          {"beamSpring":3301000,"beamDamp":100},
          {"beamDeform":24000,"beamStrength":48000},
          {"breakGroup":"doorlatch_R"},
          ["d6r","f8r"],
          ["d14r","q7r"],
          ["d6r","q7r"],
          ["d3r","f3rr"],
          ["d9r","q1r"],
          //anti-pop beam
          ["d11r","rf3r", {"optional":true}],
          ["d10r","rf1r", {"optional":true}],
          {"breakGroup":""},
*/
          {"disableTriangleBreaking":false},
          {"beamPrecompression":1, "beamType":"|NORMAL", "beamLongBound":1.0, "beamShortBound":1.0},
          {"deformLimitExpansion":1.1},
    ],
    "triangles": [
            ["id1:","id2:","id3:"],
            //right door
            {"groundModel":"metal"},
            {"dragCoef":10},
            {"group":"sbr_door_R"},
            ["d4r","d2r","d1r"],
            ["d4r","d5r","d2r"],
            ["d5r","d3r","d2r"],
            ["d5r","d6r","d3r"],
            ["d7r","d5r","d4r"],
            ["d7r","d8r","d5r"],
            ["d8r","d6r","d5r"],
            ["d8r","d9r","d6r"],
            {"dragCoef":0},
            //door edge tris
            //{"breakGroup":["doorhinge_a_R","doorhinge_b_R"]}
            //["d9r", "q1r", "d6r"],
            //["q1r", "q7r", "d6r"],
            //["q7r", "f3rr", "d6r"],
            //["d6r", "f3rr", "d3r"],
            //["d7r", "f5rr", "f6rr"],
            //["d7r", "d4r", "f5rr"],
            //["d4r", "d1r", "f5rr"],
            //["f5rr", "d1r", "f1rr"],
            //{"breakGroup":""}
            {"group":""},
    ],
},
"sbr_door_L": {
    "information":{
        "authors":"BeamNG",
        "name":"Left Door",
        "value":800,
    },
    "slotType" : "sbr_door_L",
    "slots": [
        ["type", "default", "description"],
        ["sbr_mirror_L","sbr_mirror_L", "Left Mirror"],
        ["sbr_doorpanel_L","sbr_doorpanel_L", "Left Door Panel"],
        ["sbr_doorglass_L","sbr_doorglass_L", "Left Door Glass"],
        ["sbr_doordetent_L","sbr_doordetent_L", "Left Door Detent"],
    ],
    "flexbodies": [
         ["mesh", "[group]:", "nonFlexMaterials"],
         ["sbr_door_L", ["sbr_door_L"]],
         {"deformGroup":""},
    ],
    "controller": [
        ["fileName"],
        ["advancedCouplerControl", {"name":"doorLCoupler"}],
    ]
    "doorLCoupler":{
        "couplerNodes":[
            ["cid1",    "cid2",     "autoCouplingStrength",    "autoCouplingRadius",   "autoCouplingLockRadius",   "autoCouplingSpeed", "couplingStartRadius",  "breakGroup"]
            ["q7l",      "d14ll",    35000,                     0.01,                   0.005,                      0.2,                 0.1,                   "doorL_latch"]
        ]
        "groupType": "autoCoupling",
        "attachSoundVolume":1,
        "detachSoundVolume":1,
        "soundNode:":["d14ll"]
        "attachSoundEvent": "event:>Vehicle>Latches>Door>modern_09_close",
        "detachSoundEvent": "event:>Vehicle>Latches>Door>modern_09_open",
        "breakSoundEvent":""
        "openForceMagnitude": 70,
        "openForceDuration": 0.6,
        "closeForceMagnitude": 70,
        "closeForceDuration": 0.6
    }
    "triggers":[
        ["id", "label", "idRef:", "idX:", "idY:", "type", "size", "baseRotation", "rotation", "translation", "baseTranslation"],
        ["door_L", "", "d9l","d8l","d6l", "box", {"x":0.145, "y":0.02, "z":0.030}, {"x":0, "y":-1, "z":0}, {"x":0, "y":0, "z":0}, {"x":0, "y":0, "z":0}, {"x":0.08, "y":0.065, "z":-0.03}],
        ["door_L_int", "", "d7l","d8l","d4l", "box", {"x":0.17, "y":0.03, "z":0.06}, {"x":13, "y":0, "z":1}, {"x":0, "y":0, "z":0}, {"x":0, "y":0, "z":0}, {"x":0.25, "y":0.035, "z":-0.118}],
    ],
    "triggerEventLinks":[
        ["triggerId:triggers", "action", "targetEventId:events"],
        ["door_L", "action0", "doorLatch_L"],
        ["door_L_int", "action0", "doorLatch_L"],
    ],
    "events":[
        ["id", "title", "desc"],
        ["doorLatch_L", "ui.inputActions.sbr.doorLatch_L.title", "ui.inputActions.sbr.doorLatch_L.description", {"onUp":"controller.getControllerSafe('doorLCoupler').toggleGroup()"}],
    ],
    "sounds": {
        "$+cabinFilterCoef": 0.04
    },
    "nodes": [
         ["id", "posX", "posY", "posZ"],
         {"nodeMaterial":"|NM_METAL"},
         {"frictionCoef":0.5},
         //--DOOR--
         {"collision":true},
         {"selfCollision":true},
         {"group":"sbr_door_L"},
         {"nodeWeight":1.6},
         ["d1l", 0.85,-0.68, 0.21],
         ["d2l", 0.855,-0.11, 0.225],
         ["d3l", 0.86,0.44, 0.24],

         ["d4l", 0.885,-0.68, 0.49],
         ["d5l", 0.89,-0.11,0.49, {"selfCollision":false}],
         ["d6l", 0.895,0.46,  0.49],

         ["d7l", 0.815,-0.65, 0.78],
         ["d8l", 0.815,-0.12, 0.79, {"selfCollision":false}],
         ["d9l", 0.815,0.45, 0.795],

         //rigidifier
         {"selfCollision":false},
         {"collision":false},
         {"nodeWeight":1.9},
         ["d14l", 0.46, 0.17, 0.52],
         {"group":""},

         //latch node
         {"nodeWeight":0.45},
         ["d14ll", 0.88, 0.50, 0.49],
    ],
    "beams": [
          ["id1:", "id2:"],
          //--DOOR--
          {"beamType":"|NORMAL", "beamLongBound":1.0, "beamShortBound":1.0},
          {"beamSpring":1450550,"beamDamp":75},
          {"beamDeform":32400,"beamStrength":"FLT_MAX"},
          {"deformLimitExpansion":1.1},
          //door main shape
          ["d1l","d2l"],
          ["d2l","d3l"],
          ["d4l","d5l"],
          ["d5l","d6l"],
          ["d7l","d8l"],
          ["d8l","d9l"],

          ["d1l","d4l"],
          ["d2l","d5l"],
          ["d3l","d6l"],
          ["d4l","d7l"],
          ["d5l","d8l"],
          ["d6l","d9l"],

          //corner rigids
          {"deformLimitExpansion":""},
          {"beamDeform":5000,"beamStrength":"FLT_MAX"},
          ["d1l","d7l"],
          ["d2l","d8l"],
          ["d3l","d9l"],

          //lengthwise
          ["d1l","d3l"],
          ["d4l","d6l"],
          ["d7l","d9l"],
          ["d4l","d3l"],

          {"beamDeform":7000,"beamStrength":"FLT_MAX"},
          //surficial crossing
          ["d1l","d5l"],
          ["d2l","d4l"],
          ["d2l","d6l"],
          ["d3l","d5l"],
          ["d4l","d8l"],
          ["d5l","d7l"],
          ["d5l","d9l"],
          ["d6l","d8l"],

          //rigidifier
          {"beamSpring":1450550,"beamDamp":75},
          {"beamDeform":4500,"beamStrength":"FLT_MAX"},
          {"deformGroup":"doorglass_L_break","deformationTriggerRatio":0.05},
          ["d1l","d14l"],
          ["d2l","d14l"],
          ["d3l","d14l"],
          ["d4l","d14l"],
          ["d5l","d14l"],
          ["d6l","d14l"],
          ["d7l","d14l"],
          ["d8l","d14l"],
          ["d9l","d14l"],
          {"deformGroup":""},

          //latch node
          {"beamDeform":2500,"beamStrength":"FLT_MAX"},
          ["d9l","d14ll",{"deformLimitExpansion":0.01}],
          ["d6l","d14ll",{"deformLimitExpansion":0.01}],
          ["d8l","d14ll"],
          ["d5l","d14ll"],
          ["d2l","d14ll"],
          ["d3l","d14ll"],

          //pop open
          {"breakGroupType":1},
          {"beamType":"|SUPPORT", "beamLongBound":8},
          {"beamSpring":55000,"beamDamp":1320},
          {"beamDeform":40000,"beamStrength":10000},
          {"beamPrecompression":1.017},
          ["f8l", "d6l", {"breakGroup":"doorhinge_a_L"}],
          {"beamType":"|NORMAL", "beamLongBound":1, "beamShortBound":1},
          {"beamPrecompression":1},
          {"breakGroupType":0},
          {"breakGroup":""},

          //door support beams
          {"disableTriangleBreaking":true},
          {"beamType":"|SUPPORT","beamLongBound":6},
          {"beamSpring":1450550,"beamDamp":0},
          {"beamDeform":9750,"beamStrength":200000},
          ["d3l","q1l"],
          ["d8l","q1l"],
          ["d5l","q7l"],
          ["d9l","rf3l"],
          ["d4l","f1ll", {"beamPrecompression":0.93}],
          ["d4l","f6ll", {"beamPrecompression":0.93}],
          ["d9l","f3ll"],
          ["d9l","f8l"],
          ["d1l","f1l"],
          ["d2l","f2l"],
          ["d3l","f3l"],
          ["d3l","f4ll"],
          ["d4l","f5l", {"beamPrecompression":0.92}],
          ["d7l","f6l"],
          ["d8l","f6ll", {"beamPrecompression":0.92}],
          ["d10l","rf1", {"optional":true}],
          ["d11l","rf3", {"optional":true}],

          //door hinge
          {"beamType":"|NORMAL", "beamLongBound":1, "beamShortBound":1},
          {"beamSpring":3301000,"beamDamp":100},
          {"beamDeform":25800,"beamStrength":98000},
          {"breakGroup":"doorhinge_a_L"},
          ["d7l", "f6ll", {"deformGroup":"mirrorsignal_L_break","deformationTriggerRatio":0.1}],
          ["d7l", "f5ll"],
          ["d7l", "f6l"],
          ["d7l", "f5l"],
          ["d1l", "f2ll"],

          {"breakGroup":"doorhinge_b_L"},
          ["d1l", "f1ll"],
          ["d1l", "f5ll"],
          ["d1l", "f1l"],
          ["d1l", "f5l"],
          ["d7l", "p1l"],
          {"breakGroup":""},

          //damping
          {"beamDeform":27600,"beamStrength":225000},
          {"beamSpring":0,"beamDamp":17.5},
          {"breakGroup":"doorhinge_a_L"},
          ["d3l","f3l"],
          {"breakGroup":"doorhinge_b_L"},
          ["d9l","f3l"],
          {"breakGroup":""},
/*
          //--LATCH--
          {"beamSpring":3301000,"beamDamp":100},
          {"beamDeform":24000,"beamStrength":48000},
          {"breakGroup":"doorlatch_L"},
          ["d6l","f8l"],
          ["d14l","q7l"],
          ["d6l","q7l"],
          ["d3l","f3ll"],
          ["d9l","q1l"],
          //anti-pop beam
          ["d11l","rf3l", {"optional":true}],
          ["d10l","rf1l", {"optional":true}],
          {"breakGroup":""},
*/
          {"disableTriangleBreaking":false},
          {"beamPrecompression":1, "beamType":"|NORMAL", "beamLongBound":1.0, "beamShortBound":1.0},
          {"deformLimitExpansion":1.1},
    ],
    "triangles": [
            ["id1:","id2:","id3:"],
            //left door
            {"groundModel":"metal"},
            {"group":"sbr_door_L"},
            {"dragCoef":10},
            ["d4l","d1l","d2l"],
            ["d4l","d2l","d5l"],
            ["d5l","d2l","d3l"],
            ["d5l","d3l","d6l"],
            ["d7l","d4l","d5l"],
            ["d7l","d5l","d8l"],
            ["d8l","d5l","d6l"],
            ["d8l","d6l","d9l"],
            {"dragCoef":0},
            //door edge tris
            //{"breakGroup":["doorhinge_a_L","doorhinge_b_L"]}
            //["d6l", "q1l", "d9l"],
            //["d6l", "q7l", "q1l"],
            //["d6l", "d3l", "f3ll"],
            //["d6l", "f3ll", "q7l"],
            //["f6ll", "f5ll", "d7l"],
            //["d7l", "f5ll", "d4l"],
            //["f5ll", "f1ll", "d1l"],
            //["f5ll", "d1l", "d4l"],
            //{"breakGroup":""}
            {"group":""},
    ],
},
"sbr_doordetent_R": {
    "information":{
        "authors":"BeamNG",
        "name":"Right Door Detent",
        "value":30,
    },
    "slotType" : "sbr_doordetent_R",
     "beams": [
          ["id1:", "id2:"],
          //door detent
          {"beamLimitSpring":0,"beamLimitDamp":0},
          {"beamType":"|BOUNDED","beamLongBound":0.2,"beamShortBound":0.1},
          {"beamPrecompression":2.4,"beamSpring":770,"beamDamp":30,"beamStrength":8000},
          {"breakGroup":"doorhinge_a_R"},
          ["d5r", "fi2rr",{"boundZone":0.01}],
          {"beamPrecompression":3.0},
          ["d5r", "fi2rr",{"boundZone":0.01}],
          {"beamType":"|BOUNDED","beamLongBound":2.2,"beamShortBound":0.2},
          {"beamPrecompression":1.0,"beamSpring":0,"beamDamp":0},
          {"beamLimitSpring":16000,"beamLimitDamp":300},
          {"boundZone":0.1},
          ["d5r", "fi2rr"],
          {"boundZone":""},
          {"breakGroup":""},
          {"disableTriangleBreaking":false},
          {"beamPrecompression":1, "beamType":"|NORMAL", "beamLongBound":1.0, "beamShortBound":1.0},
          {"deformLimitExpansion":1.1},
    ],

},
"sbr_doordetent_L": {
    "information":{
        "authors":"BeamNG",
        "name":"Left Door Detent",
        "value":30,
    },
    "slotType" : "sbr_doordetent_L",
     "beams": [
          ["id1:", "id2:"],
          //door detent
          {"beamLimitSpring":0,"beamLimitDamp":0},
          {"beamType":"|BOUNDED","beamLongBound":0.2,"beamShortBound":0.1},
          {"beamPrecompression":2.4,"beamSpring":770,"beamDamp":30,"beamStrength":8000},
          {"breakGroup":"doorhinge_a_L"},
          ["d5l", "fi2ll",{"boundZone":0.01}],
          {"beamPrecompression":3.0},
          ["d5l", "fi2ll",{"boundZone":0.01}],
          {"beamType":"|BOUNDED","beamLongBound":2.2,"beamShortBound":0.2},
          {"beamPrecompression":1.0,"beamSpring":0,"beamDamp":0},
          {"beamLimitSpring":16000,"beamLimitDamp":300},
          {"boundZone":0.1},
          ["d5l", "fi2ll"],
          {"boundZone":""},
          {"breakGroup":""},
          {"disableTriangleBreaking":false},
          {"beamPrecompression":1, "beamType":"|NORMAL", "beamLongBound":1.0, "beamShortBound":1.0},
          {"deformLimitExpansion":1.1},
    ],

},
"sbr_doorpanel_R": {
    "information":{
        "authors":"BeamNG",
        "name":"Right Door Panel",
        "value":250,
    },
    "slotType" : "sbr_doorpanel_R",
    "flexbodies": [
         ["mesh", "[group]:", "nonFlexMaterials"],
         ["sbr_doorpanel_R", ["sbr_door_R"]],
    ],
    "sounds": {
        "$+cabinFilterCoef": 0.06
    },
    "nodes": [
         ["id", "posX", "posY", "posZ"],
         {"nodeMaterial":"|NM_METAL"},
         {"frictionCoef":0.5},
         //--DOOR--
         {"selfCollision":false},
         {"collision":false},
         {"nodeWeight":6.9},
         {"group":"sbr_door_R"},
         ["d14r",-0.46, 0.17, 0.52],
         {"group":""},
    ],
},
"sbr_doorpanel_L": {
    "information":{
        "authors":"BeamNG",
        "name":"Left Door Panel",
        "value":250,
    },
    "slotType" : "sbr_doorpanel_L",
    "flexbodies": [
         ["mesh", "[group]:", "nonFlexMaterials"],
         ["sbr_doorpanel_L", ["sbr_door_L"]],
    ],
    "sounds": {
        "$+cabinFilterCoef": 0.06
    },
    "nodes": [
         ["id", "posX", "posY", "posZ"],
         {"nodeMaterial":"|NM_METAL"},
         {"frictionCoef":0.5},
         //--DOOR--
         {"selfCollision":false},
         {"collision":false},
         {"nodeWeight":6.9},
         {"group":"sbr_door_L"},
         ["d14l" 0.46, 0.17, 0.52],
         {"group":""},
    ],
},
}