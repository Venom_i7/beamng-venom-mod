{
"tire_F_245_50_15_sport": {
    "information":{
        "authors":"BeamNG",
        "name":"245/50R15 Sport Front Tires",
        "value":190,
    },
    "slotType" : "tire_F_15x9",
    "flexbodies": [
        ["mesh", "[group]:", "nonFlexMaterials"],
        ["tire_01a_15x10_25_wide", ["wheel_FR","tire_FR"], [], {"pos":{"x":-0.54, "y":-0.0, "z":0.0}, "rot":{"x":0, "y":0, "z":180}, "scale":{"x":0.98, "y":0.97, "z":0.97}}],
        ["tire_01a_15x10_25_wide", ["wheel_FL","tire_FL"], [], {"pos":{"x": 0.54, "y":-0.0, "z":0.0}, "rot":{"x":0, "y":0, "z":0},   "scale":{"x":0.98, "y":0.97, "z":0.97}}],
    ],
    "variables": [
        ["name", "type", "unit", "category", "default", "min", "max", "title", "description"],
        ["$tirepressure_F", "range", "psi", "Wheels", 28, 0, 50, "Tire Pressure", "Relative to atmospheric pressure", {"subCategory":"Front"}],
    ],
    "pressureWheels": [
        ["name","hubGroup","group","node1:","node2:","nodeS","nodeArm:","wheelDir"],
        {"disableMeshBreaking":false,"disableHubMeshBreaking":false},
        {"hasTire":true},
        {"enableTireReinfBeams":false},
        {"enableTireLbeams":true},
        {"enableTireSideReinfBeams":false},
        {"enableTreadReinfBeams":true},
        {"enableTirePeripheryReinfBeams":true},

        //general settings
        {"radius":0.312},
        {"tireWidth":0.23},

        //tire options
        //shared between bolide 350 rear and 390gtr front!!
        {"wheelSideBeamSpring":"$=$tirepressure_F*600","wheelSideBeamDamp":35},
        {"wheelSideBeamSpringExpansion":421000,"wheelSideBeamDampExpansion":35},
        {"wheelSideTransitionZone":0.1,"wheelSideBeamPrecompression":0.985},

        {"wheelReinfBeamSpring":31000,"wheelReinfBeamDamp":180},
        {"wheelReinfBeamDampCutoffHz":500,"wheelReinfBeamPrecompression":0.98},

        {"wheelTreadBeamSpring":91000,"wheelTreadBeamDamp":75},
        {"wheelTreadBeamDampCutoffHz":500,"wheelTreadBeamPrecompression":0.98},

        {"wheelTreadReinfBeamSpring":161000,"wheelTreadReinfBeamDamp":55},
        {"wheelTreadReinfBeamDampCutoffHz":500,"wheelTreadReinfBeamPrecompression":0.98},

        {"wheelPeripheryBeamSpring":51000,"wheelPeripheryBeamDamp":55},
        {"wheelPeripheryBeamDampCutoffHz":500,"wheelPeripheryBeamPrecompression":0.98},

        {"wheelPeripheryReinfBeamSpring":151000,"wheelPeripheryReinfBeamDamp":35},
        {"wheelPeripheryReinfBeamDampCutoffHz":500,"wheelPeripheryReinfBeamPrecompression":0.98},

        //general tire values
        {"nodeWeight":0.155},
        {"nodeMaterial":"|NM_RUBBER"},
        {"triangleCollision":false},
        {"pressurePSI":"$tirepressure_F"},
        {"dragCoef":5},

        //groundmodel friction multipliers
        {"frictionCoef":1.0},
        {"slidingFrictionCoef":1.0},
        {"treadCoef":0.5},

        //advanced friction values
        {"noLoadCoef":1.35},
        {"loadSensitivitySlope":0.000135},
        {"fullLoadCoef":0.5},
        {"softnessCoef":0.7},

        //deform values
        {"wheelSideBeamDeform":15000,"wheelSideBeamStrength":20000},
        {"wheelTreadBeamDeform":11000,"wheelTreadBeamStrength":14000},
        {"wheelPeripheryBeamDeform":50000,"wheelPeripheryBeamStrength":50000},
    ],
},
"tire_F_255_55_15_sport": {
    "information":{
        "authors":"BeamNG",
        "name":"255/55R15 Grip-All Radial T/A Front Tires",
        "value":230,
    },
    "slotType" : "tire_F_15x9",
    "flexbodies": [
        ["mesh", "[group]:", "nonFlexMaterials"],
        ["tire_01c_14x7_26", ["wheel_FR","tire_FR"], [], {"pos":{"x":-0.54, "y":0.0, "z":0.0}, "rot":{"x":0, "y":0, "z":180}, "scale":{"x":1.26, "y":1.015, "z":1.015}}],
        ["tire_01c_14x7_26", ["wheel_FL","tire_FL"], [], {"pos":{"x": 0.54, "y":0.0, "z":0.0}, "rot":{"x":0, "y":0, "z":0},   "scale":{"x":1.26, "y":1.015, "z":1.015}}],
    ],
    "variables": [
        ["name", "type", "unit", "category", "default", "min", "max", "title", "description"],
        ["$tirepressure_F", "range", "psi", "Wheels", 28, 0, 50, "Tire Pressure", "Relative to atmospheric pressure", {"subCategory":"Front"}],
    ],
    "pressureWheels": [
        ["name","hubGroup","group","node1:","node2:","nodeS","nodeArm:","wheelDir"],
        {"disableMeshBreaking":false,"disableHubMeshBreaking":false},
        {"hasTire":true},
        {"enableTireReinfBeams":false},
        {"enableTireLbeams":true},
        {"enableTireSideReinfBeams":false},
        {"enableTreadReinfBeams":true},
        {"enableTirePeripheryReinfBeams":true},

        //general settings
        {"radius":0.33},
        {"tireWidth":0.23},

        //tire options
        {"wheelSideBeamSpring":"$=$tirepressure_F*714","wheelSideBeamDamp":35},
        {"wheelSideBeamSpringExpansion":431000,"wheelSideBeamDampExpansion":45},
        {"wheelSideTransitionZone":0.08,"wheelSideBeamPrecompression":0.99},

        {"wheelReinfBeamSpring":23000,"wheelReinfBeamDamp":195},
        {"wheelReinfBeamDampCutoffHz":500,"wheelReinfBeamPrecompression":0.985},

        {"wheelTreadBeamSpring":55000,"wheelTreadBeamDamp":95},
        {"wheelTreadBeamDampCutoffHz":500,"wheelTreadBeamPrecompression":0.985},

        {"wheelTreadReinfBeamSpring":111000,"wheelTreadReinfBeamDamp":70},
        {"wheelTreadReinfBeamDampCutoffHz":500,"wheelTreadReinfBeamPrecompression":0.985},

        {"wheelPeripheryBeamSpring":41000,"wheelPeripheryBeamDamp":45},
        {"wheelPeripheryBeamDampCutoffHz":500,"wheelPeripheryBeamPrecompression":0.985},

        {"wheelPeripheryReinfBeamSpring":101000,"wheelPeripheryReinfBeamDamp":35},
        {"wheelPeripheryReinfBeamDampCutoffHz":500,"wheelPeripheryReinfBeamPrecompression":0.985},

        //general tire values
        {"nodeWeight":0.165},
        {"nodeMaterial":"|NM_RUBBER"},
        {"triangleCollision":false},
        {"pressurePSI":"$tirepressure_F"},
        {"dragCoef":5},

        //groundmodel friction multipliers
        {"frictionCoef":1.0},
        {"slidingFrictionCoef":1.0},
        {"treadCoef":0.5},

        //advanced friction values
        {"noLoadCoef":1.43},
        {"loadSensitivitySlope":0.000152},
        {"fullLoadCoef":0.5},
        {"softnessCoef":0.7},

        //deform values
        {"wheelSideBeamDeform":19000,"wheelSideBeamStrength":26000},
        {"wheelTreadBeamDeform":14000,"wheelTreadBeamStrength":18000},
        {"wheelPeripheryBeamDeform":55000,"wheelPeripheryBeamStrength":55000},
    ],
},
"tire_F_255_60_15_sport": {
    "information":{
        "authors":"BeamNG",
        "name":"255/60R15 Grip-All Radial T/A Front Tires",
        "value":190,
    },
    "slotType" : "tire_F_15x9",
    "flexbodies": [
        ["mesh", "[group]:", "nonFlexMaterials"],
        ["tire_01c_15x9_27", ["wheel_FR","tire_FR"], [], {"pos":{"x":-0.54, "y":0.0, "z":0.0}, "rot":{"x":0, "y":0, "z":180}, "scale":{"x":1.0, "y":0.997, "z":0.997}}],
        ["tire_01c_15x9_27", ["wheel_FL","tire_FL"], [], {"pos":{"x": 0.54, "y":0.0, "z":0.0}, "rot":{"x":0, "y":0, "z":0},   "scale":{"x":1.0, "y":0.997, "z":0.997}}],
    ],
    "variables": [
        ["name", "type", "unit", "category", "default", "min", "max", "title", "description"],
        ["$tirepressure_F", "range", "psi", "Wheels", 28, 0, 50, "Tire Pressure", "Relative to atmospheric pressure", {"subCategory":"Front"}],
    ],
    "pressureWheels": [
        ["name","hubGroup","group","node1:","node2:","nodeS","nodeArm:","wheelDir"],
        {"disableMeshBreaking":false,"disableHubMeshBreaking":false},
        {"hasTire":true},
        {"enableTireReinfBeams":false},
        {"enableTireLbeams":true},
        {"enableTireSideReinfBeams":false},
        {"enableTreadReinfBeams":true},
        {"enableTirePeripheryReinfBeams":true},

        //general settings
        {"radius":0.343},
        {"tireWidth":0.23},

        //tire options
        {"wheelSideBeamSpring":"$=$tirepressure_F*660","wheelSideBeamDamp":35},
        {"wheelSideBeamSpringExpansion":451000,"wheelSideBeamDampExpansion":45},
        {"wheelSideTransitionZone":0.08,"wheelSideBeamPrecompression":0.99},

        {"wheelReinfBeamSpring":21000,"wheelReinfBeamDamp":200},
        {"wheelReinfBeamDampCutoffHz":500,"wheelReinfBeamPrecompression":0.985},

        {"wheelTreadBeamSpring":65000,"wheelTreadBeamDamp":95},
        {"wheelTreadBeamDampCutoffHz":500,"wheelTreadBeamPrecompression":0.985},

        {"wheelTreadReinfBeamSpring":131000,"wheelTreadReinfBeamDamp":75},
        {"wheelTreadReinfBeamDampCutoffHz":500,"wheelTreadReinfBeamPrecompression":0.985},

        {"wheelPeripheryBeamSpring":45000,"wheelPeripheryBeamDamp":45},
        {"wheelPeripheryBeamDampCutoffHz":500,"wheelPeripheryBeamPrecompression":0.985},

        {"wheelPeripheryReinfBeamSpring":111000,"wheelPeripheryReinfBeamDamp":35},
        {"wheelPeripheryReinfBeamDampCutoffHz":500,"wheelPeripheryReinfBeamPrecompression":0.985},

        //general tire values
        {"nodeWeight":0.17},
        {"nodeMaterial":"|NM_RUBBER"},
        {"triangleCollision":false},
        {"pressurePSI":"$tirepressure_F"},
        {"dragCoef":5},

        //groundmodel friction multipliers
        {"frictionCoef":1.0},
        {"slidingFrictionCoef":1.0},
        {"treadCoef":0.5},

        //advanced friction values
        {"noLoadCoef":1.45},
        {"loadSensitivitySlope":0.000143},
        {"fullLoadCoef":0.5},
        {"softnessCoef":0.7},

        //deform values
        {"wheelSideBeamDeform":19000,"wheelSideBeamStrength":26000},
        {"wheelTreadBeamDeform":14000,"wheelTreadBeamStrength":18000},
        {"wheelPeripheryBeamDeform":55000,"wheelPeripheryBeamStrength":55000},
    ],
},
}