{
"m3g81law_transmission_6M_G80": {
    "information":{
        "authors":"Iamthelaw105",
        "name":"ZF M3 G80 6-Speed Manual Transmission",
        "value":1750,
    },
    "slotType": "m3g81law_transmission",
    "slots": [
        ["type", "default", "description"],
        ["m3g81law_flywheel","m3g81law_flywheel_sport","Flywheel", {"coreSlot":true}],
        ["m3g81law_transfer_case","m3g81law_transfer_case_AWD", "Transfer Case"],
    ],
    "powertrain": [
        ["type", "name", "inputName", "inputIndex"],
        ["frictionClutch", "clutch", "mainEngine", 1],
        ["manualGearbox", "gearbox", "clutch", 1],
    ],
    "gearbox": {
        "uiName":"Gearbox",
        //premultiplied to account for final drive difference
        "gearRatios":[-3.727, 0, 4.110, 2.315, 1.542, 1.179, 1.000, 0.846],
        "friction": 1.13
        "dynamicFriction": 0.00135,
        "torqueLossCoef": 0.0152,
        "gearboxNode:":["tra1"],

        "gearWhineCoefsInput":  [0.60, 0.00, 0.15, 0.15, 0.15, 0.15, 0.15, 0.15, 0.15, 0.15],
        "gearWhineCoefsOutput": [0.00, 0.00, 0.30, 0.30, 0.30, 0.30, 0.30, 0.30, 0.30, 0.30],
        "gearWhineInputEvent": "event:>Vehicle>Transmission>helical_01>twine_in_tuned",
        "gearWhineOutputEvent": "event:>Vehicle>Transmission>helical_01>twine_out_tuned",

        //"forwardInputPitchCoef":1
        //"forwardOutputPitchCoef":1
        //"reverseInputPitchCoef":0.7
        //"reverseOutputPitchCoef":0.7

        //"gearWhineInputPitchCoefSmoothing":50
        //"gearWhineOutputPitchCoefSmoothing":50
        //"gearWhineInputVolumeCoefSmoothing":10
        //"gearWhineOutputVolumeCoefSmoothing":10

        //"gearWhineFixedCoefOutput": 0.7
        //"gearWhineFixedCoefInput": 0.4
    },
    "vehicleController": {
        "transmissionShiftDelay":0.15,
        "shiftDownRPMOffsetCoef":1.2,
        //"aggressionSmoothingDown":0.05,
        "calculateOptimalLoadShiftPoints": true,
        "aggressionHoldOffThrottleDelay":3,
        "lowShiftDownRPM":[0,0,0,1400,1500,1500,1400,1300],
        "lowShiftUpRPM":[0,0,3000,2600,2500,2400,2300],
    },
    "flexbodies": [
        ["mesh", "[group]:", "nonFlexMaterials"],
        ["m3g81law_transmission", ["m3g81law_engine","m3g81law_transmission"],[]{"pos":{"x":0,"y":0,"z":0}}],
		["m3g81law_transmission_support", ["m3g81law_engine","m3g81law_transmission"],[]{"pos":{"x":0,"y":0,"z":0}}],
    ],
    "nodes": [
         ["id", "posX", "posY", "posZ"],
         //--6 Speed Manual--
         {"selfCollision":false},
         {"collision":true},
         {"nodeMaterial":"|NM_METAL"},
         {"frictionCoef":0.5},
         {"group":"m3g81law_transmission"},
         {"nodeWeight":33},
         ["tra1", 0.0, -0.36, 0.375],
         {"group":""},
    ],
    "beams": [
          ["id1:", "id2:"],
          //--TRANSMISSION CONE--
          {"beamType":"|NORMAL", "beamLongBound":1.0, "beamShortBound":1.0},
          {"beamSpring":15001000,"beamDamp":500},
          {"beamDeform":250000,"beamStrength":"FLT_MAX"},
          ["tra1","e1r"],
          ["tra1","e1l"],
          ["tra1","e3r"],
          ["tra1","e3l"],
          {"beamPrecompression":1, "beamType":"|NORMAL", "beamLongBound":1.0, "beamShortBound":1.0},
    ],
},
"m3g81law_transmission_8DCT_G81": {
    "information":{
        "authors":"Iamthelaw105",
        "name":"ZF M Steptronic 8-Speed Transmission w. Drivelogic",
        "value":3950,
    },
    "slotType": "m3g81law_transmission",
    "slots": [
        ["type", "default", "description"],
        ["m3g81law_transfer_case","m3g81law_transfer_case_AWD", "Transfer Case"],
    ],
    "powertrain": [
        ["type", "name", "inputName", "inputIndex"],
        ["dctGearbox", "gearbox", "mainEngine", 1],
    ],
    "gearbox": {
        "uiName":"Gearbox",
        "gearRatios":[-3.456, 0, 5.000, 3.200, 2.143, 1.720, 1.313, 1.000, 0.823, 0.640],
        "parkLockTorque":2000,
        "friction": 1.31,
        "dynamicFriction": 0.00152,
        "torqueLossCoef": 0.0150,
        "gearboxNode:":["tra1"],
        "additionalEngineInertia":0.07,

        "gearWhineCoefsInput":  [0.60, 0.00, 0.13, 0.13, 0.13, 0.13, 0.13, 0.13, 0.13, 0.13],
        "gearWhineCoefsOutput": [0.00, 0.00, 0.27, 0.27, 0.27, 0.27, 0.27, 0.27, 0.27, 0.27],
        "gearWhineInputEvent": "event:>Vehicle>Transmission>helical_01>twine_in_tuned",
        "gearWhineOutputEvent": "event:>Vehicle>Transmission>helical_01>twine_out_tuned",
    },
    "vehicleController": {
        "automaticModes":"PRNDSM",
        "shiftDownRPMOffsetCoef":1.15,
        "calculateOptimalLoadShiftPoints": true,
        "transmissionGearChangeDelay":0.19,
        "aggressionHoldOffThrottleDelay":2.5,
        "aggressionSmoothingUp":2,
        "aggressionSmoothingDown":0.2,
        "gearboxDecisionSmoothingUp":2,
        "gearboxDecisionSmoothingDown":2,
        "lowShiftDownRPM":1400,
        "lowShiftUpRPM":2200,
        "clutchLaunchStartRPM": 2000,
        "clutchLaunchTargetRPM": 2500,
        "dctClutchTime": 0.017
    },
    "flexbodies": [
        ["mesh", "[group]:", "nonFlexMaterials"],
        ["m3g81law_transmission", ["m3g81law_engine","m3g81law_transmission"],[]{"pos":{"x":0,"y":0,"z":0}}],
		["m3g81law_transmission_support", ["m3g81law_engine","m3g81law_transmission"],[]{"pos":{"x":0,"y":0,"z":0}}],
    ],
    "nodes": [
         ["id", "posX", "posY", "posZ"],
         //--8 Speed DCT--
         {"selfCollision":false},
         {"collision":true},
         {"nodeMaterial":"|NM_METAL"},
         {"frictionCoef":0.5},
         {"group":"m3g81law_transmission"},
         {"nodeWeight":42},
         ["tra1", 0.0, -0.36, 0.33],
         {"group":""},
    ],
    "beams": [
          ["id1:", "id2:"],
          //--TRANSMISSION CONE--
          {"beamType":"|NORMAL", "beamLongBound":1.0, "beamShortBound":1.0},
          {"beamSpring":15001000,"beamDamp":500},
          {"beamDeform":250000,"beamStrength":"FLT_MAX"},
          ["tra1","e1r"],
          ["tra1","e1l"],
          ["tra1","e3r"],
          ["tra1","e3l"],
          {"beamPrecompression":1, "beamType":"|NORMAL", "beamLongBound":1.0, "beamShortBound":1.0},
    ],
},
"m3g81law_transmission_8DCT_G82": {
    "information":{
        "authors":"Iamthelaw105",
        "name":"ZF M Performance 8-Speed Transmission w. Speedshift",
        "value":3950,
    },
    "slotType": "m3g81law_transmission",
    "slots": [
        ["type", "default", "description"],
        ["m3g81law_transfer_case","m3g81law_transfer_case_AWD", "Transfer Case"],
    ],
    "powertrain": [
        ["type", "name", "inputName", "inputIndex"],
        ["dctGearbox", "gearbox", "mainEngine", 1],
    ],
    "gearbox": {
        "uiName":"Gearbox",
        "gearRatios":[-3.456, 0, 5.000, 3.200, 2.143, 1.720, 1.313, 1.000, 0.823, 0.640],
        "parkLockTorque":2000,
        "friction": 1.31,
        "dynamicFriction": 0.00152,
        "torqueLossCoef": 0.0150,
        "gearboxNode:":["tra1"],
        "additionalEngineInertia":0.07,

        "gearWhineCoefsInput":  [0.60, 0.00, 0.13, 0.13, 0.13, 0.13, 0.13, 0.13, 0.13, 0.13],
        "gearWhineCoefsOutput": [0.21, 0.00, 0.24, 0.27, 0.30, 0.33, 0.36, 0.39, 0.42, 0.45],
        "gearWhineInputEvent": "event:>Vehicle>Transmission>straight_01>twine_in_race",
        "gearWhineOutputEvent": "event:>Vehicle>Transmission>straight_01>twine_out_race",
    },
    "vehicleController": {
        "automaticModes":"PRNM",
        "shiftDownRPMOffsetCoef":1.15,
        "calculateOptimalLoadShiftPoints": true,
        "transmissionGearChangeDelay":0.19,
        "aggressionHoldOffThrottleDelay":2.5,
        "aggressionSmoothingUp":1,
        "aggressionSmoothingDown":0.07,
        "gearboxDecisionSmoothingUp":2,
        "gearboxDecisionSmoothingDown":2,
        "lowShiftDownRPM":1400,
        "lowShiftUpRPM":2200,
        "clutchLaunchStartRPM": 2000,
        "clutchLaunchTargetRPM": 2500,
        "dctClutchTime": 0.001
    },
    "flexbodies": [
        ["mesh", "[group]:", "nonFlexMaterials"],
        ["m3g81law_transmission", ["m3g81law_engine","m3g81law_transmission"],[]{"pos":{"x":0,"y":0,"z":0}}],
		["m3g81law_transmission_support", ["m3g81law_engine","m3g81law_transmission"],[]{"pos":{"x":0,"y":0,"z":0}}],
    ],
    "nodes": [
         ["id", "posX", "posY", "posZ"],
         //--8 Speed DCT--
         {"selfCollision":false},
         {"collision":true},
         {"nodeMaterial":"|NM_METAL"},
         {"frictionCoef":0.5},
         {"group":"m3g81law_transmission"},
         {"nodeWeight":42},
         ["tra1", 0.0, -0.36, 0.33],
         {"group":""},
    ],
    "beams": [
          ["id1:", "id2:"],
          //--TRANSMISSION CONE--
          {"beamType":"|NORMAL", "beamLongBound":1.0, "beamShortBound":1.0},
          {"beamSpring":15001000,"beamDamp":500},
          {"beamDeform":250000,"beamStrength":"FLT_MAX"},
          ["tra1","e1r"],
          ["tra1","e1l"],
          ["tra1","e3r"],
          ["tra1","e3l"],
          {"beamPrecompression":1, "beamType":"|NORMAL", "beamLongBound":1.0, "beamShortBound":1.0},
    ],
},
"m3g81law_converter": {
    "information":{
        "authors":"Iamthelaw105",
        "name":"Series Locking Torque Converter",
        "value":150,
    },
    "slotType" : "m3g81law_converter",
    "torqueConverter": {
        "uiName":"Torque Converter",
        "converterDiameter": 0.295,
        "converterStiffness":10,
        "couplingAVRatio":0.9,
        "stallTorqueRatio":1.8,
        "lockupClutchTorque":540,
        "additionalEngineInertia":0.14,
    },
    "vehicleController": {
        "torqueConverterLockupRPM":1300,
        "torqueConverterLockupMinGear":2,
    },
},
"m3g81law_converter_heavy": {
    "information":{
        "authors":"Iamthelaw105",
        "name":"M Motorsport Locking Torque Converter",
        "value":240,
    },
    "slotType" : "m3g81law_converter",
    "torqueConverter": {
        "uiName":"Torque Converter",
        "converterDiameter": 0.302,
        "converterStiffness":11,
        "couplingAVRatio":0.91,
        "stallTorqueRatio":1.78,
        "lockupClutchTorque":825,
        "additionalEngineInertia":0.16,
    },
    "vehicleController": {
        "torqueConverterLockupRPM":1300,
        "torqueConverterLockupMinGear":2,
    },
},
"m3g81law_flywheel": {
    "information":{
        "authors":"Iamthelaw105",
        "name":"Series Flywheel",
        "value":150,
    },
    "slotType" : "m3g81law_flywheel",
    "clutch": {
        "uiName":"Clutch",
        "additionalEngineInertia":0.12,
        "clutchMass":6,
    },
},
"m3g81law_flywheel_sport": {
    "information":{
        "authors":"Iamthelaw105",
        "name":"M Sport Flywheel",
        "value":400,
    },
    "slotType" : "m3g81law_flywheel",
    "clutch": {
        "uiName":"Clutch",
        "additionalEngineInertia":0.07,
        "clutchMass":4.25,
    },
},
"m3g81law_flywheel_race": {
    "information":{
        "authors":"Iamthelaw105",
        "name":"M Motorsport Lightwight Flywheel",
        "value":700,
    },
    "slotType" : "m3g81law_flywheel",
    "clutch": {
        "uiName":"Clutch",
        "additionalEngineInertia":0.02,
        "clutchMass":3.5,
    },
},
"m3g81law_transfer_case_RWD": {
    "information":{
        "authors":"Iamthelaw105",
        "name":"2WD Transfer Case",
        "value":1900,
    },
    "slotType" : "m3g81law_transfer_case",
    "powertrain" : [
        ["type", "name", "inputName", "inputIndex"],
        ["shaft", "transfercase", "gearbox", 1, {"friction":0.2, "dynamicFriction":0.00018, "uiName":"Rear Output Shaft"}],
    ],
    "nodes": [
         ["id", "posX", "posY", "posZ"],
         //--tcase weight--
         {"selfCollision":false},
         {"collision":false},
         {"nodeMaterial":"|NM_METAL"},
         {"frictionCoef":0.5},
         {"group":"m3g81law_transmission"},
         {"nodeWeight":20},
         ["tcase", 0.13, -0.43, 0.33],
         {"group":""},
    ],
    "beams": [
          ["id1:", "id2:"],
          {"beamType":"|NORMAL", "beamLongBound":1.0, "beamShortBound":1.0},
          {"beamSpring":1500600,"beamDamp":60},
          {"beamDeform":30000,"beamStrength":"FLT_MAX"},
          ["tcase","e1r"],
          ["tcase","e3r"],
          ["tcase","e1l"],
          ["tcase","e3l"],
          ["tcase","tra1"],
          {"beamPrecompression":1, "beamType":"|NORMAL", "beamLongBound":1.0, "beamShortBound":1.0},
    ],
},
"m3g81law_transfer_case_AWD": {
    "information":{
        "authors":"Iamthelaw105",
        "name":"xDrive Transfer Case",
        "value":2100,
    },
    "slotType" : "m3g81law_transfer_case",
    "controller": [
        ["fileName"],
        ["drivingDynamics/actuators/electronicSplitShaftLock" {"name": "electronicSplitShaftLock", "splitShaftName":"transfercase"}]
    ],
    "powertrain" : [
        ["type", "name", "inputName", "inputIndex"],
        ["splitShaft", "transfercase", "gearbox", 1, {"splitType":"locked", "primaryOutputID":1, "lockTorque":3000, "friction":0.35, "dynamicFriction":0.00039, "torqueLossCoef":0.012, "uiName":"Electrically Clutched Torque Splitter","defaultVirtualInertia":0.1}],
    ],
    "flexbodies": [
        ["mesh", "[group]:", "nonFlexMaterials"],
        ["m3g81law_transfercase", ["m3g81law_transmission","m3g81law_engine"]],
    ],
    "nodes": [
         ["id", "posX", "posY", "posZ"],
         //--tcase weight--
         {"selfCollision":false},
         {"collision":false},
         {"nodeMaterial":"|NM_METAL"},
         {"frictionCoef":0.5},
         {"group":"m3g81law_transmission"},
         {"nodeWeight":36},
         ["tcase", 0.13, -0.43, 0.33],
         {"group":""},
    ],
    "beams": [
          ["id1:", "id2:"],
          {"beamType":"|NORMAL", "beamLongBound":1.0, "beamShortBound":1.0},
          {"beamSpring":1500600,"beamDamp":60},
          {"beamDeform":30000,"beamStrength":"FLT_MAX"},
          ["tcase","e1r"],
          ["tcase","e3r"],
          ["tcase","e1l"],
          ["tcase","e3l"],
          ["tcase","tra1"],
          {"beamPrecompression":1, "beamType":"|NORMAL", "beamLongBound":1.0, "beamShortBound":1.0},
    ],
},
"m3g81law_transfer_case_XWD": {
    "information":{
        "authors":"Cyborella",
        "name":"Alpina XWD Transfer Case",
        "value":2100,
    },
    "slotType" : "m3g81law_transfer_case",
        "controller": [
            ["fileName"],
            ["drivingDynamics/actuators/electronicSplitShaftLock" {"name": "electronicSplitShaftLock", "splitShaftName":"transfercase", "clutchPIDkP":0.06, "clutchPIDkI":0, "clutchPIDkD":0}]
        ],
        "powertrain" : [
            ["type", "name", "inputName", "inputIndex"],
            ["splitShaft", "transfercase", "gearbox", 1, {"splitType":"locked", "primaryOutputID":1, "lockTorque":550, "friction":1.15, "dynamicFriction":0.00053, "torqueLossCoef": 0.012, "uiName":"Electrically Clutched Torque Splitter","defaultVirtualInertia":0.1}],
        ],
    "flexbodies": [
        ["mesh", "[group]:", "nonFlexMaterials"],
        ["m3g81law_transfercase", ["m3g81law_transmission","m3g81law_engine"]],
    ],
    "nodes": [
         ["id", "posX", "posY", "posZ"],
         //--tcase weight--
         {"selfCollision":false},
         {"collision":false},
         {"nodeMaterial":"|NM_METAL"},
         {"frictionCoef":0.5},
         {"group":"m3g81law_transmission"},
         {"nodeWeight":36},
         ["tcase", 0.13, -0.43, 0.33],
         {"group":""},
    ],
    "beams": [
          ["id1:", "id2:"],
          {"beamType":"|NORMAL", "beamLongBound":1.0, "beamShortBound":1.0},
          {"beamSpring":1500600,"beamDamp":60},
          {"beamDeform":30000,"beamStrength":"FLT_MAX"},
          ["tcase","e1r"],
          ["tcase","e3r"],
          ["tcase","e1l"],
          ["tcase","e3l"],
          ["tcase","tra1"],
          {"beamPrecompression":1, "beamType":"|NORMAL", "beamLongBound":1.0, "beamShortBound":1.0},
    ],
},
}