{
"atv_engine": {
    "information":{
        "authors":"BeamNG",
        "name":"800cc I3 Engine",
        "value":250,
    },
    "slotType" : "atv_engine",
    "slots": [
        ["type", "default", "description"],
        ["atv_enginemounts","atv_enginemounts","Engine Mounts", {"coreSlot":true}],
        ["atv_intake","atv_intake","Intake", {"coreSlot":true}],
        ["atv_oilpan","atv_oilpan","Oil Pan", {"coreSlot":true}],
        ["atv_radiator","atv_radiator", "Radiator"],
        ["atv_engine_ecu","atv_engine_ecu_stock", "Engine Management", {"coreSlot":true}],
        ["n2o_system","", "Nitrous Oxide System"],
        ["atv_internals","atv_internals", "Engine Long Block", {"coreSlot":true}],
        ["atv_transmission","atv_transmission", "Transmission"],
    ],
    "powertrain": [
        ["type", "name", "inputName", "inputIndex"],
        ["combustionEngine", "mainEngine", "dummy", 0],
    ],
    "mainEngine": {
        "torque":[
        ["rpm", "torque"],
        [0, 0],
        [500,  29.8],
        [1000, 55.0],
        [1500, 66.6],
        [2500, 75.3],
        [4000, 80.6],
        [5000, 77.7],
        [6000, 70.0],
        [7000, 50.2],
        [8000, 34],
        [9000, 13],
        [10000, 1],
        ],

        "idleRPM":780,
        "maxRPM":6700,
        "revLimiterRPMDrop":200,
        "inertia":0.045,
        "friction":6,
        "dynamicFriction":0.012,
        "engineBrakeTorque":13,
        //"burnEfficiency":0.3
        "burnEfficiency":[
        [0, 0.14],
        [0.05, 0.24],
        [0.4, 0.32],
        [0.7, 0.45],
        [1, 0.37],
        ],
        //fuel system
        "energyStorage": "mainTank",
        "requiredEnergyType":"gasoline",

        //exhaust
        "instantAfterFireSound": "event:>Vehicle>Afterfire>i6_01>single",
        "sustainedAfterFireSound": "event:>Vehicle>Afterfire>i6_01>multi",
        "shiftAfterFireSound": "event:>Vehicle>Afterfire>i6_01>shift",
        "particulates":0.1,
        "instantAfterFireCoef": 0.0,
        "sustainedAfterFireCoef": 0.0,

        //cooling and oil system
        "thermalsEnabled":true,
        "engineBlockMaterial":"aluminum",
        //"oilVolume":2,
        "engineBlockAirCoolingEfficiency":5,

        //engine durability
        "cylinderWallTemperatureDamageThreshold":150,
        "headGasketDamageThreshold":1500000,
        "pistonRingDamageThreshold":1500000,
        "connectingRodDamageThreshold":1500000,
        "maxTorqueRating": 115,
        "maxOverTorqueDamage": 250,

        //node beam interface
        "torqueReactionNodes:":["e1l","e2l","e4r"],
        "waterDamage": {"[engineGroup]:":["engine_intake"]},
        "radiator": {"[engineGroup]:":["radiator"]},
        "engineBlock": {"[engineGroup]:":["engine_block"]},
        "breakTriggerBeam":"engine",
        "uiName":"Engine",
        "soundConfig": "soundConfig",
        "soundConfigExhaust": "soundConfigExhaust",

        //starter motor
        "starterSample":"event:>Engine>Starter>i4_2011_eng",
        "starterSampleExhaust":"event:>Engine>Starter>i4_2011_exh",
        "shutOffSampleEngine":"event:>Engine>Shutoff>i4_2011_eng",
        "shutOffSampleExhaust":"event:>Engine>Shutoff>i4_2011_exh",
        "starterVolume":0.76,
        "starterVolumeExhaust":0.76,
        "shutOffVolumeEngine":0.76,
        "shutOffVolumeExhaust":0.76,
        "starterThrottleKillTime":0.85,
        "idleRPMStartRate":2,
        "idleRPMStartCoef":1,

        //engine deform groups
        "deformGroups":["mainEngine", "mainEngine_intake", "mainEngine_accessories"]
        "deformGroups_oilPan":["oilpan_damage"]
    },
    "soundConfig": {
        "sampleName": "I3_engine",
        "intakeMuffling": 0.7,

        "mainGain": -13,
        "onLoadGain":1.0,
        "offLoadGain":0.6,

        "maxLoadMix": 0.7,
        "minLoadMix": 0,

        "lowShelfGain":-3,
        "lowShelfFreq":150,

        "highShelfGain":10,
        "highShelfFreq":7500,

        "eqLowGain": 0,
        "eqLowFreq": 250,
        "eqLowWidth": 0.1,

        "eqHighGain": 3,
        "eqHighFreq": 800,
        "eqHighWidth": 0.1,

        "fundamentalFrequencyCylinderCount":3,
        "eqFundamentalGain": -2,
    },
    "soundConfigExhaust": {
        "sampleName": "I3_exhaust",

        "mainGain": -3.5,
        "onLoadGain":1.0,
        "offLoadGain":0.2,

        "maxLoadMix": 0.8,
        "minLoadMix": 0,

        "lowShelfGain":-6,
        "lowShelfFreq":100,

        "highShelfGain":2,
        "highShelfFreq":6000,

        "eqLowGain": -3,
        "eqLowFreq": 450,
        "eqLowWidth": 0.1,

        "eqHighGain": 4,
        "eqHighFreq": 1200,
        "eqHighWidth": 0.2,

        "fundamentalFrequencyCylinderCount":3,
        "eqFundamentalGain": -2,
    },
    "vehicleController": {
        "clutchLaunchStartRPM":1200,
        "clutchLaunchTargetRPM":1800,
        //**highShiftDown can be overwritten by automatic transmissions**
        "highShiftDownRPM":[0,0,0,1600,2100,2300,2300],
        //**highShiftUp can be overwritten by intake modifications**
        "highShiftUpRPM":4300,
    },
    "flexbodies": [
        ["mesh", "[group]:", "nonFlexMaterials"],
        ["atv_engine", ["atv_engine"]],
        ["atv_engine_intakemanifold", ["atv_engine"]],
        ["atv_enginebaycrap", ["atv_body"]],
        ["atv_muffler", ["atv_body","atv_exhaust"]],
    ],
    "props": [
        ["func",        "mesh",           "idRef:", "idX:", "idY:", "baseRotation",        "rotation",        "translation",        "min", "max", "offset", "multiplier"],
        ["rpmspin",        "atv_engine_pulley01",   "e3l","e3r","e4l",     {"x":0, "y":90, "z":0}, {"x":1, "y":0, "z":0}, {"x":0, "y":0, "z":0}, -360,   360,   0,        1],
        ["rpmspin",        "atv_engine_pulley02",   "e3l","e3r","e4l",     {"x":0, "y":90, "z":0}, {"x":1, "y":0, "z":0}, {"x":0, "y":0, "z":0}, -1080,  1080,  0,        3],
        ["rpmspin",        "atv_engine_pulley03",   "e3l","e3r","e4l",     {"x":0, "y":90, "z":0}, {"x":1, "y":0, "z":0}, {"x":0, "y":0, "z":0}, -1080,  1080,  0,        3],
        ],
    "nodes": [
        ["id", "posX", "posY", "posZ"],
        //--800cc I3 Engine--
        {"selfCollision":true},
        {"collision":true},
        {"nodeMaterial":"|NM_METAL"},
        {"frictionCoef":0.5},
        {"group":"atv_engine"},
        //{"engineGroup":"engine_block"},
        {"nodeWeight":8.5},
        ["e1r", 0.27, -0.82, 0.31,{"chemEnergy":1000,"burnRate":0.39,"flashPoint":800,"specHeat": 0.2,"selfIgnitionCoef":false,"smokePoint":650,"baseTemp":"thermals","conductionRadius":0.12}],
        ["e1l", 0.27, -1.22, 0.31, {"isExhaust":"mainEngine"}],
        ["e2r", -0.27, -0.82, 0.31,{"chemEnergy":1000,"burnRate":0.39,"flashPoint":800,"specHeat": 0.2,"selfIgnitionCoef":false,"smokePoint":650,"baseTemp":"thermals","conductionRadius":0.12}],
        ["e2l", -0.27, -1.22, 0.31],
        {"engineGroup":["engine_block","engine_intake"]},
        ["e3r", 0.27, -1.13, 0.93,{"chemEnergy":1000,"burnRate":0.39,"flashPoint":800,"specHeat": 0.2,"selfIgnitionCoef":false,"smokePoint":650,"baseTemp":"thermals","conductionRadius":0.12}],
        ["e3l", 0.27, -0.80, 0.98],
        ["e4r", -0.27, -1.13, 0.93,{"chemEnergy":1000,"burnRate":0.39,"flashPoint":800,"specHeat": 0.2,"selfIgnitionCoef":false,"smokePoint":650,"baseTemp":"thermals","conductionRadius":0.12}],
        ["e4l", -0.27, -0.80, 0.98],
        {"engineGroup":""},
        {"group":""},
    ],
    "beams": [
        ["id1:", "id2:"],
        {"beamType":"|NORMAL", "beamLongBound":1.0, "beamShortBound":1.0},
        {"beamSpring":1101000,"beamDamp":600},
        {"beamDeform":75000,"beamStrength":"FLT_MAX"},
        {"deformLimit":1.2,"deformLimitExpansion":1.2},
        //engine
        {"deformGroup":"mainEngine", "deformationTriggerRatio":0.001}
        ["e1r","e1l"],
        ["e2r","e2l"],
        ["e3r","e3l"],
        ["e4r","e4l"],

        ["e1r","e2r"],
        ["e1l","e2l"],
        ["e3r","e4r"],
        ["e3l","e4l"],

        ["e1r","e3r"],
        ["e1l","e3l"],
        ["e2r","e4r"],
        ["e2l","e4l"],

        ["e2r","e3r"],
        ["e2l","e3l"],
        ["e2r","e3l"],
        ["e2l","e3r"],

        ["e1r","e4r"],
        ["e1l","e4l"],
        ["e1r","e4l"],
        ["e1l","e4r"],

        ["e1r","e2l"],
        ["e1l","e2r"],
        ["e3r","e4l"],
        ["e3l","e4r"],

        ["e1r","e3l"],
        ["e1l","e3r"],
        ["e2r","e4l"],
        ["e2l","e4r"],

        {"deformGroup":""}
        {"breakGroup":""},

        //engine mounts
        {"beamSpring":1240000,"beamDamp":350},
        {"beamDeform":12000,"beamStrength":"FLT_MAX"},
        ["e1r","b1l"],
        ["e1l","b1l"],
        ["e2r","b1l"],
        ["e2l","b1l"],

        ["e1r","b1r"],
        ["e1l","b1r"],
        ["e2r","b1r"],
        ["e2l","b1r"],

        ["e1r","b2l"],
        ["e1l","b2l"],
        ["e2r","b2l"],
        ["e2l","b2l"],

        ["e1r","b2r"],
        ["e1l","b2r"],
        ["e2r","b2r"],
        ["e2l","b2r"],
        {"beamSpring":440000,"beamDamp":150},
        {"beamDeform":12000,"beamStrength":"FLT_MAX"},
        ["e3r","b31l"],
        ["e4r","b31l"],

        ["e3r","b31"],
        ["e4r","b31"],

        ["e3r","b31r"],
        ["e4r","b31r"],

        ["e3l","b32"],
        ["e4l","b32"],
        {"deformLimit":"","deformLimitExpansion":""},
        {"beamPrecompression":1, "beamType":"|NORMAL", "beamLongBound":1.0, "beamShortBound":1.0},
    ],
},
"atv_oilpan": {
    "information":{
        "authors":"BeamNG",
        "name":"Oil Pan",
        "value":90,
    },
    "slotType" : "atv_oilpan",
    "mainEngine": {
        //cooling and oil system
        "oilVolume":4.0,

        //engine durability
        "oilpanMaximumSafeG": 1.2

        //node beam interface
        "oilpanNodes:":["oilpan","oilref"],

        //engine deform groups
        "deformGroups_oilPan":["oilpan_damage"]
    },
    "nodes": [
        ["id", "posX", "posY", "posZ"],
        {"selfCollision":false},
        {"collision":true},
        {"frictionCoef":0.5},
        {"nodeMaterial":"|NM_METAL"},

        //oil pan node
        {"group":""},
        {"nodeWeight":2},
        ["oilpan", -0.1, -1.0, 0.33],
        ["oilref", -0.1, -1.0, 0.83, {"nodeWeight":1, "collision":false}],
    ],
    "beams": [
        ["id1:", "id2:"],
        {"beamPrecompression":1, "beamType":"|NORMAL", "beamLongBound":1.0, "beamShortBound":1.0},

        //oilpan node
        {"beamSpring":501000,"beamDamp":250},
        {"beamDeform":10000,"beamStrength":"FLT_MAX"},
        {"deformGroup":"oilpan_damage","deformationTriggerRatio":0.05},
        ["oilpan", "e1r"],
        ["oilpan", "e1l"],
        ["oilpan", "e2r"],
        ["oilpan", "e2l"],
        ["oilpan", "e3r"],
        ["oilpan", "e3l"],
        ["oilpan", "e4r"],
        ["oilpan", "e4l"],
        {"deformGroup":""},

        //oil ref
        {"beamSpring":301000,"beamDamp":150},
        {"beamDeform":25000,"beamStrength":"FLT_MAX"},
        ["oilref", "e1r"],
        ["oilref", "e1l"],
        ["oilref", "e2r"],
        ["oilref", "e2l"],
        ["oilref", "e3r"],
        ["oilref", "e3l"],
        ["oilref", "e4r"],
        ["oilref", "e4l"],
        {"beamPrecompression":1, "beamType":"|NORMAL", "beamLongBound":1.0, "beamShortBound":1.0},
    ],
},
"atv_turbo": {
    "information":{
        "authors":"BeamNG",
        "name":"Stock Turbocharger",
        "value":950,
    },
    "slotType" : "atv_intake",
    "turbocharger": {
        "bovSoundFileName":"event:>Vehicle>Forced_Induction>Turbo_01>turbo_bov",
        "hissLoopEvent":"event:>Vehicle>Forced_Induction>Turbo_07>turbo_hiss",
        "whineLoopEvent":"event:>Vehicle>Forced_Induction>Turbo_05>turbo_spin",
        "turboSizeCoef": 0.30,
        "bovSoundVolumeCoef": 0.30,
        "hissVolumePerPSI": 0.040,
        "whineVolumePer10kRPM": 0.012,
        "whinePitchPer10kRPM": 0.055,
        "maxExhaustPower": 50000,
        "backPressureCoef": 0.000001,
        "pressureRatePSI": 30,
        "frictionCoef": 16,
        "inertia":0.40,
        "damageThresholdTemperature": 610,
        "wastegateStart":9.9,
        "wastegateLimit":10,
        "pressurePSI":[
        //turbineRPM, pressure(PSI)
        [0,        -2.5],
        [30000,    -0.5],
        [60000,     5],
        [90000,     8],
        [150000,    11],
        [200000,    14],
        [250000,    17],
        [300000,    20],
        [400000,    30],
        ],
        "engineDef":[
        //engineRPM, efficiency, exhaustFactor
        [0,     0.0,     0.0],
        [650,   0.10,    0.12],
        [1500,  0.22,    0.23],
        [2000,  0.41,    0.34],
        [2500,  0.62,    0.61],
        [3000,  0.76,    0.75],
        [3500,  0.81,    0.81],
        [4000,  0.87,    0.88],
        [5000,  0.90,    0.94],
        [6000,  0.88,    0.99],
        [7000,  0.85,    0.95],
        [8000,  0.80,    0.90],
        ],
    },
    "soundConfig": {
        "$+mainGain": 0.0,
        "$+intakeMuffling":-0.2,
        "$+maxLoadMix": 0.2,
        "$+minLoadMix": 0.14,
        "$+highShelfGain": 1.0,
        "$+eqLowGain": -1.5,
        "$+eqFundamentalGain": -4,
    },
    "soundConfigExhaust": {
        "$+mainGain": -0.9,
        "$+maxLoadMix": 0.2,
        "$+minLoadMix": 0.10,
        "$+offLoadGain": 0.04,
        "$+lowShelfGain": 0.9,
        "$+highShelfGain": 1.0,
        "$+eqLowGain": 0.0,
        "$+eqHighGain": 1.0,
        "$+eqFundamentalGain": 1.5,
    },
    "mainEngine": {
        "torqueModIntake":[
        ["rpm", "torque"],
        [0, 0],
        [1000, -2],
        [1500, -3],
        [2000, -4.5],
        [2500, -6],
        [3000, -7],
        [3500, -8],
        [3500, -8],
        [4000, -9],
        [4500, -9],
        [5000, -9],
        [5500, -10],
        [6000, -10],
        [7000, -10],
        [8000, -11],
        [10000, -1],
        ],
        //turbocharger name
        "turbocharger":"turbocharger",
        //"instantAfterFireCoef": 1,
        //"sustainedAfterFireCoef": 0.75,
        //"$*instantAfterFireCoef": 2,
        //"$*sustainedAfterFireCoef": 2,

        //damage deformGroups
        "deformGroups_turbo":["mainEngine_turbo","mainEngine_intercooler"]
    },
    "flexbodies": [
        ["mesh", "[group]:", "nonFlexMaterials"],
        ["atv_engine_turbo", ["atv_engine"]],
        ["atv_engine_turbo_intake", ["atv_engine_turbo","atv_body"]],
        ["atv_engine_turbo_exhaust", ["atv_engine_turbo","atv_body"]],
        {"deformGroup":""},
    ],
    "nodes": [
        ["id", "posX", "posY", "posZ"],
        //--800cc I3 Engine--
        {"selfCollision":true},
        {"collision":true},
        {"nodeMaterial":"|NM_METAL"},
        {"frictionCoef":0.5},
        {"group":"atv_exhaust"},
        {"selfCollision":false},
        {"nodeWeight":1.5},
        ["ex1", 0.64, -0.95, 1.04, {"afterFireAudioCoef":0.33, "afterFireVisualCoef":0.33, "afterFireVolumeCoef":0.3, "afterFireMufflingCoef":0.85, "exhaustAudioMufflingCoef":0.85, "exhaustAudioGainChange":-1}],
        {"group":""},
    ],

    "beams": [
        ["id1:", "id2:"],
        {"beamType":"|NORMAL", "beamLongBound":1.0, "beamShortBound":1.0},
        {"beamSpring":1101000,"beamDamp":600},
        {"beamDeform":75000,"beamStrength":"FLT_MAX"},
        {"deformLimit":1.1,"deformLimitExpansion":1.05},
        //exhaust
        {"beamSpring":540000,"beamDamp":150},
        {"beamDeform":4000,"beamStrength":"FLT_MAX"},
        ["e1l","ex1", {"isExhaust":"mainEngine","beamSpring":10,"beamDamp":10}],
        ["b31ll","ex1"],
        ["b31lll","ex1"],
        ["b32ll","ex1"],
        ["b11ll","ex1"],
        ["b12ll","ex1"],
        {"deformLimit":"","deformLimitExpansion":""},
        {"beamPrecompression":1, "beamType":"|NORMAL", "beamLongBound":1.0, "beamShortBound":1.0},
    ],
},
"atv_turbo_stage1": {
    "information":{
        "authors":"BeamNG",
        "name":"Stage 1 Turbocharger",
        "value":950,
    },
    "slotType" : "atv_intake",
    "turbocharger": {
        "bovSoundFileName":"event:>Vehicle>Forced_Induction>Turbo_01>turbo_bov",
        "hissLoopEvent":"event:>Vehicle>Forced_Induction>Turbo_07>turbo_hiss",
        "whineLoopEvent":"event:>Vehicle>Forced_Induction>Turbo_05>turbo_spin",
        "turboSizeCoef": 0.40,
        "bovSoundVolumeCoef": 0.45,
        "hissVolumePerPSI": 0.040,
        "whineVolumePer10kRPM": 0.012,
        "whinePitchPer10kRPM": 0.055,
        "wastegateStart":14.5,
        "wastegateLimit":15.5,
        "maxExhaustPower": 11000,
        "backPressureCoef": 0.000035,
        "frictionCoef": 10.4,
        "inertia":1.2,
        "damageThresholdTemperature": 610,
        "pressurePSI":[
        //turbineRPM, pressure(PSI)
        [0,        -3.5],
        [30000,     0],
        [60000,     6],
        [90000,     11],
        [150000,    15],
        [200000,    20],
        [250000,    25],
        ],
        "engineDef":[
        //engineRPM, efficiency, exhaustFactor
        [0,     0.0,    0.0],
        [650,   0.20,   0.22],
        [1400,  0.34,   0.42],
        [2000,  0.65,    0.52],
        [2500,  0.79,    0.63],
        [3000,  0.86,    0.70],
        [3500,  0.87,    0.75],
        [4000,  0.88,    0.90],
        [5000,  0.94,    0.95],
        [6000,  0.92,    0.94],
        [7000,  0.90,    0.90],
        [8000,  0.83,    1.0],
        [9000,  0.87,    1.0],
        [10000, 0.65,    1.0],
        ],
    },
    "soundConfig": {
        "$+mainGain": 0.25,
        "$+intakeMuffling":-0.4,
        "$+maxLoadMix": 0.15,
        "$+minLoadMix": 0.21,
        "$+highShelfGain": 1.5,
        "$+eqLowGain": -1.0,
        "$+eqFundamentalGain": -3,
 },
    "soundConfigExhaust": {
        "$+mainGain": -0.6,
        "$+maxLoadMix": 0.15,
        "$+minLoadMix": 0.15,
        "$+offLoadGain": 0.06,
        "$+lowShelfGain": 0.6,
        "$+highShelfGain": 1.5,
        "$+eqLowGain": 0.0,
        "$+eqHighGain": 1.5,
        "$+eqFundamentalGain": 3.0,
    },
    "mainEngine": {
        "torqueModIntake":[
        ["rpm", "torque"],
        [0, 0],
        [1000, -2],
        [1500, -3],
        [2000, -4.5],
        [2500, -6],
        [3000, -7],
        [3500, -8],
        [3500, -8],
        [4000, -9],
        [4500, -9],
        [5000, -9],
        [5500, -10],
        [6000, -10],
        [7000, -10],
        [8000, -11],
        [10000, -1],
        ],
        //turbocharger name
        "turbocharger":"turbocharger",
        //"instantAfterFireCoef": 1,
        //"sustainedAfterFireCoef": 0.75,
        //"$*instantAfterFireCoef": 2,
        //"$*sustainedAfterFireCoef": 2,

        //damage deformGroups
        "deformGroups_turbo":["mainEngine_turbo","mainEngine_intercooler"]
    },
    "flexbodies": [
        ["mesh", "[group]:", "nonFlexMaterials"],
        ["atv_engine_turbo", ["atv_engine"]],
        ["atv_engine_turbo_intake", ["atv_engine_turbo","atv_body"]],
        ["atv_engine_turbo_exhaust", ["atv_engine_turbo","atv_body"]],
        {"deformGroup":""},
    ],
    "nodes": [
        ["id", "posX", "posY", "posZ"],
        //--800cc I3 Engine--
        {"selfCollision":true},
        {"collision":true},
        {"nodeMaterial":"|NM_METAL"},
        {"frictionCoef":0.5},
        {"group":"atv_exhaust"},
        {"selfCollision":false},
        {"nodeWeight":1.5},
        ["ex1", 0.64, -0.95, 1.04, {"afterFireAudioCoef":0.33, "afterFireVisualCoef":0.33, "afterFireVolumeCoef":0.3, "afterFireMufflingCoef":0.85, "exhaustAudioMufflingCoef":0.85, "exhaustAudioGainChange":-1}],
        {"group":""},
    ],

    "beams": [
        ["id1:", "id2:"],
        {"beamType":"|NORMAL", "beamLongBound":1.0, "beamShortBound":1.0},
        {"beamSpring":1101000,"beamDamp":600},
        {"beamDeform":75000,"beamStrength":"FLT_MAX"},
        {"deformLimit":1.1,"deformLimitExpansion":1.05},
        //exhaust
        {"beamSpring":540000,"beamDamp":150},
        {"beamDeform":4000,"beamStrength":"FLT_MAX"},
        ["e1l","ex1", {"isExhaust":"mainEngine","beamSpring":10,"beamDamp":10}],
        ["b31ll","ex1"],
        ["b31lll","ex1"],
        ["b32ll","ex1"],
        ["b11ll","ex1"],
        ["b12ll","ex1"],
        {"deformLimit":"","deformLimitExpansion":""},
        {"beamPrecompression":1, "beamType":"|NORMAL", "beamLongBound":1.0, "beamShortBound":1.0},
    ],
},
"atv_intake": {
    "information":{
        "authors":"BeamNG",
        "name":"Stock Intake",
        "value":950,
    },
    "slotType" : "atv_intake",
    "flexbodies": [
        ["mesh", "[group]:", "nonFlexMaterials"],
        ["atv_engine_NA_intake", ["atv_engine"]],
        ["atv_engine_NA_exhaust_pipe", ["atv_engine","atv_intake"]],
        ["atv_engine_NA_exhaust_manifold", ["atv_engine"]],
        {"deformGroup":""},
    ],
    "nodes": [
        ["id", "posX", "posY", "posZ"],
        //--800cc I3 Engine--
        {"selfCollision":true},
        {"collision":true},
        {"nodeMaterial":"|NM_METAL"},
        {"frictionCoef":0.5},
        {"group":"atv_exhaust"},
        {"selfCollision":false},
        {"nodeWeight":1.5},
        ["ex1", 0.64, -0.95, 1.04, {"afterFireAudioCoef":0.43, "afterFireVisualCoef":0.43, "afterFireVolumeCoef":0.4, "afterFireMufflingCoef":0.35, "exhaustAudioMufflingCoef":0.85, "exhaustAudioGainChange":-1}],
        {"group":""},
    ],

    "beams": [
        ["id1:", "id2:"],
        {"beamType":"|NORMAL", "beamLongBound":1.0, "beamShortBound":1.0},
        {"beamSpring":1101000,"beamDamp":600},
        {"beamDeform":75000,"beamStrength":"FLT_MAX"},
        {"deformLimit":1.1,"deformLimitExpansion":1.05},
        //exhaust
        {"beamSpring":540000,"beamDamp":150},
        {"beamDeform":4000,"beamStrength":"FLT_MAX"},
        ["e1l","ex1", {"isExhaust":"mainEngine","beamSpring":10,"beamDamp":10}],
        ["b31ll","ex1"],
        ["b31lll","ex1"],
        ["b32ll","ex1"],
        ["b11ll","ex1"],
        ["b12ll","ex1"],
        {"deformLimit":"","deformLimitExpansion":""},
        {"beamPrecompression":1, "beamType":"|NORMAL", "beamLongBound":1.0, "beamShortBound":1.0},
    ],
},
"atv_engine_ecu_stock": {
    "information":{
        "authors":"BeamNG",
        "name": "Stock ECU",
        "value":625,
    },
    "slotType" : "atv_engine_ecu",

    "mainEngine":{
        "revLimiterRPM":4000,
        "revLimiterType":"soft",
    },
},
"atv_engine_ecu_race": {
    "information":{
        "authors":"BeamNG",
        "name":"Race ECU",
        "value":4500,
    },
    "slotType" : "atv_engine_ecu",

    "variables": [
        ["name", "type", "unit", "category", "default", "min", "max", "title", "description"],
        ["$revLimiterRPM", "range", "rpm", "Engine", 5500, 4500, 6500, "RPM Limit", "RPM where the rev limiter prevents further revving", {"stepDis":50}],
        ["$revLimiterCutTime", "range", "s", "Engine", 0.10, 0.01, 0.5, "RPM Limit Cut Time", "How fast the rev limiter cycles", {"stepDis":0.01}],
    ],
    "mainEngine":{
        "hasRevLimiter":true,
        "$+idleRPM":200,
        "revLimiterRPM":"$revLimiterRPM",
        "revLimiterType":"timeBased",
        "revLimiterCutTime":"$revLimiterCutTime",
        "$*instantAfterFireCoef": 1.50,
        "$*sustainedAfterFireCoef": 1.50,
    },
},
"atv_engine_internals": {
    "information":{
        "authors":"BeamNG",
        "name":"Stock Long Block",
        "value":100,
    },
    "slotType" : "atv_engine_internals",
    "mainEngine":{
    },
},
}