{
"lansdale_engine_i4_2.5_diesel": {
    "information":{
        "authors":"BeamNG",
        "name":"2.5L I4 Diesel Engine",
        "value":4250,
    },
    "slotType" : "lansdale_engine",
    "slots": [
        ["type", "default", "description"],
        ["lansdale_radiator_i4","lansdale_radiator_i4", "Radiator"],
        ["lansdale_enginemounts","lansdale_enginemounts", "Engine Mounts", {"coreSlot":true}],
        ["lansdale_oilpan_i4_diesel","lansdale_oilpan_i4_diesel", "Oil Pan", {"coreSlot":true}],
        ["lansdale_intake_i4_diesel","lansdale_intake_i4_diesel_turbo_early", "Intake", {"coreSlot":true}],
        ["lansdale_exhaust","lansdale_exhaust", "Exhaust"],
        ["lansdale_engine_i4_diesel_ecu","lansdale_engine_i4_diesel_ecu", "Engine Management", {"coreSlot":true}],
        //["lansdale_enginecover_i4","", "Engine Cover"],
        ["n2o_system","", "Nitrous Oxide System"],
        ["lansdale_engine_i4_diesel_internals","lansdale_engine_i4_diesel_internals", "Engine Long Block", {"coreSlot":true}],
        ["lansdale_transmission","lansdale_transmission_4A", "Transmission"],
    ],
    "powertrain": [
        ["type", "name", "inputName", "inputIndex"],
        ["combustionEngine", "mainEngine", "dummy", 0],
    ],
    "mainEngine": {
        "torque":[
            ["rpm", "torque"],
            [0,      0],
            [350,  120],
            [600,  135],
            [1000, 146],
            [1750, 155],
            [2000, 158],
            [2500, 162],
            [3000, 157],
            [4000, 152],
            [4500, 149],
            [5000, 140],
            [6000, 80],
            [7000, 20],
        ],

        "idleRPM":800,
        "maxRPM":5000,
        "inertia":0.22,
        "friction":17,
        "dynamicFriction":0.024,
        "engineBrakeTorque":30,
        //"burnEfficiency":0.33
        "burnEfficiency":[
            [0, 0.2],
            [0.05, 0.40],
            [0.4, 0.48],
            [0.7, 0.51],
            [1, 0.3],
        ],
        //fuel system
        "energyStorage": "mainTank",
        "requiredEnergyType":"diesel",

        //exhaust
        "instantAfterFireSound": "event:>Vehicle>Afterfire>01_Single_EQ1",
        "sustainedAfterFireSound": "event:>Vehicle>Afterfire>01_Multi_EQ1",
        "shiftAfterFireSound": "event:>Vehicle>Afterfire>01_Shift_EQ1",
        "particulates":0.06
        "instantAfterFireCoef": 0,
        "sustainedAfterFireCoef": 0,

        //cooling and oil system
        "thermalsEnabled":true,
        "engineBlockMaterial":"iron",
        "oilVolume":4,
        "engineBlockAirCoolingEfficiency":25,

        //engine durability
        "cylinderWallTemperatureDamageThreshold":150,
        "headGasketDamageThreshold":1500000,
        "pistonRingDamageThreshold":1500000,
        "connectingRodDamageThreshold":2000000,
        "maxTorqueRating": 450,
        "maxOverTorqueDamage": 400,

        //node beam interface
        "torqueReactionNodes:":["e1l","e2l","e4r"],
        "waterDamage": {"[engineGroup]:":["engine_intake"]},
        "radiator": {"[engineGroup]:":["radiator"]},
        "engineBlock": {"[engineGroup]:":["engine_block"]},
        "breakTriggerBeam":"engine",
        "uiName":"Engine",
        "soundConfig": "soundConfig",
        "soundConfigExhaust": "soundConfigExhaust",

        //starter motor
        "starterSample":"event:>Engine>Starter>i4diesel_1990_eng",
        "starterSampleExhaust":"event:>Engine>Starter>i4diesel_1990_exh",
        "shutOffSampleEngine":"event:>Engine>Shutoff>i4diesel_1990_eng",
        "shutOffSampleExhaust":"event:>Engine>Shutoff>i4diesel_1990_exh",
        "starterVolume":0.76,
        "starterVolumeExhaust":0.76,
        "shutOffVolumeEngine":0.76,
        "shutOffVolumeExhaust":0.76,
        "idleRPMStartRate":1.5,
        "idleRPMStartCoef":1.5,
        //"starterTorque":100,

        //engine deform groups
        "deformGroups":["mainEngine", "mainEngine_intake", "mainEngine_accessories"]
        "deformGroups_oilPan":["oilpan_damage"]
    },
    "soundConfig": {
        "sampleName": "I4D_engine",
        "intakeMuffling": 0.8,

        "mainGain": -10,
        "onLoadGain": 1.0,
        "offLoadGain":0.63,

        "maxLoadMix": 0.8,
        "minLoadMix": 0,

        "lowShelfGain": 3,
        "lowShelfFreq": 320,

        "highShelfGain": 8,
        "highShelfFreq": 3900,

        "eqLowGain": -8,
        "eqLowFreq": 1000,
        "eqLowWidth": 0.1,

        "eqHighGain": 3,
        "eqHighFreq": 6000,
        "eqHighWidth": 0.1,

        "fundamentalFrequencyCylinderCount":4
        "eqFundamentalGain": 2,
    },
    "soundConfigExhaust": {
        "sampleName": "I4D_exhaust",

        "mainGain": -6
        "onLoadGain": 1.0,
        "offLoadGain": 0.35,

        "maxLoadMix": 0.8,
        "minLoadMix": 0,

        "lowShelfGain": -10,
        "lowShelfFreq": 170,

        "highShelfGain": 3,
        "highShelfFreq":1700,

        "eqLowGain": -4,
        "eqLowFreq": 4250,
        "eqLowWidth": 0.15,

        "eqHighGain": 0,
        "eqHighFreq": 1800,
        "eqHighWidth": 0.1,

        "fundamentalFrequencyCylinderCount": 4,
        "eqFundamentalGain": -2,
    },
    "vehicleController": {
        "clutchLaunchStartRPM":1200,
        "clutchLaunchTargetRPM":1500,
        //**highShiftDown can be overwritten by automatic transmissions**
        "highShiftDownRPM":[0,0,0,2400,2900,3200,3600,3600],
        //**highShiftUp can be overwritten by intake modifications**
        "highShiftUpRPM":4400,
    },
    "flexbodies": [
        ["mesh", "[group]:", "nonFlexMaterials"],
        ["lansdale_engine_i4", ["lansdale_engine"]],
        ["legran_engine_i4_diesel", ["lansdale_engine"]],
    ],
    "props": [
        ["func", "mesh", "idRef:", "idX:", "idY:", "baseRotation", "rotation", "translation", "min", "max", "offset", "multiplier"],
        ["rpmspin", "lansdale_pulley_i4_crank",      "e2r","e2l","e4r", {"x":0, "y":0, "z":-90}, {"x":1, "y":0, "z":0}, {"x":0, "y":0, "z":0}, -360, 360, 0, 1],
        ["rpmspin", "lansdale_pulley_i4_alternator", "e2r","e2l","e4r", {"x":0, "y":0, "z":-90}, {"x":1, "y":0, "z":0}, {"x":0, "y":0, "z":0}, -1080, 1080, 0, 2],
        ["rpmspin", "lansdale_pulley_i4_waterpump",  "e2r","e2l","e4r", {"x":0, "y":0, "z":-90}, {"x":1, "y":0, "z":0}, {"x":0, "y":0, "z":0}, -720, 720, 0, 1.5],
        ["rpmspin", "lansdale_pulley_i4_steering",   "e2r","e2l","e4r", {"x":0, "y":0, "z":-90}, {"x":1, "y":0, "z":0}, {"x":0, "y":0, "z":0}, -540, 540, 0, 1.0],
        ["rpmspin", "lansdale_pulley_i4_ac",         "e2r","e2l","e4r", {"x":0, "y":0, "z":-90}, {"x":1, "y":0, "z":0}, {"x":0, "y":0, "z":0}, -540, 540, 0, 1.5],
    ],
    "nodes": [
         ["id", "posX", "posY", "posZ"],
         //--2.5L I4 Engine--
         {"selfCollision":false},
         {"collision":true},
         {"nodeMaterial":"|NM_METAL"},
         {"frictionCoef":0.5},
         {"group":"lansdale_engine"},
         {"engineGroup":"engine_block"},
         {"nodeWeight":31},
         ["e1r", 0.26, -1.775, 0.185,{"chemEnergy":1000,"burnRate":0.39,"flashPoint":800,"specHeat": 0.2,"selfIgnitionCoef":false,"smokePoint":650,"baseTemp":"thermals","conductionRadius":0.12}],
         ["e1l", 0.26, -1.535, 0.185],
         ["e2r", -0.27, -1.775, 0.185,{"chemEnergy":1000,"burnRate":0.39,"flashPoint":800,"specHeat": 0.2,"selfIgnitionCoef":false,"smokePoint":650,"baseTemp":"thermals","conductionRadius":0.12}],
         ["e2l", -0.27, -1.535, 0.185],
         {"engineGroup":["engine_block","engine_intake"]},
         ["e3r", 0.26, -1.815, 0.66,{"isExhaust":"mainEngine","chemEnergy":1000,"burnRate":0.39,"flashPoint":800,"specHeat": 0.2,"selfIgnitionCoef":false,"smokePoint":650,"baseTemp":"thermals","conductionRadius":0.12}],
         ["e3l", 0.26, -1.495, 0.66],
         ["e4r", -0.27, -1.815, 0.66,{"chemEnergy":1000,"burnRate":0.39,"flashPoint":800,"specHeat": 0.2,"selfIgnitionCoef":false,"smokePoint":650,"baseTemp":"thermals","conductionRadius":0.12}],
         ["e4l", -0.27, -1.495, 0.66],
         {"engineGroup":""},
         {"group":""},
         //engine mount node
         ["em1r", -0.38, -1.685, 0.46, {"nodeWeight":3}],
    ],
    "beams": [
          ["id1:", "id2:"],
          {"beamType":"|NORMAL", "beamLongBound":1.0, "beamShortBound":1.0},
          {"beamSpring":8400600,"beamDamp":150},
          {"beamDeform":540000,"beamStrength":"FLT_MAX"},
          //engine
          {"deformGroup":"mainEngine", "deformationTriggerRatio":0.001}
          ["e1r","e1l"],
          ["e2r","e2l"],
          ["e3r","e3l"],
          ["e4r","e4l"],

          ["e1r","e2r"],
          ["e1l","e2l"],
          ["e3r","e4r"],
          ["e3l","e4l"],

          ["e1r","e3r"],
          ["e1l","e3l"],
          ["e2r","e4r", {"isExhaust":"mainEngine"}],
          ["e2l","e4l"],

          ["e2r","e3r"],
          ["e2l","e3l"],
          ["e2r","e3l"],
          ["e2l","e3r"],

          ["e1r","e4r"],
          ["e1l","e4l"],
          ["e1r","e4l"],
          ["e1l","e4r"],

          ["e1r","e2l"],
          ["e1l","e2r"],
          ["e3r","e4l"],
          ["e3l","e4r"],

          //engine mount node
          {"beamSpring":4400000,"beamDamp":150},
          {"beamDeform":90000,"beamStrength":"FLT_MAX"},
          ["em1r","e3l"],
          ["em1r","e3r"],
          ["em1r","e4l"],
          ["em1r","e4r"],
          ["em1r", "e1r"],
          ["em1r", "e1l"],
          ["em1r", "e2l"],
          ["em1r", "e2r"],
          {"deformGroup":""},

          {"beamPrecompression":1, "beamType":"|NORMAL", "beamLongBound":1.0, "beamShortBound":1.0},
    ],
    "triangles": [
            ["id1:","id2:","id3:"],
            {"groundModel":"metal"},
            /*
            ["e5l", "e1l", "e3l"],
            ["e4r", "e3r", "e2r"],
            ["e3r", "e1r", "e2r"],
            ["e1r", "e1l", "e2r"],
            ["e2r", "e1l", "e2l"],
            ["e3l", "e1l", "e3r"],
            ["e3r", "e1l", "e1r"],
            ["e4r", "e4l", "e3l"],
            ["e4r", "e3l", "e3r"],
            ["e4r", "e2r", "e4l"],
            ["e4l", "e2r", "e2l"],
            ["e4l", "e2l", "e6l"],
            ["e6l", "e3l", "e4l"],
            ["e6l", "e2l", "e5l"],
            ["e6l", "e5l", "e3l"],
            ["e2l", "e1l", "e5l"],
            */
            {"triangleType":"NONCOLLIDABLE"},
            {"dragCoef":0},
            ["e2l", "e2r", "e1r"],
            ["e1r", "e1l", "e2l"],
            {"triangleType":"NORMALTYPE"},
    ],
},
"lansdale_oilpan_i4_diesel": {
    "information":{
        "authors":"BeamNG",
        "name":"Stock Oil Pan",
        "value":90,
    },
    "slotType" : "lansdale_oilpan_i4_diesel",
    "mainEngine": {
        //cooling and oil system
        "oilVolume":4.0,

        //engine durability
        "oilpanMaximumSafeG": 1.2

        //node beam interface
        "oilpanNodes:":["oilpan","oilref"],

        //engine deform groups
        "deformGroups_oilPan":["oilpan_damage"]
    },
    "nodes": [
         ["id", "posX", "posY", "posZ"],
         {"selfCollision":false},
         {"collision":true},
         {"frictionCoef":0.5},
         {"nodeMaterial":"|NM_METAL"},

         //oil pan node
         {"group":""},
         {"nodeWeight":2},
         ["oilpan", -0.05, -1.685, 0.155],
         ["oilref", -0.05, -1.685, 0.655, {"nodeWeight":1, "collision":false}],
    ],
    "beams": [
          ["id1:", "id2:"],
          {"beamPrecompression":1, "beamType":"|NORMAL", "beamLongBound":1.0, "beamShortBound":1.0},

          //oilpan node
          {"beamSpring":1501000,"beamDamp":250},
          {"beamDeform":11000,"beamStrength":"FLT_MAX"},
          {"deformGroup":"oilpan_damage","deformationTriggerRatio":0.005},
          ["oilpan", "e1r"],
          ["oilpan", "e1l"],
          ["oilpan", "e2r"],
          ["oilpan", "e2l"],
          ["oilpan", "e3r"],
          ["oilpan", "e3l"],
          ["oilpan", "e4r"],
          ["oilpan", "e4l"],
          {"deformGroup":""},

          //oil ref
          {"beamSpring":301000,"beamDamp":150},
          {"beamDeform":25000,"beamStrength":"FLT_MAX"},
          ["oilref", "e1r"],
          ["oilref", "e1l"],
          ["oilref", "e2r"],
          ["oilref", "e2l"],
          ["oilref", "e3r"],
          ["oilref", "e3l"],
          ["oilref", "e4r"],
          ["oilref", "e4l"],
          {"beamPrecompression":1, "beamType":"|NORMAL", "beamLongBound":1.0, "beamShortBound":1.0},
    ],
},
"lansdale_oilpan_i4_diesel_race": {
    "information":{
        "authors":"BeamNG",
        "name":"Race Oil Pan",
        "value":250,
    },
    "slotType" : "lansdale_oilpan_i4_diesel",
    "mainEngine": {
        //cooling and oil system
        "oilVolume":4.5,

        //engine durability
        "oilpanMaximumSafeG": 1.5

        //node beam interface
        "oilpanNodes:":["oilpan","oilref"],

        //engine deform groups
        "deformGroups_oilPan":["oilpan_damage"]
    },
    "nodes": [
         ["id", "posX", "posY", "posZ"],
         {"selfCollision":false},
         {"collision":true},
         {"frictionCoef":0.5},
         {"nodeMaterial":"|NM_METAL"},

         //oil pan node
         {"group":""},
         {"nodeWeight":2},
         ["oilpan", -0.05, -1.685, 0.155],
         ["oilref", -0.05, -1.685, 0.655, {"nodeWeight":1, "collision":false}],
    ],
    "beams": [
          ["id1:", "id2:"],
          {"beamPrecompression":1, "beamType":"|NORMAL", "beamLongBound":1.0, "beamShortBound":1.0},

          //oilpan node
          {"beamSpring":1501000,"beamDamp":250},
          {"beamDeform":11000,"beamStrength":"FLT_MAX"},
          {"deformGroup":"oilpan_damage","deformationTriggerRatio":0.005},
          ["oilpan", "e1r"],
          ["oilpan", "e1l"],
          ["oilpan", "e2r"],
          ["oilpan", "e2l"],
          ["oilpan", "e3r"],
          ["oilpan", "e3l"],
          ["oilpan", "e4r"],
          ["oilpan", "e4l"],
          {"deformGroup":""},

          //oil ref
          {"beamSpring":301000,"beamDamp":150},
          {"beamDeform":25000,"beamStrength":"FLT_MAX"},
          ["oilref", "e1r"],
          ["oilref", "e1l"],
          ["oilref", "e2r"],
          ["oilref", "e2l"],
          ["oilref", "e3r"],
          ["oilref", "e3l"],
          ["oilref", "e4r"],
          ["oilref", "e4l"],
          {"beamPrecompression":1, "beamType":"|NORMAL", "beamLongBound":1.0, "beamShortBound":1.0},
    ],
},
"lansdale_engine_i4_diesel_ecu": {
    "information":{
        "authors":"BeamNG",
        "name":"Stock ECU",
        "value":725,
    },
    "slotType" : "lansdale_engine_i4_diesel_ecu",
    "mainEngine":{
        "revLimiterRPM":4300,
        "revLimiterType":"soft",
        "revLimiterSmoothOvershootRPM":500,
    },
    "vehicleController": {
    },
},
"lansdale_engine_i4_diesel_ecu_race": {
    "information":{
        "authors":"BeamNG",
        "name":"Adjustable Race ECU",
        "value":3600,
    },
    "slotType" : "lansdale_engine_i4_diesel_ecu",
    "variables": [
        ["name", "type", "unit", "category", "default", "min", "max", "title", "description"],
        ["$revLimiterRPM", "range", "rpm", "Engine", 4600, 3500, 5500, "RPM Limit", "RPM where the rev limiter prevents further revving", {"stepDis":50}],
        ["$revLimiterCutTime", "range", "s", "Engine", 0.01, 0.01, 0.1, "RPM Limit Cut Time", "How fast the rev limiter cycles", {"stepDis":0.01}],
    ],
    "controller": [
        ["fileName"],
        ["twoStepLaunch", {"rpmLimit":4000}],
    ],
    "mainEngine":{
        "$+idleRPM":100,
        "revLimiterRPM":"$revLimiterRPM",
        "revLimiterType":"timeBased",
        "revLimiterCutTime":"$revLimiterCutTime",
        "$*instantAfterFireCoef": 2.00,
        "$*sustainedAfterFireCoef": 1.25,
    },
    "vehicleController": {
        "highShiftUpRPM":"$=$revLimiterRPM - 200",
    },
},
"lansdale_engine_i4_diesel_internals": {
    "information":{
        "authors":"BeamNG",
        "name":"Stock Long Block",
        "value":600,
    },
    "slotType" : "lansdale_engine_i4_diesel_internals",
    "powertrainDamage":[
        ["deviceName", "damageID"],
        ["mainEngine", "block"],
    ],
    "mainEngine":{
    },
},
"lansdale_engine_internals_i4_diesel_heavy": {
    "information":{
        "authors":"BeamNG",
        "name":"Heavy Duty Long Block",
        "value":1600,
    },
    "slotType" : "lansdale_engine_i4_diesel_internals",
    "powertrainDamage":[
        ["deviceName", "damageID"],
        ["mainEngine", "block"],
    ],
    "mainEngine":{
        //max rpm physically capable of
        "$+maxRPM":700,
        "$*friction":1.13,
        "$*dynamicFriction":1.15,
        "$*inertia":1.15,
        "$*engineBrakeTorque":1.2,
        //engine durability
        "cylinderWallTemperatureDamageThreshold":175,
        "headGasketDamageThreshold":1700000,
        "pistonRingDamageThreshold":1700000,
        "connectingRodDamageThreshold":2200000,
        //"maxTorqueRating": 490,
        //"maxOverTorqueDamage": 450,
        "$*maxTorqueRating": 1.69,
        "$*maxOverTorqueDamage": 1.5,
    },
},
"lansdale_engine_internals_i4_diesel_ultra": {
    "information":{
        "authors":"BeamNG",
        "name":"Ultra Heavy Duty Long Block",
        "value":3100,
    },
    "slotType" : "lansdale_engine_i4_diesel_internals",
    "powertrainDamage":[
        ["deviceName", "damageID"],
        ["mainEngine", "block"],
    ],
    "mainEngine":{
        //max rpm physically capable of
        "$+maxRPM":500,
        "$*friction":1.3,
        "$*dynamicFriction":1.3,
        "$*inertia":1.3,
        "$*engineBrakeTorque":1.18,
        //engine durability
        "cylinderWallTemperatureDamageThreshold":175,
        "headGasketDamageThreshold":1700000,
        "pistonRingDamageThreshold":1700000,
        "connectingRodDamageThreshold":2200000,
        //"maxTorqueRating": 870,
        //"maxOverTorqueDamage": 450,
        "$*maxTorqueRating": 2.9,
        "$*maxOverTorqueDamage": 2.7,
    },
},
"lansdale_intake_i4_diesel_turbo_early": {
    "information":{
        "authors":"BeamNG",
        "name":"Early Turbocharger",
        "value":1950,
    },
    "slotType" : "lansdale_intake_i4_diesel",
    "turbocharger": {
        "bovEnabled":false,
        "hissLoopEvent":"event:>Vehicle>Forced_Induction>Turbo_02>turbo_hiss",
        "whineLoopEvent":"event:>Vehicle>Forced_Induction>Turbo_02>turbo_spin",
        "turboSizeCoef": 0.0,
        "bovSoundVolumeCoef": 0.0,
        "hissVolumePerPSI": 0.03,
        "whineVolumePer10kRPM": 0.028,
        "whinePitchPer10kRPM": 0.07,
        "wastegateStart":14,
        "maxExhaustPower": 4500,
        "backPressureCoef": 0.000006,
        "pressureRatePSI": 25,
        "frictionCoef": 15.5,
        "inertia":1.20,
        "damageThresholdTemperature": 610,
        "pressurePSI":[
            //turbineRPM, pressure(PSI)
            [0,         -3.5],
            [24000,     -1.5],
            [60000,     15],
            [90000,     20],
            [150000,    25],
            [200000,    35],
            [250000,    40],
        ],
        "engineDef":[
            //engineRPM, efficiency, exhaustFactor
            [0,          0.0,        0.00],
            [650,        0.45,       0.06],
            [1000,       0.85,       0.10],
            [1500,       0.95,        0.35],
            [2000,       1.00,       0.70],
            [2500,       0.95,       0.90],
            [3000,       0.90,       1.00],
            [3500,       0.82,       1.00],
            [4000,       0.74,       1.00],
            [4500,       0.57,       1.00],
            [5000,       0.37,       1.00],
            [6000,       0.1,        1.00],
        ],
    },
    "soundConfig": {
        "$+intakeMuffling":0.1,
    },
    "mainEngine":{
        //turbocharger name
        "turbocharger":"turbocharger",
        //"instantAfterFireCoef": 1,
        //"sustainedAfterFireCoef": 0.75,
        "$*instantAfterFireCoef": 2,
        "$*sustainedAfterFireCoef": 2,

        //turbocharger deform groups
        "deformGroups_turbo":["mainEngine_turbo","mainEngine_intercooler","mainEngine_piping"]
    },
    "flexbodies": [
        ["mesh", "[group]:", "nonFlexMaterials"],
        ["legran_engine_i4_diesel_intake_M", ["lansdale_engine"]],
    ],
    "nodes": [
         ["id", "posX", "posY", "posZ"],
         {"selfCollision":false},
         {"collision":true},
         {"nodeMaterial":"|NM_METAL"},
         {"frictionCoef":0.5},
         {"engineGroup":"engine_intake"},
         {"nodeWeight":2},
         {"group":"lansdale_turbo"},
         ["trb1", -0.22, -1.481, 0.387],
         {"engineGroup":""},
         ["trb2", 0.02, -1.481, 0.387],
         {"nodeWeight":3.0},
         {"group":"lansdale_header"},
         ["exm1r", 0.09, -1.325, 0.305, {"afterFireAudioCoef":1.0, "afterFireVisualCoef":1.0, "afterFireVolumeCoef":1.0, "afterFireMufflingCoef":1.0, "exhaustAudioMufflingCoef":1.0, "exhaustAudioGainChange":0}],
         {"group":""},
    ],
    "beams": [
          ["id1:", "id2:"],
          {"beamType":"|NORMAL", "beamLongBound":1.0, "beamShortBound":1.0},

          //turbo
          {"beamSpring":1001000,"beamDamp":50},
          {"beamDeform":96000,"beamStrength":"FLT_MAX"},
          {"deformGroup":"mainEngine_turbo", "deformationTriggerRatio":0.01}
          ["trb1","e1r"],
          ["trb1","e2r"],
          ["trb1","e3r", {"isExhaust":true}],
          ["trb1","e4r"],
          ["trb1","e1l"],
          ["trb1","e2l"],
          ["trb1","e3l"],
          ["trb1","e4l"],
          ["trb2","e1r"],
          ["trb2","e2r"],
          ["trb2","e3r"],
          ["trb2","e4r"],
          ["trb2","e1l"],
          ["trb2","e2l"],
          ["trb2","e3l"],
          ["trb2","e4l"],
          ["trb2","trb1", {"isExhaust":true}],

          //exhaust connector
          {"beamSpring":5010000,"beamDamp":90},
          {"beamDeform":90000,"beamStrength":"FLT_MAX"},
          ["exm1r","trb2", {"isExhaust":"mainEngine"}],
          ["exm1r","e3l"],
          ["exm1r","e3r"],
          ["exm1r","e4l"],
          ["exm1r","e4r"],
          ["exm1r", "e1r"],
          ["exm1r", "e1l"],
          ["exm1r", "e2l"],
          ["exm1r", "e2r"],
          {"beamPrecompression":1, "beamType":"|NORMAL", "beamLongBound":1.0, "beamShortBound":1.0},
    ],
},
"lansdale_intake_i4_diesel_turbo_late": {
    "information":{
        "authors":"BeamNG",
        "name":"Late Turbocharger",
        "value":3950,
    },
    "slotType" : "lansdale_intake_i4_diesel",
    "turbocharger": {
        "bovEnabled":false,
        "hissLoopEvent":"event:>Vehicle>Forced_Induction>Turbo_02>turbo_hiss",
        "whineLoopEvent":"event:>Vehicle>Forced_Induction>Turbo_02>turbo_spin",
        "turboSizeCoef": 0.0,
        "bovSoundVolumeCoef": 0.0,
        "hissVolumePerPSI": 0.03,
        "whineVolumePer10kRPM": 0.032,
        "whinePitchPer10kRPM": 0.08,
        "wastegateStart":17,
        "maxExhaustPower": 4500,
        "backPressureCoef": 0.000006,
        "pressureRatePSI": 25,
        "frictionCoef": 15.5,
        "inertia":1.00,
        "damageThresholdTemperature": 610,
        "pressurePSI":[
            //turbineRPM, pressure(PSI)
            [0,         -3.5],
            [24000,     -1.5],
            [60000,     15],
            [90000,     20],
            [150000,    25],
            [200000,    35],
            [250000,    40],
        ],
        "engineDef":[
            //engineRPM, efficiency, exhaustFactor
            [0,          0.0,        0.00],
            [650,        0.45,       0.06],
            [1000,       0.85,       0.10],
            [1500,       0.95,        0.35],
            [2000,       1.00,       0.70],
            [2500,       0.95,       0.90],
            [3000,       0.90,       1.00],
            [3500,       0.82,       1.00],
            [4000,       0.74,       1.00],
            [4500,       0.57,       1.00],
            [5000,       0.37,       1.00],
            [6000,       0.1,        1.00],
        ],
    },
    "soundConfig": {
        "$+intakeMuffling":0.2,
    },
    "mainEngine":{
        //turbocharger name
        "turbocharger":"turbocharger",
        //"instantAfterFireCoef": 1,
        //"sustainedAfterFireCoef": 0.75,
        "$*instantAfterFireCoef": 2,
        "$*sustainedAfterFireCoef": 2,
        "torqueModIntake":[
            ["rpm", "torque"],
            [0, 0],
            [1000, 10],
            [2000, 17],
            [2500, 22],
            [3000, 25],
            [3500, 24],
            [4000, 19],
            [4500, 14],
            [5000, 5],
        ],
        //turbocharger deform groups
        "deformGroups_turbo":["mainEngine_turbo","mainEngine_intercooler","mainEngine_piping"]
    },
    "flexbodies": [
        ["mesh", "[group]:", "nonFlexMaterials"],
        ["legran_engine_i4_diesel_intake_EFI", ["lansdale_engine", "lansdale_intercooler"]],
    ],
    "nodes": [
         ["id", "posX", "posY", "posZ"],
         {"selfCollision":false},
         {"collision":true},
         {"nodeMaterial":"|NM_METAL"},
         {"frictionCoef":0.5},
         {"engineGroup":"engine_intake"},
         {"nodeWeight":2},
         {"group":"lansdale_turbo"},
         ["trb1", -0.22, -1.481, 0.387],
         {"engineGroup":""},
         ["trb2", 0.02, -1.481, 0.387],
         {"group":"lansdale_intercooler"},
         {"nodeWeight":5},
         ["itc1", 0.508, -1.568, 0.74],
         ["itc2", 0.508, -1.8, 0.74],
         {"nodeWeight":3.0},
         {"group":"lansdale_header"},
         ["exm1r", 0.09, -1.325, 0.305, {"afterFireAudioCoef":1.0, "afterFireVisualCoef":1.0, "afterFireVolumeCoef":1.0, "afterFireMufflingCoef":1.0, "exhaustAudioMufflingCoef":1.0, "exhaustAudioGainChange":0}],
         {"group":""},
    ],
    "beams": [
          ["id1:", "id2:"],
          {"beamType":"|NORMAL", "beamLongBound":1.0, "beamShortBound":1.0},

          //turbo
          {"beamSpring":1001000,"beamDamp":50},
          {"beamDeform":96000,"beamStrength":"FLT_MAX"},
          {"deformGroup":"mainEngine_turbo", "deformationTriggerRatio":0.01}
          ["trb1","e1r"],
          ["trb1","e2r"],
          ["trb1","e3r", {"isExhaust":true}],
          ["trb1","e4r"],
          ["trb1","e1l"],
          ["trb1","e2l"],
          ["trb1","e3l"],
          ["trb1","e4l"],
          ["trb2","e1r"],
          ["trb2","e2r"],
          ["trb2","e3r"],
          ["trb2","e4r"],
          ["trb2","e1l"],
          ["trb2","e2l"],
          ["trb2","e3l"],
          ["trb2","e4l"],
          ["trb2","trb1", {"isExhaust":true}],

          //intercooler
          {"beamDeform":4000,"beamStrength":"FLT_MAX"},
          {"deformGroup":"mainEngine_intercooler", "deformationTriggerRatio":0.1}
          ["itc2", "itc1"],
          ["itc1", "e3l"],
          ["itc2", "e3r"],
          ["itc2", "e3l"],
          ["itc1", "e3r"],
          ["itc2", "e1r"],
          ["itc1", "e1l"],
          ["itc2", "e1l"],
          ["itc1", "e1r"],

          //charge pipes
          {"beamSpring":100000,"beamDamp":150},
          {"beamDeform":2000,"beamStrength":"FLT_MAX"},
          {"deformGroup":"mainEngine_piping", "deformationTriggerRatio":0.01}
          ["itc1", "trb2"],
          ["itc1", "trb1"],
          ["itc2", "e4r"],
          ["itc2", "e2r"],
          {"deformGroup":""}

          //exhaust connector
          {"beamSpring":5010000,"beamDamp":90},
          {"beamDeform":90000,"beamStrength":"FLT_MAX"},
          ["exm1r","trb2", {"isExhaust":"mainEngine"}],
          ["exm1r","e3l"],
          ["exm1r","e3r"],
          ["exm1r","e4l"],
          ["exm1r","e4r"],
          ["exm1r", "e1r"],
          ["exm1r", "e1l"],
          ["exm1r", "e2l"],
          ["exm1r", "e2r"],
          {"beamPrecompression":1, "beamType":"|NORMAL", "beamLongBound":1.0, "beamShortBound":1.0},
    ],
},
"lansdale_intake_i4_diesel_turbo_stage1": {
    "information":{
        "authors":"BeamNG",
        "name":"Stage 1 Turbocharger",
        "value":5200,
    },
    "slotType" : "lansdale_intake_i4_diesel",
    "turbocharger": {
        "bovEnabled":false,
        "hissLoopEvent":"event:>Vehicle>Forced_Induction>Turbo_02>turbo_hiss_tuned",
        "whineLoopEvent":"event:>Vehicle>Forced_Induction>Turbo_02>turbo_spin_tuned",
        "turboSizeCoef": 0.7,
        "bovSoundVolumeCoef": 0.00,
        "hissVolumePerPSI": 0.03,
        "whineVolumePer10kRPM": 0.021,
        "whinePitchPer10kRPM": 0.045,
        "wastegateStart":30,
        "maxExhaustPower": 4500,
        "backPressureCoef": 0.000006,
        "pressureRatePSI": 25,
        "frictionCoef": 15.5,
        "inertia":1.60,
        "damageThresholdTemperature": 610,
        "pressurePSI":[
            //turbineRPM, pressure(PSI)
            [0,         -3.5],
            [24000,     -1.5],
            [60000,     15],
            [90000,     20],
            [150000,    25],
            [200000,    35],
            [250000,    40],
        ],
        "engineDef":[
            //engineRPM, efficiency, exhaustFactor
            [0,          0.0,        0.00],
            [650,        0.45,       0.1],
            [1000,       0.85,       0.15],
            [1500,       0.95,        0.25],
            [2000,       1.00,       0.70],
            [2500,       0.98,       0.90],
            [2800,       0.95,       0.98],
            [3000,       0.93,       1.00],
            [3500,       0.85,       1.00],
            [4000,       0.74,       1.00],
            [4500,       0.57,       1.00],
            [5000,       0.37,       1.00],
            [6000,       0.1,        1.00],
        ],
    },
    "soundConfig": {
        "$+intakeMuffling":-0.2,
    },
    "mainEngine":{
        //turbocharger name
        "turbocharger":"turbocharger",
        //"instantAfterFireCoef": 1.25,
        //"sustainedAfterFireCoef": 1,
        "$*instantAfterFireCoef": 2.5,
        "$*sustainedAfterFireCoef": 2.5,
        "$*particulates": 1.5,

        //turbocharger deform groups
        "deformGroups_turbo":["mainEngine_turbo","mainEngine_intercooler","mainEngine_piping"]
    },
    "flexbodies": [
        ["mesh", "[group]:", "nonFlexMaterials"],
        ["legran_engine_i4_diesel_intake_EFI", ["lansdale_engine", "lansdale_intercooler"]],
    ],
    "nodes": [
         ["id", "posX", "posY", "posZ"],
         {"selfCollision":false},
         {"collision":true},
         {"nodeMaterial":"|NM_METAL"},
         {"frictionCoef":0.5},
         {"engineGroup":"engine_intake"},
         {"nodeWeight":2},
         {"group":"lansdale_turbo"},
         ["trb1", -0.22, -1.481, 0.387],
         {"engineGroup":""},
         ["trb2", 0.02, -1.481, 0.387],
         {"group":"lansdale_intercooler"},
         {"nodeWeight":5},
         ["itc1", 0.508, -1.568, 0.74],
         ["itc2", 0.508, -1.8, 0.74],
         {"nodeWeight":3.0},
         {"group":"lansdale_header"},
         ["exm1r", 0.09, -1.325, 0.305, {"afterFireAudioCoef":1.0, "afterFireVisualCoef":1.0, "afterFireVolumeCoef":1.0, "afterFireMufflingCoef":1.0, "exhaustAudioMufflingCoef":1.0, "exhaustAudioGainChange":0}],
         {"group":""},
    ],
    "beams": [
          ["id1:", "id2:"],
          {"beamType":"|NORMAL", "beamLongBound":1.0, "beamShortBound":1.0},

          //turbo
          {"beamSpring":1001000,"beamDamp":50},
          {"beamDeform":96000,"beamStrength":"FLT_MAX"},
          {"deformGroup":"mainEngine_turbo", "deformationTriggerRatio":0.01}
          ["trb1","e1r"],
          ["trb1","e2r"],
          ["trb1","e3r", {"isExhaust":true}],
          ["trb1","e4r"],
          ["trb1","e1l"],
          ["trb1","e2l"],
          ["trb1","e3l"],
          ["trb1","e4l"],
          ["trb2","e1r"],
          ["trb2","e2r"],
          ["trb2","e3r"],
          ["trb2","e4r"],
          ["trb2","e1l"],
          ["trb2","e2l"],
          ["trb2","e3l"],
          ["trb2","e4l"],
          ["trb2","trb1", {"isExhaust":true}],

          //intercooler
          {"beamDeform":4000,"beamStrength":"FLT_MAX"},
          {"deformGroup":"mainEngine_intercooler", "deformationTriggerRatio":0.1}
          ["itc2", "itc1"],
          ["itc1", "e3l"],
          ["itc2", "e3r"],
          ["itc2", "e3l"],
          ["itc1", "e3r"],
          ["itc2", "e1r"],
          ["itc1", "e1l"],
          ["itc2", "e1l"],
          ["itc1", "e1r"],

          //charge pipes
          {"beamSpring":100000,"beamDamp":150},
          {"beamDeform":2000,"beamStrength":"FLT_MAX"},
          {"deformGroup":"mainEngine_piping", "deformationTriggerRatio":0.01}
          ["itc1", "trb2"],
          ["itc1", "trb1"],
          ["itc2", "e4r"],
          ["itc2", "e2r"],
          {"deformGroup":""}

          //exhaust connector
          {"beamSpring":5010000,"beamDamp":90},
          {"beamDeform":90000,"beamStrength":"FLT_MAX"},
          ["exm1r","trb2", {"isExhaust":"mainEngine"}],
          ["exm1r","e3l"],
          ["exm1r","e3r"],
          ["exm1r","e4l"],
          ["exm1r","e4r"],
          ["exm1r", "e1r"],
          ["exm1r", "e1l"],
          ["exm1r", "e2l"],
          ["exm1r", "e2r"],
          {"beamPrecompression":1, "beamType":"|NORMAL", "beamLongBound":1.0, "beamShortBound":1.0},
    ],
},
"lansdale_intake_i4_diesel_turbo_stage3": {
    "information":{
        "authors":"BeamNG",
        "name":"Stage 3 Variable Boost Turbocharger",
        "value":8500,
    },
    "slotType" : "lansdale_intake_i4_diesel",
    "variables": [
        ["name", "type", "unit", "category", "default", "min", "max", "title", "description"],
        ["$wastegateStart", "range", "psi", "Engine", 20, 12, 40, "Wastegate Pressure", "Pressure at which the wastegate begins to open", {"stepDis":0.5}],
    ],
    "turbocharger": {
        "hissLoopEvent":"event:>Vehicle>Forced_Induction>Turbo_02>turbo_hiss_race",
        "whineLoopEvent":"event:>Vehicle>Forced_Induction>Turbo_02>turbo_spin_race",
        "bovEnabled":false,
        "turboSizeCoef": 1.0,
        "flutterSoundVolumeCoef":0.8,
        "hissVolumePerPSI": 0.03,
        "whineVolumePer10kRPM": 0.021,
        "whinePitchPer10kRPM": 0.045,
        "wastegateStart":"$wastegateStart",
        "wastegateLimit":"$=$wastegateStart+1",
        "maxExhaustPower": 100000,
        "backPressureCoef": 0.00004,
        "frictionCoef": 25.3,
        "inertia":15.4,
        "pressureRatePSI": 75,
        "damageThresholdTemperature": 900,
        "pressurePSI":[
            //turbineRPM, pressure(PSI)
            [0,         -4],
            [30000,     -2.5],
            [60000,     3],
            [90000,     8],
            [150000,    22],
            [200000,    29],
            [250000,    33],
            [300000,    37],
        ],
        "engineDef":[
            //engineRPM, efficiency, exhaustFactor
            [0,          0.0,        0.00],
            [650,        0.45,       0.07],
            [1000,       0.85,       0.08],
            [1500,       0.93,       0.15],
            [2000,       0.95,       0.30],
            [2500,       0.98,       0.70],
            [2800,       1.00,       0.88],
            [3000,       0.99,       1.00],
            [3500,       0.93,       1.00],
            [4000,       0.88,       1.00],
            [4500,       0.85,       1.00],
            [5000,       0.80,       1.00],
            [5500,       0.74,       1.00],
            [6000,       0.1,        1.00],
        ],
    },
    "vehicleController": {
        "revMatchThrottle":0.2,
        "clutchLaunchStartRPM":2200,
        "clutchLaunchTargetRPM":3100,
    },
    "soundConfig": {
        "$+intakeMuffling":-0.4,
    },
    "mainEngine":{
        //turbocharger name
        "turbocharger":"turbocharger",
        //"instantAfterFireCoef": 1.5,
        //"sustainedAfterFireCoef": 1.25,
        "$+idleRPMRoughness":50,
        "$*instantAfterFireCoef": 3,
        "$*sustainedAfterFireCoef": 3,
        "$*particulates": 2.9,

        //turbocharger deform groups
        "deformGroups_turbo":["mainEngine_turbo","mainEngine_intercooler","mainEngine_piping"]
    },
    "flexbodies": [
        ["mesh", "[group]:", "nonFlexMaterials"],
        ["legran_engine_i4_diesel_intake_EFI", ["lansdale_engine", "lansdale_intercooler"]],
    ],
    "nodes": [
         ["id", "posX", "posY", "posZ"],
         {"selfCollision":false},
         {"collision":true},
         {"nodeMaterial":"|NM_METAL"},
         {"frictionCoef":0.5},
         {"engineGroup":"engine_intake"},
         {"nodeWeight":2},
         {"group":"lansdale_turbo"},
         ["trb1", -0.22, -1.481, 0.387],
         {"engineGroup":""},
         ["trb2", 0.02, -1.481, 0.387],
         {"group":"lansdale_intercooler"},
         {"nodeWeight":5},
         ["itc1", 0.508, -1.568, 0.74],
         ["itc2", 0.508, -1.8, 0.74],
         {"nodeWeight":3.0},
         {"group":"lansdale_header"},
         ["exm1r", 0.09, -1.325, 0.305, {"afterFireAudioCoef":1.0, "afterFireVisualCoef":1.0, "afterFireVolumeCoef":1.0, "afterFireMufflingCoef":1.0, "exhaustAudioMufflingCoef":1.0, "exhaustAudioGainChange":0}],
         {"group":""},
    ],
    "beams": [
          ["id1:", "id2:"],
          {"beamType":"|NORMAL", "beamLongBound":1.0, "beamShortBound":1.0},

          //turbo
          {"beamSpring":1001000,"beamDamp":50},
          {"beamDeform":96000,"beamStrength":"FLT_MAX"},
          {"deformGroup":"mainEngine_turbo", "deformationTriggerRatio":0.01}
          ["trb1","e1r"],
          ["trb1","e2r"],
          ["trb1","e3r", {"isExhaust":true}],
          ["trb1","e4r"],
          ["trb1","e1l"],
          ["trb1","e2l"],
          ["trb1","e3l"],
          ["trb1","e4l"],
          ["trb2","e1r"],
          ["trb2","e2r"],
          ["trb2","e3r"],
          ["trb2","e4r"],
          ["trb2","e1l"],
          ["trb2","e2l"],
          ["trb2","e3l"],
          ["trb2","e4l"],
          ["trb2","trb1", {"isExhaust":true}],

          //intercooler
          {"beamDeform":4000,"beamStrength":"FLT_MAX"},
          {"deformGroup":"mainEngine_intercooler", "deformationTriggerRatio":0.1}
          ["itc2", "itc1"],
          ["itc1", "e3l"],
          ["itc2", "e3r"],
          ["itc2", "e3l"],
          ["itc1", "e3r"],
          ["itc2", "e1r"],
          ["itc1", "e1l"],
          ["itc2", "e1l"],
          ["itc1", "e1r"],

          //charge pipes
          {"beamSpring":100000,"beamDamp":150},
          {"beamDeform":2000,"beamStrength":"FLT_MAX"},
          {"deformGroup":"mainEngine_piping", "deformationTriggerRatio":0.01}
          ["itc1", "trb2"],
          ["itc1", "trb1"],
          ["itc2", "e4r"],
          ["itc2", "e2r"],
          {"deformGroup":""}

          //exhaust connector
          {"beamSpring":5010000,"beamDamp":90},
          {"beamDeform":90000,"beamStrength":"FLT_MAX"},
          ["exm1r","trb2", {"isExhaust":"mainEngine"}],
          ["exm1r","e3l"],
          ["exm1r","e3r"],
          ["exm1r","e4l"],
          ["exm1r","e4r"],
          ["exm1r", "e1r"],
          ["exm1r", "e1l"],
          ["exm1r", "e2l"],
          ["exm1r", "e2r"],
          {"beamPrecompression":1, "beamType":"|NORMAL", "beamLongBound":1.0, "beamShortBound":1.0},
    ],
},
"lansdale_enginecover_i4_diesel": {
    "information":{
        "authors":"BeamNG",
        "name":"Engine Cover",
        "value":30,
    },
    "slotType" : "lansdale_enginecover_i4_diesel",
    "flexbodies": [
        ["mesh", "[group]:", "nonFlexMaterials"],
        ["lansdale_intake_cover_i4", ["lansdale_engine"]],
    ],
},
}
