{
"tire_F_175_65_14_alt_sport": {
    "information":{
        "authors":"BeamNG",
        "name":"175/65R14 Sport Front Tires",
        "value":110,
    },
    "slotType" : "tire_F_14x6_alt",
    "flexbodies": [
        ["mesh", "[group]:", "nonFlexMaterials"],
        ["tire_01a_14x6_23", ["wheel_FR","tire_FR"], [], {"pos":{"x":-0.485, "y":0.0, "z":0.0}, "rot":{"x":0, "y":0, "z":180}, "scale":{"x":1.09, "y":1.003, "z":1.003}}],
        ["tire_01a_14x6_23", ["wheel_FL","tire_FL"], [], {"pos":{"x": 0.485, "y":0.0, "z":0.0}, "rot":{"x":0, "y":0, "z":0},   "scale":{"x":1.09, "y":1.003, "z":1.003}}],
    ],
    "variables": [
        ["name", "type", "unit", "category", "default", "min", "max", "title", "description"],
        ["$tirepressure_F", "range", "psi", "Wheels", 25, 0, 50, "Tire Pressure", "Relative to atmospheric pressure", {"subCategory":"Front"}],
    ],
    "pressureWheels": [
        ["name","hubGroup","group","node1:","node2:","nodeS","nodeArm:","wheelDir"],
        {"disableMeshBreaking":false,"disableHubMeshBreaking":false},
        {"hasTire":true},
        {"enableTireReinfBeams":false},
        {"enableTireLbeams":true},
        {"enableTireSideReinfBeams":false},
        {"enableTreadReinfBeams":true},
        {"enableTirePeripheryReinfBeams":true},

        //general settings
        {"radius":0.290},
        {"tireWidth":0.155},

        //tire options
        {"wheelSideBeamSpring":"$=$tirepressure_F*650","wheelSideBeamDamp":25},
        {"wheelSideBeamSpringExpansion":301000,"wheelSideBeamDampExpansion":25},
        {"wheelSideTransitionZone":0.1,"wheelSideBeamPrecompression":0.99},

        {"wheelReinfBeamSpring":16000,"wheelReinfBeamDamp":155},
        {"wheelReinfBeamDampCutoffHz":500,"wheelReinfBeamPrecompression":0.98},

        {"wheelTreadBeamSpring":65000,"wheelTreadBeamDamp":55},
        {"wheelTreadBeamDampCutoffHz":500,"wheelTreadBeamPrecompression":0.98},

        {"wheelTreadReinfBeamSpring":145000,"wheelTreadReinfBeamDamp":45},
        {"wheelTreadReinfBeamDampCutoffHz":500,"wheelTreadReinfBeamPrecompression":0.98},

        {"wheelPeripheryBeamSpring":33000,"wheelPeripheryBeamDamp":30},
        {"wheelPeripheryBeamDampCutoffHz":500,"wheelPeripheryBeamPrecompression":0.98},

        {"wheelPeripheryReinfBeamSpring":96000,"wheelPeripheryReinfBeamDamp":25},
        {"wheelPeripheryReinfBeamDampCutoffHz":500,"wheelPeripheryReinfBeamPrecompression":0.98},

        //general tire values
        {"nodeWeight":0.125},
        {"nodeMaterial":"|NM_RUBBER"},
        {"triangleCollision":false},
        {"pressurePSI":"$tirepressure_F"},
        {"dragCoef":5},

        //groundmodel friction multipliers
        {"frictionCoef":1.0},
        {"slidingFrictionCoef":1.0},
        {"treadCoef":0.5},

        //advanced friction values
        {"noLoadCoef":1.42},
        {"loadSensitivitySlope":0.0002},
        {"fullLoadCoef":0.4},
        {"softnessCoef":0.7},

        //deform values
        {"wheelSideBeamDeform":11000,"wheelSideBeamStrength":15000},
        {"wheelTreadBeamDeform":10000,"wheelTreadBeamStrength":13000},
        {"wheelPeripheryBeamDeform":45000,"wheelPeripheryBeamStrength":45000},
    ],
},
}