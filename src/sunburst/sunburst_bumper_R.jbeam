{
"sunburst_bumper_R": {
    "information":{
        "authors":"BeamNG",
        "name":"Rear Bumper",
        "value":470,
    },
    "slotType" : "sunburst_bumper_R",
    "slots": [
        ["type", "default", "description"],
        ["sunburst_mudflap_R","", "Rear Mudflaps"],
    ],
    "flexbodies": [
       ["mesh", "[group]:", "nonFlexMaterials"],
       ["sunburst_bumper_R", ["sunburst_bumper_R"]],
    ],
    "nodes": [
       ["id", "posX", "posY", "posZ"],
       {"frictionCoef":0.8},
       {"nodeMaterial":"|NM_PLASTIC"},
       //--REAR BUMPER--
       {"collision":true},
       {"selfCollision":true},
       {"nodeWeight":0.6},
       {"group":"sunburst_bumper_R"},
       ["rb1rr", -0.66, 2.01, 0.63],
       ["rb1r", -0.36, 2.13, 0.67],
       ["rb1l", 0.36, 2.13, 0.67],
       ["rb1ll", 0.66, 2.01, 0.63],

       ["rb2rr", -0.67, 2.05, 0.50],
       ["rb2r", -0.36, 2.19, 0.50],
       ["rb2", 0, 2.22, 0.50],
       ["rb2l", 0.36, 2.19, 0.50],
       ["rb2ll", 0.67, 2.05, 0.50],

       ["rb3rr", -0.66, 1.99, 0.24],
       ["rb3r", -0.36, 2.13, 0.25],
       ["rb3", 0, 2.16, 0.25],
       ["rb3l", 0.36, 2.13, 0.25],
       ["rb3ll", 0.66, 1.99, 0.24],

       ["rb4rr", -0.825, 1.59, 0.52],
       ["rb4ll", 0.825, 1.59, 0.52],

       ["rb5rr", -0.795, 1.64, 0.20],
       ["rb5ll", 0.795, 1.64, 0.20],

       {"group":""},
       {"collision":false},
       {"selfCollision":false},
       ["rb6", 0, 2.06, 0.36],
    ],
    "beams": [
          ["id1:", "id2:"],
          //--REAR BUMPER--
          //rear bumper main shape
          {"beamPrecompression":1, "beamType":"|NORMAL", "beamLongBound":1.0, "beamShortBound":1.0},
          {"beamSpring":151000,"beamDamp":100},
          {"beamDeform":20000,"beamStrength":59000},
          {"deformLimitExpansion":1.1},
          ["rb4rr","rb1rr"],
          ["rb1rr","rb1r"],
          ["rb4rr","rb2rr"],
          ["rb2rr","rb2r"],
          ["rb1l","rb1ll"],
          ["rb4ll","rb1ll"],
          ["rb2l","rb2ll"],
          ["rb4ll","rb2ll"],
          //["rb1r","rb1"],
          //["rb1","rb1l"],

          ["rb2r","rb2"],
          ["rb2","rb2l"],

          ["rb5rr","rb3rr"],
          ["rb3rr","rb3r"],
          ["rb3r","rb3"],
          ["rb3","rb3l"],
          ["rb3l","rb3ll"],
          ["rb5ll","rb3ll"],

          ["rb1rr","rb2rr"],
          ["rb1r","rb2r"],
          ["rb1l","rb2l"],
          ["rb1ll","rb2ll"],
          //[/,"rb2"],

          ["rb2rr","rb3rr"],
          ["rb2r","rb3r"],
          ["rb2","rb3"],
          ["rb2l","rb3l"],
          ["rb2ll","rb3ll"],

          ["rb4rr","rb5rr"],
          ["rb4ll","rb5ll"],

          //crossing
          {"deformLimitExpansion":""},
          {"beamDeform":9750,"beamStrength":59000},
          ["rb4rr","rb3rr"],
          ["rb5rr","rb2rr"],
          ["rb2rr","rb3r"],
          ["rb3rr","rb2r"],
          ["rb2r","rb3"],
          ["rb3r","rb2"],
          ["rb2","rb3l"],
          ["rb3","rb2l"],
          ["rb2l","rb3ll"],
          ["rb3l","rb2ll"],
          ["rb4ll","rb3ll"],
          ["rb5ll","rb2ll"],

          ["rb1rr","rb2r"],
          ["rb1ll","rb2l"],
          ["rb2rr","rb1r"],
          ["rb2ll","rb1l"],
          ["rb1r","rb2"],
          ["rb1l","rb2"],
          //["rb2r","rb1"],
          //["rb2l","rb1"],

          //rigids
          {"beamDeform":4000,"beamStrength":59000},
          {"beamSpring":81000,"beamDamp":100},
          //["rb1rr","rb1"],
          ["rb2rr","rb2"],
          //["rb1","rb1ll"],
          ["rb2","rb2ll"],
          ["rb1r","rb1l"],

          ["rb2r","rb2l"],

          ["rb3rr","rb3"],
          ["rb3r","rb3l"],
          ["rb3","rb3ll"],

          ["rb1rr","rb3rr"],
          ["rb1r","rb3r"],
          //["rb1","rb3"],
          ["rb1l","rb3l"],
          ["rb1ll","rb3ll"],

          //weak
          {"beamDeform":3200,"beamStrength":59000},
          {"beamSpring":62000,"beamDamp":100},
          ["rb4rr","rb1r"],
          ["rb1l","rb4ll"],
          ["rb5rr","rb3r"],
          ["rb3l","rb5ll"],
          ["rb4rr","rb2r"],
          ["rb2l","rb4ll"],

          //rigidifier
          {"beamDeform":1500,"beamStrength":"FLT_MAX"},
          {"beamSpring":10000,"beamDamp":100},
          ["rb1rr","rb6"],
          ["rb1r","rb6"],
          //["rb1","rb6"],
          ["rb1l","rb6"],
          ["rb1ll","rb6"],

          ["rb2rr","rb6"],
          ["rb2r","rb6"],
          ["rb2","rb6"],
          ["rb2l","rb6"],
          ["rb2ll","rb6"],

          ["rb3rr","rb6"],
          ["rb3r","rb6"],
          ["rb3","rb6"],
          ["rb3l","rb6"],
          ["rb3ll","rb6"],

          {"beamDeform":1650,"beamStrength":"FLT_MAX"},
          ["rb4rr","rb6"],
          ["rb4ll","rb6"],

          ["rb5rr","rb6"],
          ["rb5ll","rb6"],

          //attach
          {"beamSpring":301000,"beamDamp":100},
          {"beamDeform":4750,"beamStrength":9500},
          //r
          {"breakGroup":"rearbumper_a_R"},
          ["rb1rr","q9r"],
          ["rb2rr","q9r"],
          ["rb4rr","q8r"],

          ["rb4rr","q7r"],

          //l
          {"breakGroup":"rearbumper_a_L"},
          ["rb1ll","q9l"],
          ["rb2ll","q9l"],
          ["rb4ll","q8l"],

          ["rb4ll","q7l"],

          //middle
          {"beamDeform":3750,"beamStrength":7500},
          {"breakGroup":"rearbumper_M"},
          ["rb2","r7"],
          ["rb2r","r7"],
          ["rb2l","r7"],
          ["rb2","r7rr"],
          ["rb2r","r7rr"],
          ["rb2l","r7ll"],
          ["rb2","r7ll"],

          ["rb6","r7rr"],
          ["rb6","r7"],
          ["rb6","r7ll"],

          ["rb6","r4rr"],
          ["rb6","r4"],
          ["rb6","r4ll"],
          {"breakGroup":""},

          //rear bumper support beams
          {"beamType":"|SUPPORT", "beamLongBound":4},
          {"beamSpring":1001000,"beamDamp":30},
          {"beamDeform":20000,"beamStrength":125000},
          {"optional":true},
          //["rb2l","t4"],
          //["rb2r","t4"],

          //["rb2r","t4r"],
          //["rb2l","t4l"],
          //["rb2","t4r"],
          //["rb2","t4l"],
          {"optional":false},

          ["rb4rr","q8r"],
          ["rb4ll","q8l"],

          ["rb1rr","q9r"],
          ["rb1ll","q9l"],

          ["rb1r","q9r"],
          ["rb1l","q9l"],

          ["rb1r","r7"],
          //["rb1","r7"],
          ["rb1l","r7"],
          ["rb1rr","r7rr"],
          ["rb1r","r7rr"],
          //["rb1","r7rr"],
          //["rb1","r7ll"],
          ["rb1l","r7ll"],
          ["rb1ll","r7ll"],

          ["rb2r","r7"],
          ["rb2","r7"],
          ["rb2l","r7"],
          ["rb2rr","r7rr"],
          ["rb2r","r7rr"],
          ["rb2","r7rr"],
          ["rb2","r7ll"],
          ["rb2l","r7ll"],
          ["rb2ll","r7ll"],

          ["rb2r","r4"],
          ["rb2","r4"],
          ["rb2l","r4"],
          ["rb2rr","r4rr"],
          ["rb2r","r4rr"],
          ["rb2","r4rr"],
          ["rb2","r4ll"],
          ["rb2l","r4ll"],
          ["rb2ll","r4ll"],

          ["rb3r","r4"],
          ["rb3","r4"],
          ["rb3l","r4"],
          ["rb3rr","r4rr"],
          ["rb3r","r4rr"],
          ["rb3","r4rr"],
          ["rb3","r4ll"],
          ["rb3l","r4ll"],
          ["rb3ll","r4ll"],

          //weak
          //upwards
          ["rb1rr","q4r"],
          ["rb1ll","q4l"],
          ["rb1r","q4r"],
          ["rb1l","q4l"],

          ["rb3rr","r3rr"],
          ["rb3ll","r3ll"],
          ["rb5rr","r3rr"],
          ["rb5ll","r3ll"],

          ["rb2rr","q8r"],
          ["rb2rr","q9r"],
          ["rb2ll","q8l"],
          ["rb2ll","q9l"],

          //weaker
          {"beamDeform":6000,"beamStrength":125000},
          ["rb4rr","r2rr"],
          ["rb4ll","r2ll"],

          ["rb5rr","r2rr"],
          ["rb5ll","r2ll"],
          {"beamPrecompression":1, "beamType":"|NORMAL", "beamLongBound":1.0, "beamShortBound":1.0},
          {"deformLimitExpansion":1.1},
    ],
    "triangles": [
            ["id1:","id2:","id3:"],
            //rear bumper
            {"groundModel":"plastic"},
            {"dragCoef":4},
            {"group":"sunburst_bumper_R"},
            ["rb1rr","rb2rr","rb4rr"],
            ["rb2rr","rb3rr","rb5rr"],
            ["rb2rr","rb5rr","rb4rr"],

            ["rb1r","rb2r","rb2rr"],
            ["rb1r","rb2rr","rb1rr"],
            ["rb2r","rb3r","rb3rr"],
            ["rb2r","rb3rr","rb2rr"],

            //["rb2","rb1r","rb1l"],

            //["rb1","rb2","rb2r"],
            //["rb1","rb2r","rb1r"],
            ["rb2","rb3","rb3r"],
            ["rb2","rb3r","rb2r"],

            //["rb2","rb1","rb2l"],
            //["rb1","rb1l","rb2l"],
            ["rb3","rb2","rb3l"],
            ["rb2","rb2l","rb3l"],

            ["rb2l","rb1l","rb2ll"],
            ["rb1l","rb1ll","rb2ll"],
            ["rb3l","rb2l","rb3ll"],
            ["rb2l","rb2ll","rb3ll"],

            ["rb2ll","rb1ll","rb4ll"],
            ["rb3ll","rb2ll","rb5ll"],
            ["rb5ll","rb2ll","rb4ll"],

            //["rb1l","rb2l","rb2"],
            //["rb2r","rb1r","rb2"],

            //bottom
            //["rb3ll","rb5ll","rb3l"],
            //["rb3l","rb5ll","rb3"],
            //["rb5rr","rb3r","rb3"],
            //["rb5rr","rb3rr","rb3r"],
            {"group":""},
    ],
},
"sunburst_bumper_R_wide": {
    "information":{
        "authors":"BeamNG",
        "name":"Sport S Rear Bumper",
        "value":600,
    },
    "slotType" : "sunburst_bumper_R",
    "slots": [
        ["type", "default", "description"],
        ["sunburst_mudflap_R","", "Rear Mudflaps"],
    ],
    "flexbodies": [
       ["mesh", "[group]:", "nonFlexMaterials"],
       ["sunburst_bumper_R_wide", ["sunburst_bumper_R"]],
    ],
    "nodes": [
       ["id", "posX", "posY", "posZ"],
       {"frictionCoef":0.8},
       {"nodeMaterial":"|NM_PLASTIC"},
       //--REAR BUMPER--
       {"collision":true},
       {"selfCollision":true},
       {"nodeWeight":0.6},
       {"group":"sunburst_bumper_R"},
       ["rb1rr", -0.66, 2.01, 0.63],
       ["rb1r", -0.36, 2.13, 0.67],
       ["rb1l", 0.36, 2.13, 0.67],
       ["rb1ll", 0.66, 2.01, 0.63],

       ["rb2rr", -0.67, 2.05, 0.50],
       ["rb2r", -0.36, 2.19, 0.50],
       ["rb2", 0, 2.22, 0.50],
       ["rb2l", 0.36, 2.19, 0.50],
       ["rb2ll", 0.67, 2.05, 0.50],

       ["rb3rr", -0.66, 1.99, 0.24],
       ["rb3r", -0.36, 2.13, 0.25],
       ["rb3", 0, 2.16, 0.25],
       ["rb3l", 0.36, 2.13, 0.25],
       ["rb3ll", 0.66, 1.99, 0.24],

       ["rb4rr", -0.85, 1.59, 0.52],
       ["rb4ll", 0.85, 1.59, 0.52],

       ["rb5rr", -0.81, 1.64, 0.20],
       ["rb5ll", 0.81, 1.64, 0.20],

       {"group":""},
       {"collision":false},
       {"selfCollision":false},
       ["rb6", 0, 2.06, 0.36],
    ],
    "beams": [
          ["id1:", "id2:"],
          //--REAR BUMPER--
          //rear bumper main shape
          {"deformLimitExpansion":1.1},
          {"beamPrecompression":1, "beamType":"|NORMAL", "beamLongBound":1.0, "beamShortBound":1.0},
          {"beamSpring":151000,"beamDamp":100},
          {"beamDeform":20000,"beamStrength":59000},
          ["rb4rr","rb1rr"],
          ["rb1rr","rb1r"],
          ["rb4rr","rb2rr"],
          ["rb2rr","rb2r"],
          ["rb1l","rb1ll"],
          ["rb4ll","rb1ll"],
          ["rb2l","rb2ll"],
          ["rb4ll","rb2ll"],
          //["rb1r","rb1"],
          //["rb1","rb1l"],

          ["rb2r","rb2"],
          ["rb2","rb2l"],

          ["rb5rr","rb3rr"],
          ["rb3rr","rb3r"],
          ["rb3r","rb3"],
          ["rb3","rb3l"],
          ["rb3l","rb3ll"],
          ["rb5ll","rb3ll"],

          ["rb1rr","rb2rr"],
          ["rb1r","rb2r"],
          ["rb1l","rb2l"],
          ["rb1ll","rb2ll"],
          //[/,"rb2"],

          ["rb2rr","rb3rr"],
          ["rb2r","rb3r"],
          ["rb2","rb3"],
          ["rb2l","rb3l"],
          ["rb2ll","rb3ll"],

          ["rb4rr","rb5rr"],
          ["rb4ll","rb5ll"],

          //crossing
          {"deformLimitExpansion":""},
          {"beamDeform":9750,"beamStrength":59000},
          ["rb4rr","rb3rr"],
          ["rb5rr","rb2rr"],
          ["rb2rr","rb3r"],
          ["rb3rr","rb2r"],
          ["rb2r","rb3"],
          ["rb3r","rb2"],
          ["rb2","rb3l"],
          ["rb3","rb2l"],
          ["rb2l","rb3ll"],
          ["rb3l","rb2ll"],
          ["rb4ll","rb3ll"],
          ["rb5ll","rb2ll"],

          ["rb1rr","rb2r"],
          ["rb1ll","rb2l"],
          ["rb2rr","rb1r"],
          ["rb2ll","rb1l"],
          ["rb1r","rb2"],
          ["rb1l","rb2"],
          //["rb2r","rb1"],
          //["rb2l","rb1"],

          //rigids
          {"beamDeform":4000,"beamStrength":59000},
          {"beamSpring":81000,"beamDamp":100},
          //["rb1rr","rb1"],
          ["rb2rr","rb2"],
          //["rb1","rb1ll"],
          ["rb2","rb2ll"],
          ["rb1r","rb1l"],

          ["rb2r","rb2l"],

          ["rb3rr","rb3"],
          ["rb3r","rb3l"],
          ["rb3","rb3ll"],

          ["rb1rr","rb3rr"],
          ["rb1r","rb3r"],
          //["rb1","rb3"],
          ["rb1l","rb3l"],
          ["rb1ll","rb3ll"],

          //weak
          {"beamDeform":3200,"beamStrength":59000},
          {"beamSpring":62000,"beamDamp":100},
          ["rb4rr","rb1r"],
          ["rb1l","rb4ll"],
          ["rb5rr","rb3r"],
          ["rb3l","rb5ll"],
          ["rb4rr","rb2r"],
          ["rb2l","rb4ll"],

          //rigidifier
          {"beamDeform":1500,"beamStrength":"FLT_MAX"},
          {"beamSpring":10000,"beamDamp":100},
          ["rb1rr","rb6"],
          ["rb1r","rb6"],
          //["rb1","rb6"],
          ["rb1l","rb6"],
          ["rb1ll","rb6"],

          ["rb2rr","rb6"],
          ["rb2r","rb6"],
          ["rb2","rb6"],
          ["rb2l","rb6"],
          ["rb2ll","rb6"],

          ["rb3rr","rb6"],
          ["rb3r","rb6"],
          ["rb3","rb6"],
          ["rb3l","rb6"],
          ["rb3ll","rb6"],

          {"beamDeform":1650,"beamStrength":"FLT_MAX"},
          ["rb4rr","rb6"],
          ["rb4ll","rb6"],

          ["rb5rr","rb6"],
          ["rb5ll","rb6"],

          //attach
          {"beamSpring":301000,"beamDamp":100},
          {"beamDeform":4750,"beamStrength":9500},
          //r
          {"breakGroup":"rearbumper_a_R"},
          ["rb1rr","q9r"],
          ["rb2rr","q9r"],
          ["rb4rr","q8r"],

          ["rb4rr","q7r"],

          //l
          {"breakGroup":"rearbumper_a_L"},
          ["rb1ll","q9l"],
          ["rb2ll","q9l"],
          ["rb4ll","q8l"],

          ["rb4ll","q7l"],

          //middle
          {"beamDeform":3750,"beamStrength":7500},
          {"breakGroup":"rearbumper_M"},
          ["rb2","r7"],
          ["rb2r","r7"],
          ["rb2l","r7"],
          ["rb2","r7rr"],
          ["rb2r","r7rr"],
          ["rb2l","r7ll"],
          ["rb2","r7ll"],

          ["rb6","r7rr"],
          ["rb6","r7"],
          ["rb6","r7ll"],

          ["rb6","r4rr"],
          ["rb6","r4"],
          ["rb6","r4ll"],
          {"breakGroup":""},

          //rear bumper support beams
          {"beamType":"|SUPPORT", "beamLongBound":4},
          {"beamSpring":1001000,"beamDamp":30},
          {"beamDeform":20000,"beamStrength":125000},
          {"optional":true},
          //["rb2l","t4"],
          //["rb2r","t4"],

          //["rb2r","t4r"],
          //["rb2l","t4l"],
          //["rb2","t4r"],
          //["rb2","t4l"],
          {"optional":false},

          ["rb4rr","q8r"],
          ["rb4ll","q8l"],

          ["rb1rr","q9r"],
          ["rb1ll","q9l"],

          ["rb1r","q9r"],
          ["rb1l","q9l"],

          ["rb1r","r7"],
          //["rb1","r7"],
          ["rb1l","r7"],
          ["rb1rr","r7rr"],
          ["rb1r","r7rr"],
          //["rb1","r7rr"],
          //["rb1","r7ll"],
          ["rb1l","r7ll"],
          ["rb1ll","r7ll"],

          ["rb2r","r7"],
          ["rb2","r7"],
          ["rb2l","r7"],
          ["rb2rr","r7rr"],
          ["rb2r","r7rr"],
          ["rb2","r7rr"],
          ["rb2","r7ll"],
          ["rb2l","r7ll"],
          ["rb2ll","r7ll"],

          ["rb2r","r4"],
          ["rb2","r4"],
          ["rb2l","r4"],
          ["rb2rr","r4rr"],
          ["rb2r","r4rr"],
          ["rb2","r4rr"],
          ["rb2","r4ll"],
          ["rb2l","r4ll"],
          ["rb2ll","r4ll"],

          ["rb3r","r4"],
          ["rb3","r4"],
          ["rb3l","r4"],
          ["rb3rr","r4rr"],
          ["rb3r","r4rr"],
          ["rb3","r4rr"],
          ["rb3","r4ll"],
          ["rb3l","r4ll"],
          ["rb3ll","r4ll"],

          //weak
          //upwards
          ["rb1rr","q4r"],
          ["rb1ll","q4l"],
          ["rb1r","q4r"],
          ["rb1l","q4l"],

          ["rb3rr","r3rr"],
          ["rb3ll","r3ll"],
          ["rb5rr","r3rr"],
          ["rb5ll","r3ll"],

          ["rb2rr","q8r"],
          ["rb2rr","q9r"],
          ["rb2ll","q8l"],
          ["rb2ll","q9l"],

          //weaker
          {"beamDeform":6000,"beamStrength":125000},
          ["rb4rr","r2rr"],
          ["rb4ll","r2ll"],

          ["rb5rr","r2rr"],
          ["rb5ll","r2ll"],
          {"beamPrecompression":1, "beamType":"|NORMAL", "beamLongBound":1.0, "beamShortBound":1.0},
          {"deformLimitExpansion":1.1},
    ],
    "triangles": [
            ["id1:","id2:","id3:"],
            //rear bumper
            {"groundModel":"plastic"},
            {"dragCoef":4},
            {"group":"sunburst_bumper_R"},
            ["rb1rr","rb2rr","rb4rr"],
            ["rb2rr","rb3rr","rb5rr"],
            ["rb2rr","rb5rr","rb4rr"],

            ["rb1r","rb2r","rb2rr"],
            ["rb1r","rb2rr","rb1rr"],
            ["rb2r","rb3r","rb3rr"],
            ["rb2r","rb3rr","rb2rr"],

            //["rb2","rb1r","rb1l"],

            //["rb1","rb2","rb2r"],
            //["rb1","rb2r","rb1r"],
            ["rb2","rb3","rb3r"],
            ["rb2","rb3r","rb2r"],

            //["rb2","rb1","rb2l"],
            //["rb1","rb1l","rb2l"],
            ["rb3","rb2","rb3l"],
            ["rb2","rb2l","rb3l"],

            ["rb2l","rb1l","rb2ll"],
            ["rb1l","rb1ll","rb2ll"],
            ["rb3l","rb2l","rb3ll"],
            ["rb2l","rb2ll","rb3ll"],

            ["rb2ll","rb1ll","rb4ll"],
            ["rb3ll","rb2ll","rb5ll"],
            ["rb5ll","rb2ll","rb4ll"],

            //["rb1l","rb2l","rb2"],
            //["rb2r","rb1r","rb2"],

            //bottom
            //["rb3ll","rb5ll","rb3l"],
            //["rb3l","rb5ll","rb3"],
            //["rb5rr","rb3r","rb3"],
            //["rb5rr","rb3rr","rb3r"],
            {"group":""},
    ],
},
"sunburst_bumper_R_kit": {
    "information":{
        "authors":"BeamNG",
        "name":"Sport RS Rear Bumper",
        "value":850,
    },
    "slotType" : "sunburst_bumper_R",
    "slots": [
        ["type", "default", "description"],
        ["sunburst_mudflap_R","", "Rear Mudflaps"],

    ],
    "flexbodies": [
         ["mesh", "[group]:", "nonFlexMaterials"],
         ["sunburst_diffuser", ["sunburst_bumper_R"]],
    ],
    "nodes": [
         ["id", "posX", "posY", "posZ"],
         {"frictionCoef":0.8},
         {"nodeMaterial":"|NM_PLASTIC"},
         //--REAR BUMPER--
         {"collision":true},
         {"selfCollision":true},
         {"nodeWeight":0.65},
         {"group":"sunburst_bumper_R"},
         ["rb1rr", -0.66, 2.01, 0.63],
         ["rb1r", -0.36, 2.13, 0.67],
         ["rb1l", 0.36, 2.13, 0.67],
         ["rb1ll", 0.66, 2.01, 0.63],

         ["rb2rr", -0.67, 2.05, 0.50],
         ["rb2r", -0.36, 2.19, 0.50],
         ["rb2", 0, 2.22, 0.50],
         ["rb2l", 0.36, 2.19, 0.50],
         ["rb2ll", 0.67, 2.05, 0.50],

         ["rb3rr", -0.66, 1.99, 0.22],
         ["rb3r", -0.36, 2.11, 0.21],
         ["rb3", 0, 2.14, 0.21],
         ["rb3l", 0.36, 2.11, 0.21],
         ["rb3ll", 0.66, 1.99, 0.22],

         ["rb4rr", -0.84, 1.59, 0.52],
         ["rb4ll", 0.84, 1.59, 0.52],

         ["rb5rr", -0.81, 1.65, 0.16],
         ["rb5ll", 0.81, 1.65, 0.16],

         {"group":""},
         {"collision":false},
         {"selfCollision":false},
         ["rb6", 0, 2.06, 0.36],
    ],
    "beams": [
          ["id1:", "id2:"],
          //--REAR BUMPER--
          //rear bumper main shape
          {"deformLimitExpansion":1.1},
          {"beamPrecompression":1, "beamType":"|NORMAL", "beamLongBound":1.0, "beamShortBound":1.0},
          {"beamSpring":151000,"beamDamp":120},
          {"beamDeform":20000,"beamStrength":59000},
          ["rb4rr","rb1rr"],
          ["rb1rr","rb1r"],
          ["rb4rr","rb2rr"],
          ["rb2rr","rb2r"],
          ["rb1l","rb1ll"],
          ["rb4ll","rb1ll"],
          ["rb2l","rb2ll"],
          ["rb4ll","rb2ll"],
          //["rb1r","rb1"],
          //["rb1","rb1l"],

          ["rb2r","rb2"],
          ["rb2","rb2l"],

          ["rb5rr","rb3rr"],
          ["rb3rr","rb3r"],
          ["rb3r","rb3"],
          ["rb3","rb3l"],
          ["rb3l","rb3ll"],
          ["rb5ll","rb3ll"],

          ["rb1rr","rb2rr"],
          ["rb1r","rb2r"],
          ["rb1l","rb2l"],
          ["rb1ll","rb2ll"],
          //[/,"rb2"],

          ["rb2rr","rb3rr"],
          ["rb2r","rb3r"],
          ["rb2","rb3"],
          ["rb2l","rb3l"],
          ["rb2ll","rb3ll"],

          ["rb4rr","rb5rr"],
          ["rb4ll","rb5ll"],

          //crossing
          {"deformLimitExpansion":""},
          {"beamDeform":9750,"beamStrength":59000},
          ["rb4rr","rb3rr"],
          ["rb5rr","rb2rr"],
          ["rb2rr","rb3r"],
          ["rb3rr","rb2r"],
          ["rb2r","rb3"],
          ["rb3r","rb2"],
          ["rb2","rb3l"],
          ["rb3","rb2l"],
          ["rb2l","rb3ll"],
          ["rb3l","rb2ll"],
          ["rb4ll","rb3ll"],
          ["rb5ll","rb2ll"],

          ["rb1rr","rb2r"],
          ["rb1ll","rb2l"],
          ["rb2rr","rb1r"],
          ["rb2ll","rb1l"],
          ["rb1r","rb2"],
          ["rb1l","rb2"],
          //["rb2r","rb1"],
          //["rb2l","rb1"],

          //rigids
          {"beamDeform":4000,"beamStrength":59000},
          {"beamSpring":81000,"beamDamp":120},
          //["rb1rr","rb1"],
          ["rb2rr","rb2"],
          //["rb1","rb1ll"],
          ["rb2","rb2ll"],
          ["rb1r","rb1l"],

          ["rb2r","rb2l"],

          ["rb3rr","rb3"],
          ["rb3r","rb3l"],
          ["rb3","rb3ll"],

          ["rb1rr","rb3rr"],
          ["rb1r","rb3r"],
          //["rb1","rb3"],
          ["rb1l","rb3l"],
          ["rb1ll","rb3ll"],

          //weak
          {"beamDeform":3200,"beamStrength":59000},
          {"beamSpring":62000,"beamDamp":120},
          ["rb4rr","rb1r"],
          ["rb1l","rb4ll"],
          ["rb5rr","rb3r"],
          ["rb3l","rb5ll"],
          ["rb4rr","rb2r"],
          ["rb2l","rb4ll"],

          //rigidifier
          {"beamDeform":1500,"beamStrength":"FLT_MAX"},
          {"beamSpring":10000,"beamDamp":120},
          ["rb1rr","rb6"],
          ["rb1r","rb6"],
          //["rb1","rb6"],
          ["rb1l","rb6"],
          ["rb1ll","rb6"],

          ["rb2rr","rb6"],
          ["rb2r","rb6"],
          ["rb2","rb6"],
          ["rb2l","rb6"],
          ["rb2ll","rb6"],

          ["rb3rr","rb6"],
          ["rb3r","rb6"],
          ["rb3","rb6"],
          ["rb3l","rb6"],
          ["rb3ll","rb6"],

          {"beamDeform":1650,"beamStrength":"FLT_MAX"},
          ["rb4rr","rb6"],
          ["rb4ll","rb6"],

          ["rb5rr","rb6"],
          ["rb5ll","rb6"],

          //attach
          {"beamSpring":301000,"beamDamp":120},
          {"beamDeform":4750,"beamStrength":9500},
          //r
          {"breakGroup":"rearbumper_a_R"},
          ["rb1rr","q9r"],
          ["rb2rr","q9r"],
          ["rb4rr","q8r"],

          ["rb4rr","q7r"],

          //l
          {"breakGroup":"rearbumper_a_L"},
          ["rb1ll","q9l"],
          ["rb2ll","q9l"],
          ["rb4ll","q8l"],

          ["rb4ll","q7l"],

          //middle
          {"beamDeform":3750,"beamStrength":7500},
          {"breakGroup":"rearbumper_M"},
          ["rb2","r7"],
          ["rb2r","r7"],
          ["rb2l","r7"],
          ["rb2","r7rr"],
          ["rb2r","r7rr"],
          ["rb2l","r7ll"],
          ["rb2","r7ll"],

          ["rb6","r7rr"],
          ["rb6","r7"],
          ["rb6","r7ll"],

          ["rb6","r4rr"],
          ["rb6","r4"],
          ["rb6","r4ll"],
          {"breakGroup":""},

          //rear bumper support beams
          {"beamType":"|SUPPORT", "beamLongBound":4},
          {"beamSpring":1001000,"beamDamp":30},
          {"beamDeform":20000,"beamStrength":125000},
          {"optional":true},
          //["rb2l","t4"],
          //["rb2r","t4"],

          //["rb2r","t4r"],
          //["rb2l","t4l"],
          //["rb2","t4r"],
          //["rb2","t4l"],
          {"optional":false},

          ["rb4rr","q8r"],
          ["rb4ll","q8l"],

          ["rb1rr","q9r"],
          ["rb1ll","q9l"],

          ["rb1r","q9r"],
          ["rb1l","q9l"],

          ["rb1r","r7"],
          //["rb1","r7"],
          ["rb1l","r7"],
          ["rb1rr","r7rr"],
          ["rb1r","r7rr"],
          //["rb1","r7rr"],
          //["rb1","r7ll"],
          ["rb1l","r7ll"],
          ["rb1ll","r7ll"],

          ["rb2r","r7"],
          ["rb2","r7"],
          ["rb2l","r7"],
          ["rb2rr","r7rr"],
          ["rb2r","r7rr"],
          ["rb2","r7rr"],
          ["rb2","r7ll"],
          ["rb2l","r7ll"],
          ["rb2ll","r7ll"],

          ["rb2r","r4"],
          ["rb2","r4"],
          ["rb2l","r4"],
          ["rb2rr","r4rr"],
          ["rb2r","r4rr"],
          ["rb2","r4rr"],
          ["rb2","r4ll"],
          ["rb2l","r4ll"],
          ["rb2ll","r4ll"],

          ["rb3r","r4"],
          ["rb3","r4"],
          ["rb3l","r4"],
          ["rb3rr","r4rr"],
          ["rb3r","r4rr"],
          ["rb3","r4rr"],
          ["rb3","r4ll"],
          ["rb3l","r4ll"],
          ["rb3ll","r4ll"],

          //weak
          //upwards
          ["rb1rr","q4r"],
          ["rb1ll","q4l"],
          ["rb1r","q4r"],
          ["rb1l","q4l"],

          ["rb3rr","r3rr"],
          ["rb3ll","r3ll"],
          ["rb5rr","r3rr"],
          ["rb5ll","r3ll"],

          ["rb2rr","q8r"],
          ["rb2rr","q9r"],
          ["rb2ll","q8l"],
          ["rb2ll","q9l"],

          //weaker
          {"beamDeform":6000,"beamStrength":125000},
          ["rb4rr","r2rr"],
          ["rb4ll","r2ll"],

          ["rb5rr","r2rr"],
          ["rb5ll","r2ll"],
          {"beamPrecompression":1, "beamType":"|NORMAL", "beamLongBound":1.0, "beamShortBound":1.0},
          {"deformLimitExpansion":1.1},
    ],
    "triangles": [
            ["id1:","id2:","id3:"],
            //rear bumper
            {"groundModel":"plastic"},
            {"dragCoef":4},
            {"group":"sunburst_bumper_R"},
            ["rb1rr","rb2rr","rb4rr"],
            ["rb2rr","rb3rr","rb5rr"],
            ["rb2rr","rb5rr","rb4rr"],

            ["rb1r","rb2r","rb2rr"],
            ["rb1r","rb2rr","rb1rr"],
            ["rb2r","rb3r","rb3rr"],
            ["rb2r","rb3rr","rb2rr"],

            //["rb2","rb1r","rb1l"],

            //["rb1","rb2","rb2r"],
            //["rb1","rb2r","rb1r"],
            ["rb2","rb3","rb3r"],
            ["rb2","rb3r","rb2r"],

            //["rb2","rb1","rb2l"],
            //["rb1","rb1l","rb2l"],
            ["rb3","rb2","rb3l"],
            ["rb2","rb2l","rb3l"],

            ["rb2l","rb1l","rb2ll"],
            ["rb1l","rb1ll","rb2ll"],
            ["rb3l","rb2l","rb3ll"],
            ["rb2l","rb2ll","rb3ll"],

            ["rb2ll","rb1ll","rb4ll"],
            ["rb3ll","rb2ll","rb5ll"],
            ["rb5ll","rb2ll","rb4ll"],

            //["rb1l","rb2l","rb2"],
            //["rb2r","rb1r","rb2"],

            //bottom
            //["rb3ll","rb5ll","rb3l"],
            //["rb3l","rb5ll","rb3"],
            //["rb5rr","rb3r","rb3"],
            //["rb5rr","rb3rr","rb3r"],
            {"group":""},
    ],
},
"sunburst_diffuser_R": {
    "information":{
        "authors":"BeamNG",
        "name":"Carbon Fiber Rear Diffuser",
        "value":750,
    },
    "slotType" : "sunburst_diffuser_R",
    "flexbodies": [
         ["mesh", "[group]:", "nonFlexMaterials"],
         ["sunburst_lip_bumper_kit_R", ["sunburst_bumper_R"]],
    ],
    "nodes": [
         ["id", "posX", "posY", "posZ"],
         //--redefine nodes add weight
         {"nodeWeight":0.75},
         {"frictionCoef":0.8},
         {"nodeMaterial":"|NM_PLASTIC"},
         {"group":"sunburst_bumper_R"},
         {"collision":true},
         {"selfCollision":true},
         {"nodeWeight":0.85},
         ["rb3rr", -0.75, 2.16, 0.21],
         ["rb3r", -0.40, 2.09, 0.21],
         ["rb3", 0, 2.09, 0.21],
         ["rb3l", 0.40, 2.09, 0.21],
         ["rb3ll", 0.75, 2.16, 0.21],
         ["rb5rr", -0.81, 1.65, 0.16],
         ["rb5ll", 0.81, 1.65, 0.16],
    ],
},
"sunburst_bumper_R_kit_alt": {
    "information":{
        "authors":"BeamNG",
        "name":"Sport RS Rear Bumper (Alternate Diffuser)",
        "value":400,
    },
    "slotType" : "sunburst_bumper_R",
    "slots": [
        ["type", "default", "description"],
        ["sunburst_mudflap_R","", "Rear Mudflaps"],
        ["sunburst_diffuser_R","sunburst_diffuser_R","Rear Diffuser"],
    ],
    "flexbodies": [
         ["mesh", "[group]:", "nonFlexMaterials"],
         ["sunburst_diffuser_alt", ["sunburst_bumper_R"]],
    ],
    "nodes": [
         ["id", "posX", "posY", "posZ"],
         //--REAR BUMPER--
         {"nodeWeight":0.65},
         {"frictionCoef":0.8},
         {"nodeMaterial":"|NM_PLASTIC"},
         {"group":"sunburst_bumper_R"},
         {"collision":true},
         {"selfCollision":true},
         ["rb1rr", -0.66, 2.01, 0.63],
         ["rb1r", -0.36, 2.13, 0.67],
         ["rb1l", 0.36, 2.13, 0.67],
         ["rb1ll", 0.66, 2.01, 0.63],

         ["rb2rr", -0.67, 2.05, 0.50],
         ["rb2r", -0.36, 2.19, 0.50],
         ["rb2", 0, 2.22, 0.50],
         ["rb2l", 0.36, 2.19, 0.50],
         ["rb2ll", 0.67, 2.05, 0.50],

         {"nodeWeight":0.7},
         ["rb3rr", -0.68, 1.99, 0.24],
         ["rb3r", -0.36, 2.16, 0.36],
         ["rb3", 0, 2.18, 0.36],
         ["rb3l", 0.36, 2.16, 0.36],
         ["rb3ll", 0.68, 1.99, 0.24],

         {"nodeWeight":0.75},
         ["rb4rr", -0.83, 1.59, 0.52],
         ["rb4ll", 0.83, 1.59, 0.52],

         ["rb5rr", -0.81, 1.65, 0.16],
         ["rb5ll", 0.81, 1.65, 0.16],

         {"group":""},
         {"collision":false},
         {"selfCollision":false},
         ["rb6", 0, 2.06, 0.36],
    ],
    "beams": [
          ["id1:", "id2:"],
          //--REAR BUMPER--
          //rear bumper main shape
          {"deformLimitExpansion":1.1},
          {"beamPrecompression":1, "beamType":"|NORMAL", "beamLongBound":1.0, "beamShortBound":1.0},
          {"beamSpring":151000,"beamDamp":120},
          {"beamDeform":32000,"beamStrength":59000},
          ["rb4rr","rb1rr"],
          ["rb1rr","rb1r"],
          ["rb4rr","rb2rr"],
          ["rb2rr","rb2r"],
          ["rb1l","rb1ll"],
          ["rb4ll","rb1ll"],
          ["rb2l","rb2ll"],
          ["rb4ll","rb2ll"],
          //["rb1r","rb1"],
          //["rb1","rb1l"],

          ["rb2r","rb2"],
          ["rb2","rb2l"],

          ["rb5rr","rb3rr"],
          ["rb3rr","rb3r"],
          ["rb3r","rb3"],
          ["rb3","rb3l"],
          ["rb3l","rb3ll"],
          ["rb5ll","rb3ll"],

          ["rb1rr","rb2rr"],
          ["rb1r","rb2r"],
          ["rb1l","rb2l"],
          ["rb1ll","rb2ll"],
          //[/,"rb2"],

          ["rb2rr","rb3rr"],
          ["rb2r","rb3r"],
          ["rb2","rb3"],
          ["rb2l","rb3l"],
          ["rb2ll","rb3ll"],

          ["rb4rr","rb5rr"],
          ["rb4ll","rb5ll"],

          //crossing
          {"deformLimitExpansion":""},
          {"beamDeform":9750,"beamStrength":59000},
          ["rb4rr","rb3rr"],
          ["rb5rr","rb2rr"],
          ["rb2rr","rb3r"],
          ["rb3rr","rb2r"],
          ["rb2r","rb3"],
          ["rb3r","rb2"],
          ["rb2","rb3l"],
          ["rb3","rb2l"],
          ["rb2l","rb3ll"],
          ["rb3l","rb2ll"],
          ["rb4ll","rb3ll"],
          ["rb5ll","rb2ll"],

          ["rb1rr","rb2r"],
          ["rb1ll","rb2l"],
          ["rb2rr","rb1r"],
          ["rb2ll","rb1l"],
          ["rb1r","rb2"],
          ["rb1l","rb2"],
          //["rb2r","rb1"],
          //["rb2l","rb1"],

          //rigids
          {"beamDeform":4000,"beamStrength":59000},
          {"beamSpring":81000,"beamDamp":120},
          //["rb1rr","rb1"],
          ["rb2rr","rb2"],
          //["rb1","rb1ll"],
          ["rb2","rb2ll"],
          ["rb1r","rb1l"],

          ["rb2r","rb2l"],

          ["rb3rr","rb3"],
          ["rb3r","rb3l"],
          ["rb3","rb3ll"],

          ["rb1rr","rb3rr"],
          ["rb1r","rb3r"],
          //["rb1","rb3"],
          ["rb1l","rb3l"],
          ["rb1ll","rb3ll"],

          //weak
          {"beamDeform":3200,"beamStrength":59000},
          {"beamSpring":62000,"beamDamp":120},
          ["rb4rr","rb1r"],
          ["rb1l","rb4ll"],
          ["rb5rr","rb3r"],
          ["rb3l","rb5ll"],
          ["rb4rr","rb2r"],
          ["rb2l","rb4ll"],

          //rigidifier
          {"beamDeform":1500,"beamStrength":"FLT_MAX"},
          {"beamSpring":10000,"beamDamp":120},
          ["rb1rr","rb6"],
          ["rb1r","rb6"],
          //["rb1","rb6"],
          ["rb1l","rb6"],
          ["rb1ll","rb6"],

          ["rb2rr","rb6"],
          ["rb2r","rb6"],
          ["rb2","rb6"],
          ["rb2l","rb6"],
          ["rb2ll","rb6"],

          ["rb3rr","rb6"],
          ["rb3r","rb6"],
          ["rb3","rb6"],
          ["rb3l","rb6"],
          ["rb3ll","rb6"],

          {"beamDeform":1650,"beamStrength":"FLT_MAX"},
          ["rb4rr","rb6"],
          ["rb4ll","rb6"],

          ["rb5rr","rb6"],
          ["rb5ll","rb6"],

          //attach
          {"beamSpring":301000,"beamDamp":120},
          {"beamDeform":4750,"beamStrength":9500},
          //r
          {"breakGroup":"rearbumper_a_R"},
          ["rb1rr","q9r"],
          ["rb2rr","q9r"],
          ["rb4rr","q8r"],

          ["rb4rr","q7r"],

          //l
          {"breakGroup":"rearbumper_a_L"},
          ["rb1ll","q9l"],
          ["rb2ll","q9l"],
          ["rb4ll","q8l"],

          ["rb4ll","q7l"],

          //middle
          {"beamDeform":3750,"beamStrength":7500},
          {"breakGroup":"rearbumper_M"},
          ["rb2","r7"],
          ["rb2r","r7"],
          ["rb2l","r7"],
          ["rb2","r7rr"],
          ["rb2r","r7rr"],
          ["rb2l","r7ll"],
          ["rb2","r7ll"],

          ["rb6","r7rr"],
          ["rb6","r7"],
          ["rb6","r7ll"],

          ["rb6","r4rr"],
          ["rb6","r4"],
          ["rb6","r4ll"],
          {"breakGroup":""},

          //rear bumper support beams
          {"beamType":"|SUPPORT", "beamLongBound":4},
          {"beamSpring":1001000,"beamDamp":30},
          {"beamDeform":20000,"beamStrength":125000},
          {"optional":true},
          //["rb2l","t4"],
          //["rb2r","t4"],

          //["rb2r","t4r"],
          //["rb2l","t4l"],
          //["rb2","t4r"],
          //["rb2","t4l"],
          {"optional":false},

          ["rb4rr","q8r"],
          ["rb4ll","q8l"],

          ["rb1rr","q9r"],
          ["rb1ll","q9l"],

          ["rb1r","q9r"],
          ["rb1l","q9l"],

          ["rb1r","r7"],
          //["rb1","r7"],
          ["rb1l","r7"],
          ["rb1rr","r7rr"],
          ["rb1r","r7rr"],
          //["rb1","r7rr"],
          //["rb1","r7ll"],
          ["rb1l","r7ll"],
          ["rb1ll","r7ll"],

          ["rb2r","r7"],
          ["rb2","r7"],
          ["rb2l","r7"],
          ["rb2rr","r7rr"],
          ["rb2r","r7rr"],
          ["rb2","r7rr"],
          ["rb2","r7ll"],
          ["rb2l","r7ll"],
          ["rb2ll","r7ll"],

          ["rb2r","r4"],
          ["rb2","r4"],
          ["rb2l","r4"],
          ["rb2rr","r4rr"],
          ["rb2r","r4rr"],
          ["rb2","r4rr"],
          ["rb2","r4ll"],
          ["rb2l","r4ll"],
          ["rb2ll","r4ll"],

          ["rb3r","r4"],
          ["rb3","r4"],
          ["rb3l","r4"],
          ["rb3rr","r4rr"],
          ["rb3r","r4rr"],
          ["rb3","r4rr"],
          ["rb3","r4ll"],
          ["rb3l","r4ll"],
          ["rb3ll","r4ll"],

          //weak
          //upwards
          ["rb1rr","q4r"],
          ["rb1ll","q4l"],
          ["rb1r","q4r"],
          ["rb1l","q4l"],

          ["rb3rr","r3rr"],
          ["rb3ll","r3ll"],
          ["rb5rr","r3rr"],
          ["rb5ll","r3ll"],

          ["rb2rr","q8r"],
          ["rb2rr","q9r"],
          ["rb2ll","q8l"],
          ["rb2ll","q9l"],

          //weaker
          {"beamDeform":6000,"beamStrength":125000},
          ["rb4rr","r2rr"],
          ["rb4ll","r2ll"],

          ["rb5rr","r2rr"],
          ["rb5ll","r2ll"],
          {"beamPrecompression":1, "beamType":"|NORMAL", "beamLongBound":1.0, "beamShortBound":1.0},
          {"deformLimitExpansion":1.1},
    ],
    "triangles": [
            ["id1:","id2:","id3:"],
            //rear bumper
            {"groundModel":"plastic"},
            {"dragCoef":4},
            {"group":"sunburst_bumper_R"},
            ["rb1rr","rb2rr","rb4rr"],
            ["rb2rr","rb3rr","rb5rr"],
            ["rb2rr","rb5rr","rb4rr"],

            ["rb1r","rb2r","rb2rr"],
            ["rb1r","rb2rr","rb1rr"],
            ["rb2r","rb3r","rb3rr"],
            ["rb2r","rb3rr","rb2rr"],

            //["rb2","rb1r","rb1l"],

            //["rb1","rb2","rb2r"],
            //["rb1","rb2r","rb1r"],
            ["rb2","rb3","rb3r"],
            ["rb2","rb3r","rb2r"],

            //["rb2","rb1","rb2l"],
            //["rb1","rb1l","rb2l"],
            ["rb3","rb2","rb3l"],
            ["rb2","rb2l","rb3l"],

            ["rb2l","rb1l","rb2ll"],
            ["rb1l","rb1ll","rb2ll"],
            ["rb3l","rb2l","rb3ll"],
            ["rb2l","rb2ll","rb3ll"],

            ["rb2ll","rb1ll","rb4ll"],
            ["rb3ll","rb2ll","rb5ll"],
            ["rb5ll","rb2ll","rb4ll"],

            //["rb1l","rb2l","rb2"],
            //["rb2r","rb1r","rb2"],

            //bottom
            //["rb3ll","rb5ll","rb3l"],
            //["rb3l","rb5ll","rb3"],
            //["rb5rr","rb3r","rb3"],
            //["rb5rr","rb3rr","rb3r"],
            {"group":""},
    ],
},
}