{
"tire_R_345_35_15_sport": {
    "information":{
        "authors":"BeamNG",
        "name":"345/35R15 Sport Rear Tires",
        "value":500,
    },
    "slotType" : "tire_R_15x12",
    "flexbodies": [
        ["mesh", "[group]:", "nonFlexMaterials"],
        ["bolide_80s_345_15", ["wheel_RR","tire_RR"], [], {"pos":{"x":-0.53, "y":0.0, "z":0.0}, "rot":{"x":0, "y":0, "z":180}, "scale":{"x":0.94, "y":0.988, "z":0.988}}],
        ["bolide_80s_345_15", ["wheel_RL","tire_RL"], [], {"pos":{"x": 0.53, "y":0.0, "z":0.0}, "rot":{"x":0, "y":0, "z":0},   "scale":{"x":0.94, "y":0.988, "z":0.988}}],
    ],
    "variables": [
        ["name", "type", "unit", "category", "default", "min", "max", "title", "description"],
        ["$tirepressure_R", "range", "psi", "Wheels", 26, 0, 50, "Tire Pressure", "Relative to atmospheric pressure", {"subCategory":"Rear"}],
    ],
    "pressureWheels": [
        ["name","hubGroup","group","node1:","node2:","nodeS","nodeArm:","wheelDir"],
        {"disableMeshBreaking":false,"disableHubMeshBreaking":false},
        {"hasTire":true},
        {"enableTireReinfBeams":false},
        {"enableTireLbeams":true},
        {"enableTireSideReinfBeams":false},
        {"enableTreadReinfBeams":true},
        {"enableTirePeripheryReinfBeams":true},

        //general settings
        {"radius":0.312},
        {"tireWidth":0.325},

        //tire options
        {"wheelSideBeamSpring":"$=$tirepressure_R*371","wheelSideBeamDamp":35},
        {"wheelSideBeamSpringExpansion":501000,"wheelSideBeamDampExpansion":35},
        {"wheelSideTransitionZone":0.1,"wheelSideBeamPrecompression":0.985},

        {"wheelReinfBeamSpring":44000,"wheelReinfBeamDamp":200},
        {"wheelReinfBeamDampCutoffHz":500,"wheelReinfBeamPrecompression":0.98},

        {"wheelTreadBeamSpring":141000,"wheelTreadBeamDamp":105},
        {"wheelTreadBeamDampCutoffHz":500,"wheelTreadBeamPrecompression":0.98},

        {"wheelTreadReinfBeamSpring":141000,"wheelTreadReinfBeamDamp":105},
        {"wheelTreadReinfBeamDampCutoffHz":500,"wheelTreadReinfBeamPrecompression":0.98},

        {"wheelPeripheryBeamSpring":101000,"wheelPeripheryBeamDamp":65},
        {"wheelPeripheryBeamDampCutoffHz":500,"wheelPeripheryBeamPrecompression":0.98},

        {"wheelPeripheryReinfBeamSpring":101000,"wheelPeripheryReinfBeamDamp":65},
        {"wheelPeripheryReinfBeamDampCutoffHz":500,"wheelPeripheryReinfBeamPrecompression":0.98},

        //general tire values
        {"nodeWeight":0.185},
        {"nodeMaterial":"|NM_RUBBER"},
        {"triangleCollision":false},
        {"pressurePSI":"$tirepressure_R"},
        {"dragCoef":5},

        //groundmodel friction multipliers
        {"frictionCoef":1.0},
        {"slidingFrictionCoef":1.0},
        {"stribeckVelMult":2},
        {"stribeckExponent":1.25},
        {"treadCoef":0.5},

        //advanced friction values
        {"noLoadCoef":1.41},
        {"loadSensitivitySlope":0.000156},
        {"fullLoadCoef":0.55},
        {"softnessCoef":0.8},

        //deform values
        {"wheelSideBeamDeform":15000,"wheelSideBeamStrength":20000},
        {"wheelTreadBeamDeform":11000,"wheelTreadBeamStrength":14000},
        {"wheelPeripheryBeamDeform":50000,"wheelPeripheryBeamStrength":50000},
    ],
},
}