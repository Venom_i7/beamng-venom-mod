{
"atv_tire_F": {
    "information":{
        "authors":"BeamNG",
        "name":"Middle Front Tires",
        "value":50,
    },
    "slotType" : "atv_tire_F",
    "flexbodies": [
        ["mesh", "[group]:", "nonFlexMaterials"],
        //front tires
        ["atv_8x8_tire", ["wheel_FR","tire_FR"], [], {"pos":{"x":-0.48, "y":0, "z":0}, "rot":{"x":0, "y":0, "z":0}, "scale":{"x":1, "y":1, "z":1}}],
        ["atv_8x8_tire", ["wheel_FL","tire_FL"], [], {"pos":{"x": 0.48, "y":0, "z":0}, "rot":{"x":0, "y":0, "z":0}, "scale":{"x":1, "y":1, "z":1}}],
    ],
    "variables": [
        ["name", "type", "unit", "category", "default", "min", "max", "title", "description"],
        ["$tirepressure_F", "range", "psi", "Wheels",  3, 0, 10, "Tire Pressure", "Relative to atmospheric pressure", {"subCategory":"Middle Front"}],
    ],
    "pressureWheels": [
        ["name","hubGroup","group","node1:","node2:","nodeS","nodeArm:","wheelDir"],

        {"disableMeshBreaking":false,"disableHubMeshBreaking":false},
        {"hasTire":true},
        {"enableTireReinfBeams":false},
        {"enableTireLbeams":true},
        {"enableTireSideReinfBeams":false},
        {"enableTreadReinfBeams":true},
        {"enableTirePeripheryReinfBeams":true},

        //general settings
        {"radius":0.3},
        {"tireWidth":0.25},

        //tire options
        {"wheelSideBeamSpring":"$=$tirepressure_RR*700","wheelSideBeamDamp":120},
        {"wheelSideBeamSpringExpansion":701000,"wheelSideBeamDampExpansion":60},
        {"wheelSideTransitionZone":0.1,"wheelSideBeamPrecompression":0.99},

        {"wheelSideReinfBeamSpring":"$=$tirepressure_RR*200","wheelSideReinfBeamDamp":120},
        {"wheelSideReinfBeamSpringExpansion":101000,"wheelSideReinfBeamDampExpansion":60},
        {"wheelSideReinfTransitionZone":0.09,"wheelSideReinfBeamPrecompression":0.99},

        {"wheelReinfBeamSpring":2000,"wheelReinfBeamDamp":400},
        {"wheelReinfBeamDampCutoffHz":250,"wheelReinfBeamPrecompression":0.99},

        {"wheelTreadBeamSpring":61000,"wheelTreadBeamDamp":200},
        {"wheelTreadBeamDampCutoffHz":500,"wheelTreadBeamPrecompression":0.99},

        {"wheelTreadReinfBeamSpring":61000,"wheelTreadReinfBeamDamp":200},
        {"wheelTreadReinfBeamDampCutoffHz":500,"wheelTreadReinfBeamPrecompression":0.99},

        {"wheelPeripheryBeamSpring":61000,"wheelPeripheryBeamDamp":125},
        {"wheelPeripheryBeamDampCutoffHz":500,"wheelPeripheryBeamPrecompression":0.99},

        {"wheelPeripheryReinfBeamSpring":61000,"wheelPeripheryReinfBeamDamp":125},
        {"wheelPeripheryReinfBeamDampCutoffHz":500,"wheelPeripheryReinfBeamPrecompression":0.99},

        //general tire values
        {"nodeWeight":0.45},
        {"nodeMaterial":"|NM_RUBBER"},
        {"triangleCollision":false},
        {"pressurePSI":"$tirepressure_F"},
        {"dragCoef":15},

        //groundmodel friction multipliers
        {"frictionCoef":1},
        {"slidingFrictionCoef":1.1},
        {"stribeckExponent":1.5},
        {"treadCoef":1},

        //advanced friction values
        {"noLoadCoef":1.3},
        {"loadSensitivitySlope":0.00024},
        {"fullLoadCoef":0.4},
        {"softnessCoef":0.8},

        //deform values
        {"wheelSideBeamDeform":54000,"wheelSideBeamStrength":63000},
        {"wheelTreadBeamDeform":44000,"wheelTreadBeamStrength":51000},
        {"wheelPeripheryBeamDeform":101000,"wheelPeripheryBeamStrength":121000},
        {"axleBeams":[]},{"disableMeshBreaking":false,"disableTriangleBreaking":false},
        //skin drag
        {"skinDragCoef":50},
    ],
},
}