{
"lansdale_transfer_case_FWD": {
    "information":{
        "authors":"BeamNG",
        "name":"FWD Output Shaft",
        "value":70,
    },
    "slotType" : "lansdale_transfer_case",
    "powertrain" : [
        ["type", "name", "inputName", "inputIndex"],
        ["shaft", "transfercase", "gearbox", 1, {"deformGroups":["gearbox"], "outputPortOverride":[2], "friction":0.33, "dynamicFriction":0.00037, "uiName":"Front Output Shaft"}],
    ],
    "flexbodies": [
         ["mesh", "[group]:", "nonFlexMaterials"],
         ["lansdale_transfercase_fwd", ["lansdale_engine","lansdale_transmission"]],
    ],
},
"lansdale_transfer_case_RWD": {
    "information":{
        "authors":"BeamNG",
        "name":"RWD Output Shaft",
        "value":90,
    },
    "slotType" : "lansdale_transfer_case",
    "powertrain" : [
        ["type", "name", "inputName", "inputIndex"],
        ["shaft", "transfercase", "gearbox", 1, {"deformGroups":["gearbox"], "friction":0.4, "dynamicFriction":0.0005, "torqueLossCoef": 0.012, "uiName":"Rear Output Shaft"}],
    ],
    "flexbodies": [
         ["mesh", "[group]:", "nonFlexMaterials"],
         ["lansdale_transfercase", ["lansdale_engine","lansdale_transmission"]],
    ],
    "nodes": [
         ["id", "posX", "posY", "posZ"],
         {"selfCollision":false},
         {"collision":true},
         {"nodeMaterial":"|NM_METAL"},
         {"frictionCoef":0.5},
         ["tra2", 0.0, -1.335, 0.25, {"nodeWeight":15, "group":""}],
    ],
    "beams": [
          ["id1:", "id2:"],
          //differential node
          {"beamType":"|NORMAL", "beamLongBound":1.0, "beamShortBound":1.0},
          {"beamSpring":2501000,"beamDamp":150},
          {"beamDeform":35000,"beamStrength":"FLT_MAX"},
          {"deformLimitExpansion":1.2},
          {"deformGroup":"transfercase", "deformationTriggerRatio":0.001},
          ["tra2", "tra1l"],
          ["tra2", "tra1r"],
          ["tra2", "e1r"],
          ["tra2", "e2r"],
          ["tra2", "e3l"],
          ["tra2", "e4l"],
          ["tra2", "e1l"],
          ["tra2", "e2l"],
          ["tra2", "e3r"],
          ["tra2", "e4r"],
          {"deformGroup":""},
    ],
},
"lansdale_transfer_case_AWD": {
    "information":{
        "authors":"BeamNG",
        "name":"AWD Transfer Case",
        "value":1100,
    },
    "slotType" : "lansdale_transfer_case",
    "powertrain" : [
        ["type", "name", "inputName", "inputIndex"],
        //lsd center diff
        ["splitShaft", "transfercase", "gearbox", 1, {"splitType":"viscous", "primaryOutputID":2, "viscousCoef":15, "viscousTorque":500, "friction":0.44, "dynamicFriction":0.00056, "torqueLossCoef": 0.012, "uiName":"Viscous Torque Splitter","defaultVirtualInertia":0.1}],
    ],
    "flexbodies": [
         ["mesh", "[group]:", "nonFlexMaterials"],
         ["lansdale_transfercase", ["lansdale_engine","lansdale_transmission"]],
    ],
    "nodes": [
         ["id", "posX", "posY", "posZ"],
         {"selfCollision":false},
         {"collision":true},
         {"nodeMaterial":"|NM_METAL"},
         {"frictionCoef":0.5},
         ["tra2", 0.0, -1.335, 0.25, {"nodeWeight":15, "group":""}],
    ],
    "beams": [
          ["id1:", "id2:"],
          //differential node
          {"beamType":"|NORMAL", "beamLongBound":1.0, "beamShortBound":1.0},
          {"beamSpring":2501000,"beamDamp":150},
          {"beamDeform":35000,"beamStrength":"FLT_MAX"},
          {"deformLimitExpansion":1.2},
          {"deformGroup":"transfercase", "deformationTriggerRatio":0.001},
          ["tra2", "tra1l"],
          ["tra2", "tra1r"],
          ["tra2", "e1r"],
          ["tra2", "e2r"],
          ["tra2", "e3l"],
          ["tra2", "e4l"],
          ["tra2", "e1l"],
          ["tra2", "e2l"],
          ["tra2", "e3r"],
          ["tra2", "e4r"],
          {"deformGroup":""},
    ],
}
"lansdale_transfer_case_AWD_race": {
    "information":{
        "authors":"BeamNG",
        "name":"Race AWD Transfer Case",
        "value":4500,
    },
    "slotType" : "lansdale_transfer_case",
    "variables": [
        ["name", "type", "unit", "category", "default", "min", "max", "title", "description"],
        ["$difftorquesplit_C", "range", "", "Differentials", 0.5, 0.25, 0.75, "Base Torque Split", "Percent torque to rear wheels", {"minDis":25, "maxDis":75, "stepDis":1, "subCategory":"Center"}],
        ["$lsdpreload_C", "range", "N/m", "Differentials", 20, 0, 200, "Pre-load Torque", "Initial cross torque between front and rear wheels", {"subCategory":"Center"}],
        ["$lsdlockcoef_C", "range", "", "Differentials", 0.1, 0, 0.5, "Power Lock Rate", "Additional locking torque proportional to engine torque", {"minDis":0, "maxDis":100, "subCategory":"Center"}],
        ["$lsdlockcoefrev_C", "range", "", "Differentials", 0.025, 0, 0.5, "Coast Lock Rate", "Additional locking torque proportional to engine braking", {"minDis":0, "maxDis":100, "subCategory":"Center"}],
    ],
    "powertrain" : [
        ["type", "name", "inputName", "inputIndex"],
        ["differential", "transfercase", "gearbox", 1, {"diffType":"lsd", "lsdPreload":"$lsdpreload_C", "lsdLockCoef":"$lsdlockcoef_C", "lsdRevLockCoef":"$lsdlockcoefrev_C", "diffTorqueSplit":"$difftorquesplit_C", "friction":0.44, "dynamicFriction":0.00056, "torqueLossCoef": 0.012, "uiName":"Center Differential", "defaultVirtualInertia":0.1, "speedLimitCoef":0.1}],
    ],
    "flexbodies": [
         ["mesh", "[group]:", "nonFlexMaterials"],
         ["lansdale_transfercase", ["lansdale_engine","lansdale_transmission"]],
    ],
    "nodes": [
         ["id", "posX", "posY", "posZ"],
         {"selfCollision":false},
         {"collision":true},
         {"nodeMaterial":"|NM_METAL"},
         {"frictionCoef":0.5},
         ["tra2", 0.0, -1.335, 0.25, {"nodeWeight":7, "group":""}],
    ],
    "beams": [
          ["id1:", "id2:"],
          //differential node
          {"beamType":"|NORMAL", "beamLongBound":1.0, "beamShortBound":1.0},
          {"beamSpring":2501000,"beamDamp":150},
          {"beamDeform":35000,"beamStrength":"FLT_MAX"},
          {"deformLimitExpansion":1.2},
          {"deformGroup":"transfercase", "deformationTriggerRatio":0.001},
          ["tra2", "tra1l"],
          ["tra2", "tra1r"],
          ["tra2", "e1r"],
          ["tra2", "e2r"],
          ["tra2", "e3l"],
          ["tra2", "e4l"],
          ["tra2", "e1l"],
          ["tra2", "e2l"],
          ["tra2", "e3r"],
          ["tra2", "e4r"],
          {"deformGroup":""},
    ],
}
"lansdale_transfer_case_AWD_locking": {
    "information":{
        "authors":"BeamNG",
        "name":"Locking Low-Range AWD Transfer Case",
        "value":3500,
    },
    "slotType" : "lansdale_transfer_case",
    "controller": [
        ["fileName"],
        ["driveModes", {"name":"transfercaseControl"}]
        ["4wd", {"driveModesTransfercaseNames":["transfercaseControl"]}]
    ],
    "transfercaseControl":{
        "uiName": "Transfercase"
        "quickAccessTitle":"Transfercase"
        "quickAccessIcon":"radial_highrangebox"
        "quickAccessLevel":"/powertrain/"
        "enabledModes":["4hi", "4lo"]
        "defaultMode": "4hi"
        "modes": {
            "4hi": {
                "name": "4WD High Gear",
                "order": 20,
                "settings": [
                    ["type"]
                    ["powertrainDeviceMode", {"deviceName":"rangebox", "mode":"high"}]
                    ["powertrainDeviceMode", {"deviceName":"transfercase", "mode":"lsd"}]
                    ["electricsValue", {"electricsName":"transfercase_state", "value":0.33}]
                    ["simpleControlButton", {"buttonId":"transfercase", "icon":"powertrain_transfercase_high-4", "uiName":"Transfercase"}]
                    ["quickAccess", {"icon":"powertrain_transfercase_high-4"}]
                ],
            },
            "4lo": {
                "name": "4WD Low Gear",
                "order": 30,
                "settings": [
                    ["type"]
                    ["powertrainDeviceMode", {"deviceName":"rangebox", "mode":"low"}]
                    ["powertrainDeviceMode", {"deviceName":"transfercase", "mode":"locked"}]
                    ["electricsValue", {"electricsName":"transfercase_state", "value":-1}]
                    ["simpleControlButton", {"buttonId":"transfercase", "icon":"powertrain_transfercase_low-4", "uiName":"Transfercase"}]
                    ["quickAccess", {"icon":"powertrain_transfercase_low-4"}]
                ],
            },
        }
    }
    "powertrain" : [
        ["type", "name", "inputName", "inputIndex"],
        ["rangeBox", "rangebox", "gearbox", 1, {"uiSimpleModeControl":false, "gearRatios":[1,4.76], "uiName":"Rangebox"}],
        //lsd center diff
        ["differential", "transfercase", "rangebox", 1, {"defaultToggle":false,"uiSimpleModeControl":false, "diffType":["lsd","locked"], "lsdPreload":50, "lsdLockCoef":0.05, "lsdRevLockCoef":0.05, "diffTorqueSplit":0.3, "friction":0.4, "dynamicFriction":0.0005, "torqueLossCoef": 0.012, "uiName":"Transfer Case", "defaultVirtualInertia":0.1}],
    ],
    "flexbodies": [
         ["mesh", "[group]:", "nonFlexMaterials"],
         ["lansdale_transfercase", ["lansdale_engine","lansdale_transmission"]],
    ],
    "nodes": [
         ["id", "posX", "posY", "posZ"],
         {"selfCollision":false},
         {"collision":true},
         {"nodeMaterial":"|NM_METAL"},
         {"frictionCoef":0.5},
         ["tra2", 0.0, -1.335, 0.25, {"nodeWeight":9, "group":""}],
    ],
    "beams": [
          ["id1:", "id2:"],
          //differential node
          {"beamType":"|NORMAL", "beamLongBound":1.0, "beamShortBound":1.0},
          {"beamSpring":2501000,"beamDamp":150},
          {"beamDeform":35000,"beamStrength":"FLT_MAX"},
          {"deformLimitExpansion":1.2},
          {"deformGroup":"transfercase", "deformationTriggerRatio":0.001},
          ["tra2", "tra1l"],
          ["tra2", "tra1r"],
          ["tra2", "e1r"],
          ["tra2", "e2r"],
          ["tra2", "e3l"],
          ["tra2", "e4l"],
          ["tra2", "e1l"],
          ["tra2", "e2l"],
          ["tra2", "e3r"],
          ["tra2", "e4r"],
          {"deformGroup":""},
    ],
}
}