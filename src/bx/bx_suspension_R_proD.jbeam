{
"bx_suspension_R_prodrift": {
    "information":{
        "authors":"BeamNG",
        "name":"DriftGear ProDrift Rear Suspension",
        "value":750,
    },
    "slotType" : "bx_suspension_R",
    "slots": [
        ["type", "default", "description"],
        ["bx_differential_R","bx_differential_R", "Rear Differential"],
        ["bx_hub_R_prodrift","bx_hub_R_5_prodrift", "Rear Wheel Hubs"],
        ["bx_wheeldata_R","bx_wheeldata_R", "Rear Spindles", {"coreSlot":true}],
        ["bx_coilover_R_prodrift","bx_coilover_R_prodrift", "Rear Struts"],
        ["bx_swaybar_R","bx_swaybar_R", "Rear Sway Bar"],
    ],
    "variables": [
        ["name", "type", "unit", "category", "default", "min", "max", "title", "description"],
        ["$trackoffset_R", "range", "+m", "Wheels", 0.0,-0.02, 0.05, "Track Offset", "Spacing of the wheel from the hub", {"stepDis":0.001, "subCategory":"Rear"}],
        ["$camber_RR", "range", "", "Wheel Alignment", 0.991, 0.95, 1.05, "Camber Adjust", "Adjusts the wheel camber angle", {"subCategory":"Rear"}],
        ["$toe_RR", "range", "", "Wheel Alignment", 0.9985, 0.98, 1.02, "Toe Adjust", "Adjusts the wheel toe-in angle", {"subCategory":"Rear"}],
        ["$asquat_RR", "range", "", "Wheel Alignment", 0.0, -0.05, 0.05, "Anti-Squat Adjust", "Promotes / Resists rear squat on acceleration", {"subCategory":"Rear"}],
    ],
    "flexbodies": [
        ["mesh", "[group]:", "nonFlexMaterials"],
        //rear running gear
        ["bx_subframe_R", ["bx_subframe_R"]],
        ["bx_lowerarm_R", ["bx_lowermounts_R","bx_lowerarm_R"]],
        ["bx_hub_R", ["bx_hub_R"]],
        ["bx_upperarm_R", ["bx_uppermounts_R","bx_upperarm_R"]],
        ["bx_tierod_R", ["bx_uppermounts_R","bx_tierod_R"]],
    ],
    "nodes": [
         ["id", "posX", "posY", "posZ"],
         //--REAR SUBFRAME--
         {"nodeMaterial":"|NM_METAL"},
         {"frictionCoef":0.5},
         {"collision":true},
         {"selfCollision":true},
         //multilink suspension
         {"nodeWeight":5.5},
         {"group":["bx_subframe_R", "bx_lowermounts_R"]},
         //lower arm
         ["rx1r", -0.3362, 1.2192, 0.2671, {"tag":"rideheight_RR"}],
         ["rx1l",  0.3362, 1.2192, 0.2671, {"tag":"rideheight_RL"}],
         //{"selfCollision":false},
         {"nodeWeight":7.5},
         {"group":"bx_lowermounts_R"},
         ["rx2r", -0.4681, 0.9511, "$=$asquat_RR+0.3465", {"selfCollision":false}],
         ["rx2l",  0.4681, 0.9511, "$=$asquat_RR+0.3465", {"selfCollision":false}],
         {"nodeWeight":5.5},

         //upper links
         {"group":["bx_subframe_R", "bx_uppermounts_R"]},
         ["rx3r", -0.4546, 1.0876, 0.4563],
         ["rx3l",  0.4546, 1.0876, 0.4563],
         {"group":"bx_uppermounts_R"},
         ["rx4r", -0.3368, 1.3403, 0.4603],
         ["rx4l",  0.3368, 1.3403, 0.4603],
         //toe link
         ["rx5r", -0.2068, 1.3925, 0.3334],
         ["rx5l",  0.2068, 1.3925, 0.3334],

         {"nodeWeight":3.7},
         //front mounts
         {"group":"bx_subframe_R"},
         ["rx6r", -0.59, 0.78, 0.21, {"selfCollision":false}],
         ["rx6l", 0.59, 0.78, 0.21, {"selfCollision":false}],
         //rear mounts
         ["rx7r", -0.34, 1.43, 0.48],
         ["rx7l", 0.34, 1.43, 0.48],

         //rigidifier
         {"nodeWeight":2.5},
         ["rx8", 0.0, 1.1, 0.54, {"group":"", "collision":false, "selfCollision":false}],


         //--REAR INDEPENDENT SUSPENSION--
         {"collision":true},
         {"selfCollision":false},
         //rear hub
         {"nodeWeight":6.5},
         //["rh2r",-0.5890, 1.3377, 0.1947, {"group":["bx_shockbottom_R","bx_hub_R"]}],
         //["rh2l", 0.5890, 1.3377, 0.1947, {"group":["bx_shockbottom_R","bx_hub_R"]}],
         ["rh2r",-0.5796, 1.3525, 0.2571, {"group":["bx_shockbottom_R","bx_hub_R"]}],
         ["rh2l", 0.5796, 1.3525, 0.2571, {"group":["bx_shockbottom_R","bx_hub_R"]}],
         {"nodeWeight":4.75},
         {"selfCollision":true},
         //["rh1r",-0.6496, 1.2287, 0.1777, {"group":["bx_lowerarm_R","bx_hub_R"]}],
         //["rh1l", 0.6496, 1.2287, 0.1777, {"group":["bx_lowerarm_R","bx_hub_R"]}],
         ["rh1r",-0.6400, 1.2450, 0.2323, {"group":["bx_lowerarm_R","bx_hub_R"]}],
         ["rh1l", 0.6400, 1.2450, 0.2323, {"group":["bx_lowerarm_R","bx_hub_R"]}],
         {"selfCollision":false},
         //["rh4r",-0.6567, 1.3169, 0.4059, {"group":["bx_upperarm_R","bx_hub_R"]}],
         //["rh4l", 0.6567, 1.3169, 0.4059, {"group":["bx_upperarm_R","bx_hub_R"]}],
         ["rh4r",-0.6418, 1.3133, 0.4674, {"group":["bx_upperarm_R","bx_hub_R"]}],
         ["rh4l", 0.6418, 1.3133, 0.4674, {"group":["bx_upperarm_R","bx_hub_R"]}],
         //["rh3r",-0.6376, 1.2173, 0.4058, {"group":["bx_upperarm_R","bx_hub_R"]}],
         //["rh3l", 0.6376, 1.2173, 0.4058, {"group":["bx_upperarm_R","bx_hub_R"]}],
         ["rh3r",-0.6220, 1.2143, 0.4584, {"group":["bx_upperarm_R","bx_hub_R"]}],
         ["rh3l", 0.6220, 1.2143, 0.4584, {"group":["bx_upperarm_R","bx_hub_R"]}],
         //["rh5r",-0.5562, 1.4189, 0.2436, {"group":["bx_tierod_R","bx_hub_R"]}],
         //["rh5l", 0.5562, 1.4189, 0.2436, {"group":["bx_tierod_R","bx_hub_R"]}],
         ["rh5r",-0.5461, 1.4295, 0.3119, {"group":["bx_tierod_R","bx_hub_R"]}],
         ["rh5l", 0.5461, 1.4295, 0.3119, {"group":["bx_tierod_R","bx_hub_R"]}],
         {"group":"bx_hub_R"},
         {"nodeWeight":3.25},
         //["rh6r",-0.66, 1.12, 0.312],
         //["rh6l", 0.66, 1.12, 0.312],
         ["rh6r",-0.64, 1.12, 0.35],
         ["rh6l", 0.64, 1.12, 0.35],

         {"nodeWeight":6},
         //["rw1r", -0.5958, 1.2433, 0.2937],
         //["rw1l",  0.5958, 1.2433, 0.2937],
         //["rw1rr",-0.7526, 1.2446, 0.2918],
         //["rw1ll", 0.7526, 1.2446, 0.2918],
         //["rw1r", -0.6033, 1.25, 0.350],
         //["rw1l", 0.6033, 1.25, 0.350],
         //["rw1rr",-0.7600, 1.25, 0.350],
         //["rw1ll",0.7600, 1.25, 0.350],
         {"group":""},
    ],
    "torsionbars": [
        ["id1:", "id2:", "id3:", "id4:"],
        {"spring":400000, "damp":0.5, "deform":15000, "strength":150000},
        //["rw1ll", "rh1l", "rh3l", "rh5l"],
        //["rw1rr", "rh1r", "rh3r", "rh5r"],
    ],
    "beams": [
          ["id1:", "id2:"],
           //--REAR SUBFRAME--
          {"beamSpring":6001000,"beamDamp":100},
          {"beamDeform":18500,"beamStrength":"FLT_MAX"},
          //rear suspension axis points
          {"deformLimitExpansion":1.1},
          //width
          ["rx1r","rx1l"],
          ["rx2r","rx2l"],
          ["rx3r","rx3l"],
          ["rx5r","rx5l"],
          ["rx4l", "rx4r"],

          //length
          {"beamSpring":4001000,"beamDamp":100},
          ["rx1r","rx2r"],
          ["rx1l","rx2l"],
          ["rx4r", "rx5r"],
          ["rx4l", "rx5l"],
          ["rx4r", "rx1r"],
          ["rx4l", "rx1l"],
          ["rx4r", "rx3r"],
          ["rx4l", "rx3l"],
          ["rx1r","rx5r"],
          ["rx1l","rx5l"],
          ["rx2r","rx4r", {"deformLimitExpansion":""}],
          ["rx2l","rx4l", {"deformLimitExpansion":""}],
          ["rx1r","rx3r", {"deformLimitExpansion":""}],
          ["rx1l","rx3l", {"deformLimitExpansion":""}],
          ["rx2r","rx3r"],
          ["rx2l","rx3l"],
          ["rx5r", "rx3r"],
          ["rx5l", "rx3l"],

          //crossing
          {"beamSpring":5001000,"beamDamp":100},
          {"deformLimitExpansion":""},
          {"beamDeform":9500,"beamStrength":"FLT_MAX"},
          ["rx2r","rx5l"],
          ["rx2l","rx5r"],
          ["rx2r","rx3l"],
          ["rx2l","rx3r"],
          ["rx1r","rx5l"],
          ["rx1l","rx5r"],
          ["rx4l", "rx5r"],
          ["rx4r", "rx5l"],
          ["rx1r", "rx3l"],
          ["rx1l", "rx3r"],

          ["rx1r", "rx4l"],
          ["rx1l", "rx4r"],

          ["rx3r", "rx5l"],
          ["rx3l", "rx5r"],

          {"beamDeform":12500,"beamStrength":"FLT_MAX"},
          ["rx4r", "rx3l"],
          ["rx4l", "rx3r"],
          ["rx2l","rx1r"],
          ["rx2r","rx1l"],

          //attach axis to mounts
          {"beamSpring":4001000,"beamDamp":100},
          {"beamDeform":15000,"beamStrength":"FLT_MAX"},
          //front
          {"deformLimitExpansion":1.1},
          ["rx2r", "rx6r"],
          ["rx2l", "rx6l"],
          ["rx6l", "rx6r"],
          {"beamDeform":15000,"beamStrength":"FLT_MAX"},
          ["rx3r", "rx6r"],
          ["rx3l", "rx6l"],
          //crossing
          {"deformLimitExpansion":""}
          {"beamDeform":11000,"beamStrength":"FLT_MAX"},
          ["rx2l", "rx6r"],
          ["rx2r", "rx6l"],
          ["rx6l", "rx3r"],
          ["rx3l", "rx6r"],
          //rigids
          ["rx6r", "rx1r"],
          ["rx6l", "rx1l"],
          ["rx6r", "rx7r"],
          ["rx6l", "rx7l"],

          //rear
          {"deformLimitExpansion":1.1},
          {"beamDeform":9000,"beamStrength":"FLT_MAX"},
          ["rx7l", "rx4l"],
          ["rx7l", "rx5l"],
          ["rx7r", "rx5r"],
          ["rx7r", "rx4r"],
          ["rx7r", "rx7l"],
          {"beamDeform":7000,"beamStrength":"FLT_MAX"},
          ["rx7l", "rx3l"],
          ["rx7r", "rx3r"],
          //crossing
          {"deformLimitExpansion":""}
          {"beamDeform":5000,"beamStrength":"FLT_MAX"},
          ["rx7l", "rx5r"],
          ["rx7r", "rx5l"],
          ["rx7l", "rx3r"],
          ["rx7r", "rx3l"],

          //rigidifier
          {"beamSpring":3501000,"beamDamp":70},
          {"beamDeform":9000,"beamStrength":"FLT_MAX"},
          ["rx8", "rx6l"],
          ["rx8", "rx6r"],
          ["rx8", "rx2l"],
          ["rx8", "rx2r"],
          ["rx8", "rx1r"],
          ["rx8", "rx1l"],
          ["rx8", "rx5r"],
          ["rx8", "rx5l"],
          ["rx8", "rx3r"],
          ["rx8", "rx3l"],
          ["rx8", "rx4r"],
          ["rx8", "rx4l"],
          ["rx8", "rx7r"],
          ["rx8", "rx7l"],

          //attach to body
          {"beamSpring":3701000, "beamDamp":200},
          {"beamDeform":19000, "beamStrength":130000},
          //front
          {"breakGroup":"subframemount_RR1"},
          ["rx6r", "f4rr"],
          ["rx6r", "f4r"],
          ["rx6r", "f3rr"],
          ["rx6r", "f3r"],
          ["rx6r", "f8r"],
          ["rx6r", "f9r"],
          ["rx2r", "f4rr"],
          ["rx2r", "f4r"],
          ["rx2r", "f3rr"],
          ["rx2r", "f3r"],
          ["rx2r", "f8r"],
          ["rx2r", "f9r"],
          {"breakGroup":"subframemount_RL1"},
          ["rx6l", "f4ll"],
          ["rx6l", "f4l"],
          ["rx6l", "f3ll"],
          ["rx6l", "f3l"],
          ["rx6l", "f8l"],
          ["rx6l", "f9l"],
          ["rx2l", "f4ll"],
          ["rx2l", "f4l"],
          ["rx2l", "f3ll"],
          ["rx2l", "f3l"],
          ["rx2l", "f8l"],
          ["rx2l", "f9l"],
          //rear
          {"beamDeform":8000, "beamStrength":130000},
          {"breakGroup":"subframemount_RR2"},
          ["rx7r", "r5rr"],
          ["rx7r", "r2"],
          ["rx7r", "r5"],
          ["rx7r", "f9r"],
          ["rx7r", "f9l"],
          ["rx7r", "r1rr"],
          ["rx7r", "r1"],
          ["rx5r", "r5rr"],
          ["rx5r", "r5"],
          ["rx5r", "f9r"],
          ["rx5r", "f9l"],
          {"breakGroup":"subframemount_RL2"},
          ["rx7l", "r5ll"],
          ["rx7l", "r2"],
          ["rx7l", "r5"],
          ["rx7l", "f9l"],
          ["rx7l", "f9r"],
          ["rx7l", "r1ll"],
          ["rx7l", "r1"],
          ["rx5l", "r5ll"],
          ["rx5l", "r5"],
          ["rx5l", "f9l"],
          ["rx5l", "f9r"],
          {"breakGroup":""},

          //limiter
          {"beamPrecompression":1.0, "beamType":"|SUPPORT", "beamLongBound":2},
          {"beamSpring":2001000, "beamDamp":180},
          {"beamDeform":9000, "beamStrength":80000},
          {"deformLimitExpansion":""},
          ["rx6r", "f4rr"],
          ["rx6r", "f4r"],
          ["rx6r", "f3rr"],
          ["rx6r", "f3r"],
          ["rx6r", "f8r"],
          ["rx6r", "f9r"],
          ["rx6l", "f4ll"],
          ["rx6l", "f4l"],
          ["rx6l", "f3ll"],
          ["rx6l", "f3l"],
          ["rx6l", "f8l"],
          ["rx6l", "f9l"],

          ["rx1r", "f9r"],
          ["rx1r", "r5rr"],
          ["rx1r", "r5"],
          ["rx1r", "f9l"],

          ["rx1l", "f9l"],
          ["rx1l", "r5ll"],
          ["rx1l", "r5"],
          ["rx1l", "f9r"],

          {"beamType":"|NORMAL", "beamLongBound":1.0, "beamShortBound":1.0},

          //--REAR SUSPENSION--
          //multilink suspension
          {"deformLimitExpansion":1.1},
          {"beamType":"|NORMAL", "beamLongBound":1.0, "beamShortBound":1.0},
          {"beamDeform":67500,"beamStrength":335000},
          {"beamSpring":6001000,"beamDamp":100},
          //rear hub
          ["rh2r","rh1r"],
          ["rh2l","rh1l"],
          ["rh2r","rh4r"],
          ["rh2l","rh4l"],
          ["rh2r","rh3r"],
          ["rh2l","rh3l"],
          ["rh2r","rh5r"],
          ["rh2l","rh5l"],

          {"beamSpring":8001000,"beamDamp":100},
          ["rh1r","rh4r"],
          ["rh1l","rh4l"],
          ["rh1r","rh3r"],
          ["rh1l","rh3l"],
          ["rh4r","rh3r"],
          ["rh4l","rh3l"],

          ["rh1r","rh5r"],
          ["rh1l","rh5l"],
          ["rh4r","rh5r"],
          ["rh4l","rh5l"],
          ["rh3r","rh5r"],
          ["rh3l","rh5l"],

          {"beamSpring":6501000,"beamDamp":100},
          ["rh1r","rh6r"],
          ["rh1l","rh6l"],
          ["rh4r","rh6r"],
          ["rh4l","rh6l"],
          ["rh3r","rh6r"],
          ["rh3l","rh6l"],
          ["rh6r","rh5r"],
          ["rh6l","rh5l"],
          {"beamSpring":4501000,"beamDamp":100},
          ["rh2r","rh6r"],
          ["rh2l","rh6l"],

          {"optional":true},
          {"beamSpring":8501000,"beamDamp":100},
          {"beamDeform":75000,"beamStrength":275000},
          //attach to wheel
          {"breakGroup":"wheel_RR"},
          ["rh2r","rw1r", {"beamSpring":4501000}],
          ["rh1r","rw1r"],
          ["rh4r","rw1r"],
          ["rh3r","rw1r"],
          ["rh6r","rw1r", {"name":"axle_RR","beamSpring":6501000}],
          ["rh5r","rw1r"],

          ["rh2r","rw1rr", {"beamSpring":4501000}],
          ["rh1r","rw1rr"],
          ["rh4r","rw1rr"],
          ["rh3r","rw1rr"],
          ["rh6r","rw1rr", {"beamSpring":6501000}],
          ["rh5r","rw1rr"],

          {"breakGroup":"wheel_RL"},
          ["rh2l","rw1l", {"beamSpring":4501000}],
          ["rh1l","rw1l"],
          ["rh4l","rw1l"],
          ["rh3l","rw1l"],
          ["rh6l","rw1l", {"name":"axle_RL","beamSpring":6501000}],
          ["rh5l","rw1l"],

          ["rh2l","rw1ll", {"beamSpring":4501000}],
          ["rh1l","rw1ll"],
          ["rh4l","rw1ll"],
          ["rh3l","rw1ll"],
          ["rh6l","rw1ll", {"beamSpring":6501000}],
          ["rh5l","rw1ll"],

          {"breakGroup":""},
          {"optional":false},

          //lower links
          {"beamDeform":70500,"beamStrength":375000},
          {"beamSpring":11001000"beamDamp":1000},
          ["rx1r","rh1r", {"dampCutoffHz":500}],
          ["rx1l","rh1l", {"dampCutoffHz":500}],
          ["rx2r","rh1r", {"dampCutoffHz":500}],
          ["rx2l","rh1l", {"dampCutoffHz":500}],

          //toe arm
          {"beamPrecompression":1, "beamType":"|NORMAL", "beamLongBound":1.0, "beamShortBound":1.0},
          {"beamDeform":70500,"beamStrength":375000},
          {"beamSpring":11001000,"beamDamp":1000},
          ["rx5r","rh5r", {"beamPrecompression":"$toe_RR","beamPrecompressionTime":0.5,"dampCutoffHz":500}],
          ["rx5l","rh5l", {"beamPrecompression":"$toe_RR","beamPrecompressionTime":0.5,"dampCutoffHz":500}],

          //camber arm
          {"beamSpring":11001000,"beamDamp":1000},
          ["rx3r","rh3r", {"beamPrecompression":"$camber_RR","beamPrecompressionTime":0.5,"dampCutoffHz":500}],
          ["rx3l","rh3l", {"beamPrecompression":"$camber_RR","beamPrecompressionTime":0.5,"dampCutoffHz":500}],
          ["rx4r","rh4r", {"beamPrecompression":"$camber_RR","beamPrecompressionTime":0.5,"dampCutoffHz":500}],
          ["rx4l","rh4l", {"beamPrecompression":"$camber_RR","beamPrecompressionTime":0.5,"dampCutoffHz":500}],

          //damping beam
          {"beamDeform":10500,"beamStrength":27500},
          {"beamSpring":0,"beamDamp":1000},
          ["rx4r","rh3r", {"dampCutoffHz":500}],
          ["rx4l","rh3l", {"dampCutoffHz":500}],
          ["rx3r", "rh4r", {"dampCutoffHz":500}],
          ["rx3l", "rh4l", {"dampCutoffHz":500}],
          {"beamSpring":0,"beamDamp":500},
          ["rx2r", "rh5r", {"dampCutoffHz":500}],
          ["rx2l", "rh5l", {"dampCutoffHz":500}],

          //rear limiters
          {"deformLimitExpansion":""},
          {"beamPrecompression":0.75, "beamType":"|SUPPORT", "beamLongBound":2.0, "beamShortBound":1.0},
          {"beamSpring":3001000, "beamDamp":1500},
          {"beamDeform":30000, "beamStrength":400000},
          {"optional":true},
          ["rw1rr","f4rr"],
          ["rw1r","f4rr"],

          ["rw1ll","f4ll"],
          ["rw1l","f4ll"],

          {"beamPrecompression":0.85},
          {"beamDeform":20000, "beamStrength":400000},
          ["rh1r","f4rr"],
          ["rh1r","f4r"],
          ["rh1l","f4ll"],
          ["rh1l","f4l"],

          {"beamDeform":30000, "beamStrength":400000},
          ["r3rr", "rh1r"],
          ["r6rr", "rh1r"],
          ["rh4r","f4rr"],
          ["rh4r","f4r"],
          ["r3rr", "rh4r"],
          ["r6rr", "rh4r"],

          ["r3ll", "rh1l"],
          ["r6ll", "rh1l"],
          ["rh4l","f4ll"],
          ["rh4l","f4l"],
          ["r3ll", "rh4l"],
          ["r6ll", "rh4l"],
          {"optional":false},

          //upper arm anti-invert
          {"beamSpring":0,"beamDamp":0,"beamDeform":45000,"beamStrength":400000},
          {"beamLimitSpring":2501000,"beamLimitDamp":250},
          {"beamPrecompression":1, "beamType":"|BOUNDED", "beamLongBound":0.3, "beamShortBound":0.2},
          ["rh3r","rx1r"],
          ["rh3l","rx1l"],
          ["rh4r","rx1r"],
          ["rh4l","rx1l"],
          //lower arm anti-invert
          ["rh1r","rx3r"],
          ["rh1l","rx3l"],
          ["rh1r","rx4r"],
          ["rh1l","rx4l"],
          //hub anti-invert
          {"beamPrecompression":1, "beamType":"|BOUNDED", "beamLongBound":0.25, "beamShortBound":0.25},
          ["rh6r","rx5r"],
          ["rh6l","rx5l"],
          ["rh5r","rx1r"],
          ["rh5l","rx1l"],

          //suspension travel hard limit
          {"beamPrecompression":1, "beamType":"|BOUNDED", "beamLongBound":1, "beamShortBound":1},
          {"beamSpring":0,"beamDamp":100,"beamDeform":35000,"beamStrength":150000},
          {"beamLimitSpring":2001000,"beamLimitDamp":1000},
          ["rh1r","r1rr", {"longBoundRange":0.12,"shortBoundRange":0.12,"boundZone":0.025,"beamLimitDampRebound":0,"dampCutoffHz":500}],
          ["rh1l","r1ll", {"longBoundRange":0.12,"shortBoundRange":0.12,"boundZone":0.025,"beamLimitDampRebound":0,"dampCutoffHz":500}],
          {"beamPrecompression":1, "beamType":"|NORMAL", "beamLongBound":1.0, "beamShortBound":1.0},
          {"deformLimitExpansion":1.1},
    ],
    "triangles": [
          ["id1:","id2:","id3:"],
          {"dragCoef":1},
          ["rx6r", "rx2r", "rx2l",{"triangleType":"NONCOLLIDABLE"}],
          ["rx2l", "rx6l", "rx6r",{"triangleType":"NONCOLLIDABLE"}],
          ["rx2r", "rx1r", "rx1l"],
          ["rx1l", "rx2l", "rx2r"],
          ["rx1r", "rx5r", "rx5l"],
          ["rx5l", "rx1l", "rx1r"],
          ["rx5r", "rx7r", "rx7l"],
          ["rx7l", "rx5l", "rx5r"],
          ["rx3l", "rx2l", "rx1l"],
          ["rx3l", "rx1l", "rx4l"],
          ["rx4l", "rx1l", "rx5l"],
          ["rx7l", "rx4l", "rx5l"],
          ["rx7l", "rx3l", "rx4l"],
          ["rx7r", "rx4r", "rx3r"],
          ["rx7r", "rx5r", "rx4r"],
          ["rx4r", "rx5r", "rx1r"],
          ["rx4r", "rx1r", "rx3r"],
          ["rx3r", "rx1r", "rx2r"],

          {"triangleType":"NONCOLLIDABLE"},

          ["rx1r", "rx2r", "rh1r"],
          ["rh3r", "rx3r", "rx4r"],
          ["rh4r", "rx3r", "rx4r"],
          ["rh6r", "rx5r", "rh5r"],
          ["rh6r", "rh3r", "rh4r"],
          ["rh2r", "rh6r", "rh4r"],
          ["rh1r", "rh6r", "rh2r"],
          ["rh5r", "rh1r", "rh2r"],

          ["rx2l", "rx1l", "rh1l"],
          ["rx3l", "rh3l", "rx4l"],
          ["rx3l", "rh4l", "rx4l"],
          ["rx5l", "rh6l", "rh5l"],
          ["rh3l", "rh6l", "rh4l"],
          ["rh6l", "rh2l", "rh4l"],
          ["rh6l", "rh1l", "rh2l"],
          ["rh1l", "rh5l", "rh2l"],
          {"triangleType":"NORMALTYPE"},
    ],
    "pressureWheels": [
            ["name","hubGroup","group","node1:","node2:","nodeS","nodeArm:","wheelDir"],
            //cancel out brake properties
            {"brakeTorque":0},
            {"parkingTorque":0},
            {"enableBrakeThermals":false},
            {"brakeDiameter":false},
            {"brakeMass":false},
            {"brakeType":false},
            {"rotorMaterial":false},
            {"brakeVentingCoef":false},
    ],
},
"bx_hub_R_4_prodrift": {
    "information":{
        "authors":"BeamNG",
        "name":"DriftGear 4-Lug Rear Wheel Hubs",
        "value":75,
    },
    "slotType" : "bx_hub_R_prodrift",
    "slots": [
        ["type", "default", "description"],
        ["bx_brake_R","bx_brake_R", "Rear Brakes", {"nodeOffset":{"x":"0.0", "y":0.0, "z":0.05}}],
        ["wheel_R_4","steelwheel_01a_14x5.5_R","Rear Wheels", {"nodeOffset":{"x":"$=$trackoffset_R+0.220", "y":1.25, "z":0.375}}],
    ],
    "flexbodies": [
        ["mesh", "[group]:", "nonFlexMaterials"],
        ["brake_hub_4l", ["wheel_RR","wheelhub_RR"], [], {"pos":{"x":-0.695,"y":1.25,"z":0.375}, "scale":{"x":0.5,"y":1.0,"z":1.0}, "rot":{"x":0,"y":0,"z":0}}],
        ["brake_hub_4l", ["wheel_RL","wheelhub_RL"], [], {"pos":{"x":0.695,"y":1.25,"z":0.375}, "scale":{"x":0.5,"y":1.0,"z":1.0}, "rot":{"x":0,"y":0,"z":180}}],
    ],
    "nodes": [
         ["id", "posX", "posY", "posZ"],
         {"frictionCoef":0.5},
         {"nodeMaterial":"|NM_METAL"},
         {"collision":true},
         {"selfCollision":true},
         {"nodeWeight":3.75},
         {"group":"wheelhub_RR"},
         ["rw1r", -0.54, 1.25, 0.375],
         ["rw1rr", -0.75, 1.25, 0.375],
         {"group":"wheelhub_RL"},
         ["rw1l", 0.54, 1.25, 0.375],
         ["rw1ll", 0.75, 1.25, 0.375],
         {"group":""},
    ],
    "pressureWheels": [
        ["name","hubGroup","group","node1:","node2:","nodeS","nodeArm:","wheelDir"],
        {"disableMeshBreaking":false,"disableHubMeshBreaking":false,"enableHubcaps":false,"hasTire":false},
        //hub
        //general settings
        {"hubRadius":0.08},
        {"wheelOffset":-0.05},
        {"hubWidth":0.05},
        {"numRays":8},

        //hub options
        {"hubTreadBeamSpring":501000, "hubTreadBeamDamp":5},
        {"hubPeripheryBeamSpring":501000, "hubPeripheryBeamDamp":5},
        {"hubSideBeamSpring":801000, "hubSideBeamDamp":5},
        {"hubNodeWeight":0.4},
        {"hubNodeMaterial":"|NM_METAL"},
        {"hubFrictionCoef":0.2},
        {"hubBeamDeform":45500, "hubBeamStrength":87500},
    ],
},
"bx_hub_R_5_prodrift": {
    "information":{
        "authors":"BeamNG",
        "name":"DriftGear 5-Lug Rear Wheel Hubs",
        "value":350,
    },
    "slotType" : "bx_hub_R_prodrift",
    "slots": [
        ["type", "default", "description"],
        ["bx_brake_R","bx_brake_R", "Rear Brakes", {"nodeOffset":{"x":"0.0", "y":0.0, "z":0.05}}],
        ["wheel_R_5","steelwheel_01a_15x7_R","Rear Wheels", {"nodeOffset":{"x":"$=$trackoffset_R+0.220", "y":1.25, "z":0.375}}],
    ],
    "flexbodies": [
        ["mesh", "[group]:", "nonFlexMaterials"],
        ["brake_hub_5l", ["wheel_RR","wheelhub_RR"], [], {"pos":{"x":-0.695,"y":1.25,"z":0.375}, "scale":{"x":0.8,"y":1.0,"z":1.0}, "rot":{"x":0,"y":0,"z":0}}],
        ["brake_hub_5l", ["wheel_RL","wheelhub_RL"], [], {"pos":{"x":0.695,"y":1.25,"z":0.375}, "scale":{"x":0.8,"y":1.0,"z":1.0}, "rot":{"x":0,"y":0,"z":180}}],
    ],
    "nodes": [
         ["id", "posX", "posY", "posZ"],
         {"frictionCoef":0.5},
         {"nodeMaterial":"|NM_METAL"},
         {"collision":true},
         {"selfCollision":true},
         {"nodeWeight":3.75},
         {"group":"wheelhub_RR"},
         ["rw1r", -0.54, 1.25, 0.375],
         ["rw1rr", -0.75, 1.25, 0.375],
         {"group":"wheelhub_RL"},
         ["rw1l", 0.54, 1.25, 0.375],
         ["rw1ll", 0.75, 1.25, 0.375],
         {"group":""},
    ],
    "pressureWheels": [
        ["name","hubGroup","group","node1:","node2:","nodeS","nodeArm:","wheelDir"],
        {"disableMeshBreaking":false,"disableHubMeshBreaking":false,"enableHubcaps":false,"hasTire":false},
        //hub
        //general settings
        {"hubRadius":0.08},
        {"wheelOffset":-0.05},
        {"hubWidth":0.05},
        {"numRays":8},

        //hub options
        {"hubTreadBeamSpring":501000, "hubTreadBeamDamp":5},
        {"hubPeripheryBeamSpring":501000, "hubPeripheryBeamDamp":5},
        {"hubSideBeamSpring":801000, "hubSideBeamDamp":5},
        {"hubNodeWeight":0.4},
        {"hubNodeMaterial":"|NM_METAL"},
        {"hubFrictionCoef":0.2},
        {"hubBeamDeform":45500, "hubBeamStrength":87500},
    ],
},
"bx_coilover_R_prodrift": {
    "information":{
        "authors":"BeamNG",
        "name":"ProDrift Rear Coilovers",
        "value":1050,
    },

    "slotType" : "bx_coilover_R_prodrift",
    "flexbodies": [
         ["mesh", "[group]:", "nonFlexMaterials"],
         ["bx_coilover_R", ["bx_shockbottom_R","bx_shocktop_R"]],
    ],
    "variables": [
        ["name", "type", "unit", "category", "default", "min", "max", "title", "description"],
        ["$springheight_R_drift", "range", "+m", "Suspension", 0, -0.06, 0.04, "Spring Height", "Raise or lower the suspension height", {"stepDis":0.005, "subCategory":"Rear"}],
        ["$spring_R_drift", "range", "N/m", "Suspension", 60000, 30000, 120000, "Spring Rate", "Spring stiffness", {"stepDis":500, "subCategory":"Rear"}]
        ["$damp_bump_R_drift", "range", "N/m/s", "Suspension", 4000, 500, 10000, "Bump Damping", "Damper rate in compression", {"stepDis":100, "subCategory":"Rear"}]
        ["$damp_rebound_R_drift", "range", "N/m/s", "Suspension", 12500, 500, 20000, "Rebound Damping", "Damper rate in extension", {"stepDis":100, "subCategory":"Rear"}]
    ],
    "beams": [
          ["id1:", "id2:"],
          //rear springs - no soft bump stop - advanced dampers
          {"beamType":"|NORMAL"},
          {"beamDeform":12000,"beamStrength":160000},
          {"beamSpring":"$spring_R_drift","beamDamp":0},
          //{"beamSpring":0,"beamDamp":0},
          ["rh2r","r1rr", {"name":"spring_RR","precompressionRange":"$=$springheight_R_drift + 0.02",
              "soundFile":"event:>Vehicle>Suspension>car_modn_med_01>spring_compress_01","colorFactor":0.4,"attackFactor":25,"volumeFactor":1.8,"decayFactor":15,"noiseFactor":0.01,"pitchFactor":1.0,"maxStress":400,"name":"spring_RR"}],
          ["rh2l","r1ll", {"name":"spring_RL","precompressionRange":"$=$springheight_R_drift + 0.02",
              "soundFile":"event:>Vehicle>Suspension>car_modn_med_01>spring_compress_01","colorFactor":0.4,"attackFactor":25,"volumeFactor":1.8,"decayFactor":15,"noiseFactor":0.01,"pitchFactor":1.0,"maxStress":400,"name":"spring_RR"}],
          //damper
          {"beamPrecompression":1, "beamType":"|BOUNDED", "beamLongBound":1, "beamShortBound":1},
          {"beamLimitSpring":0,"beamLimitDamp":0},
          {"beamSpring":0,"beamDamp":"$damp_bump_R_drift"},
          ["rh2r","r1rr", {"name":"damper_RR","beamDampRebound":"$damp_rebound_R_drift","beamDampVelocitySplit":0.1,"beamDampFast":"$=$damp_bump_R_drift / 3","beamDampReboundFast":"$=$damp_rebound_R_drift / 2","dampCutoffHz":500}],
          ["rh2l","r1ll", {"name":"damper_RL","beamDampRebound":"$damp_rebound_R_drift","beamDampVelocitySplit":0.1,"beamDampFast":"$=$damp_bump_R_drift / 3","beamDampReboundFast":"$=$damp_rebound_R_drift / 2","dampCutoffHz":500}],
          //hard bump stop
          {"beamSpring":0,"beamDamp":0},
          {"beamLimitSpring":151000,"beamLimitDamp":10000},
          ["rh2r","r1rr", {"longBoundRange":0.03,"shortBoundRange":0.06,"boundZone":0.09,"beamLimitDampRebound":0,"dampCutoffHz":500}],
          ["rh2l","r1ll", {"longBoundRange":0.03,"shortBoundRange":0.06,"boundZone":0.09,"beamLimitDampRebound":0,"dampCutoffHz":500}],
          {"beamPrecompression":1, "beamType":"|NORMAL", "beamLongBound":1.0, "beamShortBound":1.0},
    ],
},
}