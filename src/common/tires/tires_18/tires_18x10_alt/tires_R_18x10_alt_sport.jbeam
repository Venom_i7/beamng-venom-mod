{
"tire_R_255_40_18_alt_sport": {
    "information":{
        "authors":"BeamNG",
        "name":"255/40R18 Sport Rear Tires",
        "value":380,
    },
    "slotType" : "tire_R_18x10_alt",
    "flexbodies": [
        ["mesh", "[group]:", "nonFlexMaterials"],
        ["tire_01a_18x10_26", ["wheel_RR","tire_RR"], [], {"pos":{"x":-0.52, "y":0.0, "z":0.0}, "rot":{"x":0, "y":0, "z":180}, "scale":{"x":1.041, "y":0.997, "z":0.997}}],
        ["tire_01a_18x10_26", ["wheel_RL","tire_RL"], [], {"pos":{"x": 0.52, "y":0.0, "z":0.0}, "rot":{"x":0, "y":0, "z":0},   "scale":{"x":1.041, "y":0.997, "z":0.997}}],
    ],
    "variables": [
        ["name", "type", "unit", "category", "default", "min", "max", "title", "description"],
        ["$tirepressure_R", "range", "psi", "Wheels", 28, 0, 50, "Tire Pressure", "Relative to atmospheric pressure", {"subCategory":"Rear"}],
    ],
    "pressureWheels": [
        ["name","hubGroup","group","node1:","node2:","nodeS","nodeArm:","wheelDir"],
        {"disableMeshBreaking":false,"disableHubMeshBreaking":false},
        {"hasTire":true},
        {"enableTireReinfBeams":false},
        {"enableTireLbeams":true},
        {"enableTireSideReinfBeams":false},
        {"enableTreadReinfBeams":true},
        {"enableTirePeripheryReinfBeams":true},

        //general settings
        {"radius":0.33},
        {"tireWidth":0.23},

        //tire options
        {"wheelSideBeamSpring":"$=$tirepressure_R*900","wheelSideBeamDamp":50},
        {"wheelSideBeamSpringExpansion":501000,"wheelSideBeamDampExpansion":40},
        {"wheelSideTransitionZone":0.1,"wheelSideBeamPrecompression":0.98},

        {"wheelReinfBeamSpring":33000,"wheelReinfBeamDamp":200},
        {"wheelReinfBeamDampCutoffHz":500,"wheelReinfBeamPrecompression":0.985},

        {"wheelTreadBeamSpring":101000,"wheelTreadBeamDamp":80},
        {"wheelTreadBeamDampCutoffHz":500,"wheelTreadBeamPrecompression":0.985},

        {"wheelTreadReinfBeamSpring":221000,"wheelTreadReinfBeamDamp":70},
        {"wheelTreadReinfBeamDampCutoffHz":500,"wheelTreadReinfBeamPrecompression":0.985},

        {"wheelPeripheryBeamSpring":81000,"wheelPeripheryBeamDamp":45},
        {"wheelPeripheryBeamDampCutoffHz":500,"wheelPeripheryBeamPrecompression":0.985},

        {"wheelPeripheryReinfBeamSpring":161000,"wheelPeripheryReinfBeamDamp":45},
        {"wheelPeripheryReinfBeamDampCutoffHz":500,"wheelPeripheryReinfBeamPrecompression":0.985},

        //general tire values
        {"nodeWeight":0.185},
        {"nodeMaterial":"|NM_RUBBER"},
        {"triangleCollision":false},
        {"pressurePSI":"$tirepressure_R"},
        {"dragCoef":5},

        //groundmodel friction multipliers
        {"frictionCoef":1.0},
        {"slidingFrictionCoef":1.0},
        {"stribeckExponent":1.5},
        {"treadCoef":0.5},

        //advanced friction values
        {"noLoadCoef":1.52},
        {"loadSensitivitySlope":0.00016},
        {"fullLoadCoef":0.55},
        {"softnessCoef":0.7},

        //deform values
        {"wheelSideBeamDeform":17000,"wheelSideBeamStrength":22000},
        {"wheelTreadBeamDeform":12000,"wheelTreadBeamStrength":16000},
        {"wheelPeripheryBeamDeform":55000,"wheelPeripheryBeamStrength":55000},
    ],
},
"tire_R_275_40_18_alt_sport": {
    "information":{
        "authors":"BeamNG",
        "name":"275/40R18 Sport Rear Tires",
        "value":290,
    },
    "slotType" : "tire_R_18x10_alt",
    "flexbodies": [
        ["mesh", "[group]:", "nonFlexMaterials"],
        ["tire_01a_18x10_26_5", ["wheel_RR","tire_RR"], [], {"pos":{"x":-0.52, "y":0.0, "z":0.0}, "rot":{"x":0, "y":0, "z":180}, "scale":{"x":1.045, "y":1.002, "z":1.002}}],
        ["tire_01a_18x10_26_5", ["wheel_RL","tire_RL"], [], {"pos":{"x": 0.52, "y":0.0, "z":0.0}, "rot":{"x":0, "y":0, "z":0},   "scale":{"x":1.045, "y":1.002, "z":1.002}}],
    ],
    "variables": [
        ["name", "type", "unit", "category", "default", "min", "max", "title", "description"],
        ["$tirepressure_R", "range", "psi", "Wheels", 28, 0, 50, "Tire Pressure", "Relative to atmospheric pressure", {"subCategory":"Rear"}],
    ],
    "pressureWheels": [
        ["name","hubGroup","group","node1:","node2:","nodeS","nodeArm:","wheelDir"],
        {"disableMeshBreaking":false,"disableHubMeshBreaking":false},
        {"hasTire":true},
        {"enableTireReinfBeams":false},
        {"enableTireLbeams":true},
        {"enableTireSideReinfBeams":false},
        {"enableTreadReinfBeams":true},
        {"enableTirePeripheryReinfBeams":true},

        //general settings
        {"radius":0.338},
        {"tireWidth":0.255},

        //tire options
        {"wheelSideBeamSpring":"$=$tirepressure_R*900","wheelSideBeamDamp":40},
        {"wheelSideBeamSpringExpansion":511000,"wheelSideBeamDampExpansion":40},
        {"wheelSideTransitionZone":0.11,"wheelSideBeamPrecompression":0.99},

        {"wheelReinfBeamSpring":40000,"wheelReinfBeamDamp":220},
        {"wheelReinfBeamDampCutoffHz":500,"wheelReinfBeamPrecompression":0.985},

        {"wheelTreadBeamSpring":121000,"wheelTreadBeamDamp":80},
        {"wheelTreadBeamDampCutoffHz":500,"wheelTreadBeamPrecompression":0.985},

        {"wheelTreadReinfBeamSpring":221000,"wheelTreadReinfBeamDamp":70},
        {"wheelTreadReinfBeamDampCutoffHz":500,"wheelTreadReinfBeamPrecompression":0.985},

        {"wheelPeripheryBeamSpring":81000,"wheelPeripheryBeamDamp":55},
        {"wheelPeripheryBeamDampCutoffHz":500,"wheelPeripheryBeamPrecompression":0.985},

        {"wheelPeripheryReinfBeamSpring":181000,"wheelPeripheryReinfBeamDamp":45},
        {"wheelPeripheryReinfBeamDampCutoffHz":500,"wheelPeripheryReinfBeamPrecompression":0.985},

        //general tire values
        {"nodeWeight":0.195},
        {"nodeMaterial":"|NM_RUBBER"},
        {"triangleCollision":false},
        {"pressurePSI":"$tirepressure_R"},
        {"dragCoef":5},

        //groundmodel friction multipliers
        {"frictionCoef":1.0},
        {"slidingFrictionCoef":1.0},
        {"stribeckExponent":1.5},
        {"treadCoef":0.5},

        //advanced friction values
        {"noLoadCoef":1.55},
        {"loadSensitivitySlope":0.000155},
        {"fullLoadCoef":0.55},
        {"softnessCoef":0.7},

        //deform values
        {"wheelSideBeamDeform":22000,"wheelSideBeamStrength":28000},
        {"wheelTreadBeamDeform":14000,"wheelTreadBeamStrength":18000},
        {"wheelPeripheryBeamDeform":55000,"wheelPeripheryBeamStrength":55000},
    ],
},
"tire_R_275_55_18_alt_sport": {
    "information":{
        "authors":"BeamNG",
        "name":"275/55R18 Sport Rear Tires",
        "value":320,
    },
    "slotType" : "tire_R_18x10_alt",
    "flexbodies": [
        ["mesh", "[group]:", "nonFlexMaterials"],
        ["tire_01a_18x10_29", ["tire_RR","wheel_RR"], [], {"pos":{"x":-0.52, "y":0.0, "z":0.0}, "rot":{"x":0, "y":0, "z":180}, "scale":{"x":1.035, "y":1, "z":1}}],
        ["tire_01a_18x10_29", ["tire_RL","wheel_RL"], [], {"pos":{"x": 0.52, "y":0.0, "z":0.0}, "rot":{"x":0, "y":0, "z":0},   "scale":{"x":1.035, "y":1, "z":1}}],
    ],
    "variables": [
        ["name", "type", "unit", "category", "default", "min", "max", "title", "description"],
        ["$tirepressure_R", "range", "psi", "Wheels", 30, 0, 50, "Tire Pressure", "Relative to atmospheric pressure", {"subCategory":"Rear"}],
    ],
    "pressureWheels": [
        ["name","hubGroup","group","node1:","node2:","nodeS","nodeArm:","wheelDir"],
        {"disableMeshBreaking":false,"disableHubMeshBreaking":false},
        {"hasTire":true},
        {"enableTireReinfBeams":false},
        {"enableTireLbeams":true},
        {"enableTireSideReinfBeams":false},
        {"enableTreadReinfBeams":true},
        {"enableTirePeripheryReinfBeams":true},

        //general settings
        {"radius":0.375},
        {"tireWidth":0.235},

        //tire options
        {"wheelSideBeamSpring":"$=$tirepressure_R*850","wheelSideBeamDamp":50},
        {"wheelSideBeamSpringExpansion":551000,"wheelSideBeamDampExpansion":40},
        {"wheelSideTransitionZone":0.08,"wheelSideBeamPrecompression":0.99},

        {"wheelReinfBeamSpring":40000,"wheelReinfBeamDamp":225},
        {"wheelReinfBeamDampCutoffHz":500,"wheelReinfBeamPrecompression":0.985},

        {"wheelTreadBeamSpring":151000,"wheelTreadBeamDamp":85},
        {"wheelTreadBeamDampCutoffHz":500,"wheelTreadBeamPrecompression":0.985},

        {"wheelTreadReinfBeamSpring":281000,"wheelTreadReinfBeamDamp":75},
        {"wheelTreadReinfBeamDampCutoffHz":500,"wheelTreadReinfBeamPrecompression":0.985},

        {"wheelPeripheryBeamSpring":101000,"wheelPeripheryBeamDamp":55},
        {"wheelPeripheryBeamDampCutoffHz":500,"wheelPeripheryBeamPrecompression":0.985},

        {"wheelPeripheryReinfBeamSpring":201000,"wheelPeripheryReinfBeamDamp":45},
        {"wheelPeripheryReinfBeamDampCutoffHz":500,"wheelPeripheryReinfBeamPrecompression":0.985},

        //general tire values
        {"nodeWeight":0.22},
        {"nodeMaterial":"|NM_RUBBER"},
        {"triangleCollision":false},
        {"pressurePSI":"$tirepressure_R"},
        {"dragCoef":5},

        //groundmodel friction multipliers
        {"frictionCoef":1.0},
        {"slidingFrictionCoef":1.0},
        {"stribeckExponent":1.5},
        {"treadCoef":0.5},

        //advanced friction values
        {"noLoadCoef":1.53},
        {"loadSensitivitySlope":0.000135},
        {"fullLoadCoef":0.54},
        {"softnessCoef":0.8},

        //deform values
        {"wheelSideBeamDeform":22000,"wheelSideBeamStrength":30000},
        {"wheelTreadBeamDeform":17000,"wheelTreadBeamStrength":22000},
        {"wheelPeripheryBeamDeform":60000,"wheelPeripheryBeamStrength":60000},
    ],
},
}