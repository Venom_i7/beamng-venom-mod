{
"vivace_differential_R": {
    "information":{
        "authors":"BeamNG",
        "name":"Open Rear Differential",
        "value":650,
    },
    "slotType" : "vivace_differential_R",
    "slots": [
        ["type", "default", "description"],
        ["vivace_finaldrive_R","vivace_finaldrive_R_4187", "Rear Final Drive", {"coreSlot":true}],
        ["vivace_driveshaft","vivace_driveshaft", "Rear Driveshaft"],
        ["vivace_halfshaft_R","vivace_halfshaft_R", "Rear Half Shafts"],
    ],
    "controller": [
        ["fileName"],
        ["drivingDynamics/actuators/electronicDiffLock" {"name":"lockRear", "differentialName":"differential_R"}]
    ],
    "powertrain" : [
        ["type", "name", "inputName", "inputIndex"],
        ["differential", "differential_R", "driveshaft", 1, {"deformGroups":["differential_R"], "diffType":"open", "friction":6, "uiName":"Rear Differential","defaultVirtualInertia":0.25}],
    ],
    "differential_R": {
        "friction": 3.13,
        "dynamicFriction": 0.00148,
        "torqueLossCoef": 0.03,
    },
    "flexbodies": [
         ["mesh", "[group]:", "nonFlexMaterials"],
         ["vivace_diff_R", ["vivace_subframe_R"]],
    ],
    "nodes": [
         ["id", "posX", "posY", "posZ"],
         //--diff weight--
         {"selfCollision":false},
         {"collision":false},
         {"nodeMaterial":"|NM_METAL"},
         {"frictionCoef":0.5},
         {"group":""},
         {"nodeWeight":15.5},
         ["rdiffr", -0.1, 1.37, 0.27],
         ["rdiffl", 0.1, 1.37, 0.27],
         {"group":""},
    ],
    "beams": [
          ["id1:", "id2:"],
          //differential node
          {"beamType":"|NORMAL", "beamLongBound":1.0, "beamShortBound":1.0},
          {"beamSpring":1101000,"beamDamp":60},
          {"beamDeform":35000,"beamStrength":"FLT_MAX"},
          {"optional":true},
          {"deformGroup":"differential_R", "deformationTriggerRatio":0.001}
          ["rdiffr", "rx2"],
          ["rdiffr", "rx3r"],
          ["rdiffr", "rx3l"],
          ["rdiffr", "rx5r"],
          ["rdiffr", "rx5l"],
          ["rdiffr", "rx2r"],
          ["rdiffr", "rx2l"],
          ["rdiffr", "rx3rr"],
          ["rdiffr", "rx3ll"],
          ["rdiffr", "rx4r"],
          ["rdiffr", "rx4l"],
          ["rdiffr", "rx5r"],
          ["rdiffr", "rx5l"],
          ["rdiffr", "rx6"],

          ["rdiffl", "rx2"],
          ["rdiffl", "rx3r"],
          ["rdiffl", "rx3l"],
          ["rdiffl", "rx5r"],
          ["rdiffl", "rx5l"],
          ["rdiffl", "rx2r"],
          ["rdiffl", "rx2l"],
          ["rdiffl", "rx3rr"],
          ["rdiffl", "rx3ll"],
          ["rdiffl", "rx4r"],
          ["rdiffl", "rx4l"],
          ["rdiffl", "rx5r"],
          ["rdiffl", "rx5l"],
          ["rdiffl", "rx6"],

          ["rdiffl", "rdiffr"],
          {"deformGroup":""}
          {"optional":false},
    ],
},
"vivace_differential_R_welded": {
    "information":{
        "authors":"BeamNG",
        "name":"Welded Rear Differential",
        "value":700,
    },
    "slotType" : "vivace_differential_R",
    "slots": [
        ["type", "default", "description"],
        ["vivace_finaldrive_R","vivace_finaldrive_R_4187", "Rear Final Drive", {"coreSlot":true}],
        ["vivace_driveshaft","vivace_driveshaft", "Rear Driveshaft"],
        ["vivace_halfshaft_R","vivace_halfshaft_R", "Rear Half Shafts"],
    ],
    "controller": [
        ["fileName"],
        //["drivingDynamics/actuators/electronicDiffLock" {"name":"lockRear", "differentialName":"differential_R"}]
    ],
    "powertrain" : [
        ["type", "name", "inputName", "inputIndex"],
        ["differential", "differential_R", "driveshaft", 1, {"deformGroups":["differential_R"], "diffType":"locked", "lockTorque":10000, "friction":6, "uiName":"Rear Differential","defaultVirtualInertia":0.25}],
    ],
    "differential_R": {
        "friction": 3.13,
        "dynamicFriction": 0.00148,
        "torqueLossCoef": 0.03,
    },
    "flexbodies": [
         ["mesh", "[group]:", "nonFlexMaterials"],
         ["vivace_diff_R", ["vivace_subframe_R"]],
    ],
    "nodes": [
         ["id", "posX", "posY", "posZ"],
         //--diff weight--
         {"selfCollision":false},
         {"collision":false},
         {"nodeMaterial":"|NM_METAL"},
         {"frictionCoef":0.5},
         {"group":""},
         {"nodeWeight":15.5},
         ["rdiffr", -0.1, 1.37, 0.27],
         ["rdiffl", 0.1, 1.37, 0.27],
         {"group":""},
    ],
    "beams": [
          ["id1:", "id2:"],
          //differential node
          {"beamType":"|NORMAL", "beamLongBound":1.0, "beamShortBound":1.0},
          {"beamSpring":1101000,"beamDamp":60},
          {"beamDeform":35000,"beamStrength":"FLT_MAX"},
          {"optional":true},
          {"deformGroup":"differential_R", "deformationTriggerRatio":0.001}
          ["rdiffr", "rx2"],
          ["rdiffr", "rx3r"],
          ["rdiffr", "rx3l"],
          ["rdiffr", "rx5r"],
          ["rdiffr", "rx5l"],
          ["rdiffr", "rx2r"],
          ["rdiffr", "rx2l"],
          ["rdiffr", "rx3rr"],
          ["rdiffr", "rx3ll"],
          ["rdiffr", "rx4r"],
          ["rdiffr", "rx4l"],
          ["rdiffr", "rx5r"],
          ["rdiffr", "rx5l"],
          ["rdiffr", "rx6"],

          ["rdiffl", "rx2"],
          ["rdiffl", "rx3r"],
          ["rdiffl", "rx3l"],
          ["rdiffl", "rx5r"],
          ["rdiffl", "rx5l"],
          ["rdiffl", "rx2r"],
          ["rdiffl", "rx2l"],
          ["rdiffl", "rx3rr"],
          ["rdiffl", "rx3ll"],
          ["rdiffl", "rx4r"],
          ["rdiffl", "rx4l"],
          ["rdiffl", "rx5r"],
          ["rdiffl", "rx5l"],
          ["rdiffl", "rx6"],

          ["rdiffl", "rdiffr"],
          {"deformGroup":""}
          {"optional":false},
    ],
},
"vivace_differential_R_LSD": {
    "information":{
        "authors":"BeamNG",
        "name":"Limited Slip Rear Differential",
        "value":850,
    },
    "slotType" : "vivace_differential_R",
    "slots": [
        ["type", "default", "description"],
        ["vivace_finaldrive_R","vivace_finaldrive_R_4187", "Rear Final Drive", {"coreSlot":true}],
        ["vivace_driveshaft","vivace_driveshaft", "Rear Driveshaft"],
        ["vivace_halfshaft_R","vivace_halfshaft_R", "Rear Half Shafts"],
    ],
    "powertrain" : [
        ["type", "name", "inputName", "inputIndex"],
        ["differential", "differential_R", "driveshaft", 1, {"deformGroups":["differential_R"], "diffType":"lsd", "lsdPreload":10, "lsdLockCoef":0.25, "lsdRevLockCoef":0.05, "friction":6, "uiName":"Rear Differential","defaultVirtualInertia":0.25}],
    ],
    "differential_R": {
        "friction": 3.13,
        "dynamicFriction": 0.00148,
        "torqueLossCoef": 0.03,
    },
    "flexbodies": [
         ["mesh", "[group]:", "nonFlexMaterials"],
         ["vivace_diff_R", ["vivace_subframe_R"]],
    ],
    "nodes": [
         ["id", "posX", "posY", "posZ"],
         //--diff weight--
         {"selfCollision":false},
         {"collision":false},
         {"nodeMaterial":"|NM_METAL"},
         {"frictionCoef":0.5},
         {"group":""},
         {"nodeWeight":17},
         ["rdiffr", -0.1, 1.37, 0.27],
         ["rdiffl", 0.1, 1.37, 0.27],
         {"group":""},
    ],
    "beams": [
          ["id1:", "id2:"],
          //differential node
          {"beamType":"|NORMAL", "beamLongBound":1.0, "beamShortBound":1.0},
          {"beamSpring":1101000,"beamDamp":60},
          {"beamDeform":35000,"beamStrength":"FLT_MAX"},
          {"optional":true},
          {"deformGroup":"differential_R", "deformationTriggerRatio":0.001}
          ["rdiffr", "rx2"],
          ["rdiffr", "rx3r"],
          ["rdiffr", "rx3l"],
          ["rdiffr", "rx5r"],
          ["rdiffr", "rx5l"],
          ["rdiffr", "rx2r"],
          ["rdiffr", "rx2l"],
          ["rdiffr", "rx3rr"],
          ["rdiffr", "rx3ll"],
          ["rdiffr", "rx4r"],
          ["rdiffr", "rx4l"],
          ["rdiffr", "rx5r"],
          ["rdiffr", "rx5l"],
          ["rdiffr", "rx6"],

          ["rdiffl", "rx2"],
          ["rdiffl", "rx3r"],
          ["rdiffl", "rx3l"],
          ["rdiffl", "rx5r"],
          ["rdiffl", "rx5l"],
          ["rdiffl", "rx2r"],
          ["rdiffl", "rx2l"],
          ["rdiffl", "rx3rr"],
          ["rdiffl", "rx3ll"],
          ["rdiffl", "rx4r"],
          ["rdiffl", "rx4l"],
          ["rdiffl", "rx5r"],
          ["rdiffl", "rx5l"],
          ["rdiffl", "rx6"],

          ["rdiffl", "rdiffr"],
          {"deformGroup":""}
          {"optional":false},
    ],
},
"vivace_differential_R_race": {
    "information":{
        "authors":"BeamNG",
        "name":"Race Limited Slip Rear Differential",
        "value":1650,
    },
    "slotType" : "vivace_differential_R",
    "slots": [
        ["type", "default", "description"],
        ["vivace_finaldrive_R","vivace_finaldrive_R_race", "Rear Final Drive", {"coreSlot":true}],
        ["vivace_driveshaft","vivace_driveshaft", "Rear Driveshaft"],
        ["vivace_halfshaft_R","vivace_halfshaft_R", "Rear Half Shafts"],
    ],
    "variables": [
        ["name", "type", "unit", "category", "default", "min", "max", "title", "description"],
        ["$lsdpreload_R", "range", "N/m", "Differentials", 50, 0, 500, "Pre-load Torque", "Initial locking torque between left and right wheels", {"subCategory":"Rear"}]
        ["$lsdlockcoef_R", "range", "", "Differentials", 0.2, 0, 0.5, "Power Lock Rate", "Additional locking torque proportional to engine torque", {"minDis":0, "maxDis":100, "subCategory":"Rear"}],
        ["$lsdlockcoefrev_R", "range", "", "Differentials", 0.05, 0, 0.5, "Coast Lock Rate", "Additional locking torque proportional to engine braking", {"minDis":0, "maxDis":100, "subCategory":"Rear"}],
    ],
    "powertrain" : [
        ["type", "name", "inputName", "inputIndex"],
        ["differential", "differential_R", "driveshaft", 1, {"deformGroups":["differential_R"], "diffType":"lsd", "lsdPreload":"$lsdpreload_R", "lsdLockCoef":"$lsdlockcoef_R", "lsdRevLockCoef":"$lsdlockcoefrev_R", "friction":6, "uiName":"Rear Differential","defaultVirtualInertia":0.25}],
    ],
    "differential_R": {
        "friction": 3.13,
        "dynamicFriction": 0.00148,
        "torqueLossCoef": 0.03,
    },
    "flexbodies": [
         ["mesh", "[group]:", "nonFlexMaterials"],
         ["vivace_diff_R", ["vivace_subframe_R"]],
    ],
    "nodes": [
         ["id", "posX", "posY", "posZ"],
         //--diff weight--
         {"selfCollision":false},
         {"collision":false},
         {"nodeMaterial":"|NM_METAL"},
         {"frictionCoef":0.5},
         {"group":""},
         {"nodeWeight":7.5},
         ["rdiffr", -0.1, 1.37, 0.27],
         ["rdiffl", 0.1, 1.37, 0.27],
         {"group":""},
    ],
    "beams": [
          ["id1:", "id2:"],
          //differential node
          {"beamType":"|NORMAL", "beamLongBound":1.0, "beamShortBound":1.0},
          {"beamSpring":1101000,"beamDamp":60},
          {"beamDeform":35000,"beamStrength":"FLT_MAX"},
          {"optional":true},
          {"deformGroup":"differential_R", "deformationTriggerRatio":0.001}
          ["rdiffr", "rx2"],
          ["rdiffr", "rx3r"],
          ["rdiffr", "rx3l"],
          ["rdiffr", "rx5r"],
          ["rdiffr", "rx5l"],
          ["rdiffr", "rx2r"],
          ["rdiffr", "rx2l"],
          ["rdiffr", "rx3rr"],
          ["rdiffr", "rx3ll"],
          ["rdiffr", "rx4r"],
          ["rdiffr", "rx4l"],
          ["rdiffr", "rx5r"],
          ["rdiffr", "rx5l"],
          ["rdiffr", "rx6"],

          ["rdiffl", "rx2"],
          ["rdiffl", "rx3r"],
          ["rdiffl", "rx3l"],
          ["rdiffl", "rx5r"],
          ["rdiffl", "rx5l"],
          ["rdiffl", "rx2r"],
          ["rdiffl", "rx2l"],
          ["rdiffl", "rx3rr"],
          ["rdiffl", "rx3ll"],
          ["rdiffl", "rx4r"],
          ["rdiffl", "rx4l"],
          ["rdiffl", "rx5r"],
          ["rdiffl", "rx5l"],
          ["rdiffl", "rx6"],

          ["rdiffl", "rdiffr"],
          {"deformGroup":""}
          {"optional":false},
    ],
},
"vivace_finaldrive_R_486": {
    "information":{
        "authors":"BeamNG",
        "name":"4.86:1 Rear Final Drive",
        "value":150,
    },

    "slotType" : "vivace_finaldrive_R",

    "differential_R" : {
        "gearRatio":4.86,
    },
},
"vivace_finaldrive_R_455": {
    "information":{
        "authors":"BeamNG",
        "name":"4.55:1 Rear Final Drive",
        "value":150,
    },

    "slotType" : "vivace_finaldrive_R",

    "differential_R" : {
        "gearRatio":4.55,
    },
},
"vivace_finaldrive_R_437": {
    "information":{
        "authors":"BeamNG",
        "name":"4.37:1 Rear Final Drive",
        "value":150,
    },

    "slotType" : "vivace_finaldrive_R",

    "differential_R" : {
        "gearRatio":4.37,
    },
},
"vivace_finaldrive_R_4187": {
    "information":{
        "authors":"BeamNG",
        "name":"4.187:1 Rear Final Drive",
        "value":150,
    },

    "slotType" : "vivace_finaldrive_R",

    "differential_R" : {
        "gearRatio":4.187,
    },
},
"vivace_finaldrive_R_3389": {
    "information":{
        "authors":"BeamNG",
        "name":"3.389:1 Rear Final Drive",
        "value":150,
    },

    "slotType" : "vivace_finaldrive_R",

    "differential_R" : {
        "gearRatio":3.389,
    },
},
"vivace_finaldrive_R_race": {
    "information":{
        "authors":"BeamNG",
        "name":"Race Adjustable Rear Final Drive",
        "value":750,
    },

    "slotType" : "vivace_finaldrive_R",

    "variables": [
        ["name", "type", "unit", "category", "default", "min", "max", "title", "description"],
        ["$finaldrive_R", "range", ":1", "Differentials", 4.27, 2.0, 6.0, "Final Drive Gear Ratio", "Torque multiplication ratio", {"subCategory":"Rear", "stepDis":0.01}],
    ],

    "differential_R" : {
        "gearRatio":"$finaldrive_R",
    },
},
"vivace_driveshaft": {
    "information":{
        "authors":"BeamNG",
        "name":"Rear Driveshaft",
        "value":450,
    },
    "slotType" : "vivace_driveshaft",
    "powertrain" : [
        ["type", "name", "inputName", "inputIndex"],
        ["torsionReactor", "torsionReactorR", "transfercase", 1, {}],
        ["shaft", "driveshaft", "torsionReactorR", 1, {"deformGroup":"driveshaft", "breakTriggerBeam":"driveshaft", "uiName":"Rear Driveshaft", "electricsName":"driveshaft", "friction":0.33, "dynamicFriction":0.0037}],
    ],
    "torsionReactorR": {
        "torqueReactionNodes:":["e2l","e2r","e3r"],
    },
    "props": [
        ["func"      , "mesh"           , "idRef:", "idX:", "idY:", "baseRotation"        , "rotation"            , "translation"        , "min", "max", "offset", "multiplier",],
        ["driveshaft", "vivace_driveshaft", "rdiffr", "rdiffl", "tra1",    {"x":-89.4, "y":0, "z":0}, {"x":0, "y":1, "z":0} , {"x":0, "y":0, "z":0}, -360, 360, 0, 1,{"breakGroup":"driveshaft","deformGroup":"driveshaft","optional":true}],
    ],
    "beams": [
          ["id1:", "id2:"],
          //driveshaft
          {"beamPrecompression":1, "beamType":"|BOUNDED", "beamLongBound":0.02, "beamShortBound":0.02},
          {"beamSpring":0,"beamDamp":0,"beamDeform":1500,"beamStrength":3500},
          {"beamLimitSpring":10001000,"beamLimitDamp":250},
          ["rdiffr","tra1", {"name":"driveshaft","optional":true,"breakGroup":"driveshaft","deformGroup":"driveshaft"}],
          ["rdiffl","tra1", {"name":"driveshaft","optional":true,"breakGroup":"driveshaft","deformGroup":"driveshaft"}],
          {"beamPrecompression":1, "beamType":"|NORMAL", "beamLongBound":1.0, "beamShortBound":1.0},
    ],
},
"vivace_halfshaft_R": {
    "information":{
        "authors":"BeamNG",
        "name":"Rear Half Shafts",
        "value":200,
    },
    "slotType" : "vivace_halfshaft_R",
    "flexbodies": [
         ["mesh", "[group]:", "nonFlexMaterials"],
         ["vivace_halfshaft_R", ["vivace_halfshaft_R","vivace_hub_R"]],
    ],
    "beams": [
          ["id1:", "id2:"],
          //halfshafts
          {"beamPrecompression":1, "beamType":"|BOUNDED", "beamLongBound":0.05, "beamShortBound":0.05},
          {"beamSpring":0,"beamDamp":0,"beamDeform":3600,"beamStrength":9500},
          {"beamLimitSpring":7501000,"beamLimitDamp":250},
          ["rw1r","rdiffr", {"name":"halfshaft_RR", "breakGroup":"wheel_RR", "breakGroupType":1, "deformGroup":"wheelaxleRR", "deformationTriggerRatio":0.001, "optional":true}],
          ["rw1l","rdiffl", {"name":"halfshaft_RL", "breakGroup":"wheel_RL", "breakGroupType":1, "deformGroup":"wheelaxleRL", "deformationTriggerRatio":0.001, "optional":true}],
          {"beamPrecompression":1, "beamType":"|NORMAL", "beamLongBound":1.0, "beamShortBound":1.0},
    ],
    "powertrain" : [
        ["type", "name", "inputName", "inputIndex"],
        ["shaft", "wheelaxleRL", "differential_R", 1, {"deformGroups":["wheelaxleRL"], "breakTriggerBeam":"halfshaft_RL", "uiName":"Rear Left Halfshaft", "friction":1.39, "dynamicFriction":0.0034}],
        ["shaft", "wheelaxleRR", "differential_R", 2, {"deformGroups":["wheelaxleRR"], "breakTriggerBeam":"halfshaft_RR", "uiName":"Rear Right Halfshaft", "friction":1.39, "dynamicFriction":0.0034}],
    ],
},
}