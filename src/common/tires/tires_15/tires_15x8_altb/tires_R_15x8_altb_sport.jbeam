{
"tire_R_225_50_15_altb_sport": {
    "information":{
        "authors":"BeamNG",
        "name":"225/50R15 Sport Rear Tires",
        "value":150,
    },
    "slotType" : "tire_R_15x8_altb",
    "flexbodies": [
        ["mesh", "[group]:", "nonFlexMaterials"],
        ["bolide_80s_225_15", ["wheel_RR","tire_RR"], [], {"pos":{"x":-0.48, "y":0.0, "z":0.0}, "rot":{"x":0, "y":0, "z":180}, "scale":{"x":1, "y":0.995, "z":0.995}}],
        ["bolide_80s_225_15", ["wheel_RL","tire_RL"], [], {"pos":{"x": 0.48, "y":0.0, "z":0.0}, "rot":{"x":0, "y":0, "z":0},   "scale":{"x":1, "y":0.995, "z":0.995}}],
    ],
    "variables": [
        ["name", "type", "unit", "category", "default", "min", "max", "title", "description"],
        ["$tirepressure_R", "range", "psi", "Wheels", 25, 0, 50, "Tire Pressure", "Relative to atmospheric pressure", {"subCategory":"Rear"}],
    ],
    "pressureWheels": [
        ["name","hubGroup","group","node1:","node2:","nodeS","nodeArm:","wheelDir"],
        {"disableMeshBreaking":false,"disableHubMeshBreaking":false},
        {"hasTire":true},
        {"enableTireReinfBeams":false},
        {"enableTireLbeams":true},
        {"enableTireSideReinfBeams":false},
        {"enableTreadReinfBeams":true},
        {"enableTirePeripheryReinfBeams":true},

        //general settings
        {"radius":0.304},
        {"tireWidth":0.205},

        //tire options
        {"wheelSideBeamSpring":"$=$tirepressure_R*714","wheelSideBeamDamp":35},
        {"wheelSideBeamSpringExpansion":411000,"wheelSideBeamDampExpansion":35},
        {"wheelSideTransitionZone":0.1,"wheelSideBeamPrecompression":0.98},

        {"wheelReinfBeamSpring":45000,"wheelReinfBeamDamp":190},
        {"wheelReinfBeamDampCutoffHz":500,"wheelReinfBeamPrecompression":0.98},

        {"wheelTreadBeamSpring":80000,"wheelTreadBeamDamp":95},
        {"wheelTreadBeamDampCutoffHz":500,"wheelTreadBeamPrecompression":0.98},

        {"wheelTreadReinfBeamSpring":80000,"wheelTreadReinfBeamDamp":95},
        {"wheelTreadReinfBeamDampCutoffHz":500,"wheelTreadReinfBeamPrecompression":0.98},

        {"wheelPeripheryBeamSpring":71000,"wheelPeripheryBeamDamp":45},
        {"wheelPeripheryBeamDampCutoffHz":500,"wheelPeripheryBeamPrecompression":0.98},

        {"wheelPeripheryReinfBeamSpring":71000,"wheelPeripheryReinfBeamDamp":45},
        {"wheelPeripheryReinfBeamDampCutoffHz":500,"wheelPeripheryReinfBeamPrecompression":0.98},

        //general tire values
        {"nodeWeight":0.15},
        {"nodeMaterial":"|NM_RUBBER"},
        {"triangleCollision":false},
        {"pressurePSI":"$tirepressure_R"},
        {"dragCoef":5},

        //groundmodel friction multipliers
        {"frictionCoef":1.0},
        {"slidingFrictionCoef":1.0},
        {"stribeckVelMult":1.5},
        {"stribeckExponent":1.4},
        {"stribeckExponent":1},
        {"treadCoef":0.5},

        //advanced friction values
        {"noLoadCoef":1.36},
        {"loadSensitivitySlope":0.00019},
        {"fullLoadCoef":0.5},
        {"softnessCoef":0.7},


        //deform values
        {"wheelSideBeamDeform":14000,"wheelSideBeamStrength":18000},
        {"wheelTreadBeamDeform":11000,"wheelTreadBeamStrength":14000},
        {"wheelPeripheryBeamDeform":45000,"wheelPeripheryBeamStrength":45000},
    ],
},
"tire_R_235_55_15_altb_sport": {
    "information":{
        "authors":"BeamNG",
        "name":"235/55R15 Sport Rear Tires",
        "value":180,
    },
    "slotType" : "tire_R_15x8_altb",
    "flexbodies": [
        ["mesh", "[group]:", "nonFlexMaterials"],
        ["tire_01a_15x8_25", ["wheel_RR","tire_RR"], [], {"pos":{"x":-0.48, "y":0.0, "z":0.0}, "rot":{"x":0, "y":0, "z":180}, "scale":{"x":0.995, "y":1, "z":1}}],
        ["tire_01a_15x8_25", ["wheel_RL","tire_RL"], [], {"pos":{"x": 0.48, "y":0.0, "z":0.0}, "rot":{"x":0, "y":0, "z":0},   "scale":{"x":0.995, "y":1, "z":1}}],
    ],
    "variables": [
        ["name", "type", "unit", "category", "default", "min", "max", "title", "description"],
        ["$tirepressure_R", "range", "psi", "Wheels", 28, 0, 50, "Tire Pressure", "Relative to atmospheric pressure", {"subCategory":"Rear"}],
    ],
    "pressureWheels": [
        ["name","hubGroup","group","node1:","node2:","nodeS","nodeArm:","wheelDir"],
        {"disableMeshBreaking":false,"disableHubMeshBreaking":false},
        {"hasTire":true},
        {"enableTireReinfBeams":false},
        {"enableTireLbeams":true},
        {"enableTireSideReinfBeams":false},
        {"enableTreadReinfBeams":true},
        {"enableTirePeripheryReinfBeams":true},

        //general settings
        {"radius":0.316},
        {"tireWidth":0.225},

        //tire options
        {"wheelSideBeamSpring":"$=$tirepressure_R*810","wheelSideBeamDamp":30},
        {"wheelSideBeamSpringExpansion":401000,"wheelSideBeamDampExpansion":40},
        {"wheelSideTransitionZone":0.08,"wheelSideBeamPrecompression":0.99},

        {"wheelReinfBeamSpring":25000,"wheelReinfBeamDamp":190},
        {"wheelReinfBeamDampCutoffHz":500,"wheelReinfBeamPrecompression":0.985},

        {"wheelTreadBeamSpring":55000,"wheelTreadBeamDamp":90},
        {"wheelTreadBeamDampCutoffHz":500,"wheelTreadBeamPrecompression":0.985},

        {"wheelTreadReinfBeamSpring":105000,"wheelTreadReinfBeamDamp":60},
        {"wheelTreadReinfBeamDampCutoffHz":500,"wheelTreadReinfBeamPrecompression":0.985},

        {"wheelPeripheryBeamSpring":41000,"wheelPeripheryBeamDamp":45},
        {"wheelPeripheryBeamDampCutoffHz":500,"wheelPeripheryBeamPrecompression":0.985},

        {"wheelPeripheryReinfBeamSpring":101000,"wheelPeripheryReinfBeamDamp":35},
        {"wheelPeripheryReinfBeamDampCutoffHz":500,"wheelPeripheryReinfBeamPrecompression":0.985},

        //general tire values
        {"nodeWeight":0.16},
        {"nodeMaterial":"|NM_RUBBER"},
        {"triangleCollision":false},
        {"pressurePSI":"$tirepressure_R"},
        {"dragCoef":5},

        //groundmodel friction multipliers
        {"frictionCoef":1.0},
        {"slidingFrictionCoef":1.0},
        {"treadCoef":0.5},

        //advanced friction values
        {"noLoadCoef":1.44},
        {"loadSensitivitySlope":0.000156},
        {"fullLoadCoef":0.5},
        {"softnessCoef":0.7},

        //deform values
        {"wheelSideBeamDeform":15000,"wheelSideBeamStrength":20000},
        {"wheelTreadBeamDeform":11000,"wheelTreadBeamStrength":14000},
        {"wheelPeripheryBeamDeform":45000,"wheelPeripheryBeamStrength":45000},
    ],
},
"tire_R_235_60_15_altb_sport": {
    "information":{
        "authors":"BeamNG",
        "name":"235/60R15 Grip-All Radial T/A Rear Tires",
        "value":170,
    },
    "slotType" : "tire_R_15x8_altb",
    "flexbodies": [
        ["mesh", "[group]:", "nonFlexMaterials"],
        ["tire_01c_15x8_26", ["wheel_RR","tire_RR"], [], {"pos":{"x":-0.48, "y":0.0, "z":0.0}, "rot":{"x":0, "y":0, "z":180}, "scale":{"x":1.0, "y":1.01, "z":1.01}}],
        ["tire_01c_15x8_26", ["wheel_RL","tire_RL"], [], {"pos":{"x": 0.48, "y":0.0, "z":0.0}, "rot":{"x":0, "y":0, "z":0},   "scale":{"x":1.0, "y":1.01, "z":1.01}}],
    ],
    "variables": [
        ["name", "type", "unit", "category", "default", "min", "max", "title", "description"],
        ["$tirepressure_R", "range", "psi", "Wheels", 28, 0, 50, "Tire Pressure", "Relative to atmospheric pressure", {"subCategory":"Rear"}],
    ],
    "pressureWheels": [
        ["name","hubGroup","group","node1:","node2:","nodeS","nodeArm:","wheelDir"],
        {"disableMeshBreaking":false,"disableHubMeshBreaking":false},
        {"hasTire":true},
        {"enableTireReinfBeams":false},
        {"enableTireLbeams":true},
        {"enableTireSideReinfBeams":false},
        {"enableTreadReinfBeams":true},
        {"enableTirePeripheryReinfBeams":true},

        //general settings
        {"radius":0.33},
        {"tireWidth":0.205},

        //tire options
        {"wheelSideBeamSpring":"$=$tirepressure_R*785","wheelSideBeamDamp":30},
        {"wheelSideBeamSpringExpansion":401000,"wheelSideBeamDampExpansion":40},
        {"wheelSideTransitionZone":0.08,"wheelSideBeamPrecompression":0.99},

        {"wheelReinfBeamSpring":21000,"wheelReinfBeamDamp":190},
        {"wheelReinfBeamDampCutoffHz":500,"wheelReinfBeamPrecompression":0.985},

        {"wheelTreadBeamSpring":51000,"wheelTreadBeamDamp":90},
        {"wheelTreadBeamDampCutoffHz":500,"wheelTreadBeamPrecompression":0.985},

        {"wheelTreadReinfBeamSpring":101000,"wheelTreadReinfBeamDamp":60},
        {"wheelTreadReinfBeamDampCutoffHz":500,"wheelTreadReinfBeamPrecompression":0.985},

        {"wheelPeripheryBeamSpring":41000,"wheelPeripheryBeamDamp":45},
        {"wheelPeripheryBeamDampCutoffHz":500,"wheelPeripheryBeamPrecompression":0.985},

        {"wheelPeripheryReinfBeamSpring":101000,"wheelPeripheryReinfBeamDamp":35},
        {"wheelPeripheryReinfBeamDampCutoffHz":500,"wheelPeripheryReinfBeamPrecompression":0.985},

        //general tire values
        {"nodeWeight":0.16},
        {"nodeMaterial":"|NM_RUBBER"},
        {"triangleCollision":false},
        {"pressurePSI":"$tirepressure_R"},
        {"dragCoef":5},

        //groundmodel friction multipliers
        {"frictionCoef":1.0},
        {"slidingFrictionCoef":1.0},
        {"treadCoef":0.5},

        //advanced friction values
        {"noLoadCoef":1.42},
        {"loadSensitivitySlope":0.00016},
        {"fullLoadCoef":0.5},
        {"softnessCoef":0.7},

        //deform values
        {"wheelSideBeamDeform":22000,"wheelSideBeamStrength":28000},
        {"wheelTreadBeamDeform":14000,"wheelTreadBeamStrength":18000},
        {"wheelPeripheryBeamDeform":50000,"wheelPeripheryBeamStrength":50000},
    ],
},
}