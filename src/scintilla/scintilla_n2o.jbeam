{
"n2o_system": {
    "information":{
        "authors":"BeamNG",
        "name":"Nitrous Oxide Injection",
        "value":350,
    },
    "slotType" : "n2o_system",
    "slots": [
        ["type", "default", "description"],
        ["n2o_bottle","n2o_bottle_10lb", "Nitrous Oxide Bottle"],
        ["n2o_shot","n2o_shot_50", "Nitrous Oxide Shot Size"],
    ],
    "variables": [
        ["name", "type", "unit", "category", "default", "min", "max", "title", "description"],
        ["$n2o_rpm", "range", "RPM", "Nitrous Oxide", 2000, 1200, 6000, "Minimum RPM", "Minimum RPM where nitrous oxide can spray", {"stepDis":50}],
        ["$n2o_gear", "range", "Gear", "Nitrous Oxide", 2, 1, 6, "Minimum Gear", "Minimum gear where nitrous oxide can spray", {"minDis":1, "maxDis":6, "stepDis":1}],
    ],
    "powertrain": [
        ["type", "name", "inputName", "inputIndex"],
    ],
    "mainEngine": {
        "nitrousOxideInjection": "n2o",
    },
    "n2o": {
        "cutInRPM": "$n2o_rpm",
        "minimumGear": "$n2o_gear",
    },
    "controller": [
        ["fileName"],
        ["nitrousOxideInjection", {}],
    ],
    "vehicleController": {
    },
    "soundConfig": {
    },
},
"n2o_bottle_10lb": {
    "information":{
        "authors":"BeamNG",
        "name":"10lb Nitrous Oxide Bottle",
        "value":200,
    },
    "slotType" : "n2o_bottle",
    "flexbodies": [
        ["mesh", "[group]:", "nonFlexMaterials"],
    ],
    "props": [
        ["func",     "mesh",        "idRef:", "idX:", "idY:", "baseRotation",            "rotation",            "translation",         "min", "max", "offset", "multiplier"],
        {"optional":true},
        ["null", "n2o_bottle_10lb", "tb4r","tb4l","tb2r",        {"x":-90, "y":90, "z":-25}, {"x":0, "y":0, "z":0}, {"x":0, "y":0, "z":0}, 0, 0, 0, 1, {"baseTranslation":{"x":0.2,"y":0.2, "z":-0.1}}],
        {"optional":false},
    ],
    "powertrain": [
        ["type", "name", "inputName", "inputIndex"],
    ],
    "energyStorage": [
        ["type", "name"],
        ["n2oTank", "mainBottle"],
    ],
    "mainBottle": {
        "capacity": 4.54,
        "startingCapacity": 4.54,
    },
    "mainEngine": {
        "energyStorage": ["fueltank_R", "fueltank_L", "mainBottle"],
    },
    "n2o": {
        "purgeValves:": ["rx7l","rx6l"],
    },
    "nodes": [
         ["id", "posX", "posY", "posZ"],
         //--N2O Bottle--
         {"nodeMaterial":"|NM_METAL"},
         {"frictionCoef":0.5},
         {"collision":true},
         {"selfCollision":false},
         {"group":"n2o_bottle"},
         {"nodeWeight":11},
         {"engineGroup":"n2o_bottle"},
         ["n2o", -0.0,1.502,0.78],
         {"engineGroup":""},
         {"group":""},
         {"collision":true},
    ],
    "beams": [
          ["id1:", "id2:"],
          {"beamType":"|NORMAL", "beamLongBound":1.0, "beamShortBound":1.0},
          {"beamSpring":1501000,"beamDamp":150},
          {"beamDeform":12750,"beamStrength":"FLT_MAX"},
          {"deformLimitExpansion":""},
          ["n2o", "rx7r"],
          ["n2o", "rx7l"],
          ["n2o", "rx6r"],
          ["n2o", "rx6l"],
          ["n2o", "rx4l"],
          ["n2o", "rx4r"],
          {"beamType":"|NORMAL", "beamLongBound":1.0, "beamShortBound":1.0},
    ],
},
"n2o_bottle_20lb": {
    "information":{
        "authors":"BeamNG",
        "name":"Twin 10lb Nitrous Oxide Bottles",
        "value":400,
    },
    "slotType" : "n2o_bottle",
    "flexbodies": [
        ["mesh", "[group]:", "nonFlexMaterials"],
    ],
    "props": [
        ["func",     "mesh",        "idRef:", "idX:", "idY:", "baseRotation",            "rotation",            "translation",         "min", "max", "offset", "multiplier"],
        {"optional":true},
        ["null", "n2o_bottle_10lb", "tb4r","tb4l","tb2r",        {"x":-90, "y":90, "z":-25}, {"x":0, "y":0, "z":0}, {"x":0, "y":0, "z":0}, 0, 0, 0, 1, {"baseTranslation":{"x":0.08,"y":0.2, "z":-0.1}}],
        ["null", "n2o_bottle_10lb", "tb4r","tb4l","tb2r",        {"x":-90, "y":90, "z":-25}, {"x":0, "y":0, "z":0}, {"x":0, "y":0, "z":0}, 0, 0, 0, 1, {"baseTranslation":{"x":0.3,"y":0.2, "z":-0.1}}],
        {"optional":false},
    ],
    "powertrain": [
        ["type", "name", "inputName", "inputIndex"],
    ],
    "energyStorage": [
        ["type", "name"],
        ["n2oTank", "mainBottle"],
    ],
    "mainBottle": {
        "capacity": 9.08,
        "startingCapacity": 9.08,
    },
    "mainEngine": {
        "energyStorage": ["fueltank_R", "fueltank_L", "mainBottle"],
    },
    "n2o": {
        "purgeValves:": ["rx7l","rx6l"],
    },
    "nodes": [
         ["id", "posX", "posY", "posZ"],
         //--N2O Bottle--
         {"nodeMaterial":"|NM_METAL"},
         {"frictionCoef":0.5},
         {"collision":true},
         {"selfCollision":false},
         {"group":"n2o_bottle"},
         {"nodeWeight":22},
         {"engineGroup":"n2o_bottle"},
         ["n2o", -0.0,1.502,0.78],
         {"engineGroup":""},
         {"group":""},
         {"collision":true},
    ],
    "beams": [
          ["id1:", "id2:"],
          {"beamType":"|NORMAL", "beamLongBound":1.0, "beamShortBound":1.0},
          {"beamSpring":1501000,"beamDamp":150},
          {"beamDeform":12750,"beamStrength":"FLT_MAX"},
          {"deformLimitExpansion":""},
          ["n2o", "rx7r"],
          ["n2o", "rx7l"],
          ["n2o", "rx6r"],
          ["n2o", "rx6l"],
          ["n2o", "rx4l"],
          ["n2o", "rx4r"],
          {"beamType":"|NORMAL", "beamLongBound":1.0, "beamShortBound":1.0},
    ],
},
"n2o_shot_50": {
    "information":{
        "authors":"BeamNG",
        "name":"50kW Shot Size",
        "value":100,
    },
    "slotType" : "n2o_shot",
    "n2o": {
        "addedPower": 50,
    },
},
"n2o_shot_100": {
    "information":{
        "authors":"BeamNG",
        "name":"100kW Shot Size",
        "value":100,
    },
    "slotType" : "n2o_shot",
    "n2o": {
        "addedPower": 100,
    },
},
"n2o_shot_150": {
    "information":{
        "authors":"BeamNG",
        "name":"150kW Shot Size",
        "value":100,
    },
    "slotType" : "n2o_shot",
    "n2o": {
        "addedPower": 150,
    },
},
"n2o_shot_250": {
    "information":{
        "authors":"BeamNG",
        "name":"250kW Shot Size",
        "value":100,
    },
    "slotType" : "n2o_shot",
    "n2o": {
        "addedPower": 200,
    },
},
"n2o_shot_variable": {
    "information":{
        "authors":"BeamNG",
        "name":"Adjustable Shot Size",
        "value":200,
    },
    "slotType" : "n2o_shot",
    "variables": [
        ["name", "type", "unit", "category", "default", "min", "max", "title", "description"],
        ["$n2o_power", "range", "kW", "Nitrous Oxide", 75, 25, 500, "Added Power", "Power increase the N2O injection grants", {"stepDis":5}],
    ],
    "n2o": {
        "addedPower": "$n2o_power",
    },
},
}