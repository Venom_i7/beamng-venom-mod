{
"pickup_differential_F": {
    "information":{
        "authors":"BeamNG",
        "name":"Open Front Differential",
        "value":450,
    },
    "slotType" : "pickup_differential_F",
    "slots": [
        ["type", "default", "description"],
        ["pickup_driveshaft_F","pickup_driveshaft_F","Front Driveshaft"],
        ["pickup_halfshafts_F","pickup_halfshafts_F","Front Halfshafts"],
        ["pickup_finaldrive_F","pickup_finaldrive_F_355", "Front Final Drive", {"coreSlot":true}],
    ],
    "flexbodies": [
         ["mesh", "[group]:", "nonFlexMaterials"],
         ["pickup_diff_F", ["pickup_diff_F","pickup_frame","pickup_lowermounts"]],
    ],
    "powertrain" : [
        ["type", "name", "inputName", "inputIndex"],
        //front diff
        ["torsionReactor", "torsionReactorF", "driveshaft_F", 1],
        ["differential", "differential_F", "torsionReactorF", 1, {"diffType":"open", "gearRatio":1, "uiName":"Front Differential","defaultVirtualInertia":0.5}],
    ],
    "differential_F": {
        "friction": 3.72
        "dynamicFriction": 0.00163,
        "torqueLossCoef": 0.03,
    },
    "torsionReactorF": {
        "torqueReactionNodes:":["fx2l","fx2r","fx1r"],
    },
    "nodes": [
         ["id", "posX", "posY", "posZ"],
         //--diff weight--
         {"selfCollision":false},
         {"collision":false},
         {"nodeMaterial":"|NM_METAL"},
         {"frictionCoef":0.5},
         {"group":"pickup_diff_F"},
         {"nodeWeight":23.5},
         ["fdiffl", 0.3, -1.463, 0.42],
         ["fdiffr", -0.3, -1.463, 0.42],
         {"group":""},
    ],
    "beams": [
          ["id1:", "id2:"],
          {"beamType":"|NORMAL", "beamLongBound":1.0, "beamShortBound":1.0},

          {"deformLimitExpansion":1.2},
          {"beamSpring":2820940,"beamDamp":235},
          {"beamDeform":17500,"beamStrength":"FLT_MAX"},
          ["fdiffr","fdiffl"],
          {"deformLimitExpansion":""},
          {"beamSpring":1101000,"beamDamp":350},
          {"beamDeform":12000,"beamStrength":"FLT_MAX"},
          ["fdiffl", "fx2l"],
          ["fdiffl", "fx1l"],
          ["fdiffl", "f3l"],
          ["fdiffl", "f2l"],
          ["fdiffr", "fx2r"],
          ["fdiffr", "fx1r"],
          ["fdiffr", "f3r"],
          ["fdiffr", "f2r"],
          ["fdiffr", "f3l"],
          ["fdiffr", "f2l"],
          ["fdiffl", "f3r"],
          ["fdiffl", "f2r"],

          ["fdiffl", "f13l"],
          ["fdiffl", "f12l"],
          ["fdiffr", "f13r"],
          ["fdiffr", "f12r"],
          ["fdiffr", "f13l"],
          ["fdiffr", "f12l"],
          ["fdiffl", "f13r"],
          ["fdiffl", "f12r"],
    ],
},
"pickup_differential_F_LSD": {
    "information":{
        "authors":"BeamNG",
        "name":"Limited Slip Front Differential",
        "value":750,
    },
    "slotType" : "pickup_differential_F",
    "slots": [
        ["type", "default", "description"],
        ["pickup_driveshaft_F","pickup_driveshaft_F","Front Driveshaft"],
        ["pickup_halfshafts_F","pickup_halfshafts_F","Front Halfshafts"],
        ["pickup_finaldrive_F","pickup_finaldrive_F_355", "Front Final Drive", {"coreSlot":true}],
    ],
    "flexbodies": [
         ["mesh", "[group]:", "nonFlexMaterials"],
         ["pickup_diff_F", ["pickup_diff_F","pickup_frame","pickup_lowermounts"]],
    ],
    "powertrain" : [
        ["type", "name", "inputName", "inputIndex"],
        //front diff
        ["torsionReactor", "torsionReactorF", "driveshaft_F", 1],
        ["differential", "differential_F", "torsionReactorF", 1, {"diffType":"lsd", "gearRatio":1, "lsdPreload":100, "lsdLockCoef":0.2, "lsdRevLockCoef":0.1, "uiName":"Front Differential","defaultVirtualInertia":0.5}],
    ],
    "differential_F": {
        "friction": 3.72
        "dynamicFriction": 0.00163,
        "torqueLossCoef": 0.03,
    },
    "torsionReactorF": {
        "torqueReactionNodes:":["fx2l","fx2r","fx1r"],
    },
    "nodes": [
         ["id", "posX", "posY", "posZ"],
         //--diff weight--
         {"selfCollision":false},
         {"collision":false},
         {"nodeMaterial":"|NM_METAL"},
         {"frictionCoef":0.5},
         {"group":"pickup_diff_F"},
         {"nodeWeight":23.5},
         ["fdiffl", 0.3, -1.463, 0.42],
         ["fdiffr", -0.3, -1.463, 0.42],
         {"group":""},
    ],
    "beams": [
          ["id1:", "id2:"],
          {"beamType":"|NORMAL", "beamLongBound":1.0, "beamShortBound":1.0},

          {"deformLimitExpansion":1.2},
          {"beamSpring":2820940,"beamDamp":235},
          {"beamDeform":17500,"beamStrength":"FLT_MAX"},
          ["fdiffr","fdiffl"],
          {"deformLimitExpansion":""},
          {"beamSpring":1101000,"beamDamp":350},
          {"beamDeform":12000,"beamStrength":"FLT_MAX"},
          ["fdiffl", "fx2l"],
          ["fdiffl", "fx1l"],
          ["fdiffl", "f3l"],
          ["fdiffl", "f2l"],
          ["fdiffr", "fx2r"],
          ["fdiffr", "fx1r"],
          ["fdiffr", "f3r"],
          ["fdiffr", "f2r"],
          ["fdiffr", "f3l"],
          ["fdiffr", "f2l"],
          ["fdiffl", "f3r"],
          ["fdiffl", "f2r"],

          ["fdiffl", "f13l"],
          ["fdiffl", "f12l"],
          ["fdiffr", "f13r"],
          ["fdiffr", "f12r"],
          ["fdiffr", "f13l"],
          ["fdiffr", "f12l"],
          ["fdiffl", "f13r"],
          ["fdiffl", "f12r"],
    ],
},
"pickup_differential_F_welded": {
    "information":{
        "authors":"BeamNG",
        "name":"Welded Front Differential",
        "value":450,
    },
    "slotType" : "pickup_differential_F",
    "slots": [
        ["type", "default", "description"],
        ["pickup_driveshaft_F","pickup_driveshaft_F","Front Driveshaft"],
        ["pickup_halfshafts_F","pickup_halfshafts_F","Front Halfshafts"],
        ["pickup_finaldrive_F","pickup_finaldrive_F_355", "Front Final Drive", {"coreSlot":true}],
    ],
    "flexbodies": [
         ["mesh", "[group]:", "nonFlexMaterials"],
         ["pickup_diff_F", ["pickup_diff_F","pickup_frame","pickup_lowermounts"]],
    ],
    "powertrain" : [
        ["type", "name", "inputName", "inputIndex"],
        //front diff
        ["torsionReactor", "torsionReactorF", "driveshaft_F", 1],
        ["differential", "differential_F", "torsionReactorF", 1, {"diffType":"locked", "gearRatio":1, "lockTorque":10000, "uiName":"Front Differential","defaultVirtualInertia":0.5}],
    ],
    "differential_F": {
        "friction": 3.72
        "dynamicFriction": 0.00163,
        "torqueLossCoef": 0.03,
    },
    "torsionReactorF": {
        "torqueReactionNodes:":["fx2l","fx2r","fx1r"],
    },
    "nodes": [
         ["id", "posX", "posY", "posZ"],
         //--diff weight--
         {"selfCollision":false},
         {"collision":false},
         {"nodeMaterial":"|NM_METAL"},
         {"frictionCoef":0.5},
         {"group":"pickup_diff_F"},
         {"nodeWeight":23.5},
         ["fdiffl", 0.3, -1.463, 0.42],
         ["fdiffr", -0.3, -1.463, 0.42],
         {"group":""},
    ],
    "beams": [
          ["id1:", "id2:"],
          {"beamType":"|NORMAL", "beamLongBound":1.0, "beamShortBound":1.0},

          {"deformLimitExpansion":1.2},
          {"beamSpring":2820940,"beamDamp":235},
          {"beamDeform":17500,"beamStrength":"FLT_MAX"},
          ["fdiffr","fdiffl"],
          {"deformLimitExpansion":""},
          {"beamSpring":1101000,"beamDamp":350},
          {"beamDeform":12000,"beamStrength":"FLT_MAX"},
          ["fdiffl", "fx2l"],
          ["fdiffl", "fx1l"],
          ["fdiffl", "f3l"],
          ["fdiffl", "f2l"],
          ["fdiffr", "fx2r"],
          ["fdiffr", "fx1r"],
          ["fdiffr", "f3r"],
          ["fdiffr", "f2r"],
          ["fdiffr", "f3l"],
          ["fdiffr", "f2l"],
          ["fdiffl", "f3r"],
          ["fdiffl", "f2r"],

          ["fdiffl", "f13l"],
          ["fdiffl", "f12l"],
          ["fdiffr", "f13r"],
          ["fdiffr", "f12r"],
          ["fdiffr", "f13l"],
          ["fdiffr", "f12l"],
          ["fdiffl", "f13r"],
          ["fdiffl", "f12r"],
    ],
},
"pickup_differential_F_locking": {
    "information":{
        "authors":"BeamNG",
        "name":"Locking Front Differential",
        "value":950,
    },
    "slotType" : "pickup_differential_F",
    "slots": [
        ["type", "default", "description"],
        ["pickup_driveshaft_F","pickup_driveshaft_F","Front Driveshaft"],
        ["pickup_halfshafts_F","pickup_halfshafts_F","Front Halfshafts"],
        ["pickup_finaldrive_F","pickup_finaldrive_F_355", "Front Final Drive", {"coreSlot":true}],
    ],
    "flexbodies": [
         ["mesh", "[group]:", "nonFlexMaterials"],
         ["pickup_diff_F", ["pickup_diff_F","pickup_frame","pickup_lowermounts"]],
    ],
    "powertrain" : [
        ["type", "name", "inputName", "inputIndex"],
        //front diff
        ["torsionReactor", "torsionReactorF", "driveshaft_F", 1, {"gearRatio":4.10}],
        ["differential", "differential_F", "torsionReactorF", 1, {"diffType":["open","locked"], "gearRatio":1, "lockTorque":20000, "friction":5, "uiName":"Front Differential","defaultVirtualInertia":0.5}],
    ],
    "differential_F": {
        "friction": 3.72
        "dynamicFriction": 0.00163,
        "torqueLossCoef": 0.03,
    },
    "torsionReactorF": {
        "torqueReactionNodes:":["fx2l","fx2r","fx1r"],
    },
    "nodes": [
         ["id", "posX", "posY", "posZ"],
         //--diff weight--
         {"selfCollision":false},
         {"collision":false},
         {"nodeMaterial":"|NM_METAL"},
         {"frictionCoef":0.5},
         {"group":"pickup_diff_F"},
         {"nodeWeight":23.5},
         ["fdiffl", 0.3, -1.463, 0.42],
         ["fdiffr", -0.3, -1.463, 0.42],
         {"group":""},
    ],
    "beams": [
          ["id1:", "id2:"],
          {"beamType":"|NORMAL", "beamLongBound":1.0, "beamShortBound":1.0},

          {"deformLimitExpansion":1.2},
          {"beamSpring":2820940,"beamDamp":235},
          {"beamDeform":17500,"beamStrength":"FLT_MAX"},
          ["fdiffr","fdiffl"],
          {"deformLimitExpansion":""},
          {"beamSpring":1101000,"beamDamp":350},
          {"beamDeform":12000,"beamStrength":"FLT_MAX"},
          ["fdiffl", "fx2l"],
          ["fdiffl", "fx1l"],
          ["fdiffl", "f3l"],
          ["fdiffl", "f2l"],
          ["fdiffr", "fx2r"],
          ["fdiffr", "fx1r"],
          ["fdiffr", "f3r"],
          ["fdiffr", "f2r"],
          ["fdiffr", "f3l"],
          ["fdiffr", "f2l"],
          ["fdiffl", "f3r"],
          ["fdiffl", "f2r"],

          ["fdiffl", "f13l"],
          ["fdiffl", "f12l"],
          ["fdiffr", "f13r"],
          ["fdiffr", "f12r"],
          ["fdiffr", "f13l"],
          ["fdiffr", "f12l"],
          ["fdiffl", "f13r"],
          ["fdiffl", "f12r"],
    ],
},
"pickup_finaldrive_F_355": {
    "information":{
        "authors":"BeamNG",
        "name":"3.55:1 Front Final Drive",
        "value":150,
    },

    "slotType" : "pickup_finaldrive_F",

    "torsionReactorF" : {
        "gearRatio":3.55,
    },
},
"pickup_finaldrive_F_410": {
    "information":{
        "authors":"BeamNG",
        "name":"4.10:1 Front Final Drive",
        "value":150,
    },

    "slotType" : "pickup_finaldrive_F",

    "torsionReactorF" : {
        "gearRatio":4.10,
    },
},
"pickup_finaldrive_F_444": {
    "information":{
        "authors":"BeamNG",
        "name":"4.44:1 Front Final Drive",
        "value":150,
    },

    "slotType" : "pickup_finaldrive_F",

    "torsionReactorF" : {
        "gearRatio":4.44,
    },
},
"pickup_finaldrive_F_race": {
    "information":{
        "authors":"BeamNG",
        "name":"Race Adjustable Front Final Drive",
        "value":750,
    },

    "slotType" : "pickup_finaldrive_F",

    "variables": [
        ["name", "type", "unit", "category", "default", "min", "max", "title", "description"],
        ["$finaldrive_F", "range", ":1", "Differentials", 4.10, 2.0, 6.0, "Final Drive Gear Ratio", "Torque multiplication ratio", {"subCategory":"Front", "stepDis":0.01}],
    ],

    "torsionReactorF" : {
        "gearRatio":"$finaldrive_F",
    },
},
"pickup_driveshaft_F": {
    "information":{
        "authors":"BeamNG",
        "name":"IFS Driveshaft",
        "value":300,
    },
    "slotType" : "pickup_driveshaft_F",
    "flexbodies": [
         ["mesh", "[group]:", "nonFlexMaterials"],
         ["pickup_frontshaft", ["pickup_diff_F", "pickup_transmission"]],
    ],
    "powertrain" : [
        ["type", "name", "inputName", "inputIndex"],
        //front driveshaft
        ["shaft", "driveshaft_F", "transfercase_F", 1, {"breakTriggerBeam":"driveshaft_F", "uiName":"Front Driveshaft", "electricsName":"driveshaft_F", "friction":0.467, "dynamicFriction": 0.00101}],
    ],
    "beams": [
          ["id1:", "id2:"],
          //driveshaft
          {"beamPrecompression":1.0, "beamType":"|BOUNDED", "beamLongBound":0.075, "beamShortBound":0.075},
          {"beamSpring":0,"beamDamp":0,"beamDeform":3535,"beamStrength":4599},
          {"beamLimitSpring":10001000,"beamLimitDamp":1000},
          ["fdiffl","tra3", {"name":"driveshaft_F","optional":true,"breakGroup":"driveshaft_F","deformGroup":"driveshaft_F"}],
          {"beamPrecompression":1, "beamType":"|NORMAL", "beamLongBound":1.0, "beamShortBound":1.0},
    ],
},
"pickup_halfshafts_F": {
    "information":{
        "authors":"BeamNG",
        "name":"Front Halfshafts",
        "value":150,
    },
    "slotType" : "pickup_halfshafts_F",
    "flexbodies": [
         ["mesh", "[group]:", "nonFlexMaterials"],
         ["pickup_halfshaft", ["pickup_diff_F","pickup_uppermounts","pickup_lowermounts",  "pickup_upperarm_F", "pickup_lowerarm_F", "pickup_hub1_F","pickup_hub5_F","wheelhub_FL","wheelhub_FR"]],
    ],
    "beams": [
          ["id1:", "id2:"],
          {"beamType":"|NORMAL", "beamLongBound":1.0, "beamShortBound":1.0},

          //halfshafts
          {"beamPrecompression":1, "beamType":"|BOUNDED", "beamLongBound":0.2, "beamShortBound":0.2},
          {"beamSpring":0,"beamDamp":0,"beamDeform":3000,"beamStrength":4500},
          {"beamLimitSpring":4001000,"beamLimitDamp":100},
          {"breakGroupType":1},
          {"optional":true},
          ["fw1r","fdiffr", {"name":"halfshaft_FR", "breakGroup":"wheel_FR", "deformGroup":"halfshaftFR", "deformationTriggerRatio":0.001}],
          ["fw1l","fdiffl", {"name":"halfshaft_FL", "breakGroup":"wheel_FL", "deformGroup":"halfshaftFL", "deformationTriggerRatio":0.001}],
          {"optional":false},
          {"breakGroupType":0},
          {"breakGroup":""},
          {"beamPrecompression":1, "beamType":"|NORMAL", "beamLongBound":1, "beamShortBound":1},
          {"deformLimitExpansion":1.2},
    ],
    "powertrain" : [
        ["type", "name", "inputName", "inputIndex"],
        ["shaft", "halfshaftFL", "differential_F", 1, {"deformGroups":["halfshaftFL"], "breakTriggerBeam":"halfshaft_FL", "uiName":"Front Left Halfshaft", "friction":1.97, "dynamicFriction": 0.00489}],
        ["shaft", "halfshaftFR", "differential_F", 2, {"deformGroups":["halfshaftFR"], "breakTriggerBeam":"halfshaft_FR", "uiName":"Front Right Halfshaft", "friction":1.97, "dynamicFriction": 0.00489}],
    ],
},
}