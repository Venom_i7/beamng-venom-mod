{
"scintilla_fender_R_offroad": {
    "information":{
        "authors":"BeamNG",
        "name":"Cut Front Right Fender",
        "value":1180,
    },

    "slotType" : "scintilla_fender_R_offroad",

    "slots":[
        ["type", "default", "description"],
        ["scintilla_headlight_R_offroad","scintilla_headlight_R_offroad", "Right Headlight"],
        ["scintilla_fender_cover_R_offroad","scintilla_fender_cover_R_offroad", "Right Fender Trim", {"coreSlot":true}],
    ],
    "flexbodies": [
         ["mesh", "[group]:", "nonFlexMaterials"],
         ["scintilla_fender_offroad_R", ["scintilla_fender_R"]],
    ],
    "nodes": [
         ["id", "posX", "posY", "posZ"],
         {"frictionCoef":0.5},
         {"nodeMaterial":"|NM_METAL"},
         //--FENDER--
         {"collision":true},
         {"selfCollision":true},
         {"nodeWeight":0.7},
         {"group":"scintilla_fender_R"},
         ["fe1r", -0.58, -2.22, 0.50],

         ["fe2r", -0.51, -1.9, 0.68],
         ["fe2rr", -0.878, -2.008, 0.55],

         ["fe3r", -0.480, -1.65, 0.79],
         ["fe3rr", -0.929, -1.72, 0.76],
         ["fe3rrr", -1.02, -1.78, 0.54, {"group":["scintilla_fender_R", "scintilla_innerfender_FR"]}],

         ["fe4r", -0.635, -1.378, 0.87],
         ["fe4rr", -0.84, -1.419, 0.89],
         ["fe4rrr", -1.03, -1.41, 0.84, {"group":["scintilla_fender_R", "scintilla_innerfender_FR"]}],

         ["fe5rr", -0.88, -0.89, 0.90],
         ["fe5rrr", -1.04, -1.09, 0.74, {"group":["scintilla_fender_R", "scintilla_innerfender_FR"]}],


         //rigidifier
         {"selfCollision":false, "collision":false},
         {"nodeWeight":1.5},
         ["fe6r",-0.81,-1.415,0.583, {"group":""}],
    ],
    "beams": [
          ["id1:", "id2:"],
          //--FENDER--
          {"beamType":"|NORMAL", "beamPrecompression":1, "beamLongBound":1.0, "beamShortBound":1.0},
          {"beamSpring":701000,"beamDamp":20},
          {"beamDeform":17500,"beamStrength":"FLT_MAX"},
          //main shape
          {"deformLimitExpansion":1.2},
          {"deformGroup":"headlightglass_R_break", "deformationTriggerRatio":0.01}
          ["fe1r", "fe2rr"],
          ["fe2rr", "fe3rr"],
          ["fe3rr", "fe2r"],
          ["fe2r", "fe1r"],
          ["fe3rr", "fe4rr"],
          ["fe3r", "fe2r"],
          ["fe4rr", "fe3r"],
          {"deformGroup":""}
          ["fe2rr", "fe3rrr"],
          ["fe3rrr", "fe4rrr"],
          ["fe4rrr", "fe4rr"],
          ["fe3rr", "fe4rrr"],
          ["fe3r", "fe4r"],
          ["fe4rr", "fe5rr"],
          ["fe5rr", "fe4r"],
          ["fe4rrr", "fe5rrr"],
          ["fe5rrr", "fe5rr"],

          //cross
          {"deformLimitExpansion":""},
          {"beamDeform":7600,"beamStrength":"FLT_MAX"},
          {"deformGroup":"headlightglass_R_break", "deformationTriggerRatio":0.01}
          ["fe2rr", "fe2r"],
          ["fe1r", "fe3rr"],
          ["fe3rr", "fe3r"],
          ["fe2r", "fe4rr"],
          {"deformGroup":""}
          ["fe2rr", "fe4rrr"],
          ["fe3rrr", "fe3rr"],
          ["fe3rrr", "fe4rr"],
          ["fe4rr", "fe4r"],
          ["fe3r", "fe5rr"],
          ["fe4rr", "fe5rrr"],
          ["fe4rrr", "fe5rr"],

          //rigids
          {"beamDeform":8280,"beamStrength":"FLT_MAX"},
          //vertical/corner
          ["fe3rrr", "fe3r"],
          ["fe4rrr", "fe4r"],
          ["fe5rrr", "fe4r"],

          //lengthwise
          {"beamDeform":5280,"beamStrength":"FLT_MAX"}
          ["fe2rr", "fe4rr"],
          ["fe3rr", "fe5rr"],
          ["fe3rrr", "fe5rrr"],
          ["fe1r", "fe3r", {"deformGroup":"headlightglass_R_break", "deformationTriggerRatio":0.01}],
          ["fe2r", "fe4r"],

          //rigidifier
          {"beamDeform":5280,"beamStrength":"FLT_MAX"}
          ["fe6r", "fe4rrr"],
          ["fe6r", "fe4rr"],
          ["fe6r", "fe4r"],
          ["fe6r", "fe5rrr"],
          ["fe6r", "fe5rr"],
          ["fe6r", "fe3rrr"],
          {"deformGroup":"headlightglass_R_break", "deformationTriggerRatio":0.01}
          ["fe6r", "fe3rr"],
          ["fe6r", "fe3r"],
          ["fe6r", "fe2rr"],
          ["fe6r", "fe2r"],
          ["fe6r", "fe1r"],
          {"deformGroup":""}

          //attach
          {"beamPrecompression":1, "beamType":"|NORMAL", "beamLongBound":1.0, "beamShortBound":1.0},
          {"beamDeform":5800,"beamStrength":136030},
          {"breakGroup":"fender_a_R"},
          ["fe5rr", "f5rr"],
          ["f5rr", "fe5rrr"],
          ["f6rrr", "fe5rrr", {"optional":true}],
          ["f6rrr", "fe5rr", {"optional":true}],
          ["fe5rr", "f6rr"],
          ["fe4r", "f6rr"],
          ["fe4r", "f6r"],
          ["f6r", "fe5rr"],
          ["fe4r", "fx4r"],
          ["fe5rr", "fx4r"],
          {"breakGroup":"fender_b_R"},
          {"beamDeform":5800,"beamStrength":56030},
          {"deformGroup":"headlightglass_R_break", "deformationTriggerRatio":0.01}
          ["fe1r", "bbf2r"],
          ["fe2r", "bbf2r"],
          ["fe2rr", "bbf2r"],
          ["fe1r", "bbf1r"],
          {"deformGroup":""}
          {"optional":true},
          {"breakGroup":"fender_c_R"},
          ["fe3rr", "f13rr"],
          ["fe3rrr", "f13rr"],
          ["fe4rrr", "f13rr"],
          ["f13rr", "fe3r"],
          {"breakGroup":"fender_d_R"},
          ["fe3rrr", "fbs4rrr"],
          ["fbs4rrr", "fe2rr"],
          ["fe3rr", "fbs4rrr"],
          {"optional":false},
          {"breakGroup":""},

          //fender support beams
          {"beamType":"|SUPPORT","beamLongBound":10},
          {"beamSpring":101000,"beamDamp":10},
          {"beamDeform":15080,"beamStrength":178000},
          ["fe5rr", "f5rr"],
          ["fe5rrr", "f5rr"],
          ["fe4r", "f6r"],
          ["fe4r", "fx4r"],
          ["fe4r", "fx3r"],
          ["fe3r", "fx3r"],
          ["fe2r", "bbf2r"],
          ["fe2r", "fx3r"],
          ["fe1r", "bbf2r"],
          ["fe1r", "bbf2"],
          ["fe3rr", "f13rr", {"optional":true}],
          ["fe5rr", "f6rr"],
          ["fe5rr", "f6r"],
          //to hood
          {"beamLongBound":35},
          {"optional":true},
          ["fe1r", "h4r"],
          ["fe2r", "h3r"],
          ["fe3r", "h2r"],
          ["fe4r", "h1"],
          {"beamLongBound":10},
          ["fe2rr", "h4r"],
          ["fe3rr", "h3r"],
          ["fx4r", "h2r"],
          ["fe5rr", "h1r"],
          //to front bumper support
          ["fe3rrr", "fbs2rrr"],
          ["fe2rr", "fbs2rrr"],
          ["fe3rr", "fbs4rrr"],
          //to door
          {"beamPrecompression":0.7},
          ["fe4rrr", "d4r"],
          ["fe4rr", "d7r"],
          {"beamLongBound":25},
          ["fe5rr", "d7r"],
          ["fe5rrr", "d4r"],
          {"optional":false},

          {"beamPrecompression":1, "beamType":"|NORMAL", "beamLongBound":1.0, "beamShortBound":1.0},
          {"deformLimitExpansion":1.2},
    ],
    "triangles": [
            ["id1:","id2:","id3:"],
            //right fender
            {"groundModel":"metal"},
            {"group":"scintilla_fender_R"},
            {"dragCoef":9},
            ["fe2rr",  "fe1r",   "fe3rr"],
            ["fe2r",   "fe3rr",  "fe1r"],
            ["fe3rr",  "fe2r",   "fe3r"],
            ["fe3rr",  "fe3r",   "fe4rr"],
            ["fe4rr",  "fe3r",   "fe4r"],
            ["fe5rr",  "fe4rr",  "fe4r"],
            ["fe4rrr", "fe4rr",  "fe5rr"],
            ["fe5rrr", "fe4rrr", "fe5rr"],
            ["fe4rrr", "fe3rr",  "fe4rr"],
            ["fe3rrr", "fe3rr",  "fe4rrr"],
            ["fe3rrr", "fe2rr",  "fe3rr"],
            {"group":""},
    ],
},
"scintilla_fender_L_offroad": {
    "information":{
        "authors":"BeamNG",
        "name":"Cut Front Left Fender",
        "value":1180,
    },

    "slotType" : "scintilla_fender_L_offroad",

    "slots":[
        ["type", "default", "description"],
        ["scintilla_headlight_L_offroad","scintilla_headlight_L_offroad", "Left Headlight"],
        ["scintilla_fender_cover_L_offroad","scintilla_fender_cover_L_offroad", "Left Fender Trim", {"coreSlot":true}],
    ],
    "flexbodies": [
         ["mesh", "[group]:", "nonFlexMaterials"],
         ["scintilla_fender_offroad_L", ["scintilla_fender_L"]],
    ],
    "nodes": [
         ["id", "posX", "posY", "posZ"],
         {"frictionCoef":0.5},
         {"nodeMaterial":"|NM_METAL"},
         //--FENDER--
         {"collision":true},
         {"selfCollision":true},
         {"nodeWeight":0.7},
         {"group":"scintilla_fender_L"},
         ["fe1l",  0.58, -2.22, 0.50],

         ["fe2l",  0.51, -1.9, 0.68],
         ["fe2ll",  0.878, -2.008, 0.55],

         ["fe3l",  0.480, -1.65, 0.79],
         ["fe3ll",  0.929, -1.72, 0.76],
         ["fe3lll",  1.02, -1.78, 0.54, {"group":["scintilla_fender_L", "scintilla_innerfender_FL"]}],

         ["fe4l",  0.635, -1.378, 0.87],
         ["fe4ll",  0.84, -1.419, 0.89],
         ["fe4lll",  1.03, -1.41, 0.84, {"group":["scintilla_fender_L", "scintilla_innerfender_FL"]}],

         ["fe5ll",  0.88, -0.89, 0.90],
         ["fe5lll",  1.04, -1.09, 0.74, {"group":["scintilla_fender_L", "scintilla_innerfender_FL"]}],


         //rigidifier
         {"selfCollision":false, "collision":false},
         {"nodeWeight":1.5},
         ["fe6l", 0.81,-1.415,0.583, {"group":""}],
    ],
    "beams": [
          ["id1:", "id2:"],
          //--FENDER--
          {"beamType":"|NORMAL", "beamPrecompression":1, "beamLongBound":1.0, "beamShortBound":1.0},
          {"beamSpring":701000,"beamDamp":20},
          {"beamDeform":17500,"beamStrength":"FLT_MAX"},
          //main shape
          {"deformLimitExpansion":1.2},
          {"deformGroup":"headlightglass_L_break", "deformationTriggerRatio":0.01}
          ["fe1l", "fe2ll"],
          ["fe2ll", "fe3ll"],
          ["fe3ll", "fe2l"],
          ["fe2l", "fe1l"],
          ["fe3ll", "fe4ll"],
          ["fe3l", "fe2l"],
          ["fe4ll", "fe3l"],
          {"deformGroup":""}
          ["fe2ll", "fe3lll"],
          ["fe3lll", "fe4lll"],
          ["fe4lll", "fe4ll"],
          ["fe3ll", "fe4lll"],
          ["fe3l", "fe4l"],
          ["fe4ll", "fe5ll"],
          ["fe5ll", "fe4l"],
          ["fe4lll", "fe5lll"],
          ["fe5lll", "fe5ll"],

          //cross
          {"deformLimitExpansion":""},
          {"beamDeform":7600,"beamStrength":"FLT_MAX"},
          {"deformGroup":"headlightglass_L_break", "deformationTriggerRatio":0.01}
          ["fe2ll", "fe2l"],
          ["fe1l", "fe3ll"],
          ["fe3ll", "fe3l"],
          ["fe2l", "fe4ll"],
          {"deformGroup":""}
          ["fe2ll", "fe4lll"],
          ["fe3lll", "fe3ll"],
          ["fe3lll", "fe4ll"],
          ["fe4ll", "fe4l"],
          ["fe3l", "fe5ll"],
          ["fe4ll", "fe5lll"],
          ["fe4lll", "fe5ll"],

          //rigids
          {"beamDeform":8280,"beamStrength":"FLT_MAX"},
          //vertical/corner
          ["fe3lll", "fe3l"],
          ["fe4lll", "fe4l"],
          ["fe5lll", "fe4l"],

          //lengthwise
          {"beamDeform":5280,"beamStrength":"FLT_MAX"}
          ["fe2ll", "fe4ll"],
          ["fe3ll", "fe5ll"],
          ["fe3lll", "fe5lll"],
          ["fe1l", "fe3l", {"deformGroup":"headlightglass_L_break", "deformationTriggerRatio":0.01}],
          ["fe2l", "fe4l"],

          //rigidifier
          {"beamDeform":5280,"beamStrength":"FLT_MAX"}
          ["fe6l", "fe4lll"],
          ["fe6l", "fe4ll"],
          ["fe6l", "fe4l"],
          ["fe6l", "fe5lll"],
          ["fe6l", "fe5ll"],
          ["fe6l", "fe3lll"],
          {"deformGroup":"headlightglass_L_break", "deformationTriggerRatio":0.01}
          ["fe6l", "fe3ll"],
          ["fe6l", "fe3l"],
          ["fe6l", "fe2ll"],
          ["fe6l", "fe2l"],
          ["fe6l", "fe1l"],
          {"deformGroup":""}

          //attach
          {"beamPrecompression":1, "beamType":"|NORMAL", "beamLongBound":1.0, "beamShortBound":1.0},
          {"beamDeform":5800,"beamStrength":136030},
          {"breakGroup":"fender_a_L"},
          ["fe5ll", "f5ll"],
          ["f5ll", "fe5lll"],
          ["f6lll", "fe5lll", {"optional":true}],
          ["f6lll", "fe5ll", {"optional":true}],
          ["fe5ll", "f6ll"],
          ["fe4l", "f6ll"],
          ["fe4l", "f6l"],
          ["f6l", "fe5ll"],
          ["fe4l", "fx4l"],
          ["fe5ll", "fx4l"],
          {"breakGroup":"fender_b_L"},
          {"beamDeform":5800,"beamStrength":56030},
          {"deformGroup":"headlightglass_L_break", "deformationTriggerRatio":0.01}
          ["fe1l", "bbf2l"],
          ["fe2l", "bbf2l"],
          ["fe2ll", "bbf2l"],
          ["fe1l", "bbf1l"],
          {"deformGroup":""}
          {"breakGroup":"fender_c_L"},
          {"optional":true},
          ["fe3ll", "f13ll"],
          ["fe3lll", "f13ll"],
          ["fe4lll", "f13ll"],
          ["f13ll", "fe3l"],
          {"breakGroup":"fender_d_L"},
          ["fe3lll", "fbs4lll"],
          ["fbs4lll", "fe2ll"],
          ["fe3ll", "fbs4lll"],
          {"optional":false},
          {"breakGroup":""},

          //fender support beams
          {"beamType":"|SUPPORT","beamLongBound":10},
          {"beamSpring":101000,"beamDamp":10},
          {"beamDeform":15080,"beamStrength":178000},
          ["fe5ll", "f5ll"],
          ["fe5lll", "f5ll"],
          ["fe4l", "f6l"],
          ["fe4l", "fx4l"],
          ["fe4l", "fx3l"],
          ["fe3l", "fx3l"],
          ["fe2l", "bbf2l"],
          ["fe2l", "fx3l"],
          ["fe1l", "bbf2l"],
          ["fe1l", "bbf2"],
          ["fe3ll", "f13ll", {"optional":true}],
          ["fe5ll", "f6ll"],
          ["fe5ll", "f6l"],
          //to hood
          {"beamLongBound":35},
          {"optional":true},
          ["fe1l", "h4l"],
          ["fe2l", "h3l"],
          ["fe3l", "h2l"],
          ["fe4l", "h1"],
          {"beamLongBound":10},
          ["fe2ll", "h4l"],
          ["fe3ll", "h3l"],
          ["fx4l", "h2l"],
          ["fe5ll", "h1l"],
          //to front bumper support
          ["fe3lll", "fbs2lll"],
          ["fe2ll", "fbs2lll"],
          ["fe3ll", "fbs4lll"],
          //to door
          {"beamPrecompression":0.7},
          ["fe4lll", "d4l"],
          ["fe4ll", "d7l"],
          {"beamLongBound":25},
          ["fe5ll", "d7l"],
          ["fe5lll", "d4l"],
          {"optional":false},

          {"beamPrecompression":1, "beamType":"|NORMAL", "beamLongBound":1.0, "beamShortBound":1.0},
          {"deformLimitExpansion":1.2},
    ],
    "triangles": [
            ["id1:","id2:","id3:"],
            //left fender
            {"groundModel":"metal"},
            {"group":"scintilla_fender_L"},
            {"dragCoef":9},
            ["fe1l",   "fe2ll",  "fe3ll"],
            ["fe3ll",  "fe2l",   "fe1l"],
            ["fe2l",   "fe3ll",  "fe3l"],
            ["fe3l",   "fe3ll",  "fe4ll"],
            ["fe3l",   "fe4ll",  "fe4l"],
            ["fe4ll",  "fe5ll",  "fe4l"],
            ["fe4ll",  "fe4lll", "fe5ll"],
            ["fe4lll", "fe5lll", "fe5ll"],
            ["fe3ll",  "fe4lll", "fe4ll"],
            ["fe3ll",  "fe3lll", "fe4lll"],
            ["fe2ll",  "fe3lll", "fe3ll"],
            {"group":""},
    ],
},
}