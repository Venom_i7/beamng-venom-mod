{
"tire_F_225_40_18_race": {
    "information":{
        "authors":"BeamNG",
        "name":"225/40R18 Race Front Tires",
        "value":420,
    },
    "slotType" : "tire_F_18x8",
    "flexbodies": [
        ["mesh", "[group]:", "nonFlexMaterials"],
        ["tire_01f_18x9_25", ["wheel_FR","tire_FR"], [], {"pos":{"x":-0.48, "y":0.0, "z":0.0}, "rot":{"x":0, "y":0, "z":180}, "scale":{"x":0.92, "y":1.003, "z":1.003}}],
        ["tire_01f_18x9_25", ["wheel_FL","tire_FL"], [], {"pos":{"x": 0.48, "y":0.0, "z":0.0}, "rot":{"x":0, "y":0, "z":0},   "scale":{"x":0.92, "y":1.003, "z":1.003}}],
    ],
    "variables": [
        ["name", "type", "unit", "category", "default", "min", "max", "title", "description"],
        ["$tirepressure_F", "range", "psi", "Wheels", 28, 0, 50, "Tire Pressure", "Relative to atmospheric pressure", {"subCategory":"Front"}],
    ],
    "pressureWheels": [
        ["name","hubGroup","group","node1:","node2:","nodeS","nodeArm:","wheelDir"],
        {"disableMeshBreaking":false,"disableHubMeshBreaking":false},
        {"hasTire":true},
        {"enableTireReinfBeams":false},
        {"enableTireLbeams":true},
        {"enableTireSideReinfBeams":false},
        {"enableTreadReinfBeams":true},
        {"enableTirePeripheryReinfBeams":true},

        //general settings
        {"radius":0.32},
        {"tireWidth":0.195},

        //tire options
        {"wheelSideBeamSpring":"$=$tirepressure_F*600","wheelSideBeamDamp":15},
        {"wheelSideBeamSpringExpansion":441000,"wheelSideBeamDampExpansion":35},
        {"wheelSideTransitionZone":0.12,"wheelSideBeamPrecompression":0.985},

        {"wheelReinfBeamSpring":31000,"wheelReinfBeamDamp":190},
        {"wheelReinfBeamDampCutoffHz":500,"wheelReinfBeamPrecompression":0.98},

        {"wheelTreadBeamSpring":81000,"wheelTreadBeamDamp":80},
        {"wheelTreadBeamDampCutoffHz":500,"wheelTreadBeamPrecompression":0.98},

        {"wheelTreadReinfBeamSpring":231000,"wheelTreadReinfBeamDamp":70},
        {"wheelTreadReinfBeamDampCutoffHz":500,"wheelTreadReinfBeamPrecompression":0.98},

        {"wheelPeripheryBeamSpring":45000,"wheelPeripheryBeamDamp":40},
        {"wheelPeripheryBeamDampCutoffHz":500,"wheelPeripheryBeamPrecompression":0.98},

        {"wheelPeripheryReinfBeamSpring":131000,"wheelPeripheryReinfBeamDamp":30},
        {"wheelPeripheryReinfBeamDampCutoffHz":500,"wheelPeripheryReinfBeamPrecompression":0.98},

        //general tire values
        {"nodeWeight":0.17},
        {"nodeMaterial":"|NM_RUBBER"},
        {"triangleCollision":false},
        {"pressurePSI":"$tirepressure_F"},
        {"dragCoef":5},

        //groundmodel friction multipliers
        {"frictionCoef":1.0},
        {"slidingFrictionCoef":1.00},
        {"stribeckExponent":2},
        //{"stribeckVelMult":1.25},
        {"treadCoef":0},

        //advanced friction values
        {"noLoadCoef":2.08},
        {"loadSensitivitySlope":0.000244},
        {"fullLoadCoef":0.65},
        {"softnessCoef":1},

        //deform values
        {"wheelSideBeamDeform":17000,"wheelSideBeamStrength":22000},
        {"wheelTreadBeamDeform":13000,"wheelTreadBeamStrength":17000},
        {"wheelPeripheryBeamDeform":55000,"wheelPeripheryBeamStrength":55000},
    ],
},
"tire_F_235_40_18_race": {
    "information":{
        "authors":"BeamNG",
        "name":"235/40R18 Race Front Tires",
        "value":420,
    },
    "slotType" : "tire_F_18x8",
    "flexbodies": [
        ["mesh", "[group]:", "nonFlexMaterials"],
        ["tire_01f_18x9_26", ["wheel_FR","tire_FR"], [], {"pos":{"x":-0.48, "y":0.0, "z":0.0}, "rot":{"x":0, "y":0, "z":180}, "scale":{"x":0.93, "y":0.98, "z":0.98}}],
        ["tire_01f_18x9_26", ["wheel_FL","tire_FL"], [], {"pos":{"x": 0.48, "y":0.0, "z":0.0}, "rot":{"x":0, "y":0, "z":0},   "scale":{"x":0.93, "y":0.98, "z":0.98}}],
    ],
    "variables": [
        ["name", "type", "unit", "category", "default", "min", "max", "title", "description"],
        ["$tirepressure_F", "range", "psi", "Wheels", 28, 0, 50, "Tire Pressure", "Relative to atmospheric pressure", {"subCategory":"Front"}],
    ],
    "pressureWheels": [
        ["name","hubGroup","group","node1:","node2:","nodeS","nodeArm:","wheelDir"],
        {"disableMeshBreaking":false,"disableHubMeshBreaking":false},
        {"hasTire":true},
        {"enableTireReinfBeams":false},
        {"enableTireLbeams":true},
        {"enableTireSideReinfBeams":false},
        {"enableTreadReinfBeams":true},
        {"enableTirePeripheryReinfBeams":true},

        //general settings
        {"radius":0.325},
        {"tireWidth":0.205},

        //tire options
        {"wheelSideBeamSpring":"$=$tirepressure_F*600","wheelSideBeamDamp":15},
        {"wheelSideBeamSpringExpansion":461000,"wheelSideBeamDampExpansion":35},
        {"wheelSideTransitionZone":0.12,"wheelSideBeamPrecompression":0.985},

        {"wheelReinfBeamSpring":35000,"wheelReinfBeamDamp":190},
        {"wheelReinfBeamDampCutoffHz":500,"wheelReinfBeamPrecompression":0.98},

        {"wheelTreadBeamSpring":91000,"wheelTreadBeamDamp":80},
        {"wheelTreadBeamDampCutoffHz":500,"wheelTreadBeamPrecompression":0.98},

        {"wheelTreadReinfBeamSpring":241000,"wheelTreadReinfBeamDamp":70},
        {"wheelTreadReinfBeamDampCutoffHz":500,"wheelTreadReinfBeamPrecompression":0.98},

        {"wheelPeripheryBeamSpring":45000,"wheelPeripheryBeamDamp":40},
        {"wheelPeripheryBeamDampCutoffHz":500,"wheelPeripheryBeamPrecompression":0.98},

        {"wheelPeripheryReinfBeamSpring":141000,"wheelPeripheryReinfBeamDamp":30},
        {"wheelPeripheryReinfBeamDampCutoffHz":500,"wheelPeripheryReinfBeamPrecompression":0.98},

        //general tire values
        {"nodeWeight":0.175},
        {"nodeMaterial":"|NM_RUBBER"},
        {"triangleCollision":false},
        {"pressurePSI":"$tirepressure_F"},
        {"dragCoef":5},

        //groundmodel friction multipliers
        {"frictionCoef":1.0},
        {"slidingFrictionCoef":1.00},
        {"stribeckExponent":2},
        //{"stribeckVelMult":1.25},
        {"treadCoef":0},

        //advanced friction values
        {"noLoadCoef":2.10},
        {"loadSensitivitySlope":0.000236},
        {"fullLoadCoef":0.7},
        {"softnessCoef":1},

        //deform values
        {"wheelSideBeamDeform":17000,"wheelSideBeamStrength":22000},
        {"wheelTreadBeamDeform":13000,"wheelTreadBeamStrength":17000},
        {"wheelPeripheryBeamDeform":55000,"wheelPeripheryBeamStrength":55000},
    ],
},
"tire_F_235_40_18_tarmac": {
    "information":{
        "authors":"BeamNG",
        "name":"235/40R18 Tarmac Rally Front Tires",
        "value":420,
    },
    "slotType" : "tire_F_18x8",
    "flexbodies": [
        ["mesh", "[group]:", "nonFlexMaterials"],
        ["tire_01f_18x9_26", ["wheel_FR","tire_FR"], [], {"pos":{"x":-0.48, "y":0.0, "z":0.0}, "rot":{"x":0, "y":0, "z":180}, "scale":{"x":0.93, "y":0.98, "z":0.98}}],
        ["tire_01f_18x9_26", ["wheel_FL","tire_FL"], [], {"pos":{"x": 0.48, "y":0.0, "z":0.0}, "rot":{"x":0, "y":0, "z":0},   "scale":{"x":0.93, "y":0.98, "z":0.98}}],
    ],
    "variables": [
        ["name", "type", "unit", "category", "default", "min", "max", "title", "description"],
        ["$tirepressure_F", "range", "psi", "Wheels", 28, 0, 50, "Tire Pressure", "Relative to atmospheric pressure", {"subCategory":"Front"}],
    ],
    "pressureWheels": [
        ["name","hubGroup","group","node1:","node2:","nodeS","nodeArm:","wheelDir"],
        {"disableMeshBreaking":false,"disableHubMeshBreaking":false},
        {"hasTire":true},
        {"enableTireReinfBeams":false},
        {"enableTireLbeams":true},
        {"enableTireSideReinfBeams":false},
        {"enableTreadReinfBeams":true},
        {"enableTirePeripheryReinfBeams":true},

        //general settings
        {"radius":0.325},
        {"tireWidth":0.205},

        //tire options
        {"wheelSideBeamSpring":"$=$tirepressure_F*700","wheelSideBeamDamp":30},
        {"wheelSideBeamSpringExpansion":401000,"wheelSideBeamDampExpansion":60},
        {"wheelSideTransitionZone":0.06,"wheelSideBeamPrecompression":0.98},

        {"wheelReinfBeamSpring":38000,"wheelReinfBeamDamp":200},
        {"wheelReinfBeamDampCutoffHz":500,"wheelReinfBeamPrecompression":0.98},

        {"wheelTreadBeamSpring":81000,"wheelTreadBeamDamp":80},
        {"wheelTreadBeamDampCutoffHz":500,"wheelTreadBeamPrecompression":0.98},

        {"wheelTreadReinfBeamSpring":261000,"wheelTreadReinfBeamDamp":60},
        {"wheelTreadReinfBeamDampCutoffHz":500,"wheelTreadReinfBeamPrecompression":0.98},

        {"wheelPeripheryBeamSpring":101000,"wheelPeripheryBeamDamp":80},
        {"wheelPeripheryBeamDampCutoffHz":500,"wheelPeripheryBeamPrecompression":0.98},

        {"wheelPeripheryReinfBeamSpring":101000,"wheelPeripheryReinfBeamDamp":80},
        {"wheelPeripheryReinfBeamDampCutoffHz":500,"wheelPeripheryReinfBeamPrecompression":0.98},

        //general tire values
        {"nodeWeight":0.175},
        {"nodeMaterial":"|NM_RUBBER"},
        {"triangleCollision":false},
        {"pressurePSI":"$tirepressure_F"},
        {"dragCoef":5},

        //groundmodel friction multipliers
        {"frictionCoef":1.0},
        {"slidingFrictionCoef":1.02},
        {"stribeckExponent":1.5},
        {"treadCoef":0.1},

        //advanced friction values
        {"noLoadCoef":2.0},
        {"loadSensitivitySlope":0.000232},
        {"fullLoadCoef":0.7},
        {"softnessCoef":1},

        //deform values
        {"wheelSideBeamDeform":23000,"wheelSideBeamStrength":30000},
        {"wheelTreadBeamDeform":22000,"wheelTreadBeamStrength":28000},
        {"wheelPeripheryBeamDeform":75000,"wheelPeripheryBeamStrength":75000},
    ],
},
}